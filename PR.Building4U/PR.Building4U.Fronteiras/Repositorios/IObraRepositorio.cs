using System;
using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios
{
    public interface IObraRepositorio
    {
        IEnumerable<Obra> ListarObras(string codigoOuNome, bool somenteAtivas, bool somenteObras);
        Obra ObterObra(int idObra);
        int IncluirObra(int idArea, int? idContrato, string codigoObra, string nomeObra);
        int EditarObra(int idObra, int idArea, int? idContrato, string codigoObra, string nomeObra);
        int ExcluirObra(int idObra);
        int IncluirOrdemServico(int idObra, DateTime? dataOrdemServico, int? prazoOrdemServico);
        int EditarOrdemServico(int idOrdemServico, DateTime? dataOrdemServico, int? prazoOrdemServico);
        int ExcluirOrdemServico(int idOrdemServico);
    }
}