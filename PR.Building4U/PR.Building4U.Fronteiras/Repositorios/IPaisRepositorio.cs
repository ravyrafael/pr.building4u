﻿using PR.Building4U.Entidades;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Repositorios
{
    public interface IPaisRepositorio
    {
        IEnumerable<Pais> ListarPaises(string codigoOuNome);
        Pais ObterPais(int idPais);
        int IncluirPais(int codigoPais, string nomePais);
        int EditarPais(int idPais, string nomePais);
        int ExcluirPais(int idPais);
    }
}
