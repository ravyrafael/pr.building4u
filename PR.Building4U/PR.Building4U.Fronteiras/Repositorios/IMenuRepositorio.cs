﻿using PR.Building4U.Entidades;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Repositorios
{
    public interface IMenuRepositorio
    {
        IEnumerable<Recurso> ObterMenuPorPermissao(int permissaoUsuario);
        IEnumerable<Recurso> ObterSubMenuPorPermissao(int permissaoUsuario, int menuPai);
    }
}
