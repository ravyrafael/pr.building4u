using System;
using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface ICronogramaRepositorio
	{
		IEnumerable<Cronograma> ListarCronogramas(int idObra);
		Cronograma ObterCronograma(int idCronograma);
		int IncluirCronograma(int idObra, int ordemCronograma, string nomeCronograma, int diaMedicaoCronograma);
        int EditarCronograma(int idCronograma, string nomeCronograma, int diaMedicaoCronograma);
        IEnumerable<CronogramaServico> ListarServicos(int idCronograma);
        IEnumerable<CronogramaServicoMedicao> ListarMedicoes(int idCronogramaServico);
        int IncluirMedicao(int idCronogramaServico, DateTime mesReferencia, DateTime inicioMedicao,
            DateTime terminoMedicao, decimal? faturamento, decimal? quantidade, decimal? pesMedicao,
            decimal? equipeMedicao, bool temMedicao);
        int AtualizarMedicao(int idCronogramaServicoMedicao, DateTime inicioMedicao, DateTime terminoMedicao,
            decimal? faturamento, decimal? quantidade, decimal? pesMedicao, decimal? equipeMedicao, bool temMedicao);
        int ExcluirMedicao(int idCronogramaServicoMedicao);
        int IncluirServico(int idCronograma, int idServico, int ordemServico, string nomeServico,
            decimal quantidadeTotal, decimal quantidadeExecutada, decimal? quantidadeReal, decimal mediaEquipe,
            decimal mediaPes, decimal? mediaPesReal, decimal valorServico, int duracaoTotal, int duracaoFalta,
            DateTime? inicioServico, DateTime? terminoServico, DateTime? atualizacaoServico,
            int? idCronogramaPredecessora, int? idServicoPredecessora, char? tipoPredecessora,
            int? atrasoPredecessora, decimal chanceFaturamento);
        int ExcluirServicoMedicao(int idCronogramaServico);
        int ExcluirServicoProducao(int idCronogramaServico);
        int ExcluirServico(int idCronogramaServico);
        int AtualizarServico(int idCronogramaServico, int ordemServico, string nomeServico,
            decimal quantidadeTotal, decimal quantidadeExecutada, decimal? quantidadeReal, decimal mediaEquipe,
            decimal mediaPes, decimal? mediaPesReal, decimal valorServico, int duracaoTotal, int duracaoFalta,
            DateTime? inicioServico, DateTime? terminoServico, DateTime? atualizacaoServico,
            int? idCronogramaPredecessora, int? idServicoPredecessora, char? tipoPredecessora,
            int? atrasoPredecessora, decimal chanceFaturamento);
        int AtualizarCronograma(int idCronograma, int ordemCronograma, DateTime? inicioCronograma,
            DateTime? terminoCronograma, DateTime? atualizacaoCronograma);
    }
}