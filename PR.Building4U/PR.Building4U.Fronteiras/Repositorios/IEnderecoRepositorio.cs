﻿using PR.Building4U.Entidades;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Repositorios
{
    public interface IEnderecoRepositorio
    {
        IEnumerable<Endereco> ListarEnderecosPorMunicipio(int idMunicipio);
        int IncluirEndereco(string logradouroEndereco, string numeroEndereco, string complementoEndereco, string cepEndereco, string distritoEndereco, int idMunicipio);
        int EditarEndereco(int idEndereco, string logradouroEndereco, string numeroEndereco, string complementoEndereco, string cepEndereco, string distritoEndereco, int idMunicipio);
    }
}
