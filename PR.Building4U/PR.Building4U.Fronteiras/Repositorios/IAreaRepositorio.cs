using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IAreaRepositorio
	{
		IEnumerable<Area> ListarAreas(string codigoOuNome);
		Area ObterArea(int idArea);
        int EditarArea(int idArea, string nomeArea);
	}
}