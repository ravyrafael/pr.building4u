using System;
using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IAditivoRepositorio
	{
        IEnumerable<Aditivo> ListarAditivosPorContrato(int idContrato);
		int IncluirAditivo(int idContrato, string codigoAditivo, DateTime dataAditivo, decimal valorAditivo, int prazoAditivo, string escopoAditivo);
        int ExcluirAditivo(int idAditivo);
	}
}