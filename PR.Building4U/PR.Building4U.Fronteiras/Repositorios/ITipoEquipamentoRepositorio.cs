using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface ITipoEquipamentoRepositorio
	{
		IEnumerable<TipoEquipamento> ListarTipoEquipamentos(string codigoOuNome);
		TipoEquipamento ObterTipoEquipamento(int idTipoEquipamento);
		int IncluirTipoEquipamento(string nomeTipoEquipamento, int idCusto);
        int EditarTipoEquipamento(int idTipoEquipamento, string nomeTipoEquipamento, int idCusto);
        int ExcluirTipoEquipamento(int idTipoEquipamento);
	}
}