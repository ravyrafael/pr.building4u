﻿using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios
{
    public interface IUsuarioRepositorio
    {
        int? AutenticarUsuario(string loginUsuario, string senhaUsuario);
        Usuario ObterUsuarioPorLogin(string loginUsuario);
        int VerificarPermissaoUsuario(int permissao, string controller, string[] actions);
    }
}
