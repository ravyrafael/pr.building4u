using System;
using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IContratoRepositorio
	{
		IEnumerable<Contrato> ListarContratos(string codigoOuNome);
		Contrato ObterContrato(int idContrato);
		int IncluirContrato(int idCliente, string codigoContrato, DateTime dataContrato, decimal valorContrato, int prazoContrato, string escopoContrato);
        int EditarContrato(int idContrato, string escopoContrato);
        int ExcluirContrato(int idContrato);
	}
}