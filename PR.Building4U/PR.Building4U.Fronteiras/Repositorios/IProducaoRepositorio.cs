using System;
using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IProducaoRepositorio
	{
		IEnumerable<Producao> ListarProducaos(int idCronogramaServico);
		int IncluirProducao(int idCronogramaServico, DateTime dataProducao, decimal? quantidadeProducao, decimal? equipeProducao, string observacaoProducao);
        int AtualizarProducao(int idProducao, decimal? quantidadeProducao, decimal? equipeProducao, string observacaoProducao);
        int ExcluirProducao(int idProducao);
        decimal SomarProducaoServico(int idCronogramaServico);
    }
}