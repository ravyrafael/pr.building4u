using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface ICustoRepositorio
	{
		IEnumerable<Custo> ListarCustos(string codigoOuNome);
		Custo ObterCusto(int idCusto);
		int IncluirCusto(string siglaCusto, string nomeCusto);
        int EditarCusto(int idCusto, string nomeCusto);
        int ExcluirCusto(int idCusto);
	}
}