using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface INecessarioRepositorio
	{
        int CancelarNecessarioAprovPorObra(int idObra);
        int ExcluirNecessarioAprovPorObra(int idObra);
        int IncluirNecessarioAprovPorObra(int idObra);
        IEnumerable<Necessario> ListarNecessarioAprovPorObra(int idObra);
        bool VerificarNecessarioAprov(int idObra, int idCargo);
        int IncluirNecessarioAprov(int idObra, int idCargo, int quantidadeNecessario);
        int AtualizarNecessarioAprov(int idObra, int idCargo, int quantidadeNecessario);
        int ExcluirNecessarioAprov(int idObra, int idCargo);
        int EnviarNecessarioAprovPorObra(int idObra);
        IEnumerable<NecessarioAprov> ListarNecessarioPorObra(int idObra);
        int ExcluirNecessarioPorObra(int idObra);
        int IncluirNecessarioPorObra(int idObra);
        int VerificarNecessarioAprovPorObra(int idObra);
    }
}