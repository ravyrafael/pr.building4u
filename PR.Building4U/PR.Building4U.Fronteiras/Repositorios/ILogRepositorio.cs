﻿using PR.Building4U.Util;

namespace PR.Building4U.Fronteiras.Repositorios
{
    public interface ILogRepositorio
    {
        int IncluirLog(InformacoesLog informacoesLog);
        int IncluirLog(InformacoesLog informacoesLog, string descricao, TipoLog tipoLog);
    }
}
