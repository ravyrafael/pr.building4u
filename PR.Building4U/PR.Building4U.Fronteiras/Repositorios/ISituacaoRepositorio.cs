﻿using PR.Building4U.Entidades;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface ISituacaoRepositorio
	{
        IEnumerable<Situacao> ListarSituacoes(string codigoOuNome);
        Situacao ObterSituacao(int idSituacao);
        int EditarSituacao(int idSituacao, string nomeSituacao);
    }
}