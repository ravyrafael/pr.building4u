using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IClienteRepositorio
	{
		IEnumerable<Cliente> ListarClientes(string codigoOuNome);
		Cliente ObterCliente(int idCliente);
		int IncluirCliente(string cnpjCliente, string razaoSocialCliente, string nomeCliente, int idEndereco);
        int EditarCliente(int idCliente, string razaoSocialCliente, string nomeCliente);
        int DesativarCliente(int idCliente);
	}
}