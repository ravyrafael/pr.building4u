using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IGerenciaRepositorio
	{
		IEnumerable<Gerencia> ListarGerencias(string codigoOuNome);
		int IncluirGerencia(string nomeGerencia, string siglaGerencia);
        int ExcluirGerencia(int idGerencia);
	}
}