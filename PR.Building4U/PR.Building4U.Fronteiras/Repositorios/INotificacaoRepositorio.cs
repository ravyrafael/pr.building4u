using System;
using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface INotificacaoRepositorio
	{
		IEnumerable<Notificacao> ListarNotificacoes(int idFuncionario);
		int IncluirNotificacao(int idFuncionario, string tituloNotificacao, string textoNotificacao, DateTime dataNotificacao, int tipoNotificacao);
        int MarcarNotificacaoComoLida(int? idNotificacao, int? idFuncionario);
    }
}