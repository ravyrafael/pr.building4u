using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IUnidadeRepositorio
	{
		IEnumerable<Unidade> ListarUnidades(string codigoOuNome);
		Unidade ObterUnidade(int idUnidade);
		int IncluirUnidade(string siglaUnidade, string nomeUnidade);
        int EditarUnidade(int idUnidade, string siglaUnidade, string nomeUnidade);
        int ExcluirUnidade(int idUnidade);
	}
}