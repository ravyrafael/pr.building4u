using System;
using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IEquipamentoRepositorio
	{
		IEnumerable<Equipamento> ListarEquipamentos(string codigoOuNome);
		Equipamento ObterEquipamento(int idEquipamento);
		int IncluirEquipamento(string marcaEquipamento, string modeloEquipamento, string patrimonioEquipamento, int idTipoEquipamento, bool indicadorTerceiro, string fornecedorEquipamento, DateTime aquisicaoEquipamento);
        int EditarEquipamento(int idEquipamento, string marcaEquipamento, string modeloEquipamento, string fornecedorEquipamento);
        int SucatearEquipamento(int idEquipamento, DateTime sucataEquipamento);
        int IniciarManutencaoEquipamento(int idEquipamento, DateTime inicioManutencao, DateTime terminoManutencao);
        int TerminarManutencaoEquipamento(int idEquipamento, DateTime terminoManutencao);
        int AlterarSituacaoEquipamento(int idEquipamento, int situacaoEquipamento);
        Manutencao ObterManutencaoEquipamento(int idEquipamento);
    }
}