﻿using PR.Building4U.Entidades;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Repositorios
{
    public interface IEstadoRepositorio
    {
        IEnumerable<Estado> ListarEstados(string codigoOuNome);
        Estado ObterEstado(int idEstado);
        int IncluirEstado(string siglaEstado, string nomeEstado, int idPais);
        int EditarEstado(int idEstado, string nomeEstado, int idPais);
        int ExcluirEstado(int idEstado);
        IEnumerable<Estado> ListarEstadosPorPais(int idPais, string codigoOuNome);
    }
}
