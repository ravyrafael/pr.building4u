using System;
using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IPlanilhaRepositorio
	{
		IEnumerable<Planilha> ListarPlanilhas(int idCronograma);
        Planilha ObterPlanilha(int idPlanilha);
        IEnumerable<PlanilhaItem> ListarItens(int idPlanilha);
        IEnumerable<PlanilhaItemMedicao> ListarItensMedicao(int idPlanilhaItem);
        int AtualizarItem(int idPlanilhaItem, decimal? quantidadeAcertoItem, bool ehItemControle, int? idCronogramaServico);
        int ExcluirPlanilha(int idPlanilha);
        int ExcluirItens(int idPlanilha);
        decimal? SomarValorMedicao(int idCronogramaServico, DateTime mesReferencia);
        decimal? SomarQuantidadeMedicao(int idCronogramaServico, DateTime mesReferencia);
        bool VerificarMedicao(int idCronograma, DateTime mesReferencia);
        int IncluirPlanilha(int idCronograma, string cpPlanilha, int revPlanilha);
        int IncluirItem(int? idCronogramaServico, int idPlanilha, int? idUnidade, string codigoItem, string descricaoItem, decimal? precoItem, decimal? quantidadeItem, decimal? quantidadeAcertoItem, bool ehTituloItem, bool ehItemControle);
        IEnumerable<Medicao> ListarMedicoes(int idPlanilha);
        int IncluirMedicao(int idPlanilhaItem, DateTime periodoMedicao, decimal quantidadeMedicao, decimal valorMedicao);
        int ExcluirMedicao(int idPlanilha, DateTime periodoMedicao);
    }
}