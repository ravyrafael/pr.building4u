using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios
{
    public interface IObraGerenciaRepositorio
    {
        IEnumerable<ObraGerencia> ListarGerenciasPorObra(int idObra);
        int IncluirObraGerencia(int idObra, int idGerencia, int idFuncionario);
        int ExcluirObraGerencia(int idObraGerencia);
    }
}