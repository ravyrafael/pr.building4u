using System;
using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IFuncionarioRepositorio
	{
		IEnumerable<Funcionario> ListarFuncionarios(string codigoOuNome);
		Funcionario ObterFuncionario(int idFuncionario);
		int IncluirFuncionario(string chapaFuncionario, string nomeFuncionario, DateTime admissaoFuncionario, string emailFuncionario, string telefoneFuncionario, int idEndereco, int idCargo);
        int EditarFuncionario(int idFuncionario, string nomeFuncionario, string emailFuncionario, string telefoneFuncionario, int idCargo);
        int DemitirFuncionario(int idFuncionario, DateTime demissaoFuncionario);
        int IniciarFeriasFuncionario(int idFuncionario, DateTime inicioFerias, DateTime terminoFerias);
        int TerminarFeriasFuncionario(int idFuncionario, DateTime terminoFerias);
        int AlterarSituacaoFuncionario(int idFuncionario, int situacaoFuncionario);
        Ferias ObterFeriasFuncionario(int idFuncionario);
    }
}