﻿using PR.Building4U.Entidades;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Repositorios
{
    public interface IMunicipioRepositorio
    {
        IEnumerable<Municipio> ListarMunicipios(string codigoOuNome);
        Municipio ObterMunicipio(int idMunicipio);
        int IncluirMunicipio(int codigoMunicipio, string nomeMunicipio, int idEstado);
        int EditarMunicipio(int idMunicipio, string nomeMunicipio, int idEstado);
        int ExcluirMunicipio(int idMunicipio);
        IEnumerable<Municipio> ListarMunicipiosPorEstado(int idEstado, string codigoOuNome);
    }
}
