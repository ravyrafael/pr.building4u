using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IServicoRepositorio
	{
		IEnumerable<Servico> ListarServicos(string codigoOuNome);
		Servico ObterServico(int? idServico, int? codigoServico);
		int IncluirServico(int codigoServico, string siglaServico, string nomeServico, int idUnidade);
        int EditarServico(int idServico, string siglaServico, string nomeServico, int idUnidade);
        int ExcluirServico(int idServico);
	}
}