using System;
using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IAluguelRepositorio
	{
		IEnumerable<Aluguel> ListarAlugueis(int[] obras, int[] tiposEquipamento);
        int IncluirAluguel(int idEquipamento, int idObra, int sequenciaAluguel, DateTime dataAluguel);
        int FinalizarAluguel(int idEquipamento, int sequenciaAluguel, DateTime dataTermino);
        int IncluirReserva(int idEquipamento, int idObra, DateTime dataReserva);
        int ExcluirReserva(int idEquipamento);
        Aluguel ObterAluguelPorEquipamento(int idEquipamento);
        int ExcluirAluguel(int idEquipamento, int sequenciaAluguel);
        int ReativarAluguel(int idEquipamento, int sequenciaAluguel);
        int VerificarDestino(int idEquipamento);
    }
}