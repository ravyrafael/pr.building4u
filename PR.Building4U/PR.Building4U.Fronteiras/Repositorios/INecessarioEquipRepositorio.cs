using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface INecessarioEquipRepositorio
	{
        int CancelarNecessarioAprovPorObra(int idObra);
        int ExcluirNecessarioAprovPorObra(int idObra);
        int IncluirNecessarioAprovPorObra(int idObra);
        IEnumerable<NecessarioEquip> ListarNecessarioAprovPorObra(int idObra);
        bool VerificarNecessarioAprov(int idObra, int idTipoEquipamento);
        int IncluirNecessarioAprov(int idObra, int idTipoEquipamento, int quantidadeNecessario);
        int AtualizarNecessarioAprov(int idObra, int idTipoEquipamento, int quantidadeNecessario);
        int ExcluirNecessarioAprov(int idObra, int idTipoEquipamento);
        int EnviarNecessarioAprovPorObra(int idObra);
        IEnumerable<NecessarioEquipAprov> ListarNecessarioPorObra(int idObra);
        int ExcluirNecessarioPorObra(int idObra);
        int IncluirNecessarioPorObra(int idObra);
        int VerificarNecessarioAprovPorObra(int idObra);
    }
}