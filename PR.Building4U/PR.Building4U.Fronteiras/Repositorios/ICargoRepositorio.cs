using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface ICargoRepositorio
	{
		IEnumerable<Cargo> ListarCargos(string codigoOuNome);
		Cargo ObterCargo(int idCargo);
		int IncluirCargo(string nomeCargo, string siglaCargo, int idCusto);
        int EditarCargo(int idCargo, string nomeCargo, string siglaCargo, int idCusto);
        int ExcluirCargo(int idCargo);
	}
}