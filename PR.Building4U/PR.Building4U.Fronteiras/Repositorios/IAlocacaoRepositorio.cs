using System;
using System.Collections.Generic;
using PR.Building4U.Entidades;

namespace PR.Building4U.Fronteiras.Repositorios 
{
	public interface IAlocacaoRepositorio
	{
		IEnumerable<Alocacao> ListarAlocacoes(int[] obras, int[] cargos);
        int IncluirAlocacao(int idFuncionario, int idObra, int sequenciaAlocacao, DateTime dataAlocacao);
        int FinalizarAlocacao(int idFuncionario, int sequenciaAlocacao, DateTime dataTermino);
        int IncluirReserva(int idFuncionario, int idObra, DateTime dataReserva);
        int ExcluirReserva(int idFuncionario);
        Alocacao ObterAlocacaoPorFuncionario(int idFuncionario);
        int ExcluirAlocacao(int idFuncionario, int sequenciaAlocacao);
        int ReativarAlocacao(int idFuncionario, int sequenciaAlocacao);
        int VerificarDestino(int idFuncionario);
    }
}