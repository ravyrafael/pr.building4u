﻿using System.Threading.Tasks;

namespace PR.Building4U.Fronteiras
{
    public interface IExecutorAsync<TRequisicao, TResultado> where TResultado : IResultado
    {
        Task<TResultado> ExecutarAsync(TRequisicao requisicao);
    }
}
