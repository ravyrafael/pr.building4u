﻿using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarSituacao 
{
	public class ObterSituacaoResultado : ResultadoBase
	{
        public SituacaoDto Situacao { get; set; }
    }
}