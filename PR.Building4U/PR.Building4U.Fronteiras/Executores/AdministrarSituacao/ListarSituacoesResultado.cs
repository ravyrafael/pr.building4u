﻿using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Executores.AdministrarSituacao 
{
	public class ListarSituacoesResultado : ResultadoBase
	{
        public List<SituacaoDto> Situacoes { get; set; }
    }
}