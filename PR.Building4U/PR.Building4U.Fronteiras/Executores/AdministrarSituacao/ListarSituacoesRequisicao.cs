﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarSituacao
{
    public class ListarSituacoesRequisicao : RequisicaoBase
    {
        public string CodigoOuNome { get; set; }
    }
}
