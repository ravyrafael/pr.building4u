﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarSituacao 
{
	public class EditarSituacaoRequisicao : RequisicaoBase
	{
        public int IdSituacao { get; set; }
        public string NomeSituacao { get; set; }
    }
}
