﻿using PR.Building4U.Util;

namespace PR.Building4U.Fronteiras.Executores.AdministrarUsuario
{
    public class AutenticarUsuarioRequisicao : RequisicaoBase
    {
        public string LoginUsuario { get; set; }
        public string SenhaUsuario { get; set; }
        public string ChaveAutenticacao { get; set; }
    }
}
