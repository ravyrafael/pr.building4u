﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarUsuario
{
    public class AutenticarUsuarioResultado : ResultadoBase
    {
        public int? IdUsuario { get; set; }
    }
}
