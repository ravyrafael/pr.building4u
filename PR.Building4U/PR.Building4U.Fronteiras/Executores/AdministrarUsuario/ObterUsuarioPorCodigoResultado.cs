﻿using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarUsuario
{
    public class ObterUsuarioPorCodigoResultado : ResultadoBase
    {
        public UsuarioDto Usuario { get; set; }
    }
}
