﻿using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Executores.AdministrarUsuario 
{
	public class VerificarPermissaoUsuarioRequisicao : RequisicaoBase
    {
        public int Permissao { get; set; }
        public string Controller { get; set; }
        public string[] Actions { get; set; }
    }
}
