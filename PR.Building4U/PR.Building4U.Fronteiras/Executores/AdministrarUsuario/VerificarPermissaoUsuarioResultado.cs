﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarUsuario 
{
	public class VerificarPermissaoUsuarioResultado : ResultadoBase
    {
        public bool TemPermissao { get; set; }
    }
}