using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarServico 
{
    public class ObterServicoResultado : ResultadoBase
    {
        public ServicoDto Servico { get; set; }
    }
}