namespace PR.Building4U.Fronteiras.Executores.AdministrarServico 
{
    public class EditarServicoRequisicao : RequisicaoBase
    {
        public int IdServico { get; set; }
        public string SiglaServico { get; set; }
        public string NomeServico { get; set; }
        public int IdUnidade { get; set; }
    }
}
