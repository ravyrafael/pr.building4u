namespace PR.Building4U.Fronteiras.Executores.AdministrarServico 
{
    public class IncluirServicoRequisicao : RequisicaoBase
    {
        public int CodigoServico { get; set; }
        public string SiglaServico { get; set; }
        public string NomeServico { get; set; }
        public int IdUnidade { get; set; }
    }
}
