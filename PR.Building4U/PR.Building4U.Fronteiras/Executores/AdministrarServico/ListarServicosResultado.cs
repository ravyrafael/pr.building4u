using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarServico 
{
    public class ListarServicosResultado : ResultadoBase
    {
        public List<ServicoDto> Servicos { get; set; }
    }
}