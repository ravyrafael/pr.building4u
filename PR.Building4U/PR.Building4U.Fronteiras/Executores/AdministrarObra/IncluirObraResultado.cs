namespace PR.Building4U.Fronteiras.Executores.AdministrarObra 
{
	public class IncluirObraResultado : ResultadoBase
	{
        public int IdObra { get; set; }
        public int IdOrdemServico { get; set; }
    }
}