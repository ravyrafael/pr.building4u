namespace PR.Building4U.Fronteiras.Executores.AdministrarObra 
{
    public class ListarObrasRequisicao : RequisicaoBase
    {
        public string CodigoOuNome { get; set; }
        public bool SomenteAtivas { get; set; }
        public bool SomenteObras { get; set; }
    }
}
