using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarObra 
{
	public class IncluirObraRequisicao : RequisicaoBase
	{
        public int IdArea { get; set; }
        public int? IdContrato { get; set; }
        public string CodigoObra { get; set; }
        public string NomeObra { get; set; }
        public DateTime? DataOrdemServico { get; set; }
        public int? PrazoOrdemServico { get; set; }
    }
}
