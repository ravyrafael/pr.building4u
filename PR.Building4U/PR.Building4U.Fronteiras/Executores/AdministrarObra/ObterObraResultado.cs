using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarObra 
{
    public class ObterObraResultado : ResultadoBase
    {
        public ObraDto Obra { get; set; }
    }
}