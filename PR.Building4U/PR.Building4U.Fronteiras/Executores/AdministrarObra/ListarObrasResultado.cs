using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarObra 
{
    public class ListarObrasResultado : ResultadoBase
    {
        public List<ObraDto> Obras { get; set; }
    }
}