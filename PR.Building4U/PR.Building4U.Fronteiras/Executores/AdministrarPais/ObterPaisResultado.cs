﻿using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPais 
{
    public class ObterPaisResultado : ResultadoBase
    {
        public PaisDto Pais { get; set; }
    }
}