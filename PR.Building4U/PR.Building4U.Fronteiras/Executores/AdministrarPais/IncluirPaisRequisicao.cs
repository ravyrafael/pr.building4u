﻿using PR.Building4U.Util;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPais
{
	public class IncluirPaisRequisicao : RequisicaoBase
    {
        public int CodigoPais { get; set; }
        public string NomePais { get; set; }
    }
}
