﻿using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPais
{
    public class ListarPaisesResultado : ResultadoBase
    {
        public List<PaisDto> Paises { get; set; }
    }
}
