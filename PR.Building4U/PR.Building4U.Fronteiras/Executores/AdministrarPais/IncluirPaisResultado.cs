﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarPais
{
	public class IncluirPaisResultado : ResultadoBase
    {
        public int IdPais { get; set; }
    }
}