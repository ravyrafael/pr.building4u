﻿using PR.Building4U.Util;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPais
{
	public class ExcluirPaisRequisicao : RequisicaoBase
    {
        public int IdPais { get; set; }
    }
}
