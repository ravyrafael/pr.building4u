﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarPais
{
	public class EditarPaisRequisicao : RequisicaoBase
    {
        public int IdPais { get; set; }
        public int CodigoPais { get; set; }
        public string NomePais { get; set; }
    }
}
