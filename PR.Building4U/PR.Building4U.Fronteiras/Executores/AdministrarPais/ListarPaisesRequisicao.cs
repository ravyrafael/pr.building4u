﻿using PR.Building4U.Util;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPais
{
    public class ListarPaisesRequisicao : RequisicaoBase
    {
        public string CodigoOuNome { get; set; }
    }
}
