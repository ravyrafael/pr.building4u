﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarEstado 
{
	public class ExcluirEstadoRequisicao : RequisicaoBase
    {
        public int IdEstado { get; set; }
    }
}
