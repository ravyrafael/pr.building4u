﻿using PR.Building4U.Util;

namespace PR.Building4U.Fronteiras.Executores.AdministrarEstado
{
    public class ListarEstadosRequisicao : RequisicaoBase
    {
        public string CodigoOuNome { get; set; }
    }
}
