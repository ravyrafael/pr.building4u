﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarEstado 
{
	public class EditarEstadoRequisicao : RequisicaoBase
	{
        public int IdEstado { get; set; }
        public string SiglaEstado { get; set; }
        public string NomeEstado { get; set; }
        public int IdPais { get; set; }
    }
}
