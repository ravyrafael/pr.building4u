﻿using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarEstado 
{
	public class ObterEstadoResultado : ResultadoBase
    {
        public EstadoDto Estado { get; set; }
    }
}