﻿using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Executores.AdministrarEstado
{
    public class ListarEstadosResultado : ResultadoBase
    {
        public List<EstadoDto> Estados { get; set; }
    }
}
