﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarEstado 
{
	public class IncluirEstadoRequisicao : RequisicaoBase
    {
        public string SiglaEstado { get; set; }
        public string NomeEstado { get; set; }
        public int IdPais { get; set; }
    }
}
