﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarEstado 
{
	public class IncluirEstadoResultado : ResultadoBase
    {
        public int IdEstado { get; set; }
    }
}