﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarEstado 
{
	public class ListarEstadosPorPaisRequisicao : RequisicaoBase
	{
        public int IdPais { get; set; }
        public string CodigoOuNome { get; set; }
    }
}
