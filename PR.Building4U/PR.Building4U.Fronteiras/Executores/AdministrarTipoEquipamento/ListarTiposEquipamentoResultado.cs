using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento 
{
    public class ListarTiposEquipamentoResultado : ResultadoBase
    {
        public List<TipoEquipamentoDto> TiposEquipamento { get; set; }
    }
}