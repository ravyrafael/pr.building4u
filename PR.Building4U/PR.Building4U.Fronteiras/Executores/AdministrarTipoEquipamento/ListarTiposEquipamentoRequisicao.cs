namespace PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento 
{
    public class ListarTiposEquipamentoRequisicao : RequisicaoBase
    {
        public string CodigoOuNome { get; set; }
    }
}
