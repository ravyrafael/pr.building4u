using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento 
{
    public class ObterTipoEquipamentoResultado : ResultadoBase
    {
        public TipoEquipamentoDto TipoEquipamento { get; set; }
    }
}