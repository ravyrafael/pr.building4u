namespace PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento 
{
    public class ObterTipoEquipamentoRequisicao : RequisicaoBase
    {
        public int IdTipoEquipamento { get; set; }
    }
}
