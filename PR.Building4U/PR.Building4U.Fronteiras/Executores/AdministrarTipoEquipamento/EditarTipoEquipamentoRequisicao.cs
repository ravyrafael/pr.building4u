namespace PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento 
{
    public class EditarTipoEquipamentoRequisicao : RequisicaoBase
    {
        public int IdTipoEquipamento { get; set; }
        public string NomeTipoEquipamento { get; set; }
        public int IdCusto { get; set; }
    }
}
