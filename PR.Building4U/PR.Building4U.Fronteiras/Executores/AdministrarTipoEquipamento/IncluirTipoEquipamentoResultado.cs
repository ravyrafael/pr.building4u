namespace PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento 
{
    public class IncluirTipoEquipamentoResultado : ResultadoBase
    {
        public int IdTipoEquipamento { get; set; }
    }
}