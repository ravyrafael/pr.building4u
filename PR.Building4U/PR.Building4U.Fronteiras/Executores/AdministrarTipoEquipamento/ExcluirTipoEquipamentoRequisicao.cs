namespace PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento 
{
    public class ExcluirTipoEquipamentoRequisicao : RequisicaoBase
    {
        public int IdTipoEquipamento { get; set; }
    }
}
