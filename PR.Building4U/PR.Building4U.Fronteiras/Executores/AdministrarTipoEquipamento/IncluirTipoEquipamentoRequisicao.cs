namespace PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento 
{
    public class IncluirTipoEquipamentoRequisicao : RequisicaoBase
    {
        public string NomeTipoEquipamento { get; set; }
        public int IdCusto { get; set; }
    }
}
