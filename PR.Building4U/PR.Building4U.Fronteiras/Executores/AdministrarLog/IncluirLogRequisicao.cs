using PR.Building4U.Util;

namespace PR.Building4U.Fronteiras.Executores.AdministrarLog 
{
    public class IncluirLogRequisicao : RequisicaoBase
    {
        public string Mensagem { get; set; }
        public TipoLog TipoLog { get; set; }
    }
}
