using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarCliente 
{
    public class ListarClientesResultado : ResultadoBase
    {
        public List<ClienteDto> Clientes { get; set; }
    }
}