namespace PR.Building4U.Fronteiras.Executores.AdministrarCliente 
{
    public class IncluirClienteResultado : ResultadoBase
    {
        public int IdCliente { get; set; }
    }
}