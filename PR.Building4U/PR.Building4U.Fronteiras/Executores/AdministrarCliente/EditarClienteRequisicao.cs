namespace PR.Building4U.Fronteiras.Executores.AdministrarCliente 
{
    public class EditarClienteRequisicao : RequisicaoBase
    {
        public int IdCliente { get; set; }
        public string RazaoSocialCliente { get; set; }
        public string NomeCliente { get; set; }

        public int IdEndereco { get; set; }
        public string LogradouroEndereco { get; set; }
        public string NumeroEndereco { get; set; }
        public string ComplementoEndereco { get; set; }
        public string CepEndereco { get; set; }
        public string DistritoEndereco { get; set; }
        public int IdMunicipio { get; set; }
    }
}
