using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarCliente 
{
    public class DesativarClienteRequisicao : RequisicaoBase
    {
        public int IdCliente { get; set; }
        public DateTime DataDesativado { get; set; }
    }
}
