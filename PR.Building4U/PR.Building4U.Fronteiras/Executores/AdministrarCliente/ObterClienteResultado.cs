using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarCliente 
{
    public class ObterClienteResultado : ResultadoBase
    {
        public ClienteDto Cliente { get; set; }
    }
}