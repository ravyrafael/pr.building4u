using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Executores.AdministrarCronograma 
{
	public class CalcularCronogramaRequisicao : RequisicaoBase
	{
        public List<CronogramaDto> Cronogramas { get; set; }
        public int IdCronograma { get; set; }
    }
}
