using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarCronograma 
{
    public class ModificarCronogramaResultado : ResultadoBase
    {
        public List<CronogramaDto> Cronogramas { get; set; }
    }
}