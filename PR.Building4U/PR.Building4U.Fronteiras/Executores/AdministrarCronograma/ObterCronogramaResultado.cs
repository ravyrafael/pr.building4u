using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarCronograma 
{
    public class ObterCronogramaResultado : ResultadoBase
    {
        public CronogramaDto Cronograma { get; set; }
    }
}