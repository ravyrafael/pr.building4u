namespace PR.Building4U.Fronteiras.Executores.AdministrarCronograma 
{
    public class EditarCronogramaRequisicao : RequisicaoBase
    {
        public int IdCronograma { get; set; }
        public string NomeCronograma { get; set; }
        public int DiaMedicaoCronograma { get; set; }
    }
}
