using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PR.Building4U.Fronteiras.Executores.AdministrarCronograma
{
    public class ModificarCronogramaRequisicao : RequisicaoBase
    {
        public string IdCronograma { get; set; }
        public string IdCronogramaServico { get; set; }
        public string MesReferencia { get; set; }
        public string DataProducao { get; set; }
        public string IdPlanilha { get; set; }
        public string IdItemPlanilha { get; set; }

        public string NomePropriedade { get; set; }
        public string ValorPropriedade { get; set; }

        public string CronogramasJson { get; set; }
        public int? IdCronogramaJson { get; set; }

        public List<CronogramaDto> Cronogramas
        {
            get
            {
                return string.IsNullOrEmpty(CronogramasJson) ? null : JsonConvert.DeserializeObject<List<CronogramaDto>>(CronogramasJson);
            }
        }
    }
}
