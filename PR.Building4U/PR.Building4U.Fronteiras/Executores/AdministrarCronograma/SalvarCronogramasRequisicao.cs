using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Executores.AdministrarCronograma 
{
    public class SalvarCronogramasRequisicao : RequisicaoBase
    {
        public List<CronogramaDto> Cronogramas { get; set; }
    }
}
