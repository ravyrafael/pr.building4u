namespace PR.Building4U.Fronteiras.Executores.AdministrarCronograma 
{
    public class IncluirCronogramaRequisicao : RequisicaoBase
    {
        public int IdObra { get; set; }
        public int OrdemCronograma { get; set; }
        public string NomeCronograma { get; set; }
        public int DiaMedicaoCronograma { get; set; }
    }
}
