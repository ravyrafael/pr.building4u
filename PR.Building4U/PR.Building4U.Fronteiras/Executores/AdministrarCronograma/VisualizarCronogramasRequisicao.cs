namespace PR.Building4U.Fronteiras.Executores.AdministrarCronograma 
{
    public class VisualizarCronogramasRequisicao : RequisicaoBase
    {
        public bool VisaoMultipla { get; set; }
        public int IdObra { get; set; }
    }
}
