﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarMunicipio
{
    public class EditarMunicipioRequisicao : RequisicaoBase
    {
        public int IdMunicipio { get; set; }
        public int CodigoMunicipio { get; set; }
        public string NomeMunicipio { get; set; }
        public int IdEstado { get; set; }
    }
}
