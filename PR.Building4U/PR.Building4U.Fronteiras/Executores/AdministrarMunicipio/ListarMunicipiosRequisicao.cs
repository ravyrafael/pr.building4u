﻿using PR.Building4U.Util;

namespace PR.Building4U.Fronteiras.Executores.AdministrarMunicipio
{
    public class ListarMunicipiosRequisicao : RequisicaoBase
    {
        public string CodigoOuNome { get; set; }
    }
}
