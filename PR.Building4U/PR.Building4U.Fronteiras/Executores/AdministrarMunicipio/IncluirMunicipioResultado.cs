﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarMunicipio 
{
	public class IncluirMunicipioResultado : ResultadoBase
    {
        public int IdMunicipio { get; set; }
    }
}