﻿using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarMunicipio 
{
	public class ObterMunicipioResultado : ResultadoBase
    {
        public MunicipioDto Municipio { get; set; }
    }
}