﻿namespace PR.Building4U.Fronteiras.Executores.AdministrarMunicipio
{
    public class IncluirMunicipioRequisicao : RequisicaoBase
    {
        public int CodigoMunicipio { get; set; }
        public string NomeMunicipio { get; set; }
        public int IdEstado { get; set; }
    }
}
