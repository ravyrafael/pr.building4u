﻿using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Executores.AdministrarMunicipio
{
    public class ListarMunicipiosResultado : ResultadoBase
    {
        public List<MunicipioDto> Municipios { get; set; }
    }
}
