using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarMunicipio 
{
    public class ListarMunicipiosPorEstadoResultado : ResultadoBase
    {
        public List<MunicipioDto> Municipios { get; set; }
    }
}