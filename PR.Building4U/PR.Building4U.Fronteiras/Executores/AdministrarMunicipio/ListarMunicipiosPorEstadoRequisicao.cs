namespace PR.Building4U.Fronteiras.Executores.AdministrarMunicipio 
{
    public class ListarMunicipiosPorEstadoRequisicao : RequisicaoBase
    {
        public int IdEstado { get; set; }
        public string CodigoOuNome { get; set; }
    }
}
