using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Executores.AdministrarCusto 
{
	public class ListarCustosResultado : ResultadoBase
	{
        public List<CustoDto> Custos { get; set; }
    }
}