using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarCusto 
{
    public class ObterCustoResultado : ResultadoBase
    {
        public CustoDto Custo { get; set; }
    }
}