namespace PR.Building4U.Fronteiras.Executores.AdministrarCusto 
{
    public class EditarCustoRequisicao : RequisicaoBase
    {
        public int IdCusto { get; set; }
        public string NomeCusto { get; set; }
    }
}
