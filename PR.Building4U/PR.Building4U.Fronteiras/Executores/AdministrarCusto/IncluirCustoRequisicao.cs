namespace PR.Building4U.Fronteiras.Executores.AdministrarCusto 
{
    public class IncluirCustoRequisicao : RequisicaoBase
    {
        public string SiglaCusto { get; set; }
        public string NomeCusto { get; set; }
    }
}
