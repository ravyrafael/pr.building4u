namespace PR.Building4U.Fronteiras.Executores.AdministrarNecessario 
{
    public class AtualizarNecessarioAprovRequisicao : RequisicaoBase
    {
        public int IdObra { get; set; }
        public int IdCargo { get; set; }
        public int QuantidadeNecessario { get; set; }
    }
}
