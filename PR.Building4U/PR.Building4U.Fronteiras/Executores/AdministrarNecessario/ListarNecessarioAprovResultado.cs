using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarNecessario 
{
    public class ListarNecessarioAprovResultado : ResultadoBase
    {
        public List<NecessarioDto> Necessarios { get; set; }
    }
}