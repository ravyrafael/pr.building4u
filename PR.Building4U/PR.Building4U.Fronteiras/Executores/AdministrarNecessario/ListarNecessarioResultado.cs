using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarNecessario 
{
    public class ListarNecessarioResultado : ResultadoBase
    {
        public List<NecessarioAprovDto> Necessarios { get; set; }
    }
}