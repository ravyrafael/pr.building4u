namespace PR.Building4U.Fronteiras.Executores.AdministrarUnidade 
{
    public class EditarUnidadeRequisicao : RequisicaoBase
    {
        public int IdUnidade { get; set; }
        public string SiglaUnidade { get; set; }
        public string NomeUnidade { get; set; }
    }
}
