using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarUnidade 
{
    public class ListarUnidadesResultado : ResultadoBase
    {
        public List<UnidadeDto> Unidades { get; set; }
    }
}