namespace PR.Building4U.Fronteiras.Executores.AdministrarUnidade 
{
    public class IncluirUnidadeResultado : ResultadoBase
    {
        public int IdUnidade { get; set; }
    }
}