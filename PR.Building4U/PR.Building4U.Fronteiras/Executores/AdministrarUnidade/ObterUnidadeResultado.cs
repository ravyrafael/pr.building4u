using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarUnidade 
{
    public class ObterUnidadeResultado : ResultadoBase
    {
        public UnidadeDto Unidade { get; set; }
    }
}