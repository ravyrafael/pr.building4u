namespace PR.Building4U.Fronteiras.Executores.AdministrarUnidade 
{
    public class IncluirUnidadeRequisicao : RequisicaoBase
    {
        public string SiglaUnidade { get; set; }
        public string NomeUnidade { get; set; }
    }
}
