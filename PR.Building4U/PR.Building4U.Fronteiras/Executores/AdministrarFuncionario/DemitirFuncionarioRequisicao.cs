using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarFuncionario 
{
    public class DemitirFuncionarioRequisicao : RequisicaoBase
    {
        public int IdFuncionario { get; set; }
        public DateTime DemissaoFuncionario { get; set; }
    }
}
