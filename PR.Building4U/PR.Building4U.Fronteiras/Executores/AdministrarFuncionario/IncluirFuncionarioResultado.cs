namespace PR.Building4U.Fronteiras.Executores.AdministrarFuncionario 
{
    public class IncluirFuncionarioResultado : ResultadoBase
    {
        public int IdFuncionario { get; set; }
        public int IdAlocacao { get; set; }
    }
}