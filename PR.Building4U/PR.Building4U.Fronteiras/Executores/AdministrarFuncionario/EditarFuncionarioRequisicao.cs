namespace PR.Building4U.Fronteiras.Executores.AdministrarFuncionario 
{
	public class EditarFuncionarioRequisicao : RequisicaoBase
	{
        public int IdFuncionario { get; set; }
        public string NomeFuncionario { get; set; }
        public string EmailFuncionario { get; set; }
        public string TelefoneFuncionario { get; set; }
        public int IdCargo { get; set; }

        public int IdEndereco { get; set; }
        public string LogradouroEndereco { get; set; }
        public string NumeroEndereco { get; set; }
        public string ComplementoEndereco { get; set; }
        public string CepEndereco { get; set; }
        public string DistritoEndereco { get; set; }
        public int IdMunicipio { get; set; }
    }
}
