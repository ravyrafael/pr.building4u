using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarFuncionario 
{
    public class ListarFuncionariosResultado : ResultadoBase
    {
        public List<FuncionarioDto> Funcionarios { get; set; }
    }
}