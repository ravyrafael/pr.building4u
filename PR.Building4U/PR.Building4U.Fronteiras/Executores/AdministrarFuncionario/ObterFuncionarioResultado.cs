using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarFuncionario 
{
    public class ObterFuncionarioResultado : ResultadoBase
    {
        public FuncionarioDto Funcionario { get; set; }
    }
}