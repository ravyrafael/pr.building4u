namespace PR.Building4U.Fronteiras.Executores.AdministrarContrato 
{
    public class IncluirContratoResultado : ResultadoBase
    {
        public int IdContrato { get; set; }
    }
}