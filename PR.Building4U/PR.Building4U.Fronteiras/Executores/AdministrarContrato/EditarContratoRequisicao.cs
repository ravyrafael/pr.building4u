namespace PR.Building4U.Fronteiras.Executores.AdministrarContrato 
{
    public class EditarContratoRequisicao : RequisicaoBase
    {
        public int IdContrato { get; set; }
        public string EscopoContrato { get; set; }
    }
}
