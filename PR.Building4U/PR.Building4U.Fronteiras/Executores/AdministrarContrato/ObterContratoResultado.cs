using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarContrato 
{
    public class ObterContratoResultado : ResultadoBase
    {
        public ContratoDto Contrato { get; set; }
    }
}