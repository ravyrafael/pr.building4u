using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarContrato 
{
    public class ListarContratosResultado : ResultadoBase
    {
        public List<ContratoDto> Contratos { get; set; }
    }
}