namespace PR.Building4U.Fronteiras.Executores.AdministrarContrato 
{
    public class ExcluirContratoRequisicao : RequisicaoBase
    {
        public int IdContrato { get; set; }
    }
}
