using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarContrato 
{
    public class IncluirContratoRequisicao : RequisicaoBase
    {
        public int IdCliente { get; set; }
        public string CodigoContrato { get; set; }
        public DateTime DataContrato { get; set; }
        public decimal ValorContrato { get; set; }
        public int PrazoContrato { get; set; }
        public string EscopoContrato { get; set; }
    }
}
