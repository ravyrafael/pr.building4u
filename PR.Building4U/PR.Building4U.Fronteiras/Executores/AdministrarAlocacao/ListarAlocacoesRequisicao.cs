namespace PR.Building4U.Fronteiras.Executores.AdministrarAlocacao 
{
	public class ListarAlocacoesRequisicao : RequisicaoBase
	{
        public int[] Obras { get; set; }
        public int[] Cargos { get; set; }
    }
}
