namespace PR.Building4U.Fronteiras.Executores.AdministrarAlocacao 
{
    public class DesfazerTransferenciaRequisicao : RequisicaoBase
    {
        public int IdFuncionario { get; set; }
        public int SequenciaAlocacao { get; set; }
    }
}
