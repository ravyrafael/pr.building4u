using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarAlocacao 
{
    public class TerminarFeriasFuncionarioRequisicao : RequisicaoBase
    {
        public int IdFuncionario { get; set; }
        public DateTime TerminoFerias { get; set; }
    }
}
