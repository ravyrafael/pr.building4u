using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarAlocacao 
{
    public class ListarAlocacoesResultado : ResultadoBase
    {
        public List<AlocacaoDto> Alocacoes { get; set; }
    }
}