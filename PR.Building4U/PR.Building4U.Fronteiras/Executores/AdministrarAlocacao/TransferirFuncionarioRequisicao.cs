using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarAlocacao 
{
    public class TransferirFuncionarioRequisicao : RequisicaoBase
    {
        public int IdFuncionario { get; set; }
        public int SequenciaAlocacao { get; set; }
        public int IdObra { get; set; }
        public DateTime DataTransferencia { get; set; }
    }
}
