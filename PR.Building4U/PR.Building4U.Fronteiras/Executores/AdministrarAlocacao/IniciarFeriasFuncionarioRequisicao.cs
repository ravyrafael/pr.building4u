using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarAlocacao 
{
    public class IniciarFeriasFuncionarioRequisicao : RequisicaoBase
    {
        public int IdFuncionario { get; set; }
        public DateTime InicioFerias { get; set; }
        public DateTime TerminoFerias { get; set; }
    }
}
