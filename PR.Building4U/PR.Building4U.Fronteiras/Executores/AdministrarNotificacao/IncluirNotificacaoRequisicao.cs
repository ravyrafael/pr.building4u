using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarNotificacao 
{
    public class IncluirNotificacaoRequisicao : RequisicaoBase
    {
        public int IdFuncionario { get; set; }
        public string TituloNotificacao { get; set; }
        public string TextoNotificacao { get; set; }
        public DateTime DataNotificacao { get; set; }
        public int TipoNotificacao { get; set; }
    }
}
