using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarNotificacao 
{
    public class ListarNotificacoesResultado : ResultadoBase
    {
        public List<NotificacaoDto> Notificacoes { get; set; }
    }
}