namespace PR.Building4U.Fronteiras.Executores.AdministrarNotificacao 
{
    public class MarcarNotificacaoComoLidaRequisicao : RequisicaoBase
    {
        public int? IdNotificacao { get; set; }
        public int? IdFuncionario { get; set; }
    }
}
