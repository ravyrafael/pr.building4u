﻿using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Executores.AdministrarMenu
{
    public class ObterMenuPorPermissaoResultado : ResultadoBase
    {
        public List<RecursoDto> Menu { get; set; }
    }
}
