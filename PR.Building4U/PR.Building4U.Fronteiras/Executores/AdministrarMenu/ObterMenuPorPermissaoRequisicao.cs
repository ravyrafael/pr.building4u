﻿using PR.Building4U.Util;

namespace PR.Building4U.Fronteiras.Executores.AdministrarMenu
{
    public class ObterMenuPorPermissaoRequisicao : RequisicaoBase
    {
        public int PermissaoUsuario { get; set; }
    }
}
