namespace PR.Building4U.Fronteiras.Executores.AdministrarObraGerencia 
{
    public class IncluirObraGerenciaResultado : ResultadoBase
    {
        public int IdObraGerencia { get; set; }
    }
}