namespace PR.Building4U.Fronteiras.Executores.AdministrarObraGerencia 
{
    public class ExcluirObraGerenciaRequisicao : RequisicaoBase
    {
        public int IdObraGerencia { get; set; }
    }
}
