using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarObraGerencia 
{
    public class ListarGerenciasPorObraResultado : ResultadoBase
    {
        public List<ObraGerenciaDto> ObraGerencia { get; set; }
    }
}