namespace PR.Building4U.Fronteiras.Executores.AdministrarObraGerencia 
{
    public class IncluirObraGerenciaRequisicao : RequisicaoBase
    {
        public int IdObra { get; set; }
        public int IdGerencia { get; set; }
        public int IdFuncionario { get; set; }
    }
}
