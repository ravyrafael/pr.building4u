namespace PR.Building4U.Fronteiras.Executores.AdministrarArea 
{
    public class EditarAreaRequisicao : RequisicaoBase
    {
        public int IdArea { get; set; }
        public string NomeArea { get; set; }
    }
}
