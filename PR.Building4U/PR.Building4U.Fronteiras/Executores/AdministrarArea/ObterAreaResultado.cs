using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarArea 
{
    public class ObterAreaResultado : ResultadoBase
    {
        public AreaDto Area { get; set; }
    }
}