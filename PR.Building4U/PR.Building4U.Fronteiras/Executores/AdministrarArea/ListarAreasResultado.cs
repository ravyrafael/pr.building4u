using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarArea 
{
    public class ListarAreasResultado : ResultadoBase
    {
        public List<AreaDto> Areas { get; set; }
    }
}