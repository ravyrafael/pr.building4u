namespace PR.Building4U.Fronteiras.Executores.AdministrarEquipamento 
{
    public class EditarEquipamentoRequisicao : RequisicaoBase
    {
        public int IdEquipamento { get; set; }
        public string MarcaEquipamento { get; set; }
        public string ModeloEquipamento { get; set; }
        public string FornecedorEquipamento { get; set; }
    }
}
