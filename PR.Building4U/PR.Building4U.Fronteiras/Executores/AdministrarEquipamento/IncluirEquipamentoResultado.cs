namespace PR.Building4U.Fronteiras.Executores.AdministrarEquipamento 
{
    public class IncluirEquipamentoResultado : ResultadoBase
    {
        public int IdEquipamento { get; set; }
        public int IdAluguel { get; set; }
    }
}