using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarEquipamento 
{
    public class ObterEquipamentoResultado : ResultadoBase
    {
        public EquipamentoDto Equipamento { get; set; }
    }
}