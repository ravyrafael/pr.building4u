using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarEquipamento 
{
    public class IncluirEquipamentoRequisicao : RequisicaoBase
    {
        public string PatrimonioEquipamento { get; set; }
        public string MarcaEquipamento { get; set; }
        public string ModeloEquipamento { get; set; }
        public int IdTipoEquipamento { get; set; }
        public bool IndicadorTercerio { get; set; }
        public string FornecedorEquipamento { get; set; }
        public int IdObra { get; set; }
        public DateTime AquisicaoEquipamento { get; set; }
    }
}
