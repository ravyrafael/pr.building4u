using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarEquipamento 
{
    public class ListarEquipamentosResultado : ResultadoBase
    {
        public List<EquipamentoDto> Equipamentos { get; set; }
    }
}