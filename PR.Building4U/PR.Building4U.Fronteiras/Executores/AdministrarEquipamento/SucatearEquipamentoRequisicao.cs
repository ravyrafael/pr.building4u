using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarEquipamento 
{
    public class SucatearEquipamentoRequisicao : RequisicaoBase
    {
        public int IdEquipamento { get; set; }
        public DateTime SucataEquipamento { get; set; }
    }
}
