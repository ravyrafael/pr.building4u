namespace PR.Building4U.Fronteiras.Executores.AdministrarAluguel 
{
    public class DesfazerTransferenciaEquipRequisicao : RequisicaoBase
    {
        public int IdEquipamento { get; set; }
        public int SequenciaAluguel { get; set; }
    }
}
