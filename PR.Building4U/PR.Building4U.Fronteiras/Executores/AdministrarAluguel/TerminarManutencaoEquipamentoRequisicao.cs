using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarAluguel 
{
    public class TerminarManutencaoEquipamentoRequisicao : RequisicaoBase
    {
        public int IdEquipamento { get; set; }
        public DateTime TerminoManutencao { get; set; }
    }
}
