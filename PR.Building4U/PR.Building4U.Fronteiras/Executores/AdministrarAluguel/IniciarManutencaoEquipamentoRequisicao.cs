using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarAluguel 
{
    public class IniciarManutencaoEquipamentoRequisicao : RequisicaoBase
    {
        public int IdEquipamento { get; set; }
        public DateTime InicioManutencao { get; set; }
        public DateTime TerminoManutencao { get; set; }
    }
}
