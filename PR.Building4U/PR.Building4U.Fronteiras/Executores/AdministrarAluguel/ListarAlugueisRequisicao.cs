namespace PR.Building4U.Fronteiras.Executores.AdministrarAluguel 
{
    public class ListarAlugueisRequisicao : RequisicaoBase
    {
        public int[] Obras { get; set; }
        public int[] TiposEquipamento { get; set; }
    }
}
