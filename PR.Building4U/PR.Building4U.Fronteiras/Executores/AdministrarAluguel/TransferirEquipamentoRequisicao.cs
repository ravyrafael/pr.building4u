using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarAluguel 
{
    public class TransferirEquipamentoRequisicao : RequisicaoBase
    {
        public int IdEquipamento { get; set; }
        public int IdObra { get; set; }
        public DateTime DataTransferencia { get; set; }
        public int SequenciaAluguel { get; set; }
    }
}
