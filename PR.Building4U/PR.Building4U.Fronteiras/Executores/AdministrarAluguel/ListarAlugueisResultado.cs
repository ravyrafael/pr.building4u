using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarAluguel 
{
    public class ListarAlugueisResultado : ResultadoBase
    {
        public List<AluguelDto> Alugueis { get; set; }
    }
}