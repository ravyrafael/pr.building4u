namespace PR.Building4U.Fronteiras.Executores.AdministrarGerencia 
{
    public class IncluirGerenciaRequisicao : RequisicaoBase
    {
        public string NomeGerencia { get; set; }
        public string SiglaGerencia { get; set; }
    }
}
