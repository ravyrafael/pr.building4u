namespace PR.Building4U.Fronteiras.Executores.AdministrarGerencia 
{
    public class IncluirGerenciaResultado : ResultadoBase
    {
        public int IdGerencia { get; set; }
    }
}