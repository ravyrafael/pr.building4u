using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarGerencia 
{
    public class ListarGerenciasResultado : ResultadoBase
    {
        public List<GerenciaDto> Gerencias { get; set; }
    }
}