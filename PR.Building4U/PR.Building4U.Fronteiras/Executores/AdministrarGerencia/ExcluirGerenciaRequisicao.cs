namespace PR.Building4U.Fronteiras.Executores.AdministrarGerencia 
{
    public class ExcluirGerenciaRequisicao : RequisicaoBase
    {
        public int IdGerencia { get; set; }
    }
}
