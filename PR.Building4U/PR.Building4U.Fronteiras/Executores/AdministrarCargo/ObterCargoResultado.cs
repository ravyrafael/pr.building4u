using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarCargo 
{
    public class ObterCargoResultado : ResultadoBase
    {
        public CargoDto Cargo { get; set; }
    }
}