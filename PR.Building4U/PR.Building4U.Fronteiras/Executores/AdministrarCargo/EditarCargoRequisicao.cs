namespace PR.Building4U.Fronteiras.Executores.AdministrarCargo 
{
    public class EditarCargoRequisicao : RequisicaoBase
    {
        public int IdCargo { get; set; }
        public string NomeCargo { get; set; }
        public string SiglaCargo { get; set; }
        public int IdCusto { get; set; }
    }
}
