using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarCargo 
{
    public class ListarCargosResultado : ResultadoBase
    {
        public List<CargoDto> Cargos { get; set; }
    }
}