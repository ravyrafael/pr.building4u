namespace PR.Building4U.Fronteiras.Executores.AdministrarCargo 
{
    public class IncluirCargoRequisicao : RequisicaoBase
    {
        public string NomeCargo { get; set; }
        public string SiglaCargo { get; set; }
        public int IdCusto { get; set; }
    }
}
