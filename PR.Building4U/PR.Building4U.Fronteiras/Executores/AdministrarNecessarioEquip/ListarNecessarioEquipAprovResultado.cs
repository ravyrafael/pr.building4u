using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarNecessarioEquip 
{
    public class ListarNecessarioEquipAprovResultado : ResultadoBase
    {
        public List<NecessarioEquipDto> Necessarios { get; set; }
    }
}