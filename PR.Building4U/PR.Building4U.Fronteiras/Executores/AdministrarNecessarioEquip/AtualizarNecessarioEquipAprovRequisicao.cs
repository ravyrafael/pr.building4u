namespace PR.Building4U.Fronteiras.Executores.AdministrarNecessarioEquip 
{
	public class AtualizarNecessarioEquipAprovRequisicao : RequisicaoBase
	{
        public int IdObra { get; set; }
        public int IdTipoEquipamento { get; set; }
        public int QuantidadeNecessario { get; set; }
    }
}
