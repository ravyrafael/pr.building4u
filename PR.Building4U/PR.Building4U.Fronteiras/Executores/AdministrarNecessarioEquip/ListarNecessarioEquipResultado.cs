using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarNecessarioEquip 
{
    public class ListarNecessarioEquipResultado : ResultadoBase
    {
        public List<NecessarioEquipAprovDto> Necessarios { get; set; }
    }
}