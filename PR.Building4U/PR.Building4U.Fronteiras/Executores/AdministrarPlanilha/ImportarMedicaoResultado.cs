using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPlanilha 
{
    public class ImportarMedicaoResultado : ResultadoBase
    {
        public PlanilhaDto Planilha { get; set; }
        public object TemInconsistencia { get; set; }
    }
}