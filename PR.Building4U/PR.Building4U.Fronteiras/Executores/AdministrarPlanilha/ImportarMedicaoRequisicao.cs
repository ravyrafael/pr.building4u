using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPlanilha 
{
    public class ImportarMedicaoRequisicao : RequisicaoBase
    {
        public int IdPlanilha { get; set; }
        public DateTime PeriodoMedicao { get; set; }
        public byte[] arquivo { get; set; }
    }
}
