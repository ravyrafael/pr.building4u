using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPlanilha 
{
    public class ListarMedicoesResultado : ResultadoBase
    {
        public List<MedicaoDto> Medicoes { get; set; }
    }
}