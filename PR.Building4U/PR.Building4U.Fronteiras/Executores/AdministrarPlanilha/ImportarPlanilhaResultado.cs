using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPlanilha 
{
    public class ImportarPlanilhaResultado : ResultadoBase
    {
        public PlanilhaDto Planilha { get; set; }
        public bool TemInconsistencia { get; set; }
    }
}