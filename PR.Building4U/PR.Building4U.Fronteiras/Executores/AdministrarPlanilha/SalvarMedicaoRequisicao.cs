using PR.Building4U.Fronteiras.Dtos;
using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPlanilha 
{
	public class SalvarMedicaoRequisicao : RequisicaoBase
	{
        public PlanilhaDto Planilha { get; set; }
        public DateTime PeriodoMedicao { get; set; }
    }
}
