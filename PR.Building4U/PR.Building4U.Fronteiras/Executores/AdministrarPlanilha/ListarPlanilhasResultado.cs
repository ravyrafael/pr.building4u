using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPlanilha 
{
    public class ListarPlanilhasResultado : ResultadoBase
    {
        public List<PlanilhaDto> Planilhas { get; set; }
    }
}