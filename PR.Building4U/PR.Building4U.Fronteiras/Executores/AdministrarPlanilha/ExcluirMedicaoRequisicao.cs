using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPlanilha 
{
    public class ExcluirMedicaoRequisicao : RequisicaoBase
    {
        public int IdPlanilha { get; set; }
        public DateTime PeriodoMedicao { get; set; }
    }
}
