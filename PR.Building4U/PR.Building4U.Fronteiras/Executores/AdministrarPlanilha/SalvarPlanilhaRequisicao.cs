using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarPlanilha 
{
	public class SalvarPlanilhaRequisicao : RequisicaoBase
	{
        public PlanilhaDto Planilha { get; set; }
    }
}
