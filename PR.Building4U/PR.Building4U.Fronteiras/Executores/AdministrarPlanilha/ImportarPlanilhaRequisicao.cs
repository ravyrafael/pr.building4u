namespace PR.Building4U.Fronteiras.Executores.AdministrarPlanilha 
{
    public class ImportarPlanilhaRequisicao : RequisicaoBase
    {
        public byte[] arquivo { get; set; }
        public int IdCronograma { get; set; }
        public string CpPlanilha { get; set; }
        public int RevPlanilha { get; set; }
    }
}
