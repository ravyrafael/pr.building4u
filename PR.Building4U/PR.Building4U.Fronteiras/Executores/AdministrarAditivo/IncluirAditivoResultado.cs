namespace PR.Building4U.Fronteiras.Executores.AdministrarAditivo 
{
    public class IncluirAditivoResultado : ResultadoBase
    {
        public int IdAditivo { get; set; }
    }
}