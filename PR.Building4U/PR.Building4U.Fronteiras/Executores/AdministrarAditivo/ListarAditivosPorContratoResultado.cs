using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.Fronteiras.Executores.AdministrarAditivo 
{
    public class ListarAditivosPorContratoResultado : ResultadoBase
    {
        public List<AditivoDto> Aditivos { get; set; }
    }
}