using System;

namespace PR.Building4U.Fronteiras.Executores.AdministrarAditivo 
{
    public class IncluirAditivoRequisicao : RequisicaoBase
    {
        public int IdContrato { get; set; }
        public string CodigoAditivo { get; set; }
        public DateTime DataAditivo { get; set; }
        public decimal ValorAditivo { get; set; }
        public int PrazoAditivo { get; set; }
        public string EscopoAditivo { get; set; }
    }
}
