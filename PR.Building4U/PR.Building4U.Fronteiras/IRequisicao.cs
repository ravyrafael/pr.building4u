﻿using PR.Building4U.Util;

namespace PR.Building4U.Fronteiras
{
    public interface IRequisicao
    {
        InformacoesLog InformacoesLog { get; set; }
    }
}
