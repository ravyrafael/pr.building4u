﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class NecessarioDto
    {
        public int IdNecessario { get; set; } // id

        public CargoDto Cargo { get; set; } // cargo_id
        public ObraDto Obra { get; set; } // obra_id

        public int QuantidadeNecessario { get; set; } // quantidade_necessario
        public bool IndicadorAprovacao { get; set; } // indicador_aprov
    }
}
