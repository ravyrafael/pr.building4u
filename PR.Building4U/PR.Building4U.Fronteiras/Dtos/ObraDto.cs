﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class ObraDto
    {
        public int IdObra { get; set; } // id
        public string CodigoObra { get; set; } // codigo_obra
        public string NomeObra { get; set; } // nome_obra

        public SituacaoDto Situacao { get; set; } // situacao_id
        public AreaDto Area { get; set; } // area_id
        public ContratoDto Contrato { get; set; } // contrato_id
        public OrdemServicoDto OrdemServico { get; set; }
    }
}
