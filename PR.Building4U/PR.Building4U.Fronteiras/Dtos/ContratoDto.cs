﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class ContratoDto
    {
        public int IdContrato { get; set; } // id
        public string CodigoContrato { get; set; } // codigo_contrato
        public DateTime DataContrato { get; set; } // data_contrato
        public decimal ValorContrato { get; set; } // valor_contrato
        public decimal ValorAditivo { get; set; } // valor_aditivo
        public int PrazoContrato { get; set; } // prazo_contrato
        public int PrazoAditivo { get; set; } // prazo_aditivo
        public string EscopoContrato { get; set; } // escopo_contrato

        public DateTime VencimentoContrato
        {
            get
            {
                return DataContrato.AddDays(PrazoContrato).AddDays(PrazoAditivo);
            }
        }

        public ClienteDto Cliente { get; set; } // cliente_id
    }
}
