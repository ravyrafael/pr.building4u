﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class GerenciaDto
    {
        public int IdGerencia { get; set; } // id
        public string NomeGerencia { get; set; } // nome_gerencia
        public string SiglaGerencia { get; set; } // sigla_gerencia
    }
}
