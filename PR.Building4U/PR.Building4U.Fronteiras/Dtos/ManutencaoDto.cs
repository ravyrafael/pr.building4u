﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class ManutencaoDto
    {
        public int IdManutencao { get; set; } // id
        public DateTime InicioManutencao { get; set; } // inicio_manutencao
        public DateTime TerminoManutencao { get; set; } // termino_manutencao
        public int SequenciaManutencao { get; set; } // sequencia_manutencao

        public EquipamentoDto Equipamento { get; set; } // equipamento_id
    }
}
