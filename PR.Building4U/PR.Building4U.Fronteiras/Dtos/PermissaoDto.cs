﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class PermissaoDto
    {
        public int IdPermissao { get; set; } // id
        public string SiglaPermissao { get; set; } // sigla_permissao
        public string NomePermissao { get; set; } // nome_permissao
    }
}
