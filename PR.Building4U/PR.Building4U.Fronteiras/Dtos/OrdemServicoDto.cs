﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class OrdemServicoDto
    {
        public int IdOrdemServico { get; set; } // id
        public DateTime? DataOrdemServico { get; set; } // data_os
        public int? PrazoOrdemServico { get; set; } // prazo_os
    }
}
