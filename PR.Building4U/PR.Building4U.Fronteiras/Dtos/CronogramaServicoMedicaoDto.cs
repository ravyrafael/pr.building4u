﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class CronogramaServicoMedicaoDto
    {
        public int IdCronogramaServicoMedicao { get; set; } // id
        public int IdCronogramaServico { get; set; } // servico_id
        public DateTime MesReferencia { get; set; } // mes_referencia
        public DateTime InicioMedicao { get; set; } // inicio_medicao
        public DateTime TerminoMedicao { get; set; } // termino_medicao
        public decimal? Faturamento { get; set; } // faturamento_medicao
        public decimal? Quantidade { get; set; } // quantidade_medicao
        public decimal? PesMedicao { get; set; } // pes_medicao
        public decimal? EquipeMedicao { get; set; } // equipe_medicao
        public bool TemMedicao { get; set; } // tem_medicao
    }
}
