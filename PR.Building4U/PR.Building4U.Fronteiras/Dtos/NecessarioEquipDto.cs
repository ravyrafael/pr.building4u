﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class NecessarioEquipDto
    {
        public int IdNecessario { get; set; } // id

        public TipoEquipamentoDto TipoEquipamento { get; set; } // tipo_equipamento_id
        public ObraDto Obra { get; set; } // obra_id

        public int QuantidadeNecessario { get; set; } // quantidade_necessario
        public bool IndicadorAprovacao { get; set; } // indicador_aprov
    }
}
