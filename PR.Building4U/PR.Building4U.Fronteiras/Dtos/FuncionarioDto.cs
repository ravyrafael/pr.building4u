﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class FuncionarioDto
    {
        public int IdFuncionario { get; set; } // id
        public string ChapaFuncionario { get; set; } // chapa_funcionario
        public string NomeFuncionario { get; set; } // nome_funcionario
        public DateTime AdmissaoFuncionario { get; set; } // admissao_funcionario
        public DateTime? DemissaoFuncionario { get; set; } // demissao_funcionario
        public string EmailFuncionario { get; set; } // email_funcionario
        public string TelefoneFuncionario { get; set; } // telefone_funcionario

        public EnderecoDto Endereco { get; set; } // endereco_id
        public SituacaoDto Situacao { get; set; } // situacao_id
        public CargoDto Cargo { get; set; } // cargo_id
        public ObraDto Obra { get; set; }
    }
}
