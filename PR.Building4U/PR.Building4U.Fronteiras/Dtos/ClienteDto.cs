﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class ClienteDto
    {
        public int IdCliente { get; set; } // id
        public string CnpjCliente { get; set; } // cnpj_cliente
        public string RazaoSocialCliente { get; set; } // razao_social_cliente
        public string NomeCliente { get; set; } // nome_cliente

        public EnderecoDto Endereco { get; set; } // endereco_id
        public SituacaoDto Situacao { get; set; } // situacao_id
    }
}
