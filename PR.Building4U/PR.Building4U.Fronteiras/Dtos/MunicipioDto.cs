﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class MunicipioDto
    {
        public int IdMunicipio { get; set; } // id
        public string NomeMunicipio { get; set; } // nome_municipio
        public int CodigoMunicipio { get; set; } // codigo_municipio

        public EstadoDto Estado { get; set; } // estado_id
    }
}
