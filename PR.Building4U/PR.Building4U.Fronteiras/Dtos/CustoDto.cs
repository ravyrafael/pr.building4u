﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class CustoDto
    {
        public int IdCusto { get; set; } // id
        public string SiglaCusto { get; set; } // sigla_custo
        public string NomeCusto { get; set; } // nome_custo
    }
}
