﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class FeriasDto
    {
        public int IdFerias { get; set; } // id
        public DateTime InicioFerias { get; set; } // inicio_ferias
        public DateTime TerminoFerias { get; set; } // termino_ferias
        public int SequenciaFerias { get; set; } // sequencia_ferias

        public FuncionarioDto Funcionario { get; set; } // funcionario_id
    }
}
