﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class AditivoDto
    {
        public int IdAditivo { get; set; } // id
        public string CodigoAditivo { get; set; } // codigo_aditivo
        public DateTime DataAditivo { get; set; } // data_aditivo
        public decimal ValorAditivo { get; set; } // valor_aditivo
        public int PrazoAditivo { get; set; } // prazo_aditivo
        public string EscopoAditivo { get; set; } // escopo_aditivo

        public ContratoDto Contrato { get; set; } // contrato_id
    }
}
