﻿using PR.Building4U.Util;
using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class NotificacaoDto
    {
        public int IdNotificacao { get; set; } // id
        public string TituloNotificacao { get; set; } // titulo_notificacao
        public string TextoNotificacao { get; set; } // texto_notificacao
        public DateTime DataNotificacao { get; set; } // data_notificacao
        public TipoNotificacao TipoNotificacao { get; set; } // tipo_notificacao
        public bool LidoNotificacao { get; set; } // lido_notificacao
    }
}
