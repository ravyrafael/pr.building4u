﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class PlanilhaItemMedicaoDto
    {
        public int IdPlanilhaItemMedicao { get; set; } // id
        public int IdPlanilhaItem { get; set; } // planilha_item_id
        public DateTime PeriodoMedicao { get; set; } // periodo_medicao
        public decimal? QuantidadeMedicao { get; set; } // quantidade_medicao
        public decimal ValorMedicao { get; set; } // valor_medicao
    }
}
