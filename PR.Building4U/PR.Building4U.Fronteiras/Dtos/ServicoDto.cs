﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class ServicoDto
    {
        public int IdServico { get; set; } // id
        public int CodigoServico { get; set; } // codigo_servico
        public string SiglaServico { get; set; } // sigla_servico
        public string NomeServico { get; set; } // nome_servico

        public UnidadeDto Unidade { get; set; } // unidade_id
    }
}
