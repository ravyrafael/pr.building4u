﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class UnidadeDto
    {
        public int IdUnidade { get; set; } // id
        public string SiglaUnidade { get; set; } // sigla_unidade
        public string NomeUnidade { get; set; } // nome_unidade
    }
}
