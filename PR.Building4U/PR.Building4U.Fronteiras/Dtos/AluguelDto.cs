﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class AluguelDto
    {
        public int IdAluguel { get; set; } // id
        public int SequenciaAluguel { get; set; } // sequencia_aluguel

        public int IdEquipamento { get; set; }
        public string PatrimonioEquipamento { get; set; }
        public string TipoEquipamento { get; set; }
        public string MarcaEquipamento { get; set; }
        public string ModeloEquipamento { get; set; }
        public string NomeCusto { get; set; }
        public string SituacaoEquipamento { get; set; }
        public string SiglaSituacaoEquipamento { get; set; }
        public int IdObra { get; set; }
        public string CodigoObra { get; set; }
        public string NomeObra { get; set; }
        public DateTime InicioAluguel { get; set; }
        public DateTime? InicioReservaAluguel { get; set; }
        public string ObraOrigem { get; set; }
        public string ObraDestino { get; set; }
        public string SituacaoObra { get; set; }
        public DateTime? TerminoManutencao { get; set; }
    }
}
