﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class NecessarioEquipAprovDto
    {
        public virtual int IdNecessario { get; set; } // id
        public int QuantidadeNecessario { get; set; } // quantidade_necessario
        public int QuantidadeAprovacao { get; set; } // quantidade_necessario

        public TipoEquipamentoDto TipoEquipamento { get; set; } // tipo_equipamento_id
    }
}
