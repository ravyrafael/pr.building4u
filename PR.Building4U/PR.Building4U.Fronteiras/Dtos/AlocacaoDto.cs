﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class AlocacaoDto
    {
        public int IdAlocacao { get; set; } // id
        public int SequenciaAlocacao { get; set; } // sequencia_alocacao

        public int IdFuncionario { get; set; }
        public string ChapaFuncionario { get; set; }
        public string NomeFuncionario { get; set; }
        public string SituacaoFuncionario { get; set; }
        public string SiglaSituacaoFuncionario { get; set; }
        public string NomeCusto { get; set; }
        public string NomeCargo { get; set; }
        public int IdObra { get; set; }
        public string CodigoObra { get; set; }
        public string NomeObra { get; set; }
        public DateTime InicioAlocacao { get; set; }
        public DateTime? InicioReserva { get; set; }
        public string ObraOrigem { get; set; }
        public string ObraDestino { get; set; }
        public string SituacaoObra { get; set; }
        public DateTime? TerminoFerias { get; set; }
    }
}