﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class PaisDto
    {
        public int IdPais { get; set; } // id
        public int CodigoPais { get; set; } // codigo_pais
        public string NomePais { get; set; } // nome_pais
    }
}
