﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class ObraGerenciaDto
    {
        public int IdObraGerencia { get; set; } // id

        public ObraDto Obra { get; set; } // obra_id
        public GerenciaDto Gerencia { get; set; } // gerencia_id
        public FuncionarioDto Funcionario { get; set; } // funcionario_id
    }
}
