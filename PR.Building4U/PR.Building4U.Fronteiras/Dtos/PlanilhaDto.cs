﻿using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class PlanilhaDto
    {
        public int IdPlanilha { get; set; } // id
        public int IdCronograma { get; set; } // cronograma_id
        public string CpPlanilha { get; set; } // cp_planilha
        public int RevPlanilha { get; set; } // rev_planilha

        public List<PlanilhaItemDto> Itens { get; set; }
    }
}
