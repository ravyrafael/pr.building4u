﻿using PR.Building4U.Util;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class SituacaoDto
    {
        public int IdSituacao { get; set; } // id
        public string SiglaSituacao { get; set; } // sigla_situacao
        public string NomeSituacao { get; set; } // nome_situacao
        public TipoSituacao TipoSituacao { get; set; } // tipo_situacao
    }
}
