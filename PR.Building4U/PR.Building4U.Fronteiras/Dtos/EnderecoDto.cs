﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class EnderecoDto
    {
        public int IdEndereco { get; set; } // id
        public string LogradouroEndereco { get; set; } // logradouro_endereco
        public string NumeroEndereco { get; set; } // numero_endereco
        public string ComplementoEndereco { get; set; } // complemento_endereco
        public string CepEndereco { get; set; } // cep_endereco
        public string DistritoEndereco { get; set; } // distrito_endereco

        public MunicipioDto Municipio { get; set; } // municipio_id
    }
}
