﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class MedicaoDto
    {
        public int IdPlanilha { get; set; }
        public DateTime PeriodoMedicao { get; set; }
        public decimal ValorMedicao { get; set; }
    }
}
