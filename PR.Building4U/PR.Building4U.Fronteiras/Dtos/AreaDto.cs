﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class AreaDto
    {
        public int IdArea { get; set; } // id
        public string SiglaArea { get; set; } // sigla_area
        public string NomeArea { get; set; } // nome_area
    }
}
