﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class ProducaoDto
    {
        public int IdProducao { get; set; } // id
        public int IdCronogramaServico { get; set; } // cronograma_servico_id
        public DateTime DataProducao { get; set; } // data_producao
        public decimal? QuantidadeProducao { get; set; } // quantidade_producao
        public decimal? EquipeProducao { get; set; } // equipe_producao
        public string ObservacaoProducao { get; set; } // observacao_producao
    }
}
