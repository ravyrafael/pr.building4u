﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class CargoDto
    {
        public int IdCargo { get; set; } // id
        public string NomeCargo { get; set; } // nome_cargo
        public string SiglaCargo { get; set; } // sigla_cargo

        public CustoDto Custo { get; set; } // custo_id
    }
}
