﻿using System;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class CronogramaServicoDto
    {
        public int IdCronogramaServico { get; set; } // id
        public int IdCronograma { get; set; } // cronograma_id
        public int OrdemServico { get; set; } // ordem_cron_servico
        public string NomeServico { get; set; } // nome_cron_servico
        public decimal QuantidadeTotal { get; set; } // quantidade_total
        public decimal QuantidadeExecutada { get; set; } // quantidade_executada
        public decimal? QuantidadeReal { get; set; } // quantidade_real
        public decimal MediaEquipe { get; set; } // media_equipe
        public decimal MediaPes { get; set; } // media_pes
        public decimal? MediaPesReal { get; set; } // media_pes_real
        public decimal ValorServico { get; set; } // valor_total
        public int DuracaoTotal { get; set; } // duracao_total
        public int DuracaoFalta { get; set; } // duracao_falta
        public DateTime? InicioServico { get; set; } // inicio_servico
        public DateTime? TerminoServico { get; set; } // termino_servico
        public DateTime? AtualizacaoServico { get; set; } // atualizacao_servico
        public int? IdCronogramaPredecessora { get; set; } // id_cronograma_predecessora
        public int? IdServicoPredecessora { get; set; } // id_servico_predecessora
        public char? TipoPredecessora { get; set; } // tipo_predecessora
        public int? AtrasoPredecessora { get; set; } // atraso_predecessora
        public decimal ChanceFaturamento { get; set; } // chance_faturamento

        public ServicoDto Servico { get; set; } // servico_id

        public List<CronogramaServicoMedicaoDto> Medicoes { get; set; }
        public List<int> MedicoesRemovidas { get; set; }

        public List<ProducaoDto> Producoes { get; set; }
        public List<int> ProducoesRemovidas { get; set; }
    }
}
