﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class NecessarioAprovDto
    {
        public virtual int IdNecessario { get; set; } // id
        public int QuantidadeNecessario { get; set; } // quantidade_necessario
        public int QuantidadeAprovacao { get; set; } // quantidade_necessario

        public CargoDto Cargo { get; set; } // cargo_id
    }
}
