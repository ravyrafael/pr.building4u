﻿using System.Security;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class UsuarioDto
    {
        public UsuarioDto()
        {
            Funcionario = new FuncionarioDto();
            Situacao = new SituacaoDto();
            Permissao = new PermissaoDto();
        }
        public int IdUsuario { get; set; } // id
        public string LoginUsuario { get; set; } // login_usuario
        public string SenhaUsuario { get; set; } // senha_usuario
        public string NomeReduzidoUsuario { get; set; } // reduzido_usuario

        public FuncionarioDto Funcionario { get; set; } // funcionario_id
        public SituacaoDto Situacao { get; set; } // situacao_id
        public PermissaoDto Permissao { get; set; } // permissao_id
    }
}
