﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class EstadoDto
    {
        public int IdEstado { get; set; } // id
        public string SiglaEstado { get; set; } // sigla_estado
        public string NomeEstado { get; set; } // nome_estado

        public PaisDto Pais { get; set; } // pais_id
    }
}
