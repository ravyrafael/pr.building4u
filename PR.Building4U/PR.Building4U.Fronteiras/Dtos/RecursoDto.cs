﻿using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class RecursoDto
    {
        public int IdRecurso { get; set; } // id
        public string NomeRecurso { get; set; } // nome_recurso
        public string IconeRecurso { get; set; } // icone_recurso
        public string ControllerRecurso { get; set; } // controller_recurso
        public string ActionRecurso { get; set; } // action_recurso
        public string ParametrosRecurso { get; set; } // parametros_recurso
        public bool EhMenuRecurso { get; set; } // ehmenu_recurso

        public List<RecursoDto> SubMenu { get; set; } // menu_pai_id
    }
}
