﻿using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class PlanilhaItemDto
    {
        public int IdPlanilhaItem { get; set; } // id
        public int? IdCronogramaServico { get; set; } // cronograma_servico_id
        public string NomeCronogramaServico { get; set; }
        public string IdServicoAux { get; set; }
        public int IdPlanilha { get; set; } // planilha_id
        public string CodigoItem { get; set; } // codigo_item_planilha
        public string DescricaoItem { get; set; } // descricao_item_planilha
        public decimal? PrecoItem { get; set; } // preco_item_planilha
        public decimal? QuantidadeItem { get; set; } // quantidade_item_planilha
        public decimal? QuantidadeAcertoItem { get; set; } // quantidade_acerto_item
        public bool EhTituloItem { get; set; } // eh_titulo_item
        public bool EhItemControle { get; set; } // eh_item_controle

        public UnidadeDto Unidade { get; set; } // unidade_id

        public List<PlanilhaItemMedicaoDto> Medicoes { get; set; }
    }
}
