﻿using System;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class EquipamentoDto
    {
        public int IdEquipamento { get; set; } // id
        public string MarcaEquipamento { get; set; } // marca_equipamento
        public string ModeloEquipamento { get; set; } // modelo_equipamento
        public string PatrimonioEquipamento { get; set; } // patrimonio_equipamento
        public bool IndicadorTerceiro { get; set; } // indicador_terceiro
        public string FornecedorEquipamento { get; set; } // fornecedor_equipamento
        public DateTime AquisicaoEquipamento { get; set; } // aquisicao_equipamento
        public DateTime? SucataEquipamento { get; set; } // sucata_equipamento

        public TipoEquipamentoDto TipoEquipamento { get; set; } // tipo_equipamento_id
        public SituacaoDto Situacao { get; set; } // situacao_id
        public ObraDto Obra { get; set; }
    }
}
