﻿namespace PR.Building4U.Fronteiras.Dtos
{
    public class TipoEquipamentoDto
    {
        public int IdTipoEquipamento { get; set; } // id
        public string NomeTipoEquipamento { get; set; } // nome_tipo

        public CustoDto Custo { get; set; } // custo_id
    }
}
