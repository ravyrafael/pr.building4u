﻿using System;
using System.Collections.Generic;

namespace PR.Building4U.Fronteiras.Dtos
{
    public class CronogramaDto
    {
        public int IdCronograma { get; set; } // id
        public int OrdemCronograma { get; set; } // ordem_cronograma
        public string NomeCronograma { get; set; } // nome_cronograma
        public int DiaMedicaoCronograma { get; set; } // medicao_cronograma
        public DateTime? InicioCronograma { get; set; } // inicio_cronograma
        public DateTime? TerminoCronograma { get; set; } // termino_cronograma
        public DateTime? AtualizacaoCronograma { get; set; } // atualizacao_cronograma

        public ObraDto Obra { get; set; } // obra_id

        public List<CronogramaServicoDto> Servicos { get; set; }
        public List<int> ServicosRemovidos { get; set; }

        public List<PlanilhaDto> Planilhas { get; set; }
    }
}
