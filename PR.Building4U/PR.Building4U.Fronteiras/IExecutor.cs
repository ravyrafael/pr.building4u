﻿namespace PR.Building4U.Fronteiras
{
    public interface IExecutor<in TRequisicao, out TResultado> where TResultado : IResultado
    {
        TResultado Executar(TRequisicao requisicao);
    }
}
