﻿namespace PR.Building4U.Fronteiras
{
    public interface IResultado
    {
        bool Sucesso { get; set; }
    }
}
