﻿using PR.Building4U.Util;

namespace PR.Building4U.Fronteiras
{
    public class ResultadoBase : IResultado
    {
        public bool Sucesso { get; set; } = false;
        public Mensagem Mensagem { get; set; } = new Mensagem(TipoMensagem.Info, string.Empty);
    }
}
