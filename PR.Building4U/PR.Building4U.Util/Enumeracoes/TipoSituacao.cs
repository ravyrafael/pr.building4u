﻿namespace PR.Building4U.Util
{
    public enum TipoSituacao
    {
        Pessoal = 1,
        Obra = 2,
        Sistema = 3,
        Equipamento = 4
    }
}
