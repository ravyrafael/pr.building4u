﻿namespace PR.Building4U.Util
{
    public enum TipoMensagem
    {
        Info = 1,
        Aviso = 2,
        Sucesso = 3,
        Erro = 4
    }
}
