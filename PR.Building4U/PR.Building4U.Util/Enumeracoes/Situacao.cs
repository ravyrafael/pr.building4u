﻿namespace PR.Building4U.Util.Enumeracoes
{
    public enum Situacao
    {
        Ativo = 1,
        Inativo = 2,
        Trabalhando = 3,
        Demitido = 4,
        Ferias = 5,
        EmAndamento = 6,
        EmNegociacao = 7,
        Terminada = 8,
        Funcionando = 9,
        Sucata = 10,
        Manutencao = 11
    }
}
