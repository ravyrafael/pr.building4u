﻿namespace PR.Building4U.Util
{
    public enum TipoNotificacao
    {
        Producao = 1,
        Medicao = 2,
        Cronograma = 3,
        Pessoal = 4,
        Reuniao = 5
    }
}
