﻿using System;

namespace PR.Building4U.Util
{
    public class InformacoesLog
    {
        public int IdUsuario { get; set; }
        public string LoginUsuario { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Descricao { get; set; }
        public DateTime DataLog { get; set; }
        public TipoLog TipoLog { get; set; }
    }
}
