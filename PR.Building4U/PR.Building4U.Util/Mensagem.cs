﻿namespace PR.Building4U.Util
{
    public class Mensagem
    {
        public TipoMensagem Tipo { get; set; }
        public string Texto { get; set; }
        
        /// <summary>
        /// Initialize a new instance of Mensagem
        /// </summary>
        /// <param name="tipo">O valor do tipo de mensagem</param>
        /// <param name="texto">O texto da mensagem a ser exibida</param>
        public Mensagem(TipoMensagem tipo, string texto)
        {
            Tipo = tipo;
            Texto = texto;
        }
    }
}
