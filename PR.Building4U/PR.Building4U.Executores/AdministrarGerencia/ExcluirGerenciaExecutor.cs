using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarGerencia;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarGerencia
{
    public class ExcluirGerenciaExecutor : IExecutor<ExcluirGerenciaRequisicao, ExcluirGerenciaResultado>
    {
        private readonly IGerenciaRepositorio gerenciaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirGerenciaExecutor(IGerenciaRepositorio gerenciaRepositorio, ILogRepositorio logRepositorio)
        {
            this.gerenciaRepositorio = gerenciaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirGerenciaResultado Executar(ExcluirGerenciaRequisicao requisicao)
        {
            var resultado = new ExcluirGerenciaResultado();

            gerenciaRepositorio.ExcluirGerencia(requisicao.IdGerencia);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Gerencia {requisicao.IdGerencia}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}