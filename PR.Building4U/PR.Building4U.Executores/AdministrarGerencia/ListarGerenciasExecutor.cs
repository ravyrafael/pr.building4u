using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarGerencia;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarGerencia
{
    public class ListarGerenciasExecutor : IExecutor<ListarGerenciasRequisicao, ListarGerenciasResultado>
    {
        private readonly IGerenciaRepositorio gerenciaRepositorio;

        public ListarGerenciasExecutor(IGerenciaRepositorio gerenciaRepositorio)
        {
            this.gerenciaRepositorio = gerenciaRepositorio;
        }

        [Transacao]
        public ListarGerenciasResultado Executar(ListarGerenciasRequisicao requisicao)
        {
            var resultado = new ListarGerenciasResultado();

            var gerencias = gerenciaRepositorio.ListarGerencias(requisicao.CodigoOuNome);
            resultado.Gerencias = Mapper.Map<List<GerenciaDto>>(gerencias) ?? new List<GerenciaDto>();

            return resultado;
        }
    }
}