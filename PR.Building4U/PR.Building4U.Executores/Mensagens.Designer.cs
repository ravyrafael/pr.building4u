﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PR.Building4U.Executores {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Mensagens {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Mensagens() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("PR.Building4U.Executores.Mensagens", typeof(Mensagens).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível salvar registro com os dados repetidos..
        /// </summary>
        internal static string AVI001 {
            get {
                return ResourceManager.GetString("AVI001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível excluir o registro, existem dados associados..
        /// </summary>
        internal static string AVI002 {
            get {
                return ResourceManager.GetString("AVI002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível transferir para mesma obra..
        /// </summary>
        internal static string AVI003 {
            get {
                return ResourceManager.GetString("AVI003", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível transferir com data anterior ao último registro..
        /// </summary>
        internal static string AVI004 {
            get {
                return ResourceManager.GetString("AVI004", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível desfazer trasnferência anterior à hoje..
        /// </summary>
        internal static string AVI005 {
            get {
                return ResourceManager.GetString("AVI005", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível demitir, funcionário já está demitido..
        /// </summary>
        internal static string AVI006 {
            get {
                return ResourceManager.GetString("AVI006", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível registrar férias com data anterior ao último registro..
        /// </summary>
        internal static string AVI007 {
            get {
                return ResourceManager.GetString("AVI007", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível terminar férias antes da data de início..
        /// </summary>
        internal static string AVI008 {
            get {
                return ResourceManager.GetString("AVI008", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível registrar férias com término anterior à hoje..
        /// </summary>
        internal static string AVI009 {
            get {
                return ResourceManager.GetString("AVI009", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível terminar férias com data futura..
        /// </summary>
        internal static string AVI010 {
            get {
                return ResourceManager.GetString("AVI010", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível registrar férias com início posterior à hoje..
        /// </summary>
        internal static string AVI011 {
            get {
                return ResourceManager.GetString("AVI011", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível demitir com data anterior à data de admissão..
        /// </summary>
        internal static string AVI012 {
            get {
                return ResourceManager.GetString("AVI012", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível sucatear, equipamento já está sucateado..
        /// </summary>
        internal static string AVI013 {
            get {
                return ResourceManager.GetString("AVI013", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível sucatear com data anterior à data de aquisição..
        /// </summary>
        internal static string AVI014 {
            get {
                return ResourceManager.GetString("AVI014", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível registrar manutenção com data anterior ao último registro..
        /// </summary>
        internal static string AVI015 {
            get {
                return ResourceManager.GetString("AVI015", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível registrar manutenção com início posterior à hoje..
        /// </summary>
        internal static string AVI016 {
            get {
                return ResourceManager.GetString("AVI016", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível registrar manutenção com término anterior à hoje..
        /// </summary>
        internal static string AVI017 {
            get {
                return ResourceManager.GetString("AVI017", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível terminar manutenção antes da data de início..
        /// </summary>
        internal static string AVI018 {
            get {
                return ResourceManager.GetString("AVI018", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível terminar manutenção com data futura..
        /// </summary>
        internal static string AVI019 {
            get {
                return ResourceManager.GetString("AVI019", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível cancelar destino inexistente..
        /// </summary>
        internal static string AVI020 {
            get {
                return ResourceManager.GetString("AVI020", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível desativar registro já desativado..
        /// </summary>
        internal static string AVI021 {
            get {
                return ResourceManager.GetString("AVI021", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível deixar quantidade executada maior que quantidade total..
        /// </summary>
        internal static string AVI022 {
            get {
                return ResourceManager.GetString("AVI022", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível ler dados. Formato inválido..
        /// </summary>
        internal static string AVI023 {
            get {
                return ResourceManager.GetString("AVI023", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível informar equipe negativa..
        /// </summary>
        internal static string AVI024 {
            get {
                return ResourceManager.GetString("AVI024", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não é possível reordernar com outro cronograma..
        /// </summary>
        internal static string AVI025 {
            get {
                return ResourceManager.GetString("AVI025", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Erro ao executar operação. Contate o administrador..
        /// </summary>
        internal static string ERR001 {
            get {
                return ResourceManager.GetString("ERR001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Erro ao alterar registro. Contate o administrador..
        /// </summary>
        internal static string ERR002 {
            get {
                return ResourceManager.GetString("ERR002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Erro ao incluir registro. Contate o administrador..
        /// </summary>
        internal static string ERR003 {
            get {
                return ResourceManager.GetString("ERR003", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Erro ao excluir registro. Contate o administrador..
        /// </summary>
        internal static string ERR004 {
            get {
                return ResourceManager.GetString("ERR004", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registro alterado com sucesso!.
        /// </summary>
        internal static string SUC001 {
            get {
                return ResourceManager.GetString("SUC001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registro excluído com sucesso!.
        /// </summary>
        internal static string SUC002 {
            get {
                return ResourceManager.GetString("SUC002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registro incluído com sucesso!.
        /// </summary>
        internal static string SUC003 {
            get {
                return ResourceManager.GetString("SUC003", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Operação concluída com sucesso!.
        /// </summary>
        internal static string SUC004 {
            get {
                return ResourceManager.GetString("SUC004", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registro desativado com sucesso!.
        /// </summary>
        internal static string SUC005 {
            get {
                return ResourceManager.GetString("SUC005", resourceCulture);
            }
        }
    }
}
