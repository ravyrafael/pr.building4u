using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNecessario;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarNecessario
{
    public class ListarNecessarioAprovExecutor : IExecutor<ListarNecessarioAprovRequisicao, ListarNecessarioAprovResultado>
    {
        private readonly INecessarioRepositorio necessarioRepositorio;

        public ListarNecessarioAprovExecutor(INecessarioRepositorio necessarioRepositorio)
        {
            this.necessarioRepositorio = necessarioRepositorio;
        }

        [Transacao]
        public ListarNecessarioAprovResultado Executar(ListarNecessarioAprovRequisicao requisicao)
        {
            var resultado = new ListarNecessarioAprovResultado();

            var necessarios = necessarioRepositorio.ListarNecessarioAprovPorObra(requisicao.IdObra);

            if (!necessarios.Any())
            {
                necessarioRepositorio.IncluirNecessarioAprovPorObra(requisicao.IdObra);
                necessarios = necessarioRepositorio.ListarNecessarioAprovPorObra(requisicao.IdObra);
            }

            resultado.Necessarios = Mapper.Map<List<NecessarioDto>>(necessarios) ?? new List<NecessarioDto>();

            return resultado;
        }
    }
}