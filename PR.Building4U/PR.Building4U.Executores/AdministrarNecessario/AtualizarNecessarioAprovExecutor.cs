using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNecessario;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarNecessario
{
    public class AtualizarNecessarioAprovExecutor : IExecutor<AtualizarNecessarioAprovRequisicao, AtualizarNecessarioAprovResultado>
    {
        private readonly INecessarioRepositorio necessarioRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public AtualizarNecessarioAprovExecutor(INecessarioRepositorio necessarioRepositorio, ILogRepositorio logRepositorio)
        {
            this.necessarioRepositorio = necessarioRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public AtualizarNecessarioAprovResultado Executar(AtualizarNecessarioAprovRequisicao requisicao)
        {
            var resultado = new AtualizarNecessarioAprovResultado();

            var existeNecessario = necessarioRepositorio.VerificarNecessarioAprov(requisicao.IdObra, requisicao.IdCargo);

            if (requisicao.QuantidadeNecessario <= 0)
                necessarioRepositorio.ExcluirNecessarioAprov(requisicao.IdObra, requisicao.IdCargo);
            else if (existeNecessario)
                necessarioRepositorio.AtualizarNecessarioAprov(requisicao.IdObra, requisicao.IdCargo, requisicao.QuantidadeNecessario);
            else
                necessarioRepositorio.IncluirNecessarioAprov(requisicao.IdObra, requisicao.IdCargo, requisicao.QuantidadeNecessario);

            necessarioRepositorio.CancelarNecessarioAprovPorObra(requisicao.IdObra);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Obra {requisicao.IdObra} - Cargo {requisicao.IdCargo}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}