using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNecessario;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarNecessario
{
    public class EnviarNecessarioAprovExecutor : IExecutor<EnviarNecessarioAprovRequisicao, EnviarNecessarioAprovResultado>
    {
        private readonly INecessarioRepositorio necessarioRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EnviarNecessarioAprovExecutor(INecessarioRepositorio necessarioRepositorio, ILogRepositorio logRepositorio)
        {
            this.necessarioRepositorio = necessarioRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EnviarNecessarioAprovResultado Executar(EnviarNecessarioAprovRequisicao requisicao)
        {
            var resultado = new EnviarNecessarioAprovResultado();

            necessarioRepositorio.EnviarNecessarioAprovPorObra(requisicao.IdObra);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Obra {requisicao.IdObra}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}