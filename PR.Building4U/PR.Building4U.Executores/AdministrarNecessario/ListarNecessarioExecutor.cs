using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNecessario;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using System.Collections.Generic;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarNecessario
{
    public class ListarNecessarioExecutor : IExecutor<ListarNecessarioRequisicao, ListarNecessarioResultado>
    {
        private readonly INecessarioRepositorio necessarioRepositorio;

        public ListarNecessarioExecutor(INecessarioRepositorio necessarioRepositorio)
        {
            this.necessarioRepositorio = necessarioRepositorio;
        }

        [Transacao]
        public ListarNecessarioResultado Executar(ListarNecessarioRequisicao requisicao)
        {
            var resultado = new ListarNecessarioResultado();

            var existeAprov = necessarioRepositorio.VerificarNecessarioAprovPorObra(requisicao.IdObra) > 0;

            if (existeAprov)
            {
                var necessarios = necessarioRepositorio.ListarNecessarioPorObra(requisicao.IdObra);
                resultado.Necessarios = Mapper.Map<List<NecessarioAprovDto>>(necessarios) ?? new List<NecessarioAprovDto>();
            }
            else
                resultado.Necessarios = new List<NecessarioAprovDto>();

            return resultado;
        }
    }
}