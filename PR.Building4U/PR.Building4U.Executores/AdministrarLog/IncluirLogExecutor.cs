using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarLog;
using PR.Building4U.Fronteiras.Repositorios;

namespace PR.Building4U.Executores.AdministrarLog
{
    public class IncluirLogExecutor : IExecutor<IncluirLogRequisicao, IncluirLogResultado>
    {
        private readonly ILogRepositorio logRepositorio;

        public IncluirLogExecutor(ILogRepositorio logRepositorio)
        {
            this.logRepositorio = logRepositorio;
        }

        public IncluirLogResultado Executar(IncluirLogRequisicao requisicao)
        {
            var resultado = new IncluirLogResultado();

            logRepositorio.IncluirLog(requisicao.InformacoesLog, requisicao.Mensagem, requisicao.TipoLog);
            resultado.Sucesso = true;

            return resultado;
        }
    }
}