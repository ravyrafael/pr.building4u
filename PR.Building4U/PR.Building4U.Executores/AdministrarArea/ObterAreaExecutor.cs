using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarArea;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;

namespace PR.Building4U.Executores.AdministrarArea
{
    public class ObterAreaExecutor : IExecutor<ObterAreaRequisicao, ObterAreaResultado>
    {
        private readonly IAreaRepositorio areaRepositorio;

        public ObterAreaExecutor(IAreaRepositorio areaRepositorio)
        {
            this.areaRepositorio = areaRepositorio;
        }

        [Transacao]
        public ObterAreaResultado Executar(ObterAreaRequisicao requisicao)
        {
            var resultado = new ObterAreaResultado();

            var area = areaRepositorio.ObterArea(requisicao.IdArea);
            resultado.Area = Mapper.Map<AreaDto>(area);

            return resultado;
        }
    }
}