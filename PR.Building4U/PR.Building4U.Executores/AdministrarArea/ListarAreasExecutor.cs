using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarArea;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarArea
{
    public class ListarAreasExecutor : IExecutor<ListarAreasRequisicao, ListarAreasResultado>
    {
        private readonly IAreaRepositorio areaRepositorio;

        public ListarAreasExecutor(IAreaRepositorio areaRepositorio)
        {
            this.areaRepositorio = areaRepositorio;
        }

        [Transacao]
        public ListarAreasResultado Executar(ListarAreasRequisicao requisicao)
        {
            var resultado = new ListarAreasResultado();

            var areas = areaRepositorio.ListarAreas(requisicao.CodigoOuNome);
            resultado.Areas = Mapper.Map<List<AreaDto>>(areas) ?? new List<AreaDto>();

            return resultado;
        }
    }
}