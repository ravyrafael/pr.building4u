using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarArea;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarArea
{
    public class EditarAreaExecutor : IExecutor<EditarAreaRequisicao, EditarAreaResultado>
    {
        private readonly IAreaRepositorio areaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarAreaExecutor(IAreaRepositorio areaRepositorio, ILogRepositorio logRepositorio)
        {
            this.areaRepositorio = areaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarAreaResultado Executar(EditarAreaRequisicao requisicao)
        {
            var resultado = new EditarAreaResultado();

            areaRepositorio.EditarArea(requisicao.IdArea, requisicao.NomeArea);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Area {requisicao.IdArea} - Nome {requisicao.NomeArea}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}