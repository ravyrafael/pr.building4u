using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarObra
{
    public class ExcluirObraExecutor : IExecutor<ExcluirObraRequisicao, ExcluirObraResultado>
    {
        private readonly IObraRepositorio obraRepositorio;
        private readonly IObraGerenciaRepositorio obraGerenciaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirObraExecutor(IObraRepositorio obraRepositorio, IObraGerenciaRepositorio obraGerenciaRepositorio, ILogRepositorio logRepositorio)
        {
            this.obraRepositorio = obraRepositorio;
            this.obraGerenciaRepositorio = obraGerenciaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirObraResultado Executar(ExcluirObraRequisicao requisicao)
        {
            var resultado = new ExcluirObraResultado();

            var obra = obraRepositorio.ObterObra(requisicao.IdObra);
            var obraGerencia = obraGerenciaRepositorio.ListarGerenciasPorObra(requisicao.IdObra);
            foreach (var ger in obraGerencia)
                obraGerenciaRepositorio.ExcluirObraGerencia(ger.IdObraGerencia);
            obraRepositorio.ExcluirOrdemServico(obra.OrdemServico.IdOrdemServico);
            obraRepositorio.ExcluirObra(requisicao.IdObra);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Obra {requisicao.IdObra}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}