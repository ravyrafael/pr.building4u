using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;

namespace PR.Building4U.Executores.AdministrarObra
{
    public class ObterObraExecutor : IExecutor<ObterObraRequisicao, ObterObraResultado>
    {
        private readonly IObraRepositorio obraRepositorio;

        public ObterObraExecutor(IObraRepositorio obraRepositorio)
        {
            this.obraRepositorio = obraRepositorio;
        }

        [Transacao]
        public ObterObraResultado Executar(ObterObraRequisicao requisicao)
        {
            var resultado = new ObterObraResultado();

            var obra = obraRepositorio.ObterObra(requisicao.IdObra);
            resultado.Obra = Mapper.Map<ObraDto>(obra);

            return resultado;
        }
    }
}