using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using System.Collections.Generic;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarObra
{
    public class ListarObrasExecutor : IExecutor<ListarObrasRequisicao, ListarObrasResultado>
    {
        private readonly IObraRepositorio obraRepositorio;

        public ListarObrasExecutor(IObraRepositorio obraRepositorio)
        {
            this.obraRepositorio = obraRepositorio;
        }

        [Transacao]
        public ListarObrasResultado Executar(ListarObrasRequisicao requisicao)
        {
            var resultado = new ListarObrasResultado();

            var obras = obraRepositorio.ListarObras(requisicao.CodigoOuNome, requisicao.SomenteAtivas, requisicao.SomenteObras);
            resultado.Obras = Mapper.Map<List<ObraDto>>(obras) ?? new List<ObraDto>();

            return resultado;
        }
    }
}