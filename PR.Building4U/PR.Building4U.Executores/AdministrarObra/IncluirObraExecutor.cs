using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarObra
{
    public class IncluirObraExecutor : IExecutor<IncluirObraRequisicao, IncluirObraResultado>
    {
        private readonly IObraRepositorio obraRepositorio;
        private readonly ICronogramaRepositorio cronogramaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirObraExecutor(IObraRepositorio obraRepositorio, ICronogramaRepositorio cronogramaRepositorio, ILogRepositorio logRepositorio)
        {
            this.obraRepositorio = obraRepositorio;
            this.cronogramaRepositorio = cronogramaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirObraResultado Executar(IncluirObraRequisicao requisicao)
        {
            var resultado = new IncluirObraResultado();

            resultado.IdObra = obraRepositorio.IncluirObra(requisicao.IdArea, requisicao.IdContrato, requisicao.CodigoObra, requisicao.NomeObra);
            resultado.IdOrdemServico = obraRepositorio.IncluirOrdemServico(resultado.IdObra, requisicao.DataOrdemServico, requisicao.PrazoOrdemServico);
            cronogramaRepositorio.IncluirCronograma(resultado.IdObra, 1, requisicao.NomeObra, 1);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Obra {resultado.IdObra} - Nome {requisicao.NomeObra}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}