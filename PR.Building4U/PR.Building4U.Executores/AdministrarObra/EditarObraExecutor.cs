using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarObra
{
    public class EditarObraExecutor : IExecutor<EditarObraRequisicao, EditarObraResultado>
    {
        private readonly IObraRepositorio obraRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarObraExecutor(IObraRepositorio obraRepositorio, ILogRepositorio logRepositorio)
        {
            this.obraRepositorio = obraRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarObraResultado Executar(EditarObraRequisicao requisicao)
        {
            var resultado = new EditarObraResultado();

            var obra = obraRepositorio.ObterObra(requisicao.IdObra);
            obraRepositorio.EditarObra(requisicao.IdObra, requisicao.IdArea, requisicao.IdContrato, requisicao.CodigoObra, requisicao.NomeObra);
            obraRepositorio.EditarOrdemServico(obra.OrdemServico.IdOrdemServico, requisicao.DataOrdemServico, requisicao.PrazoOrdemServico);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Obra {requisicao.IdObra} - Nome {requisicao.NomeObra}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}