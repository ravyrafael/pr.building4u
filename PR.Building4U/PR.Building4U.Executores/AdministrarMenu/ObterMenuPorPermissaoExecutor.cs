﻿using AutoMapper;
using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarMenu;
using PR.Building4U.Fronteiras.Repositorios;
using System.Collections.Generic;
using System.Linq;

namespace PR.Building4U.Executores.AdministrarMenu
{
    public class ObterMenuPorPermissaoExecutor : IExecutor<ObterMenuPorPermissaoRequisicao, ObterMenuPorPermissaoResultado>
    {
        private readonly IMenuRepositorio menuRepositorio;

        public ObterMenuPorPermissaoExecutor(IMenuRepositorio menuRepositorio)
        {
            this.menuRepositorio = menuRepositorio;
        }

        public ObterMenuPorPermissaoResultado Executar(ObterMenuPorPermissaoRequisicao requisicao)
        {
            var resultado = new ObterMenuPorPermissaoResultado();

            var menu = menuRepositorio.ObterMenuPorPermissao(requisicao.PermissaoUsuario);
            foreach (var item in menu)
                item.SubMenu = menuRepositorio.ObterSubMenuPorPermissao(requisicao.PermissaoUsuario, item.IdRecurso).ToList();

            resultado.Menu = Mapper.Map<List<RecursoDto>>(menu) ?? new List<RecursoDto>();

            return resultado;
        }
    }
}
