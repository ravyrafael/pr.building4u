﻿using AutoMapper;
using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarUsuario;
using PR.Building4U.Fronteiras.Repositorios;

namespace PR.Building4U.Executores.AdministrarUsuario
{
    public class ObterUsuarioPorCodigoExecutor : IExecutor<ObterUsuarioPorCodigoRequisicao, ObterUsuarioPorCodigoResultado>
    {
        private readonly IUsuarioRepositorio usuarioRepositorio;

        public ObterUsuarioPorCodigoExecutor(IUsuarioRepositorio usuarioRepositorio)
        {
            this.usuarioRepositorio = usuarioRepositorio;
        }

        public ObterUsuarioPorCodigoResultado Executar(ObterUsuarioPorCodigoRequisicao requisicao)
        {
            var resultado = new ObterUsuarioPorCodigoResultado();

            var usuario = usuarioRepositorio.ObterUsuarioPorLogin(requisicao.LoginUsuario);

            resultado.Usuario = Mapper.Map<UsuarioDto>(usuario);

            return resultado;
        }
    }
}
