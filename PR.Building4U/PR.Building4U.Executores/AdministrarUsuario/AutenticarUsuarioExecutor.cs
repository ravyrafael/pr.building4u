﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarUsuario;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Util;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace PR.Building4U.Executores.AdministrarUsuario
{
    public class AutenticarUsuarioExecutor : IExecutor<AutenticarUsuarioRequisicao, AutenticarUsuarioResultado>
    {
        // This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
        // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
        private const string initVector = "pemgail9uzpgzl88";
        // This constant is used to determine the keysize of the encryption algorithm
        private const int keysize = 256;

        private readonly ILogRepositorio logRepositorio;
        private readonly IUsuarioRepositorio usuarioRepositorio;

        public AutenticarUsuarioExecutor(ILogRepositorio logRepositorio, IUsuarioRepositorio usuarioRepositorio)
        {
            this.logRepositorio = logRepositorio;
            this.usuarioRepositorio = usuarioRepositorio;
        }

        public AutenticarUsuarioResultado Executar(AutenticarUsuarioRequisicao requisicao)
        {
            var resultado = new AutenticarUsuarioResultado();

            var senha = EncryptString(requisicao.SenhaUsuario, requisicao.ChaveAutenticacao);
            resultado.IdUsuario = usuarioRepositorio.AutenticarUsuario(requisicao.LoginUsuario, senha);

            requisicao.InformacoesLog = new InformacoesLog()
            {
                IdUsuario = resultado.IdUsuario.HasValue ? resultado.IdUsuario.Value : 0,
                LoginUsuario = requisicao.LoginUsuario,
                Controller = "Login",
                Action = "ValidarUsuario",
                Descricao = resultado.IdUsuario.HasValue ? "Usuário autenticado." : "Usuário não autenticado.",
                DataLog = DateTime.Now,
                TipoLog = TipoLog.Sucesso
            };

            logRepositorio.IncluirLog(requisicao.InformacoesLog);

            return resultado;
        }

        public static string EncryptString(string senha, string chave)
        {
            byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(senha);
            PasswordDeriveBytes password = new PasswordDeriveBytes(chave, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(cipherTextBytes);
        }

        //Decrypt
        public static string DecryptString(string senha, string chave)
        {
            byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
            byte[] cipherTextBytes = Convert.FromBase64String(senha);
            PasswordDeriveBytes password = new PasswordDeriveBytes(chave, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }
    }
}
