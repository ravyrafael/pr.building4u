﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarUsuario;
using PR.Building4U.Fronteiras.Repositorios;

namespace PR.Building4U.Executores.AdministrarUsuario 
{
	public class VerificarPermissaoUsuarioExecutor : IExecutor<VerificarPermissaoUsuarioRequisicao, VerificarPermissaoUsuarioResultado>
	{
        private readonly IUsuarioRepositorio usuarioRepositorio;

		public VerificarPermissaoUsuarioExecutor(IUsuarioRepositorio usuarioRepositorio)
        {
            this.usuarioRepositorio = usuarioRepositorio;
        }

		public VerificarPermissaoUsuarioResultado Executar(VerificarPermissaoUsuarioRequisicao requisicao)
        {
            var resultado = new VerificarPermissaoUsuarioResultado();

            resultado.TemPermissao = usuarioRepositorio.VerificarPermissaoUsuario(requisicao.Permissao, requisicao.Controller, requisicao.Actions) > 0;

            return resultado;
        }
	}
}