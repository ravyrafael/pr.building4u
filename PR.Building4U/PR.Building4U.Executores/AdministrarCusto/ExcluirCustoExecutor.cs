using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCusto;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarCusto
{
    public class ExcluirCustoExecutor : IExecutor<ExcluirCustoRequisicao, ExcluirCustoResultado>
    {
        private readonly ICustoRepositorio custoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirCustoExecutor(ICustoRepositorio custoRepositorio, ILogRepositorio logRepositorio)
        {
            this.custoRepositorio = custoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirCustoResultado Executar(ExcluirCustoRequisicao requisicao)
        {
            var resultado = new ExcluirCustoResultado();

            custoRepositorio.ExcluirCusto(requisicao.IdCusto);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Custo {requisicao.IdCusto}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}
