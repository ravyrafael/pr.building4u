using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCusto;
using PR.Building4U.Fronteiras.Repositorios;
using AutoMapper;
using System.Collections.Generic;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarCusto
{
    public class ListarCustosExecutor : IExecutor<ListarCustosRequisicao, ListarCustosResultado>
    {
        private readonly ICustoRepositorio custoRepositorio;

        public ListarCustosExecutor(ICustoRepositorio custoRepositorio)
        {
            this.custoRepositorio = custoRepositorio;
        }

        [Transacao]
        public ListarCustosResultado Executar(ListarCustosRequisicao requisicao)
        {
            var resultado = new ListarCustosResultado();

            var custos = custoRepositorio.ListarCustos(requisicao.CodigoOuNome);
            resultado.Custos = Mapper.Map<List<CustoDto>>(custos) ?? new List<CustoDto>();

            return resultado;
        }
    }
}
