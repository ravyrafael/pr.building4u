using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCusto;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarCusto
{
    public class EditarCustoExecutor : IExecutor<EditarCustoRequisicao, EditarCustoResultado>
    {
        private readonly ICustoRepositorio custoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarCustoExecutor(ICustoRepositorio custoRepositorio, ILogRepositorio logRepositorio)
        {
            this.custoRepositorio = custoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarCustoResultado Executar(EditarCustoRequisicao requisicao)
        {
            var resultado = new EditarCustoResultado();

            custoRepositorio.EditarCusto(requisicao.IdCusto, requisicao.NomeCusto);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Custo {requisicao.IdCusto} - Nome {requisicao.NomeCusto}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}
