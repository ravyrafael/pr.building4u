using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCusto;
using PR.Building4U.Fronteiras.Repositorios;
using AutoMapper;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarCusto
{
    public class ObterCustoExecutor : IExecutor<ObterCustoRequisicao, ObterCustoResultado>
    {
        private readonly ICustoRepositorio custoRepositorio;

        public ObterCustoExecutor(ICustoRepositorio custoRepositorio)
        {
            this.custoRepositorio = custoRepositorio;
        }

        [Transacao]
        public ObterCustoResultado Executar(ObterCustoRequisicao requisicao)
        {
            var resultado = new ObterCustoResultado();

            var custo = custoRepositorio.ObterCusto(requisicao.IdCusto);
            resultado.Custo = Mapper.Map<CustoDto>(custo);

            return resultado;
        }
    }
}
