using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCusto;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarCusto
{
    public class IncluirCustoExecutor : IExecutor<IncluirCustoRequisicao, IncluirCustoResultado>
    {
        private readonly ICustoRepositorio custoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirCustoExecutor(ICustoRepositorio custoRepositorio, ILogRepositorio logRepositorio)
        {
            this.custoRepositorio = custoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirCustoResultado Executar(IncluirCustoRequisicao requisicao)
        {
            var resultado = new IncluirCustoResultado();

            resultado.IdCusto = custoRepositorio.IncluirCusto(requisicao.SiglaCusto, requisicao.NomeCusto);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Custo {resultado.IdCusto} - Nome {requisicao.NomeCusto}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}
