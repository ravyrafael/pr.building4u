using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNecessarioEquip;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarNecessarioEquip
{
    public class ListarNecessarioEquipAprovExecutor : IExecutor<ListarNecessarioEquipAprovRequisicao, ListarNecessarioEquipAprovResultado>
    {
        private readonly INecessarioEquipRepositorio necessarioEquipRepositorio;

        public ListarNecessarioEquipAprovExecutor(INecessarioEquipRepositorio necessarioEquipRepositorio)
        {
            this.necessarioEquipRepositorio = necessarioEquipRepositorio;
        }

        [Transacao]
        public ListarNecessarioEquipAprovResultado Executar(ListarNecessarioEquipAprovRequisicao requisicao)
        {
            var resultado = new ListarNecessarioEquipAprovResultado();

            var necessarios = necessarioEquipRepositorio.ListarNecessarioAprovPorObra(requisicao.IdObra);

            if (!necessarios.Any())
            {
                necessarioEquipRepositorio.IncluirNecessarioAprovPorObra(requisicao.IdObra);
                necessarios = necessarioEquipRepositorio.ListarNecessarioAprovPorObra(requisicao.IdObra);
            }

            resultado.Necessarios = Mapper.Map<List<NecessarioEquipDto>>(necessarios) ?? new List<NecessarioEquipDto>();

            return resultado;
        }
    }
}