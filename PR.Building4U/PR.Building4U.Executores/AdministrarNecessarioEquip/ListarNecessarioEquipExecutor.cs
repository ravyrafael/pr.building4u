using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNecessarioEquip;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using System.Collections.Generic;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarNecessarioEquip
{
    public class ListarNecessarioEquipExecutor : IExecutor<ListarNecessarioEquipRequisicao, ListarNecessarioEquipResultado>
    {
        private readonly INecessarioEquipRepositorio necessarioEquipRepositorio;

        public ListarNecessarioEquipExecutor(INecessarioEquipRepositorio necessarioEquipRepositorio)
        {
            this.necessarioEquipRepositorio = necessarioEquipRepositorio;
        }

        [Transacao]
        public ListarNecessarioEquipResultado Executar(ListarNecessarioEquipRequisicao requisicao)
        {
            var resultado = new ListarNecessarioEquipResultado();

            var existeAprov = necessarioEquipRepositorio.VerificarNecessarioAprovPorObra(requisicao.IdObra) > 0;

            if (existeAprov)
            {
                var necessarios = necessarioEquipRepositorio.ListarNecessarioPorObra(requisicao.IdObra);
                resultado.Necessarios = Mapper.Map<List<NecessarioEquipAprovDto>>(necessarios) ?? new List<NecessarioEquipAprovDto>();
            }
            else
                resultado.Necessarios = new List<NecessarioEquipAprovDto>();

            return resultado;
        }
    }
}