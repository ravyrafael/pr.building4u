using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNecessarioEquip;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Util;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarNecessarioEquip
{
    public class AtualizarNecessarioEquipAprovExecutor : IExecutor<AtualizarNecessarioEquipAprovRequisicao, AtualizarNecessarioEquipAprovResultado>
    {
        private readonly INecessarioEquipRepositorio necessarioEquipRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public AtualizarNecessarioEquipAprovExecutor(INecessarioEquipRepositorio necessarioEquipRepositorio, ILogRepositorio logRepositorio)
        {
            this.necessarioEquipRepositorio = necessarioEquipRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public AtualizarNecessarioEquipAprovResultado Executar(AtualizarNecessarioEquipAprovRequisicao requisicao)
        {
            var resultado = new AtualizarNecessarioEquipAprovResultado();

            var existeNecessario = necessarioEquipRepositorio.VerificarNecessarioAprov(requisicao.IdObra, requisicao.IdTipoEquipamento);

            if (requisicao.QuantidadeNecessario <= 0)
                resultado.Sucesso = necessarioEquipRepositorio.ExcluirNecessarioAprov(requisicao.IdObra, requisicao.IdTipoEquipamento) > 0;
            else if (existeNecessario)
                resultado.Sucesso = necessarioEquipRepositorio.AtualizarNecessarioAprov(requisicao.IdObra, requisicao.IdTipoEquipamento, requisicao.QuantidadeNecessario) > 0;
            else
                resultado.Sucesso = necessarioEquipRepositorio.IncluirNecessarioAprov(requisicao.IdObra, requisicao.IdTipoEquipamento, requisicao.QuantidadeNecessario) > 0;

            necessarioEquipRepositorio.CancelarNecessarioAprovPorObra(requisicao.IdObra);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Obra {requisicao.IdObra} - Tipo {requisicao.IdTipoEquipamento}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}