using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNecessarioEquip;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Util;
using AutoMapper;
using System.Collections.Generic;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarNecessarioEquip
{
    public class ReprovarNecessarioEquipExecutor : IExecutor<ReprovarNecessarioEquipRequisicao, ReprovarNecessarioEquipResultado>
    {
        private readonly INecessarioEquipRepositorio necessarioEquipRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ReprovarNecessarioEquipExecutor(INecessarioEquipRepositorio necessarioEquipRepositorio, ILogRepositorio logRepositorio)
        {
            this.necessarioEquipRepositorio = necessarioEquipRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ReprovarNecessarioEquipResultado Executar(ReprovarNecessarioEquipRequisicao requisicao)
        {
            var resultado = new ReprovarNecessarioEquipResultado();

            necessarioEquipRepositorio.ExcluirNecessarioAprovPorObra(requisicao.IdObra);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Obra {requisicao.IdObra}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}