using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNecessarioEquip;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;


namespace PR.Building4U.Executores.AdministrarNecessarioEquip
{
    public class AprovarNecessarioEquipExecutor : IExecutor<AprovarNecessarioEquipRequisicao, AprovarNecessarioEquipResultado>
    {
        private readonly INecessarioEquipRepositorio necessarioEquipRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public AprovarNecessarioEquipExecutor(INecessarioEquipRepositorio necessarioEquipRepositorio, ILogRepositorio logRepositorio)
        {
            this.necessarioEquipRepositorio = necessarioEquipRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public AprovarNecessarioEquipResultado Executar(AprovarNecessarioEquipRequisicao requisicao)
        {
            var resultado = new AprovarNecessarioEquipResultado();

            necessarioEquipRepositorio.ExcluirNecessarioPorObra(requisicao.IdObra);
            necessarioEquipRepositorio.IncluirNecessarioPorObra(requisicao.IdObra);
            necessarioEquipRepositorio.ExcluirNecessarioAprovPorObra(requisicao.IdObra);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Obra {requisicao.IdObra}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}