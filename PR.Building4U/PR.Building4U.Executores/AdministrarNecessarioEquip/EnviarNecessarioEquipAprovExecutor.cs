using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNecessarioEquip;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarNecessarioEquip
{
    public class EnviarNecessarioEquipAprovExecutor : IExecutor<EnviarNecessarioEquipAprovRequisicao, EnviarNecessarioEquipAprovResultado>
    {
        private readonly INecessarioEquipRepositorio necessarioEquipRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EnviarNecessarioEquipAprovExecutor(INecessarioEquipRepositorio necessarioEquipRepositorio, ILogRepositorio logRepositorio)
        {
            this.necessarioEquipRepositorio = necessarioEquipRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EnviarNecessarioEquipAprovResultado Executar(EnviarNecessarioEquipAprovRequisicao requisicao)
        {
            var resultado = new EnviarNecessarioEquipAprovResultado();

            necessarioEquipRepositorio.EnviarNecessarioAprovPorObra(requisicao.IdObra);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Obra {requisicao.IdObra}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}