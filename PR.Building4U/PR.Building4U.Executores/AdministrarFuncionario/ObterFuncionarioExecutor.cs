using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarFuncionario;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarFuncionario
{
    public class ObterFuncionarioExecutor : IExecutor<ObterFuncionarioRequisicao, ObterFuncionarioResultado>
    {
        private readonly IFuncionarioRepositorio funcionarioRepositorio;
        private readonly IAlocacaoRepositorio alocacaoRepositorio;

        public ObterFuncionarioExecutor(IFuncionarioRepositorio funcionarioRepositorio, IAlocacaoRepositorio alocacaoRepositorio)
        {
            this.funcionarioRepositorio = funcionarioRepositorio;
            this.alocacaoRepositorio = alocacaoRepositorio;
        }

        [Transacao]
        public ObterFuncionarioResultado Executar(ObterFuncionarioRequisicao requisicao)
        {
            var resultado = new ObterFuncionarioResultado();

            var funcionario = funcionarioRepositorio.ObterFuncionario(requisicao.IdFuncionario);
            var alocacao = alocacaoRepositorio.ObterAlocacaoPorFuncionario(requisicao.IdFuncionario);

            resultado.Funcionario = Mapper.Map<FuncionarioDto>(funcionario);
            resultado.Funcionario.Obra = new ObraDto
            {
                IdObra = alocacao != null ? alocacao.IdObra : 0,
                CodigoObra = alocacao != null ? alocacao.CodigoObra : "",
                NomeObra = alocacao != null ? alocacao.NomeObra : ""
            };

            return resultado;
        }
    }
}