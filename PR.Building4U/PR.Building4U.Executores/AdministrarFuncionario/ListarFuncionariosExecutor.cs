using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarFuncionario;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using System.Collections.Generic;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarFuncionario
{
    public class ListarFuncionariosExecutor : IExecutor<ListarFuncionariosRequisicao, ListarFuncionariosResultado>
    {
        private readonly IFuncionarioRepositorio funcionarioRepositorio;

        public ListarFuncionariosExecutor(IFuncionarioRepositorio funcionarioRepositorio)
        {
            this.funcionarioRepositorio = funcionarioRepositorio;
        }

        [Transacao]
        public ListarFuncionariosResultado Executar(ListarFuncionariosRequisicao requisicao)
        {
            var resultado = new ListarFuncionariosResultado();

            var funcionarios = funcionarioRepositorio.ListarFuncionarios(requisicao.CodigoOuNome);
            resultado.Funcionarios = Mapper.Map<List<FuncionarioDto>>(funcionarios) ?? new List<FuncionarioDto>();

            return resultado;
        }
    }
}