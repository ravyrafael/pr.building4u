using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarFuncionario;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarFuncionario
{
    public class IncluirFuncionarioExecutor : IExecutor<IncluirFuncionarioRequisicao, IncluirFuncionarioResultado>
    {
        private readonly IFuncionarioRepositorio funcionarioRepositorio;
        private readonly IEnderecoRepositorio enderecoRepositorio;
        private readonly IAlocacaoRepositorio alocacaoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirFuncionarioExecutor(IFuncionarioRepositorio funcionarioRepositorio, IEnderecoRepositorio enderecoRepositorio, IAlocacaoRepositorio alocacaoRepositorio, ILogRepositorio logRepositorio)
        {
            this.funcionarioRepositorio = funcionarioRepositorio;
            this.enderecoRepositorio = enderecoRepositorio;
            this.alocacaoRepositorio = alocacaoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirFuncionarioResultado Executar(IncluirFuncionarioRequisicao requisicao)
        {
            var resultado = new IncluirFuncionarioResultado();

            if (!string.IsNullOrEmpty(requisicao.CepEndereco))
                requisicao.CepEndereco = requisicao.CepEndereco.Replace("-", string.Empty);
            if (!string.IsNullOrEmpty(requisicao.TelefoneFuncionario))
                requisicao.TelefoneFuncionario = requisicao.TelefoneFuncionario.Replace("-", string.Empty);

            var idEndereco = enderecoRepositorio.IncluirEndereco(requisicao.LogradouroEndereco, requisicao.NumeroEndereco, requisicao.ComplementoEndereco, requisicao.CepEndereco, requisicao.DistritoEndereco, requisicao.IdMunicipio);
            resultado.IdFuncionario = funcionarioRepositorio.IncluirFuncionario(requisicao.ChapaFuncionario, requisicao.NomeFuncionario, requisicao.AdmissaoFuncionario, requisicao.EmailFuncionario, requisicao.TelefoneFuncionario, idEndereco, requisicao.IdCargo);
            resultado.IdAlocacao = alocacaoRepositorio.IncluirAlocacao(resultado.IdFuncionario, requisicao.IdObra, 1, requisicao.AdmissaoFuncionario);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Funcionário {resultado.IdFuncionario} - Nome {requisicao.NomeFuncionario}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}