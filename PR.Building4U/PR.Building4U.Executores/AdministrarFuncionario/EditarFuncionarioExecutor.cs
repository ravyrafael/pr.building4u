using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarFuncionario;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarFuncionario
{
    public class EditarFuncionarioExecutor : IExecutor<EditarFuncionarioRequisicao, EditarFuncionarioResultado>
    {
        private readonly IFuncionarioRepositorio funcionarioRepositorio;
        private readonly IEnderecoRepositorio enderecoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarFuncionarioExecutor(IFuncionarioRepositorio funcionarioRepositorio, IEnderecoRepositorio enderecoRepositorio, ILogRepositorio logRepositorio)
        {
            this.funcionarioRepositorio = funcionarioRepositorio;
            this.enderecoRepositorio = enderecoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarFuncionarioResultado Executar(EditarFuncionarioRequisicao requisicao)
        {
            var resultado = new EditarFuncionarioResultado();

            if (!string.IsNullOrEmpty(requisicao.CepEndereco))
                requisicao.CepEndereco = requisicao.CepEndereco.Replace("-", string.Empty).Substring(0, 8);
            if (!string.IsNullOrEmpty(requisicao.TelefoneFuncionario))
                requisicao.TelefoneFuncionario = requisicao.TelefoneFuncionario.Replace("-", string.Empty).Replace(" ", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Substring(0, 11);

            enderecoRepositorio.EditarEndereco(requisicao.IdEndereco, requisicao.LogradouroEndereco, requisicao.NumeroEndereco, requisicao.ComplementoEndereco, requisicao.CepEndereco, requisicao.DistritoEndereco, requisicao.IdMunicipio);
            funcionarioRepositorio.EditarFuncionario(requisicao.IdFuncionario, requisicao.NomeFuncionario, requisicao.EmailFuncionario, requisicao.TelefoneFuncionario, requisicao.IdCargo);

            resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Funcionário {requisicao.IdFuncionario} - Nome {requisicao.NomeFuncionario}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}