using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarFuncionario;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarFuncionario
{
    public class DemitirFuncionarioExecutor : IExecutor<DemitirFuncionarioRequisicao, DemitirFuncionarioResultado>
    {
        private readonly IFuncionarioRepositorio funcionarioRepositorio;
        private readonly IAlocacaoRepositorio alocacaoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public DemitirFuncionarioExecutor(IFuncionarioRepositorio funcionarioRepositorio, IAlocacaoRepositorio alocacaoRepositorio, ILogRepositorio logRepositorio)
        {
            this.funcionarioRepositorio = funcionarioRepositorio;
            this.alocacaoRepositorio = alocacaoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public DemitirFuncionarioResultado Executar(DemitirFuncionarioRequisicao requisicao)
        {
            var resultado = new DemitirFuncionarioResultado();

            var funcionario = funcionarioRepositorio.ObterFuncionario(requisicao.IdFuncionario);

            if (funcionario.DemissaoFuncionario.HasValue)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI006);
            else if (requisicao.DemissaoFuncionario <= funcionario.AdmissaoFuncionario)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI012);
            else
            {
                var alocacao = alocacaoRepositorio.ObterAlocacaoPorFuncionario(requisicao.IdFuncionario);

                funcionarioRepositorio.DemitirFuncionario(requisicao.IdFuncionario, requisicao.DemissaoFuncionario);
                if (alocacao != null)
                    alocacaoRepositorio.FinalizarAlocacao(alocacao.IdFuncionario, alocacao.SequenciaAlocacao, requisicao.DemissaoFuncionario);

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Funcionário {requisicao.IdFuncionario} - Data {requisicao.DemissaoFuncionario:dd/MM/yyyy}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}