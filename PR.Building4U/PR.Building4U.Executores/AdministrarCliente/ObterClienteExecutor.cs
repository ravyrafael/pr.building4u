using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCliente;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;

namespace PR.Building4U.Executores.AdministrarCliente
{
    public class ObterClienteExecutor : IExecutor<ObterClienteRequisicao, ObterClienteResultado>
    {
        private readonly IClienteRepositorio clienteRepositorio;

        public ObterClienteExecutor(IClienteRepositorio clienteRepositorio)
        {
            this.clienteRepositorio = clienteRepositorio;
        }

        [Transacao]
        public ObterClienteResultado Executar(ObterClienteRequisicao requisicao)
        {
            var resultado = new ObterClienteResultado();

            var cliente = clienteRepositorio.ObterCliente(requisicao.IdCliente);
            resultado.Cliente = Mapper.Map<ClienteDto>(cliente);

            return resultado;
        }
    }
}