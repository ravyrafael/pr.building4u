using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCliente;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarCliente
{
    public class IncluirClienteExecutor : IExecutor<IncluirClienteRequisicao, IncluirClienteResultado>
    {
        private readonly IClienteRepositorio clienteRepositorio;
        private readonly IEnderecoRepositorio enderecoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirClienteExecutor(IClienteRepositorio clienteRepositorio, IEnderecoRepositorio enderecoRepositorio, ILogRepositorio logRepositorio)
        {
            this.clienteRepositorio = clienteRepositorio;
            this.enderecoRepositorio = enderecoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirClienteResultado Executar(IncluirClienteRequisicao requisicao)
        {
            var resultado = new IncluirClienteResultado();

            if (!string.IsNullOrEmpty(requisicao.CnpjCliente))
                requisicao.CnpjCliente = requisicao.CnpjCliente.Replace("-", string.Empty).Replace(".", string.Empty).Replace("/", string.Empty);
            if (!string.IsNullOrEmpty(requisicao.CepEndereco))
                requisicao.CepEndereco = requisicao.CepEndereco.Replace("-", string.Empty);

            var idEndereco = enderecoRepositorio.IncluirEndereco(requisicao.LogradouroEndereco, requisicao.NumeroEndereco, requisicao.ComplementoEndereco, requisicao.CepEndereco, requisicao.DistritoEndereco, requisicao.IdMunicipio);
            resultado.IdCliente = clienteRepositorio.IncluirCliente(requisicao.CnpjCliente, requisicao.RazaoSocialCliente, requisicao.NomeCliente, idEndereco);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Cliente {resultado.IdCliente} - Nome {requisicao.NomeCliente}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}