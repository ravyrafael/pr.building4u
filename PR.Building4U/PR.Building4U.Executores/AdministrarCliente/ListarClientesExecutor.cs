using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCliente;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarCliente
{
    public class ListarClientesExecutor : IExecutor<ListarClientesRequisicao, ListarClientesResultado>
    {
        private readonly IClienteRepositorio clienteRepositorio;

        public ListarClientesExecutor(IClienteRepositorio clienteRepositorio)
        {
            this.clienteRepositorio = clienteRepositorio;
        }

        [Transacao]
        public ListarClientesResultado Executar(ListarClientesRequisicao requisicao)
        {
            var resultado = new ListarClientesResultado();

            if (!string.IsNullOrEmpty(requisicao.CodigoOuNome))
                requisicao.CodigoOuNome = requisicao.CodigoOuNome.Replace("-", string.Empty).Replace(".", string.Empty).Replace("/", string.Empty);

            var clientes = clienteRepositorio.ListarClientes(requisicao.CodigoOuNome);
            resultado.Clientes = Mapper.Map<List<ClienteDto>>(clientes) ?? new List<ClienteDto>();

            return resultado;
        }
    }
}