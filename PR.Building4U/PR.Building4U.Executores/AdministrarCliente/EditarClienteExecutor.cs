using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCliente;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarCliente
{
    public class EditarClienteExecutor : IExecutor<EditarClienteRequisicao, EditarClienteResultado>
    {
        private readonly IClienteRepositorio clienteRepositorio;
        private readonly IEnderecoRepositorio enderecoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarClienteExecutor(IClienteRepositorio clienteRepositorio, IEnderecoRepositorio enderecoRepositorio, ILogRepositorio logRepositorio)
        {
            this.clienteRepositorio = clienteRepositorio;
            this.enderecoRepositorio = enderecoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarClienteResultado Executar(EditarClienteRequisicao requisicao)
        {
            var resultado = new EditarClienteResultado();

            if (!string.IsNullOrEmpty(requisicao.CepEndereco))
                requisicao.CepEndereco = requisicao.CepEndereco.Replace("-", string.Empty).Substring(0, 8);

            enderecoRepositorio.EditarEndereco(requisicao.IdEndereco, requisicao.LogradouroEndereco, requisicao.NumeroEndereco, requisicao.ComplementoEndereco, requisicao.CepEndereco, requisicao.DistritoEndereco, requisicao.IdMunicipio);
            clienteRepositorio.EditarCliente(requisicao.IdCliente, requisicao.RazaoSocialCliente, requisicao.NomeCliente);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Cliente {requisicao.IdCliente} - Nome {requisicao.NomeCliente}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}