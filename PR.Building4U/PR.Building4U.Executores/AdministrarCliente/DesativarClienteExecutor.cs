using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCliente;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarCliente
{
    public class DesativarClienteExecutor : IExecutor<DesativarClienteRequisicao, DesativarClienteResultado>
    {
        private readonly IClienteRepositorio clienteRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public DesativarClienteExecutor(IClienteRepositorio clienteRepositorio, ILogRepositorio logRepositorio)
        {
            this.clienteRepositorio = clienteRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public DesativarClienteResultado Executar(DesativarClienteRequisicao requisicao)
        {
            var resultado = new DesativarClienteResultado();

            var cliente = clienteRepositorio.ObterCliente(requisicao.IdCliente);

            if (cliente.Situacao.IdSituacao == 2)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI021);
            else
            {
                clienteRepositorio.DesativarCliente(requisicao.IdCliente);

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC005);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Cliente {requisicao.IdCliente} - Data {requisicao.DataDesativado:dd/MM/yyyy}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}