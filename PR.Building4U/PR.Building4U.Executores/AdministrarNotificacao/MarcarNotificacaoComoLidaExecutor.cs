using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNotificacao;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarNotificacao
{
    public class MarcarNotificacaoComoLidaExecutor : IExecutor<MarcarNotificacaoComoLidaRequisicao, MarcarNotificacaoComoLidaResultado>
    {
        private readonly INotificacaoRepositorio notificacaoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public MarcarNotificacaoComoLidaExecutor(INotificacaoRepositorio notificacaoRepositorio, ILogRepositorio logRepositorio)
        {
            this.notificacaoRepositorio = notificacaoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public MarcarNotificacaoComoLidaResultado Executar(MarcarNotificacaoComoLidaRequisicao requisicao)
        {
            var resultado = new MarcarNotificacaoComoLidaResultado();

            notificacaoRepositorio.MarcarNotificacaoComoLida(requisicao.IdNotificacao, requisicao.IdFuncionario);

            resultado.Sucesso = true;

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Notificacao {requisicao.IdNotificacao}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}