using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNotificacao;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarNotificacao
{
    public class ListarNotificacoesExecutor : IExecutor<ListarNotificacoesRequisicao, ListarNotificacoesResultado>
    {
        private readonly INotificacaoRepositorio notificacaoRepositorio;

        public ListarNotificacoesExecutor(INotificacaoRepositorio notificacaoRepositorio)
        {
            this.notificacaoRepositorio = notificacaoRepositorio;
        }

        [Transacao]
        public ListarNotificacoesResultado Executar(ListarNotificacoesRequisicao requisicao)
        {
            var resultado = new ListarNotificacoesResultado();

            var notificacoes = notificacaoRepositorio.ListarNotificacoes(requisicao.IdFuncionario);
            resultado.Notificacoes = Mapper.Map<List<NotificacaoDto>>(notificacoes) ?? new List<NotificacaoDto>();

            return resultado;
        }
    }
}