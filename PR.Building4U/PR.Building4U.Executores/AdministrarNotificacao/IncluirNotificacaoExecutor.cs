using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarNotificacao;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarNotificacao
{
    public class IncluirNotificacaoExecutor : IExecutor<IncluirNotificacaoRequisicao, IncluirNotificacaoResultado>
    {
        private readonly INotificacaoRepositorio notificacaoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirNotificacaoExecutor(INotificacaoRepositorio notificacaoRepositorio, ILogRepositorio logRepositorio)
        {
            this.notificacaoRepositorio = notificacaoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirNotificacaoResultado Executar(IncluirNotificacaoRequisicao requisicao)
        {
            var resultado = new IncluirNotificacaoResultado();

            resultado.IdNotificacao = notificacaoRepositorio.IncluirNotificacao(requisicao.IdFuncionario, requisicao.TituloNotificacao, requisicao.TextoNotificacao, requisicao.DataNotificacao, requisicao.TipoNotificacao);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Notifica��o {resultado.IdNotificacao} - T�tulo {requisicao.TituloNotificacao}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}