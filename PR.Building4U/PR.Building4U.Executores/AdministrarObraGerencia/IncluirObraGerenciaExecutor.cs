using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarObraGerencia;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarObraGerencia
{
    public class IncluirObraGerenciaExecutor : IExecutor<IncluirObraGerenciaRequisicao, IncluirObraGerenciaResultado>
    {
        private readonly IObraGerenciaRepositorio obraGerenciaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirObraGerenciaExecutor(IObraGerenciaRepositorio obraGerenciaRepositorio, ILogRepositorio logRepositorio)
        {
            this.obraGerenciaRepositorio = obraGerenciaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirObraGerenciaResultado Executar(IncluirObraGerenciaRequisicao requisicao)
        {
            var resultado = new IncluirObraGerenciaResultado();

            resultado.IdObraGerencia = obraGerenciaRepositorio.IncluirObraGerencia(requisicao.IdObra, requisicao.IdGerencia, requisicao.IdFuncionario);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"ObraGerencia {resultado.IdObraGerencia} - Obra {requisicao.IdObra} - Funcionário {requisicao.IdFuncionario}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}