using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarObraGerencia;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarObraGerencia
{
    public class ListarGerenciasPorObraExecutor : IExecutor<ListarGerenciasPorObraRequisicao, ListarGerenciasPorObraResultado>
    {
        private readonly IObraGerenciaRepositorio obraGerenciaRepositorio;

        public ListarGerenciasPorObraExecutor(IObraGerenciaRepositorio obraGerenciaRepositorio)
        {
            this.obraGerenciaRepositorio = obraGerenciaRepositorio;
        }

        [Transacao]
        public ListarGerenciasPorObraResultado Executar(ListarGerenciasPorObraRequisicao requisicao)
        {
            var resultado = new ListarGerenciasPorObraResultado();

            var obraGerencias = obraGerenciaRepositorio.ListarGerenciasPorObra(requisicao.IdObra);
            resultado.ObraGerencia = Mapper.Map<List<ObraGerenciaDto>>(obraGerencias) ?? new List<ObraGerenciaDto>();

            return resultado;
        }
    }
}