using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarObraGerencia;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarObraGerencia
{
    public class ExcluirObraGerenciaExecutor : IExecutor<ExcluirObraGerenciaRequisicao, ExcluirObraGerenciaResultado>
    {
        private readonly IObraGerenciaRepositorio obraGerenciaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirObraGerenciaExecutor(IObraGerenciaRepositorio obraGerenciaRepositorio, ILogRepositorio logRepositorio)
        {
            this.obraGerenciaRepositorio = obraGerenciaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirObraGerenciaResultado Executar(ExcluirObraGerenciaRequisicao requisicao)
        {
            var resultado = new ExcluirObraGerenciaResultado();

            obraGerenciaRepositorio.ExcluirObraGerencia(requisicao.IdObraGerencia);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"ObraGerencia {requisicao.IdObraGerencia}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}