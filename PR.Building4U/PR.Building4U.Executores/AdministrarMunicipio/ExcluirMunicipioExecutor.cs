﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarMunicipio;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarMunicipio
{
    public class ExcluirMunicipioExecutor : IExecutor<ExcluirMunicipioRequisicao, ExcluirMunicipioResultado>
    {
        private readonly IMunicipioRepositorio municipioRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirMunicipioExecutor(IMunicipioRepositorio municipioRepositorio, ILogRepositorio logRepositorio)
        {
            this.municipioRepositorio = municipioRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirMunicipioResultado Executar(ExcluirMunicipioRequisicao requisicao)
        {
            var resultado = new ExcluirMunicipioResultado();

            municipioRepositorio.ExcluirMunicipio(requisicao.IdMunicipio);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Município {requisicao.IdMunicipio}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}
