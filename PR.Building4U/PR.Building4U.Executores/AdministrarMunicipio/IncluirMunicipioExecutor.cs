﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarMunicipio;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarMunicipio
{
    public class IncluirMunicipioExecutor : IExecutor<IncluirMunicipioRequisicao, IncluirMunicipioResultado>
    {
        private readonly IMunicipioRepositorio municipioRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirMunicipioExecutor(IMunicipioRepositorio municipioRepositorio, ILogRepositorio logRepositorio)
        {
            this.municipioRepositorio = municipioRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirMunicipioResultado Executar(IncluirMunicipioRequisicao requisicao)
        {
            var resultado = new IncluirMunicipioResultado();

            resultado.IdMunicipio = municipioRepositorio.IncluirMunicipio(requisicao.CodigoMunicipio, requisicao.NomeMunicipio, requisicao.IdEstado);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Município {resultado.IdMunicipio} - Nome {requisicao.NomeMunicipio}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}
