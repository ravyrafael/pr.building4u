﻿using AutoMapper;
using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarMunicipio;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarMunicipio
{
    public class ListarMunicipiosExecutor : IExecutor<ListarMunicipiosRequisicao, ListarMunicipiosResultado>
    {
        private readonly IMunicipioRepositorio municipioRepositorio;

        public ListarMunicipiosExecutor(IMunicipioRepositorio municipioRepositorio)
        {
            this.municipioRepositorio = municipioRepositorio;
        }

        [Transacao]
        public ListarMunicipiosResultado Executar(ListarMunicipiosRequisicao requisicao)
        {
            var resultado = new ListarMunicipiosResultado();

            var municipios = municipioRepositorio.ListarMunicipios(requisicao.CodigoOuNome);
            resultado.Municipios = Mapper.Map<List<MunicipioDto>>(municipios);

            return resultado;
        }
    }
}
