using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarMunicipio;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using System.Collections.Generic;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarMunicipio
{
    public class ListarMunicipiosPorEstadoExecutor : IExecutor<ListarMunicipiosPorEstadoRequisicao, ListarMunicipiosPorEstadoResultado>
    {
        private readonly IMunicipioRepositorio municipioRepositorio;

        public ListarMunicipiosPorEstadoExecutor(IMunicipioRepositorio municipioRepositorio)
        {
            this.municipioRepositorio = municipioRepositorio;
        }

        [Transacao]
        public ListarMunicipiosPorEstadoResultado Executar(ListarMunicipiosPorEstadoRequisicao requisicao)
        {
            var resultado = new ListarMunicipiosPorEstadoResultado();

            var municipios = municipioRepositorio.ListarMunicipiosPorEstado(requisicao.IdEstado, requisicao.CodigoOuNome);
            resultado.Municipios = Mapper.Map<List<MunicipioDto>>(municipios) ?? new List<MunicipioDto>();

            return resultado;
        }
    }
}