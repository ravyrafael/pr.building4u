﻿using AutoMapper;
using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarMunicipio;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarMunicipio 
{
	public class ObterMunicipioExecutor : IExecutor<ObterMunicipioRequisicao, ObterMunicipioResultado>
	{
        private readonly IMunicipioRepositorio municipioRepositorio;

        public ObterMunicipioExecutor(IMunicipioRepositorio municipioRepositorio)
        {
            this.municipioRepositorio = municipioRepositorio;
        }

        [Transacao]
        public ObterMunicipioResultado Executar(ObterMunicipioRequisicao requisicao)
        {
            var resultado = new ObterMunicipioResultado();

            var estado = municipioRepositorio.ObterMunicipio(requisicao.IdMunicipio);
            resultado.Municipio = Mapper.Map<MunicipioDto>(estado);

            return resultado;
        }
	}
}
