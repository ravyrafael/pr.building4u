using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarContrato;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarContrato
{
    public class EditarContratoExecutor : IExecutor<EditarContratoRequisicao, EditarContratoResultado>
    {
        private readonly IContratoRepositorio contratoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarContratoExecutor(IContratoRepositorio contratoRepositorio, ILogRepositorio logRepositorio)
        {
            this.contratoRepositorio = contratoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarContratoResultado Executar(EditarContratoRequisicao requisicao)
        {
            var resultado = new EditarContratoResultado();

            contratoRepositorio.EditarContrato(requisicao.IdContrato, requisicao.EscopoContrato);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Contrato {requisicao.IdContrato}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}