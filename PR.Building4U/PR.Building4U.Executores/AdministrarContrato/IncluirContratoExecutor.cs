using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarContrato;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarContrato
{
    public class IncluirContratoExecutor : IExecutor<IncluirContratoRequisicao, IncluirContratoResultado>
    {
        private readonly IContratoRepositorio contratoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirContratoExecutor(IContratoRepositorio contratoRepositorio, ILogRepositorio logRepositorio)
        {
            this.contratoRepositorio = contratoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirContratoResultado Executar(IncluirContratoRequisicao requisicao)
        {
            var resultado = new IncluirContratoResultado();

            resultado.IdContrato = contratoRepositorio.IncluirContrato(requisicao.IdCliente, requisicao.CodigoContrato, requisicao.DataContrato, requisicao.ValorContrato, requisicao.PrazoContrato, requisicao.EscopoContrato);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Contrato {resultado.IdContrato} - C�digo {requisicao.CodigoContrato}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}