using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarContrato;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarContrato
{
    public class ListarContratosExecutor : IExecutor<ListarContratosRequisicao, ListarContratosResultado>
    {
        private readonly IContratoRepositorio contratoRepositorio;

        public ListarContratosExecutor(IContratoRepositorio contratoRepositorio)
        {
            this.contratoRepositorio = contratoRepositorio;
        }

        [Transacao]
        public ListarContratosResultado Executar(ListarContratosRequisicao requisicao)
        {
            var resultado = new ListarContratosResultado();

            var contratos = contratoRepositorio.ListarContratos(requisicao.CodigoOuNome);
            resultado.Contratos = Mapper.Map<List<ContratoDto>>(contratos) ?? new List<ContratoDto>();

            return resultado;
        }
    }
}