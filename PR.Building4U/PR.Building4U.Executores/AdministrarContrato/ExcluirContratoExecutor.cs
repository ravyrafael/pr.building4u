using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarContrato;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarContrato
{
    public class ExcluirContratoExecutor : IExecutor<ExcluirContratoRequisicao, ExcluirContratoResultado>
    {
        private readonly IContratoRepositorio contratoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirContratoExecutor(IContratoRepositorio contratoRepositorio, ILogRepositorio logRepositorio)
        {
            this.contratoRepositorio = contratoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirContratoResultado Executar(ExcluirContratoRequisicao requisicao)
        {
            var resultado = new ExcluirContratoResultado();

            contratoRepositorio.ExcluirContrato(requisicao.IdContrato);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Contrato {requisicao.IdContrato}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}