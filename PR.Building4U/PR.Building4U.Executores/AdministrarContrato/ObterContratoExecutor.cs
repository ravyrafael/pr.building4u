using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarContrato;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;

namespace PR.Building4U.Executores.AdministrarContrato
{
    public class ObterContratoExecutor : IExecutor<ObterContratoRequisicao, ObterContratoResultado>
    {
        private readonly IContratoRepositorio contratoRepositorio;

        public ObterContratoExecutor(IContratoRepositorio contratoRepositorio)
        {
            this.contratoRepositorio = contratoRepositorio;
        }

        [Transacao]
        public ObterContratoResultado Executar(ObterContratoRequisicao requisicao)
        {
            var resultado = new ObterContratoResultado();

            var contrato = contratoRepositorio.ObterContrato(requisicao.IdContrato);
            resultado.Contrato = Mapper.Map<ContratoDto>(contrato);

            return resultado;
        }
    }
}