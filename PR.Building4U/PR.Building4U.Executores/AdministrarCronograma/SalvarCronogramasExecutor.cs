using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCronograma;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarCronograma
{
    public class SalvarCronogramasExecutor : IExecutor<SalvarCronogramasRequisicao, SalvarCronogramasResultado>
    {
        private readonly ICronogramaRepositorio cronogramaRepositorio;
        private readonly IProducaoRepositorio producaoRepositorio;
        private readonly IPlanilhaRepositorio planilhaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public SalvarCronogramasExecutor(ICronogramaRepositorio cronogramaRepositorio, IProducaoRepositorio producaoRepositorio, IPlanilhaRepositorio planilhaRepositorio, ILogRepositorio logRepositorio)
        {
            this.cronogramaRepositorio = cronogramaRepositorio;
            this.producaoRepositorio = producaoRepositorio;
            this.planilhaRepositorio = planilhaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public SalvarCronogramasResultado Executar(SalvarCronogramasRequisicao requisicao)
        {
            var resultado = new SalvarCronogramasResultado();

            foreach (var cronograma in requisicao.Cronogramas)
            {
                cronogramaRepositorio.AtualizarCronograma(cronograma.IdCronograma, cronograma.OrdemCronograma,
                    cronograma.InicioCronograma, cronograma.TerminoCronograma, cronograma.AtualizacaoCronograma);

                var idsServicos = new Dictionary<int, int>();

                foreach (var servico in cronograma.Servicos)
                {
                    if (servico.IdCronogramaServico > 0)
                        cronogramaRepositorio.AtualizarServico(servico.IdCronogramaServico, servico.OrdemServico,
                            servico.NomeServico, servico.QuantidadeTotal, servico.QuantidadeExecutada,
                            servico.QuantidadeReal, servico.MediaEquipe, servico.MediaPes, servico.MediaPesReal,
                            servico.ValorServico, servico.DuracaoTotal, servico.DuracaoFalta, servico.InicioServico,
                            servico.TerminoServico, servico.AtualizacaoServico, servico.IdCronogramaPredecessora,
                            servico.IdServicoPredecessora, servico.TipoPredecessora, servico.AtrasoPredecessora,
                            servico.ChanceFaturamento);
                    else
                    {
                        var id = cronogramaRepositorio.IncluirServico(servico.IdCronograma, servico.Servico.IdServico,
                            servico.OrdemServico, servico.NomeServico, servico.QuantidadeTotal, servico.QuantidadeExecutada,
                            servico.QuantidadeReal, servico.MediaEquipe, servico.MediaPes, servico.MediaPesReal,
                            servico.ValorServico, servico.DuracaoTotal, servico.DuracaoFalta, servico.InicioServico,
                            servico.TerminoServico, servico.AtualizacaoServico, servico.IdCronogramaPredecessora,
                            servico.IdServicoPredecessora, servico.TipoPredecessora, servico.AtrasoPredecessora,
                            servico.ChanceFaturamento);

                        idsServicos.Add(servico.IdCronogramaServico, id);
                    }

                    if (servico.MedicoesRemovidas != null)
                        foreach (var idMedicao in servico.MedicoesRemovidas)
                            cronogramaRepositorio.ExcluirMedicao(idMedicao);

                    foreach (var medicao in servico.Medicoes)
                    {
                        if (medicao.IdCronogramaServicoMedicao > 0)
                            cronogramaRepositorio.AtualizarMedicao(medicao.IdCronogramaServicoMedicao,
                                medicao.InicioMedicao, medicao.TerminoMedicao, medicao.Faturamento,
                                medicao.Quantidade, medicao.PesMedicao, medicao.EquipeMedicao, medicao.TemMedicao);
                        else
                            cronogramaRepositorio.IncluirMedicao(medicao.IdCronogramaServico, medicao.MesReferencia,
                                medicao.InicioMedicao, medicao.TerminoMedicao, medicao.Faturamento,
                                medicao.Quantidade, medicao.PesMedicao, medicao.EquipeMedicao, medicao.TemMedicao);
                    }

                    if (servico.ProducoesRemovidas != null)
                        foreach (var idProducao in servico.ProducoesRemovidas)
                            producaoRepositorio.ExcluirProducao(idProducao);

                    foreach (var producao in servico.Producoes)
                    {
                        if (producao.IdProducao > 0)
                            producaoRepositorio.AtualizarProducao(producao.IdProducao, producao.QuantidadeProducao,
                                producao.EquipeProducao, producao.ObservacaoProducao);
                        else
                            producaoRepositorio.IncluirProducao(producao.IdCronogramaServico, producao.DataProducao,
                                producao.QuantidadeProducao, producao.EquipeProducao, producao.ObservacaoProducao);
                    }
                }

                foreach (var planilha in cronograma.Planilhas)
                {
                    foreach (var item in planilha.Itens)
                    {
                        if (item.IdCronogramaServico.HasValue)
                        {
                            var idCronogramaServico = idsServicos.ContainsKey(item.IdCronogramaServico.Value) ? idsServicos[item.IdCronogramaServico.Value] : item.IdCronogramaServico;
                            planilhaRepositorio.AtualizarItem(item.IdPlanilhaItem, item.QuantidadeAcertoItem, item.EhItemControle, idCronogramaServico);
                        }
                    }
                }

                if (cronograma.ServicosRemovidos != null)
                {
                    foreach (var idServico in cronograma.ServicosRemovidos)
                    {
                        cronogramaRepositorio.ExcluirServicoMedicao(idServico);
                        cronogramaRepositorio.ExcluirServicoProducao(idServico);
                        cronogramaRepositorio.ExcluirServico(idServico);
                    }
                }

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Cronograma {cronograma.IdCronograma} - Nome {cronograma.NomeCronograma}.", TipoLog.Sucesso);
            }

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

            return resultado;
        }
    }
}