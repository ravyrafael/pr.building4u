using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCronograma;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarCronograma
{
    public class ListarCronogramasExecutor : IExecutor<ListarCronogramasRequisicao, ListarCronogramasResultado>
    {
        private readonly ICronogramaRepositorio cronogramaRepositorio;

        public ListarCronogramasExecutor(ICronogramaRepositorio cronogramaRepositorio)
        {
            this.cronogramaRepositorio = cronogramaRepositorio;
        }

        [Transacao]
        public ListarCronogramasResultado Executar(ListarCronogramasRequisicao requisicao)
        {
            var resultado = new ListarCronogramasResultado();

            var cronogramas = cronogramaRepositorio.ListarCronogramas(requisicao.IdObra);
            resultado.Cronogramas = Mapper.Map<List<CronogramaDto>>(cronogramas) ?? new List<CronogramaDto>();

            return resultado;
        }
    }
}