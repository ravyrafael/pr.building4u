using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCronograma;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarCronograma
{
    public class IncluirCronogramaExecutor : IExecutor<IncluirCronogramaRequisicao, IncluirCronogramaResultado>
    {
        private readonly ICronogramaRepositorio cronogramaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirCronogramaExecutor(ICronogramaRepositorio cronogramaRepositorio, ILogRepositorio logRepositorio)
        {
            this.cronogramaRepositorio = cronogramaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirCronogramaResultado Executar(IncluirCronogramaRequisicao requisicao)
        {
            var resultado = new IncluirCronogramaResultado();

            resultado.IdCronograma = cronogramaRepositorio.IncluirCronograma(requisicao.IdObra, requisicao.OrdemCronograma, requisicao.NomeCronograma, requisicao.DiaMedicaoCronograma);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Cronograma {resultado.IdCronograma} - Nome {requisicao.NomeCronograma}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}