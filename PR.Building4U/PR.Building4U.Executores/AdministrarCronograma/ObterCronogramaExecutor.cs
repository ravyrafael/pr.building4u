using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCronograma;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;

namespace PR.Building4U.Executores.AdministrarCronograma
{
    public class ObterCronogramaExecutor : IExecutor<ObterCronogramaRequisicao, ObterCronogramaResultado>
    {
        private readonly ICronogramaRepositorio cronogramaRepositorio;

        public ObterCronogramaExecutor(ICronogramaRepositorio cronogramaRepositorio)
        {
            this.cronogramaRepositorio = cronogramaRepositorio;
        }

        [Transacao]
        public ObterCronogramaResultado Executar(ObterCronogramaRequisicao requisicao)
        {
            var resultado = new ObterCronogramaResultado();

            var cronograma = cronogramaRepositorio.ObterCronograma(requisicao.IdCronograma);
            resultado.Cronograma = Mapper.Map<CronogramaDto>(cronograma);

            return resultado;
        }
    }
}