using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCronograma;
using PR.Building4U.Fronteiras.Dtos;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
using System;
using PR.Building4U.Util;
using System.Collections.Generic;
using PR.Building4U.Fronteiras.Repositorios;
using AutoMapper;

namespace PR.Building4U.Executores.AdministrarCronograma
{
    public class ModificarCronogramaExecutor : IExecutor<ModificarCronogramaRequisicao, ModificarCronogramaResultado>
    {
        private readonly IServicoRepositorio servicoRepositorio;

        public ModificarCronogramaExecutor(IServicoRepositorio servicoRepositorio)
        {
            this.servicoRepositorio = servicoRepositorio;
        }

        public ModificarCronogramaResultado Executar(ModificarCronogramaRequisicao requisicao)
        {
            var resultado = new ModificarCronogramaResultado();

            try
            {
                var cronogramas = requisicao.Cronogramas;
                var cronograma = cronogramas.FirstOrDefault(c => c.IdCronograma == int.Parse(requisicao.IdCronograma));

                if (!string.IsNullOrEmpty(requisicao.IdPlanilha))
                {
                    var planilha = cronograma.Planilhas.FirstOrDefault(p => p.IdPlanilha == int.Parse(requisicao.IdPlanilha));
                    var itemPlanilha = planilha.Itens.FirstOrDefault(i => i.IdPlanilhaItem == int.Parse(requisicao.IdItemPlanilha));
                    ModificarPlanilha(ref cronograma, ref itemPlanilha, requisicao.NomePropriedade, requisicao.ValorPropriedade);
                }
                else if (!string.IsNullOrEmpty(requisicao.IdCronogramaServico))
                {
                    var servico = cronograma.Servicos.FirstOrDefault(s => s.IdCronogramaServico == int.Parse(requisicao.IdCronogramaServico));

                    if (!string.IsNullOrEmpty(requisicao.MesReferencia))
                    {
                        var medicao = servico.Medicoes.FirstOrDefault(m => m.MesReferencia == DateTime.Parse(requisicao.MesReferencia));
                        ModificarMedicao(ref servico, ref medicao, requisicao.MesReferencia, cronograma.DiaMedicaoCronograma, requisicao.NomePropriedade, requisicao.ValorPropriedade);
                    }
                    else if (!string.IsNullOrEmpty(requisicao.DataProducao))
                    {
                        var producao = servico.Producoes.FirstOrDefault(p => p.DataProducao == DateTime.Parse(requisicao.DataProducao));
                        ModificarProducao(ref servico, ref producao, requisicao.DataProducao, requisicao.NomePropriedade, requisicao.ValorPropriedade);
                    }
                    else
                        ModificarServico(ref cronogramas, ref cronograma, ref servico, requisicao.NomePropriedade, requisicao.ValorPropriedade, !requisicao.IdCronogramaJson.HasValue);
                }
                else
                    ModificarCronograma(ref cronogramas, ref cronograma, requisicao.NomePropriedade, requisicao.ValorPropriedade);

                resultado.Sucesso = true;
                resultado.Cronogramas = cronogramas;

                return resultado;
            }
            catch (Exception ex)
            {
                if (ex.GetType().IsAssignableFrom(typeof(InvalidCastException)) || ex.GetType().IsAssignableFrom(typeof(FormatException)))
                {
                    resultado.Sucesso = false;
                    resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI023);

                    return resultado;
                }
                else if (ex.GetType().IsAssignableFrom(typeof(WarningException)))
                {
                    resultado.Sucesso = false;
                    resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, ex.Message);

                    return resultado;
                }
                else
                    throw;
            }
        }

        private void ModificarCronograma(ref List<CronogramaDto> cronogramas, ref CronogramaDto cronograma, string nomePropriedade, string valorPropriedade)
        {
            if (nomePropriedade == "OrdemCronograma")
            {
                var campoOrdem = cronogramas.FirstOrDefault(x => x.OrdemCronograma == int.Parse(valorPropriedade));
                if (campoOrdem != null)
                    campoOrdem.OrdemCronograma = cronograma.OrdemCronograma;
            }

            var oProp = cronograma.GetType().GetProperty(nomePropriedade, BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty);
            var tProp = Nullable.GetUnderlyingType(oProp.PropertyType) != null ? new NullableConverter(oProp.PropertyType).UnderlyingType : oProp.PropertyType;
            var value = valorPropriedade != null ? Convert.ChangeType(valorPropriedade, tProp) : null;
            if (oProp.CanWrite)
                oProp.SetValue(cronograma, value, null);
        }

        private void ModificarServico(ref List<CronogramaDto> cronogramas, ref CronogramaDto cronograma, ref CronogramaServicoDto servico, string nomePropriedade, string valorPropriedade, bool visaoMultipla)
        {
            if (nomePropriedade == "OrdemServico")
            {
                var campoOrdem = (CronogramaServicoDto)null;

                if (!string.IsNullOrEmpty(valorPropriedade))
                {
                    if (!valorPropriedade.Contains("."))
                        campoOrdem = cronograma.Servicos.FirstOrDefault(x => x.OrdemServico == int.Parse(valorPropriedade));
                    else if (visaoMultipla)
                    {
                        if (cronograma.OrdemCronograma == int.Parse(valorPropriedade.Split('.')[0]))
                            campoOrdem = cronograma.Servicos.FirstOrDefault(x => x.OrdemServico == int.Parse(valorPropriedade.Split('.')[1]));
                        else
                            throw new WarningException(Mensagens.AVI025);

                        valorPropriedade = valorPropriedade.Split('.')[1];
                    }
                }

                if (campoOrdem != null)
                    campoOrdem.OrdemServico = servico.OrdemServico;
            }
            else if (nomePropriedade == "NomeServico")
            {
                if (valorPropriedade.IndexOf(servico.Servico.SiglaServico) == 0)
                    valorPropriedade = valorPropriedade.Substring(servico.Servico.SiglaServico.Length);
            }
            else if (nomePropriedade == "QuantidadeReal")
            {
                if ((string.IsNullOrEmpty(valorPropriedade) ? 0 : decimal.Parse(valorPropriedade)) < servico.QuantidadeExecutada)
                    throw new WarningException(Mensagens.AVI022);
            }
            else if (nomePropriedade == "IdServicoPredecessora")
            {
                var nomePropriedade2 = "IdCronogramaPredecessora";
                var valorPropriedade2 = (string)null;

                if (!string.IsNullOrEmpty(valorPropriedade))
                {
                    var servico2 = (CronogramaServicoDto)null;

                    if (!valorPropriedade.Contains("."))
                        servico2 = cronograma.Servicos.FirstOrDefault(x => x.OrdemServico == int.Parse(valorPropriedade));
                    else if (visaoMultipla)
                    {
                        var cronograma2 = cronogramas.FirstOrDefault(c => c.OrdemCronograma == int.Parse(valorPropriedade.Split('.')[0]));
                        servico2 = cronograma2?.Servicos.FirstOrDefault(s => s.OrdemServico == int.Parse(valorPropriedade.Split('.')[1]));
                    }

                    valorPropriedade = servico2?.IdCronogramaServico.ToString();
                    valorPropriedade2 = servico2?.IdCronograma.ToString();
                }

                if (valorPropriedade2 == cronograma.IdCronograma.ToString() && valorPropriedade == servico.IdCronogramaServico.ToString())
                {
                    valorPropriedade = null;
                    valorPropriedade2 = null;
                }

                ModificarServico(ref cronogramas, ref cronograma, ref servico, nomePropriedade2, valorPropriedade2, visaoMultipla);
            }
            else if (nomePropriedade == "TipoPredecessora")
            {
                if (valorPropriedade.ToUpper() != "I" && valorPropriedade.ToUpper() != "T")
                    throw new FormatException();
                else
                    valorPropriedade = valorPropriedade.ToUpper();
            }

            var oProp = servico.GetType().GetProperty(nomePropriedade, BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty);
            var tProp = Nullable.GetUnderlyingType(oProp.PropertyType) != null ? new NullableConverter(oProp.PropertyType).UnderlyingType : oProp.PropertyType;
            var value = valorPropriedade != null ? Convert.ChangeType(valorPropriedade, tProp) : null;
            if (oProp.CanWrite)
                oProp.SetValue(servico, value, null);
        }

        private void ModificarMedicao(ref CronogramaServicoDto servico, ref CronogramaServicoMedicaoDto medicao, string mesReferencia, int diaMedicaoCronograma, string nomePropriedade, string valorPropriedade)
        {
            if (medicao == null)
            {
                servico.Medicoes.Add(AdicionarMedicaoServico(servico.IdCronogramaServico, DateTime.Parse(mesReferencia), diaMedicaoCronograma));
                medicao = servico.Medicoes.FirstOrDefault(m => m.MesReferencia == DateTime.Parse(mesReferencia));
            }
            var oProp = medicao.GetType().GetProperty(nomePropriedade, BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty);
            var tProp = Nullable.GetUnderlyingType(oProp.PropertyType) != null ? new NullableConverter(oProp.PropertyType).UnderlyingType : oProp.PropertyType;
            var value = valorPropriedade != null ? Convert.ChangeType(valorPropriedade, tProp) : null;
            if (oProp.CanWrite)
                oProp.SetValue(medicao, value, null);
        }

        private void ModificarProducao(ref CronogramaServicoDto servico, ref ProducaoDto producao, string dataProducao, string nomePropriedade, string valorPropriedade)
        {
            if (nomePropriedade == "QuantidadeProducao")
            {
                var quantExec = servico.QuantidadeExecutada + (string.IsNullOrEmpty(valorPropriedade) ? 0 : decimal.Parse(valorPropriedade));
                if (quantExec > servico.QuantidadeTotal)
                    throw new WarningException(Mensagens.AVI022);
            }
            else if (nomePropriedade == "EquipeProducao" && (string.IsNullOrEmpty(valorPropriedade) ? 0 : decimal.Parse(valorPropriedade)) < 0)
            {
                throw new WarningException(Mensagens.AVI024);
            }

            if (producao == null)
            {
                servico.Producoes.Add(AdicionarProducaoServico(servico.IdCronogramaServico, DateTime.Parse(dataProducao)));
                producao = servico.Producoes.FirstOrDefault(p => p.DataProducao == DateTime.Parse(dataProducao));
            }
            var oProp = producao.GetType().GetProperty(nomePropriedade, BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty);
            var tProp = Nullable.GetUnderlyingType(oProp.PropertyType) != null ? new NullableConverter(oProp.PropertyType).UnderlyingType : oProp.PropertyType;
            var value = valorPropriedade != null ? Convert.ChangeType(valorPropriedade, tProp) : null;
            if (oProp.CanWrite)
                oProp.SetValue(producao, value, null);
        }

        private void ModificarPlanilha(ref CronogramaDto cronograma, ref PlanilhaItemDto itemPlanilha, string nomePropriedade, string valorPropriedade)
        {
            if (nomePropriedade == "EhItemControle")
                valorPropriedade = (!string.IsNullOrEmpty(valorPropriedade) && valorPropriedade.ToUpper() == "X").ToString();
            else if (nomePropriedade == "IdCronogramaServico" && valorPropriedade.Contains("+"))
            {
                var idServico = valorPropriedade.Substring(1);
                var idCronogramaServico = cronograma.Servicos.Min(s => s.IdCronogramaServico) < 0 ?
                    cronograma.Servicos.Min(s => s.IdCronogramaServico) - 1 : -1;
                var ordemServico = cronograma.Servicos.Max(s => s.OrdemServico) + 1;

                cronograma.Servicos.Add(AdicionarServico(idServico, cronograma.IdCronograma, idCronogramaServico, ordemServico));

                valorPropriedade = idCronogramaServico.ToString();
            }

            var oProp = itemPlanilha.GetType().GetProperty(nomePropriedade, BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty);
            var tProp = Nullable.GetUnderlyingType(oProp.PropertyType) != null ? new NullableConverter(oProp.PropertyType).UnderlyingType : oProp.PropertyType;
            var value = valorPropriedade != null ? Convert.ChangeType(valorPropriedade, tProp) : null;
            if (oProp.CanWrite)
                oProp.SetValue(itemPlanilha, value, null);
        }

        private CronogramaServicoMedicaoDto AdicionarMedicaoServico(int idCronogramaServico, DateTime mesReferencia, int diaMedicao)
        {
            return new CronogramaServicoMedicaoDto
            {
                IdCronogramaServicoMedicao = 0,
                IdCronogramaServico = idCronogramaServico,
                MesReferencia = mesReferencia,
                InicioMedicao = new DateTime(mesReferencia.Year, mesReferencia.Month, diaMedicao).AddMonths(diaMedicao > 15 ? -1 : 0),
                TerminoMedicao = new DateTime(mesReferencia.Year, mesReferencia.Month, diaMedicao).AddMonths(diaMedicao > 15 ? -1 : 0).AddMonths(1).AddDays(-1),
                TemMedicao = false
            };
        }

        private ProducaoDto AdicionarProducaoServico(int idCronogramaServico, DateTime dataProducao)
        {
            return new ProducaoDto
            {
                IdProducao = 0,
                IdCronogramaServico = idCronogramaServico,
                DataProducao = dataProducao
            };
        }

        private CronogramaServicoDto AdicionarServico(string idServico, int idCronograma, int idCronogramaServico, int ordemServico)
        {
            var servico = servicoRepositorio.ObterServico(int.Parse(idServico), null);
            return new CronogramaServicoDto
            {
                IdCronogramaServico = idCronogramaServico,
                IdCronograma = idCronograma,
                OrdemServico = ordemServico,
                Servico = Mapper.Map<ServicoDto>(servico),
                Producoes = new List<ProducaoDto>()
            };
        }
    }
}