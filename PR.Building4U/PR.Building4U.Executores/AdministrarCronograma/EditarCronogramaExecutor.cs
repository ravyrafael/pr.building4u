using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCronograma;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarCronograma
{
    public class EditarCronogramaExecutor : IExecutor<EditarCronogramaRequisicao, EditarCronogramaResultado>
    {
        private readonly ICronogramaRepositorio cronogramaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarCronogramaExecutor(ICronogramaRepositorio cronogramaRepositorio, ILogRepositorio logRepositorio)
        {
            this.cronogramaRepositorio = cronogramaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarCronogramaResultado Executar(EditarCronogramaRequisicao requisicao)
        {
            var resultado = new EditarCronogramaResultado();

            cronogramaRepositorio.EditarCronograma(requisicao.IdCronograma, requisicao.NomeCronograma, requisicao.DiaMedicaoCronograma);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Cronograma {requisicao.IdCronograma} - Nome {requisicao.NomeCronograma}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}