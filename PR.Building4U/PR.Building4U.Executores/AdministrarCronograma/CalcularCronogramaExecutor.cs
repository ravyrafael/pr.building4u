using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCronograma;
using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;
using System.Linq;
using System;
using PR.Building4U.Fronteiras.Repositorios;

namespace PR.Building4U.Executores.AdministrarCronograma
{
    public class CalcularCronogramaExecutor : IExecutor<CalcularCronogramaRequisicao, CalcularCronogramaResultado>
    {
        private readonly IPlanilhaRepositorio planilhaRepositorio;

        public CalcularCronogramaExecutor(IPlanilhaRepositorio planilhaRepositorio)
        {
            this.planilhaRepositorio = planilhaRepositorio;
        }

        public CalcularCronogramaResultado Executar(CalcularCronogramaRequisicao requisicao)
        {
            var resultado = new CalcularCronogramaResultado();

            requisicao.Cronogramas = requisicao.Cronogramas.OrderBy(c => c.OrdemCronograma).ToList();

            var cronograma = requisicao.Cronogramas.FirstOrDefault(c => c.IdCronograma == requisicao.IdCronograma);

            // Itera pelos servi�os do cronograma
            for (var i = 0; i < cronograma.Servicos.Count; i++)
            {
                var servico = cronograma.Servicos[i];

                // Calcula dados do servi�o
                CalcularDadosServico(cronograma, ref servico);
                // Calcula data de in�cio do servi�o
                CalcularInicioServico(ref servico, requisicao.Cronogramas);
                // Calcula data de t�rmino do servi�o
                CalcularTerminoServico(ref servico, cronograma.DiaMedicaoCronograma);
                // Calcula dura��o do servi�o
                CalcularDuracaoServico(ref servico);
            }

            // Calcula datas gerais do cronograma
            cronograma.InicioCronograma = cronograma.Servicos.Where(s => s.InicioServico.HasValue).Min(x => x.InicioServico);
            cronograma.TerminoCronograma = cronograma.Servicos.Where(s => s.TerminoServico.HasValue).Max(x => x.TerminoServico) ?? cronograma.InicioCronograma;
            cronograma.AtualizacaoCronograma = cronograma.Servicos.Where(s => s.QuantidadeExecutada > 0 && s.AtualizacaoServico.HasValue).Max(s => s.AtualizacaoServico) ?? DateTime.Now.Date;

            // Se o cronograma possuir data de in�cio e data de t�rmino
            if (cronograma.InicioCronograma.HasValue && cronograma.TerminoCronograma.HasValue)
            {
                // Itera pelos servi�os do cronograma
                for (var i = 0; i < cronograma.Servicos.Count; i++)
                {
                    if (cronograma.Servicos[i].InicioServico.HasValue)
                    {
                        var servico = cronograma.Servicos[i];

                        // Calcula os meses de medi��o baseado nas datas de in�cio e t�rmino do cronograma
                        CalcularMedicoesServico(ref servico, cronograma.DiaMedicaoCronograma);
                        // Organiza e limpa medi��es do servi�o
                        OrdernarMedicoesServico(ref servico);
                        // Organiza e limpa produ��es do servi�o
                        OrdernarProducaoServico(ref servico);
                    }
                }
            }

            // Itera pelos servi�os do cronograma
            for (var i = 0; i < cronograma.Servicos.Count; i++)
            {
                // Itera pelas medi��es do servi�o
                for (var j = 0; j < cronograma.Servicos[i].Medicoes.Count; j++)
                {
                    var medicao = cronograma.Servicos[i].Medicoes[j];

                    // Calcula o P/E/S e equipe e cada medi��o do servi�o
                    CalcularPesEquipeMedicao(ref medicao, cronograma.Servicos[i]);
                    // Calcula o faturamento e a quantidade para a medi��o do servi�o
                    CalcularFaturamentoMedicao(ref medicao, cronograma.Servicos[i]);
                }

                var servico = cronograma.Servicos[i];

                // Calcula data de in�cio do servi�o
                CalcularInicioServico(ref servico, requisicao.Cronogramas);
                // Calcula data de t�rmino do servi�o
                CalcularTerminoServico(ref servico, cronograma.DiaMedicaoCronograma);
                // Calcular chance de faturamento do servi�o
                CalcularChanceServico(ref servico, cronograma.AtualizacaoCronograma.Value);
                // Calcula dura��o do servi�o
                CalcularDuracaoServico(ref servico);
            }

            // Organiza e limpa servi�os do cronograma
            OrdernarServicos(ref cronograma);

            // Retorna o cronograma calculado
            resultado.Cronogramas = requisicao.Cronogramas;

            return resultado;
        }

        private void CalcularDadosServico(CronogramaDto cronograma, ref CronogramaServicoDto servico)
        {
            var idCronogramaServico = servico.IdCronogramaServico;

            // Verificar se � quantidade real ou calculada
            if (servico.QuantidadeReal.HasValue)
                // Atribui valor da quantidade real
                servico.QuantidadeTotal = servico.QuantidadeReal.Value;
            else
                // Soma quantidade total proveniente da planilha
                servico.QuantidadeTotal = cronograma.Planilhas.Sum(p => p.Itens.Where(i => i.IdCronogramaServico == idCronogramaServico && i.EhItemControle).Sum(i => i.QuantidadeItem ?? 0 + i.QuantidadeAcertoItem ?? 0));

            // Soma quantidade executada proveniente da produ��o di�ria
            servico.QuantidadeExecutada = servico.Producoes.Sum(p => p.QuantidadeProducao ?? 0);

            // Soma valor total proveniente da planilha
            servico.ValorServico = cronograma.Planilhas.Sum(p => p.Itens.Where(i => i.IdCronogramaServico == idCronogramaServico).Sum(i => (i.QuantidadeItem ?? 0 + i.QuantidadeAcertoItem ?? 0) * i.PrecoItem ?? 0));

            // Se servi�o n�o possui medi��es ainda, cria lista vazia
            if (servico.Medicoes == null)
                servico.Medicoes = new List<CronogramaServicoMedicaoDto>();

            // Calcula as m�dias de equipe e P/E/S para mostrar no cronograma
            servico.MediaEquipe = Math.Round(servico.Medicoes.Where(m => m.EquipeMedicao.HasValue && m.EquipeMedicao > 0).Average(m => m.EquipeMedicao) ?? 0, 2);
            servico.MediaPes = Math.Round(servico.Medicoes.Where(m => m.PesMedicao.HasValue && m.PesMedicao > 0).Average(m => m.PesMedicao) ?? 0, 2);

            // Calcula a m�dia de P/E/S real baseada na produ��o di�ria
            if (servico.Producoes.Count > 0)
            {
                var iniProd = servico.Producoes.Min(p => p.DataProducao);
                var terProd = servico.Producoes.Max(p => p.DataProducao);
                var semProd = (int)Math.Floor((terProd - iniProd).Days / 7M);
                terProd = iniProd.AddDays(semProd * 7);
                var totProd = servico.Producoes.Where(p => p.DataProducao <= terProd && p.DataProducao >= iniProd).Sum(p => p.EquipeProducao > 0 ? p.QuantidadeProducao / p.EquipeProducao : 0);

                if (semProd > 0)
                    servico.MediaPesReal = totProd / semProd;
                else
                    servico.MediaPesReal = null;
            }
            else
                servico.MediaPesReal = null;

            // Calcula a �ltima data de atualiza��o proveniente da produ��o di�ria
            if (servico.Producoes.Count > 0)
                servico.AtualizacaoServico = servico.Producoes.Max(p => p.DataProducao);
            else
                servico.AtualizacaoServico = null;
        }

        private void CalcularInicioServico(ref CronogramaServicoDto servico, List<CronogramaDto> cronogramas)
        {
            if (servico.Producoes.Count > 0)
            {
                servico.InicioServico = servico.Producoes.Min(p => p.DataProducao);
            }
            if (servico.IdServicoPredecessora.HasValue)
            {
                var idCronogramaPredecessora = servico.IdCronogramaPredecessora ?? servico.IdCronograma;
                var idServicoPredecessora = servico.IdServicoPredecessora.Value;
                var servicoPredecessora = cronogramas.FirstOrDefault(c => c.IdCronograma == idCronogramaPredecessora).Servicos.FirstOrDefault(s => s.IdCronogramaServico == idServicoPredecessora);

                if (servicoPredecessora.TerminoServico.HasValue)
                {
                    var dataInicio = (servico.TipoPredecessora ?? 'T') == 'T' ? servicoPredecessora.TerminoServico.Value : servicoPredecessora.InicioServico.Value;
                    var atrasoData = servico.AtrasoPredecessora ?? 0;
                    servico.InicioServico = dataInicio.AddDays(atrasoData);
                }
                else
                {
                    servico.InicioServico = null;
                }
            }
            else
            {
                servico.AtrasoPredecessora = null;
                servico.TipoPredecessora = null;
            }
        }

        private void CalcularTerminoServico(ref CronogramaServicoDto servico, int diaMedicao)
        {
            if (servico.QuantidadeExecutada >= servico.QuantidadeTotal)
                // Caso o servi�o esteja finalizado, a data de t�rmino � a �ltima da de atualiza��o
                servico.TerminoServico = servico.AtualizacaoServico;
            else
            {
                // Se o servi�o j� foi iniciado, utiliza a data de atualiza��o como base, sen�o utiliza a data de in�cio
                DateTime? inicio = null;
                if (servico.QuantidadeExecutada <= 0 && servico.InicioServico.HasValue)
                    inicio = servico.InicioServico.Value;
                else if (servico.QuantidadeExecutada > 0 && servico.AtualizacaoServico.HasValue)
                    inicio = servico.AtualizacaoServico.Value;

                // Se houver data de in�cio ou atualiza��o
                if (inicio.HasValue)
                {
                    // Dias calculados para o t�rmino do servi�o
                    var diasTermino = 0;
                    // Calcula a data de in�cio da medi��o
                    var inicioMedicao = new DateTime(inicio.Value.Year, inicio.Value.Month, diaMedicao).AddMonths(inicio.Value.Day > diaMedicao ? 0 : -1);

                    // Calcula a quantidade restante para executar
                    var quantidadeRestante = servico.QuantidadeTotal - servico.QuantidadeExecutada;
                    var quantidadeAcumulada = servico.QuantidadeExecutada;

                    // Enquanto houver quantidade para executar
                    while (quantidadeRestante > 0)
                    {
                        // Calcula a data de t�rmino da medi��o
                        var terminoMedicao = inicioMedicao.AddMonths(1).AddDays(-1);
                        // Verifica se a medi��o possui P/E/S, se n�o busca a �ltima medi��o que possui P/E/S
                        var pes = servico.Medicoes.FirstOrDefault(m => m.InicioMedicao == inicioMedicao)?.PesMedicao ?? servico.Medicoes.LastOrDefault(m => /*m.InicioMedicao < inicioMedicao && */m.PesMedicao.HasValue && m.PesMedicao.Value > 0)?.PesMedicao;
                        // Verifica se a medi��o possui equipe, se n�o busca a �ltima medi��o que possui equipe
                        var equipe = servico.Medicoes.FirstOrDefault(m => m.InicioMedicao == inicioMedicao)?.EquipeMedicao ?? servico.Medicoes.LastOrDefault(m => /*m.InicioMedicao < inicioMedicao && */m.EquipeMedicao.HasValue && m.EquipeMedicao.Value > 0)?.EquipeMedicao;

                        // Se houve P/E/S e equipe
                        if (pes.HasValue && equipe.HasValue)
                        {
                            // Calcula a produ��o m�dia di�ria
                            var producaoDiaria = pes.Value / 7 * equipe.Value;
                            // Calcula quantos dias do in�cio da medi��o at� o t�rmino da medi��o
                            var periodoMedicao = (terminoMedicao - inicioMedicao).Days + 1;
                            // Calcula quantos dias do in�cio do servi�o at� o t�rmino da medi��o
                            var periodoInicio = (terminoMedicao - servico.InicioServico.Value).Days + 1;
                            // Verificar qual o menor per�odo
                            var periodo = Math.Min(periodoMedicao, Math.Max(periodoInicio, 0));
                            // Calcula qual a quantidade pode ser produzida no per�odo calculado
                            var quantidade = Math.Round(Math.Min(producaoDiaria * periodo, quantidadeRestante), 2);
                            // Incrementa a quantidade acumulada
                            quantidadeAcumulada += quantidade;
                            // Calcula a quantidade restante para executar
                            quantidadeRestante = servico.QuantidadeTotal - quantidadeAcumulada;

                            // Incrementa os dias para t�rmino do servi�o
                            if (quantidade > 0)
                                // Se a quantidade for maior que zero, calcula quantos dias o per�odo possui
                                diasTermino += (int)Math.Ceiling(quantidade / producaoDiaria) - 1;
                            else
                                // Se a quantidade for menor que zero, incrementa o per�odo inteiro
                                diasTermino += periodo;
                        }
                        else
                            // Se n�o houve P/E/S ou equipe, para a repeti��o
                            break;

                        // Passa para o pr�ximo per�odo de medi��o
                        inicioMedicao = inicioMedicao.AddMonths(1);
                    }

                    // Se ainda houver quantidade a executar, atribui nulo, sen�o calcula o t�rmino
                    if (quantidadeRestante > 0)
                        servico.TerminoServico = null;
                    else
                        servico.TerminoServico = inicio.Value.AddDays(diasTermino);
                }
                else
                    // Se n�o houve data de in�cio ou t�rmino, atribui da de in�cio para data de t�rmino
                    servico.TerminoServico = servico.InicioServico;
            }
        }

        private void CalcularDuracaoServico(ref CronogramaServicoDto servico)
        {
            servico.DuracaoTotal = servico.InicioServico.HasValue && servico.TerminoServico.HasValue ? (servico.TerminoServico.Value - servico.InicioServico.Value).Days + 1 : 0;
            if (servico.QuantidadeExecutada >= servico.QuantidadeTotal)
                servico.DuracaoFalta = servico.DuracaoTotal;
            else if (servico.QuantidadeExecutada > 0 && servico.QuantidadeExecutada < servico.QuantidadeTotal)
                servico.DuracaoFalta = servico.AtualizacaoServico.HasValue && servico.TerminoServico.HasValue ? (servico.TerminoServico.Value - servico.AtualizacaoServico.Value).Days : 0;
            else
                servico.DuracaoFalta = 0;
        }

        private void CalcularPesEquipeMedicao(ref CronogramaServicoMedicaoDto medicao, CronogramaServicoDto servico)
        {
            if (medicao.InicioMedicao <= servico.TerminoServico && medicao.TerminoMedicao >= servico.InicioServico)
            {
                medicao.PesMedicao = medicao.PesMedicao ?? servico.Medicoes.LastOrDefault(m => m.PesMedicao.HasValue)?.PesMedicao;
                medicao.EquipeMedicao = medicao.EquipeMedicao ?? servico.Medicoes.LastOrDefault(m => m.EquipeMedicao.HasValue)?.EquipeMedicao;
            }
            else if (servico.TerminoServico.HasValue && servico.InicioServico.HasValue)
            {
                medicao.PesMedicao = null;
                medicao.EquipeMedicao = null;
            }
        }

        private void CalcularFaturamentoMedicao(ref CronogramaServicoMedicaoDto medicao, CronogramaServicoDto servico)
        {
            if (medicao.PesMedicao.HasValue && medicao.EquipeMedicao.HasValue && servico.InicioServico.HasValue)
            {
                medicao.TemMedicao = planilhaRepositorio.VerificarMedicao(servico.IdCronograma, medicao.MesReferencia);

                if (medicao.TemMedicao)
                {
                    medicao.Faturamento = planilhaRepositorio.SomarValorMedicao(medicao.IdCronogramaServico, medicao.MesReferencia);
                    medicao.Quantidade = planilhaRepositorio.SomarQuantidadeMedicao(medicao.IdCronogramaServico, medicao.MesReferencia);
                }
                else
                {
                    var mesReferencia = medicao.MesReferencia;

                    var producaoDiaria = medicao.PesMedicao.Value / 7 * medicao.EquipeMedicao.Value;
                    var periodoMedicao = (medicao.TerminoMedicao - medicao.InicioMedicao).Days + 1;
                    var periodoInicio = (medicao.TerminoMedicao - servico.InicioServico.Value).Days + 1;
                    var valorDiaria = servico.QuantidadeTotal > 0 ? servico.ValorServico / servico.QuantidadeTotal : 0;
                    var valorRestante = servico.ValorServico - servico.Medicoes.Where(m => m.MesReferencia < mesReferencia).Sum(m => m.Faturamento).Value;
                    var quantidadeRestante = servico.QuantidadeTotal - servico.Medicoes.Where(m => m.MesReferencia < mesReferencia).Sum(m => m.Quantidade).Value;

                    medicao.Faturamento = Math.Round(Math.Min(producaoDiaria * Math.Min(periodoMedicao, Math.Max(periodoInicio, 0)) * valorDiaria, valorRestante), 2);
                    medicao.Quantidade = Math.Round(Math.Min(producaoDiaria * Math.Min(periodoMedicao, Math.Max(periodoInicio, 0)), quantidadeRestante), 2);
                }
            }
            else
            {
                medicao.Faturamento = null;
                medicao.Quantidade = null;

                if (!servico.InicioServico.HasValue)
                {
                    medicao.PesMedicao = null;
                    medicao.EquipeMedicao = null;
                }
            }
        }

        private void CalcularMedicoesServico(ref CronogramaServicoDto servico, int diaMedicao)
        {
            servico.TerminoServico = servico.TerminoServico ?? servico.InicioServico;
            var termino = new DateTime(servico.TerminoServico.Value.Year, servico.TerminoServico.Value.Month, diaMedicao).AddMonths(servico.TerminoServico.Value.Day > diaMedicao ? 1 : 0).AddDays(-1);

            for (var i = new DateTime(servico.InicioServico.Value.Year, servico.InicioServico.Value.Month, 1).AddMonths(servico.InicioServico.Value.Day >= diaMedicao ? 0 : -1); i <= termino; i = i.AddMonths(1))
            {
                if (!servico.Medicoes.Any(m => m.MesReferencia == i))
                {
                    servico.Medicoes.Add(new CronogramaServicoMedicaoDto
                    {
                        IdCronogramaServicoMedicao = 0,
                        IdCronogramaServico = servico.IdCronogramaServico,
                        MesReferencia = i,
                        InicioMedicao = new DateTime(i.Year, i.Month, diaMedicao).AddMonths(diaMedicao > 15 ? -1 : 0),
                        TerminoMedicao = new DateTime(i.Year, i.Month, diaMedicao).AddMonths(diaMedicao > 15 ? -1 : 0).AddMonths(1).AddDays(-1),
                        TemMedicao = false
                    });

                    var medicao = servico.Medicoes.FirstOrDefault(m => m.MesReferencia == i);
                    if (medicao != null && medicao.InicioMedicao <= servico.TerminoServico && medicao.TerminoMedicao >= servico.InicioServico)
                    {
                        var medicaoMod = servico.Medicoes.FirstOrDefault(m => m.MesReferencia == i);
                        medicaoMod.PesMedicao = medicaoMod.PesMedicao.HasValue ? medicaoMod.PesMedicao : servico.Medicoes.LastOrDefault(m2 => m2.PesMedicao.HasValue)?.PesMedicao;
                        medicaoMod.EquipeMedicao = medicaoMod.EquipeMedicao.HasValue ? medicaoMod.EquipeMedicao : servico.Medicoes.LastOrDefault(m2 => m2.EquipeMedicao.HasValue)?.EquipeMedicao;
                    }
                }
                else
                {
                    var medicao = servico.Medicoes.FirstOrDefault(m => m.MesReferencia == i);
                    medicao.InicioMedicao = new DateTime(i.Year, i.Month, diaMedicao).AddMonths(diaMedicao > 15 ? -1 : 0);
                    medicao.TerminoMedicao = new DateTime(i.Year, i.Month, diaMedicao).AddMonths(diaMedicao > 15 ? -1 : 0).AddMonths(1).AddDays(-1);
                }
            }
        }

        private void OrdernarMedicoesServico(ref CronogramaServicoDto servico)
        {
            var inicioServico = servico.InicioServico.Value;
            var terminoServico = servico.TerminoServico.Value;

            if (servico.MedicoesRemovidas == null)
                servico.MedicoesRemovidas = new List<int>();

            servico.MedicoesRemovidas.AddRange(servico.Medicoes.Where(m => m.InicioMedicao > terminoServico || m.TerminoMedicao < inicioServico).Select(m => m.IdCronogramaServicoMedicao));
            servico.Medicoes.RemoveAll(m => m.InicioMedicao > terminoServico || m.TerminoMedicao < inicioServico);
            servico.Medicoes = servico.Medicoes.OrderBy(m => m.MesReferencia).ToList();
        }

        private void CalcularChanceServico(ref CronogramaServicoDto servico, DateTime dataAtualizacao)
        {
            var medicao = servico.Medicoes?.FirstOrDefault(m => !m.TemMedicao);

            if (medicao == null)
                servico.ChanceFaturamento = 0;
            else if (servico.QuantidadeExecutada <= 0 && servico.InicioServico >= dataAtualizacao)
                servico.ChanceFaturamento = 1;
            else if (servico.InicioServico.HasValue && (servico.TerminoServico.HasValue || servico.AtrasoPredecessora.HasValue) && medicao.Quantidade > 0)
            {
                var termino = servico.TerminoServico > dataAtualizacao && servico.InicioServico < dataAtualizacao ? dataAtualizacao : servico.TerminoServico;
                var periodo = (termino.Value - servico.InicioServico.Value).Days + 1;
                var quantDia = servico.QuantidadeExecutada / periodo;
                var terminoMed = medicao.TerminoMedicao > servico.TerminoServico ? servico.TerminoServico : medicao.TerminoMedicao;
                var periodoMed = (terminoMed.Value - servico.InicioServico.Value).Days + 1;
                var prodTot = quantDia / periodoMed;
                var quantTot = Math.Min(prodTot, servico.QuantidadeTotal);
                var quantMed = servico.Medicoes.Where(m => m.MesReferencia < medicao.MesReferencia).Sum(m => m.Quantidade ?? 0);

                servico.ChanceFaturamento = (quantTot - quantMed) / medicao.Quantidade.Value;
            }
            else
                servico.ChanceFaturamento = 0;
        }

        private void OrdernarProducaoServico(ref CronogramaServicoDto servico)
        {
            if (servico.ProducoesRemovidas == null)
                servico.ProducoesRemovidas = new List<int>();

            servico.ProducoesRemovidas.AddRange(servico.Producoes.Where(p => !p.QuantidadeProducao.HasValue).Select(p => p.IdProducao));
            servico.Producoes.RemoveAll(p => !p.QuantidadeProducao.HasValue);
            servico.Producoes = servico.Producoes.OrderBy(m => m.DataProducao).ToList();
        }

        private void OrdernarServicos(ref CronogramaDto cronograma)
        {
            if (cronograma.ServicosRemovidos == null)
                cronograma.ServicosRemovidos = new List<int>();

            cronograma.ServicosRemovidos.AddRange(cronograma.Servicos.Where(s => s.ValorServico == 0 && s.QuantidadeTotal == 0).Select(s => s.IdCronogramaServico));
            cronograma.Servicos.RemoveAll(s => s.ValorServico == 0 && s.QuantidadeTotal == 0);
            cronograma.Servicos = cronograma.Servicos.OrderBy(s => s.OrdemServico).ToList();
        }
    }
}