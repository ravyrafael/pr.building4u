using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCronograma;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace PR.Building4U.Executores.AdministrarCronograma
{
    public class VisualizarCronogramasExecutor : IExecutor<VisualizarCronogramasRequisicao, VisualizarCronogramasResultado>
    {
        private readonly ICronogramaRepositorio cronogramaRepositorio;
        private readonly IPlanilhaRepositorio planilhaRepositorio;
        private readonly IProducaoRepositorio producaoRepositorio;

        public VisualizarCronogramasExecutor(ICronogramaRepositorio cronogramaRepositorio, IPlanilhaRepositorio planilhaRepositorio, IProducaoRepositorio producaoRepositorio)
        {
            this.cronogramaRepositorio = cronogramaRepositorio;
            this.planilhaRepositorio = planilhaRepositorio;
            this.producaoRepositorio = producaoRepositorio;
        }

        [Transacao]
        public VisualizarCronogramasResultado Executar(VisualizarCronogramasRequisicao requisicao)
        {
            var resultado = new VisualizarCronogramasResultado();

            var cronogramas = cronogramaRepositorio.ListarCronogramas(requisicao.IdObra);
            foreach (var cronograma in cronogramas)
            {
                cronograma.Servicos = cronogramaRepositorio.ListarServicos(cronograma.IdCronograma).ToList();
                foreach (var servico in cronograma.Servicos)
                {
                    servico.Medicoes = cronogramaRepositorio.ListarMedicoes(servico.IdCronogramaServico).ToList();
                    servico.Producoes = producaoRepositorio.ListarProducaos(servico.IdCronogramaServico).ToList();
                }

                cronograma.Planilhas = planilhaRepositorio.ListarPlanilhas(cronograma.IdCronograma).ToList();
                foreach (var planilha in cronograma.Planilhas)
                {
                    planilha.Itens = planilhaRepositorio.ListarItens(planilha.IdPlanilha).ToList();
                    foreach (var item in planilha.Itens)
                        item.Medicoes = planilhaRepositorio.ListarItensMedicao(item.IdPlanilhaItem).ToList();
                }
            }

            resultado.Cronogramas = Mapper.Map<List<CronogramaDto>>(cronogramas) ?? new List<CronogramaDto>();

            return resultado;
        }
    }
}