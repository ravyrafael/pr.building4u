using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAlocacao;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;
using System;

namespace PR.Building4U.Executores.AdministrarAlocacao
{
    public class TransferirFuncionarioExecutor : IExecutor<TransferirFuncionarioRequisicao, TransferirFuncionarioResultado>
    {
        private readonly IAlocacaoRepositorio alocacaoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public TransferirFuncionarioExecutor(IAlocacaoRepositorio alocacaoRepositorio, ILogRepositorio logRepositorio)
        {
            this.alocacaoRepositorio = alocacaoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public TransferirFuncionarioResultado Executar(TransferirFuncionarioRequisicao requisicao)
        {
            var resultado = new TransferirFuncionarioResultado();

            var alocacao = alocacaoRepositorio.ObterAlocacaoPorFuncionario(requisicao.IdFuncionario);

            if (requisicao.IdObra == alocacao.IdObra)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI003);
            else if (requisicao.DataTransferencia <= alocacao.InicioAlocacao)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI004);
            else
            {
                alocacaoRepositorio.ExcluirReserva(requisicao.IdFuncionario);

                var tipo = "Reserva";
                if (requisicao.DataTransferencia > DateTime.Now)
                    alocacaoRepositorio.IncluirReserva(requisicao.IdFuncionario, requisicao.IdObra, requisicao.DataTransferencia);
                else
                {
                    tipo = "Aloca��o";
                    alocacaoRepositorio.IncluirAlocacao(requisicao.IdFuncionario, requisicao.IdObra, requisicao.SequenciaAlocacao + 1, requisicao.DataTransferencia);
                    alocacaoRepositorio.FinalizarAlocacao(requisicao.IdFuncionario, requisicao.SequenciaAlocacao, requisicao.DataTransferencia.AddDays(-1));
                }

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"{tipo} - Funcion�rio {requisicao.IdFuncionario} - Obra {requisicao.IdObra}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}