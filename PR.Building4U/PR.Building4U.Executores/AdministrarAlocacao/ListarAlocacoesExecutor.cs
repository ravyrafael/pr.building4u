using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAlocacao;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using System.Collections.Generic;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarAlocacao
{
    public class ListarAlocacoesExecutor : IExecutor<ListarAlocacoesRequisicao, ListarAlocacoesResultado>
    {
        private readonly IAlocacaoRepositorio alocacaoRepositorio;

        public ListarAlocacoesExecutor(IAlocacaoRepositorio alocacaoRepositorio)
        {
            this.alocacaoRepositorio = alocacaoRepositorio;
        }

        [Transacao]
        public ListarAlocacoesResultado Executar(ListarAlocacoesRequisicao requisicao)
        {
            var resultado = new ListarAlocacoesResultado();

            var alocacoes = alocacaoRepositorio.ListarAlocacoes(requisicao.Obras, requisicao.Cargos);
            resultado.Alocacoes = Mapper.Map<List<AlocacaoDto>>(alocacoes) ?? new List<AlocacaoDto>();

            return resultado;
        }
    }
}