using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAlocacao;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarAlocacao
{
    public class CancelarDestinoExecutor : IExecutor<CancelarDestinoRequisicao, CancelarDestinoResultado>
    {
        private readonly IAlocacaoRepositorio alocacaoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public CancelarDestinoExecutor(IAlocacaoRepositorio alocacaoRepositorio, ILogRepositorio logRepositorio)
        {
            this.alocacaoRepositorio = alocacaoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public CancelarDestinoResultado Executar(CancelarDestinoRequisicao requisicao)
        {
            var resultado = new CancelarDestinoResultado();

            var temDestino = alocacaoRepositorio.VerificarDestino(requisicao.IdFuncionario) > 0;

            if (!temDestino)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI020);
            else
            {
                alocacaoRepositorio.ExcluirReserva(requisicao.IdFuncionario);

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Funcionário {requisicao.IdFuncionario}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}