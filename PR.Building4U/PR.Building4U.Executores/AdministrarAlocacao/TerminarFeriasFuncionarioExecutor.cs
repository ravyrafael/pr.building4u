using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAlocacao;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Util;
using PR.Building4U.Util.Enumeracoes;
using System;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarAlocacao
{
    public class TerminarFeriasFuncionarioExecutor : IExecutor<TerminarFeriasFuncionarioRequisicao, TerminarFeriasFuncionarioResultado>
    {
        private readonly IFuncionarioRepositorio funcionarioRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public TerminarFeriasFuncionarioExecutor(IFuncionarioRepositorio funcionarioRepositorio, ILogRepositorio logRepositorio)
        {
            this.funcionarioRepositorio = funcionarioRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public TerminarFeriasFuncionarioResultado Executar(TerminarFeriasFuncionarioRequisicao requisicao)
        {
            var resultado = new TerminarFeriasFuncionarioResultado();

            var ferias = funcionarioRepositorio.ObterFeriasFuncionario(requisicao.IdFuncionario);

            if (requisicao.TerminoFerias < ferias.InicioFerias.AddDays(1))
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI008);
            else if (requisicao.TerminoFerias > DateTime.Now.Date)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI010);
            else
            {
                funcionarioRepositorio.TerminarFeriasFuncionario(requisicao.IdFuncionario, requisicao.TerminoFerias);
                funcionarioRepositorio.AlterarSituacaoFuncionario(requisicao.IdFuncionario, (int)Situacao.Trabalhando);

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Funcionário {requisicao.IdFuncionario} - Término {requisicao.TerminoFerias:dd/MM/yyyy}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}