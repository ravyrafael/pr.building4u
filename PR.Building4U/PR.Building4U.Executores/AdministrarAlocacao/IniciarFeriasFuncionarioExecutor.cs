using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAlocacao;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Util;
using PR.Building4U.Util.Enumeracoes;
using System;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarAlocacao
{
    public class IniciarFeriasFuncionarioExecutor : IExecutor<IniciarFeriasFuncionarioRequisicao, IniciarFeriasFuncionarioResultado>
    {
        private readonly IFuncionarioRepositorio funcionarioRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IniciarFeriasFuncionarioExecutor(IFuncionarioRepositorio funcionarioRepositorio, ILogRepositorio logRepositorio)
        {
            this.funcionarioRepositorio = funcionarioRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IniciarFeriasFuncionarioResultado Executar(IniciarFeriasFuncionarioRequisicao requisicao)
        {
            var resultado = new IniciarFeriasFuncionarioResultado();

            var ferias = funcionarioRepositorio.ObterFeriasFuncionario(requisicao.IdFuncionario);

            if (ferias != null && requisicao.InicioFerias < ferias.TerminoFerias.AddDays(1))
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI007);
            else if (requisicao.InicioFerias > DateTime.Now.Date)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI011);
            else if (requisicao.TerminoFerias <= DateTime.Now.Date)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI009);
            else
            {
                funcionarioRepositorio.IniciarFeriasFuncionario(requisicao.IdFuncionario, requisicao.InicioFerias, requisicao.TerminoFerias);
                funcionarioRepositorio.AlterarSituacaoFuncionario(requisicao.IdFuncionario, (int)Situacao.Ferias);

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Funcionário {requisicao.IdFuncionario} - Início {requisicao.InicioFerias:dd/MM/yyyy}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}