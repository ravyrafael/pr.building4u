using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAlocacao;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Util;
using System;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarAlocacao
{
    public class DesfazerTransferenciaExecutor : IExecutor<DesfazerTransferenciaRequisicao, DesfazerTransferenciaResultado>
    {
        private readonly IAlocacaoRepositorio alocacaoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public DesfazerTransferenciaExecutor(IAlocacaoRepositorio alocacaoRepositorio, ILogRepositorio logRepositorio)
        {
            this.alocacaoRepositorio = alocacaoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public DesfazerTransferenciaResultado Executar(DesfazerTransferenciaRequisicao requisicao)
        {
            var resultado = new DesfazerTransferenciaResultado();

            var alocacao = alocacaoRepositorio.ObterAlocacaoPorFuncionario(requisicao.IdFuncionario);

            if (alocacao.InicioAlocacao < DateTime.Now.Date)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI005);
            else
            {
                alocacaoRepositorio.ReativarAlocacao(requisicao.IdFuncionario, requisicao.SequenciaAlocacao - 1);
                alocacaoRepositorio.ExcluirAlocacao(requisicao.IdFuncionario, requisicao.SequenciaAlocacao);

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Funcionário {requisicao.IdFuncionario} - Sequência {requisicao.SequenciaAlocacao}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}