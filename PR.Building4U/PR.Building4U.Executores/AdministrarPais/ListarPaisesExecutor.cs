﻿using AutoMapper;
using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarPais;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarPais
{
    public class ListarPaisesExecutor : IExecutor<ListarPaisesRequisicao, ListarPaisesResultado>
    {
        private readonly IPaisRepositorio paisRepositorio;

        public ListarPaisesExecutor(IPaisRepositorio paisRepositorio)
        {
            this.paisRepositorio = paisRepositorio;
        }

        [Transacao]
        public ListarPaisesResultado Executar(ListarPaisesRequisicao requisicao)
        {
            var resultado = new ListarPaisesResultado();

            var paises = paisRepositorio.ListarPaises(requisicao.CodigoOuNome);
            resultado.Paises = Mapper.Map<List<PaisDto>>(paises) ?? new List<PaisDto>();

            return resultado;
        }
    }
}
