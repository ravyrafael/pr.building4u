﻿using AutoMapper;
using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarPais;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarPais 
{
	public class ObterPaisExecutor : IExecutor<ObterPaisRequisicao, ObterPaisResultado>
	{
        private readonly IPaisRepositorio paisRepositorio;

        public ObterPaisExecutor(IPaisRepositorio paisRepositorio)
        {
            this.paisRepositorio = paisRepositorio;
        }

        [Transacao]
        public ObterPaisResultado Executar(ObterPaisRequisicao requisicao)
        {
            var resultado = new ObterPaisResultado();

            var pais = paisRepositorio.ObterPais(requisicao.IdPais);
            resultado.Pais = Mapper.Map<PaisDto>(pais);

            return resultado;
        }
	}
}
