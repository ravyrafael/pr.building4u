﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarPais;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarPais
{
    public class IncluirPaisExecutor : IExecutor<IncluirPaisRequisicao, IncluirPaisResultado>
    {
        private readonly IPaisRepositorio paisRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirPaisExecutor(IPaisRepositorio paisRepositorio, ILogRepositorio logRepositorio)
        {
            this.paisRepositorio = paisRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirPaisResultado Executar(IncluirPaisRequisicao requisicao)
        {
            var resultado = new IncluirPaisResultado();

            resultado.IdPais = paisRepositorio.IncluirPais(requisicao.CodigoPais, requisicao.NomePais);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"País {resultado.IdPais} - Nome {requisicao.NomePais}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}
