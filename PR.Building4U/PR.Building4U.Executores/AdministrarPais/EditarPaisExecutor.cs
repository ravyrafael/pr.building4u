﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarPais;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarPais
{
    public class EditarPaisExecutor : IExecutor<EditarPaisRequisicao, EditarPaisResultado>
    {
        private readonly IPaisRepositorio paisRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarPaisExecutor(IPaisRepositorio paisRepositorio, ILogRepositorio logRepositorio)
        {
            this.paisRepositorio = paisRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarPaisResultado Executar(EditarPaisRequisicao requisicao)
        {
            var resultado = new EditarPaisResultado();

            paisRepositorio.EditarPais(requisicao.IdPais, requisicao.NomePais);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"País {requisicao.IdPais} - Nome {requisicao.NomePais}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}
