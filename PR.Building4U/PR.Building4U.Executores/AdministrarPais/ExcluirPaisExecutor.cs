﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarPais;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarPais
{
    public class ExcluirPaisExecutor : IExecutor<ExcluirPaisRequisicao, ExcluirPaisResultado>
    {
        private readonly IPaisRepositorio paisRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirPaisExecutor(IPaisRepositorio paisRepositorio, ILogRepositorio logRepositorio)
        {
            this.paisRepositorio = paisRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirPaisResultado Executar(ExcluirPaisRequisicao requisicao)
        {
            var resultado = new ExcluirPaisResultado();

            paisRepositorio.ExcluirPais(requisicao.IdPais);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"País {requisicao.IdPais}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}
