using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarEquipamento;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarEquipamento
{
    public class ObterEquipamentoExecutor : IExecutor<ObterEquipamentoRequisicao, ObterEquipamentoResultado>
    {
        private readonly IEquipamentoRepositorio equipamentoRepositorio;
        private readonly IAluguelRepositorio aluguelRepositorio;

        public ObterEquipamentoExecutor(IEquipamentoRepositorio equipamentoRepositorio, IAluguelRepositorio aluguelRepositorio)
        {
            this.equipamentoRepositorio = equipamentoRepositorio;
            this.aluguelRepositorio = aluguelRepositorio;
        }

        [Transacao]
        public ObterEquipamentoResultado Executar(ObterEquipamentoRequisicao requisicao)
        {
            var resultado = new ObterEquipamentoResultado();

            var equipamento = equipamentoRepositorio.ObterEquipamento(requisicao.IdEquipamento);
            var aluguel = aluguelRepositorio.ObterAluguelPorEquipamento(requisicao.IdEquipamento);

            resultado.Equipamento = Mapper.Map<EquipamentoDto>(equipamento);
            resultado.Equipamento.Obra = new ObraDto
            {
                IdObra = aluguel != null ? aluguel.IdObra : 0,
                CodigoObra = aluguel != null ? aluguel.CodigoObra : "",
                NomeObra = aluguel != null ? aluguel.NomeObra : ""
            };

            return resultado;
        }
    }
}