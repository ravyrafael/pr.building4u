using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarEquipamento;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarEquipamento
{
    public class EditarEquipamentoExecutor : IExecutor<EditarEquipamentoRequisicao, EditarEquipamentoResultado>
    {
        private readonly IEquipamentoRepositorio equipamentoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarEquipamentoExecutor(IEquipamentoRepositorio equipamentoRepositorio, ILogRepositorio logRepositorio)
        {
            this.equipamentoRepositorio = equipamentoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarEquipamentoResultado Executar(EditarEquipamentoRequisicao requisicao)
        {
            var resultado = new EditarEquipamentoResultado();

            equipamentoRepositorio.EditarEquipamento(requisicao.IdEquipamento, requisicao.MarcaEquipamento, requisicao.ModeloEquipamento, requisicao.FornecedorEquipamento);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Equipamento {requisicao.IdEquipamento} - Marca {requisicao.MarcaEquipamento} - Modelo {requisicao.ModeloEquipamento}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}