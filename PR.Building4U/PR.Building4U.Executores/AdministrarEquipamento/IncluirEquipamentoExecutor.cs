using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarEquipamento;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarEquipamento
{
    public class IncluirEquipamentoExecutor : IExecutor<IncluirEquipamentoRequisicao, IncluirEquipamentoResultado>
    {
        private readonly IEquipamentoRepositorio equipamentoRepositorio;
        private readonly IAluguelRepositorio aluguelRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirEquipamentoExecutor(IEquipamentoRepositorio equipamentoRepositorio, IAluguelRepositorio aluguelRepositorio, ILogRepositorio logRepositorio)
        {
            this.equipamentoRepositorio = equipamentoRepositorio;
            this.aluguelRepositorio = aluguelRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirEquipamentoResultado Executar(IncluirEquipamentoRequisicao requisicao)
        {
            var resultado = new IncluirEquipamentoResultado();

            resultado.IdEquipamento = equipamentoRepositorio.IncluirEquipamento(requisicao.MarcaEquipamento, requisicao.ModeloEquipamento, requisicao.PatrimonioEquipamento, requisicao.IdTipoEquipamento, requisicao.IndicadorTercerio, requisicao.FornecedorEquipamento, requisicao.AquisicaoEquipamento);
            resultado.IdAluguel = aluguelRepositorio.IncluirAluguel(resultado.IdEquipamento, requisicao.IdObra, 1, requisicao.AquisicaoEquipamento);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Equipamento {resultado.IdEquipamento} - Marca {requisicao.MarcaEquipamento} - Modelo {requisicao.ModeloEquipamento}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}