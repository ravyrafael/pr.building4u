using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarEquipamento;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarEquipamento
{
    public class SucatearEquipamentoExecutor : IExecutor<SucatearEquipamentoRequisicao, SucatearEquipamentoResultado>
    {
        private readonly IEquipamentoRepositorio equipamentoRepositorio;
        private readonly IAluguelRepositorio aluguelRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public SucatearEquipamentoExecutor(IEquipamentoRepositorio equipamentoRepositorio, IAluguelRepositorio aluguelRepositorio, ILogRepositorio logRepositorio)
        {
            this.equipamentoRepositorio = equipamentoRepositorio;
            this.aluguelRepositorio = aluguelRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public SucatearEquipamentoResultado Executar(SucatearEquipamentoRequisicao requisicao)
        {
            var resultado = new SucatearEquipamentoResultado();

            var equipamento = equipamentoRepositorio.ObterEquipamento(requisicao.IdEquipamento);

            if (equipamento.SucataEquipamento.HasValue)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI013);
            else if (requisicao.SucataEquipamento <= equipamento.AquisicaoEquipamento)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI014);
            else
            {
                var aluguel = aluguelRepositorio.ObterAluguelPorEquipamento(requisicao.IdEquipamento);

                equipamentoRepositorio.SucatearEquipamento(requisicao.IdEquipamento, requisicao.SucataEquipamento);
                if (aluguel != null)
                    aluguelRepositorio.FinalizarAluguel(aluguel.IdEquipamento, aluguel.SequenciaAluguel, requisicao.SucataEquipamento);

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Equipamento {requisicao.IdEquipamento} - Data {requisicao.SucataEquipamento:dd/MM/yyyy}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}