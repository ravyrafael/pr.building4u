using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarEquipamento;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using System.Collections.Generic;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarEquipamento
{
    public class ListarEquipamentosExecutor : IExecutor<ListarEquipamentosRequisicao, ListarEquipamentosResultado>
    {
        private readonly IEquipamentoRepositorio equipamentoRepositorio;

        public ListarEquipamentosExecutor(IEquipamentoRepositorio equipamentoRepositorio)
        {
            this.equipamentoRepositorio = equipamentoRepositorio;
        }

        [Transacao]
        public ListarEquipamentosResultado Executar(ListarEquipamentosRequisicao requisicao)
        {
            var resultado = new ListarEquipamentosResultado();

            var equipamentos = equipamentoRepositorio.ListarEquipamentos(requisicao.codigoOuNome);
            resultado.Equipamentos = Mapper.Map<List<EquipamentoDto>>(equipamentos) ?? new List<EquipamentoDto>();

            return resultado;
        }
    }
}