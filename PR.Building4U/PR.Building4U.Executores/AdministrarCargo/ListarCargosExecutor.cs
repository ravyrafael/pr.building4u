using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCargo;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using System.Collections.Generic;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarCargo
{
    public class ListarCargosExecutor : IExecutor<ListarCargosRequisicao, ListarCargosResultado>
    {
        private readonly ICargoRepositorio cargoRepositorio;

        public ListarCargosExecutor(ICargoRepositorio cargoRepositorio)
        {
            this.cargoRepositorio = cargoRepositorio;
        }

        [Transacao]
        public ListarCargosResultado Executar(ListarCargosRequisicao requisicao)
        {
            var resultado = new ListarCargosResultado();

            var cargos = cargoRepositorio.ListarCargos(requisicao.CodigoOuNome);
            resultado.Cargos = Mapper.Map<List<CargoDto>>(cargos) ?? new List<CargoDto>();

            return resultado;
        }
    }
}