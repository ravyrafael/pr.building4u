using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCargo;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarCargo
{
    public class EditarCargoExecutor : IExecutor<EditarCargoRequisicao, EditarCargoResultado>
    {
        private readonly ICargoRepositorio cargoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarCargoExecutor(ICargoRepositorio cargoRepositorio, ILogRepositorio logRepositorio)
        {
            this.cargoRepositorio = cargoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarCargoResultado Executar(EditarCargoRequisicao requisicao)
        {
            var resultado = new EditarCargoResultado();

            cargoRepositorio.EditarCargo(requisicao.IdCargo, requisicao.NomeCargo, requisicao.SiglaCargo, requisicao.IdCusto);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Cargo {requisicao.IdCargo} - Nome {requisicao.NomeCargo}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}