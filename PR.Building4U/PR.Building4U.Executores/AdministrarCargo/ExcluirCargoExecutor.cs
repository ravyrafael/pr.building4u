using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCargo;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarCargo
{
    public class ExcluirCargoExecutor : IExecutor<ExcluirCargoRequisicao, ExcluirCargoResultado>
    {
        private readonly ICargoRepositorio cargoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirCargoExecutor(ICargoRepositorio cargoRepositorio, ILogRepositorio logRepositorio)
        {
            this.cargoRepositorio = cargoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirCargoResultado Executar(ExcluirCargoRequisicao requisicao)
        {
            var resultado = new ExcluirCargoResultado();

            cargoRepositorio.ExcluirCargo(requisicao.IdCargo);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Cargo {requisicao.IdCargo}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}