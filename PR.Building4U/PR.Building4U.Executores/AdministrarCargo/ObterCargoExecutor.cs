using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCargo;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarCargo
{
    public class ObterCargoExecutor : IExecutor<ObterCargoRequisicao, ObterCargoResultado>
    {
        private readonly ICargoRepositorio cargoRepositorio;

        public ObterCargoExecutor(ICargoRepositorio cargoRepositorio)
        {
            this.cargoRepositorio = cargoRepositorio;
        }

        [Transacao]
        public ObterCargoResultado Executar(ObterCargoRequisicao requisicao)
        {
            var resultado = new ObterCargoResultado();

            var cargo = cargoRepositorio.ObterCargo(requisicao.IdCargo);
            resultado.Cargo = Mapper.Map<CargoDto>(cargo);

            return resultado;
        }
    }
}