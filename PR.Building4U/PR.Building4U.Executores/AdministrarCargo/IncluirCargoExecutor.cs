using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarCargo;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarCargo
{
    public class IncluirCargoExecutor : IExecutor<IncluirCargoRequisicao, IncluirCargoResultado>
    {
        private readonly ICargoRepositorio cargoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirCargoExecutor(ICargoRepositorio cargoRepositorio, ILogRepositorio logRepositorio)
        {
            this.cargoRepositorio = cargoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirCargoResultado Executar(IncluirCargoRequisicao requisicao)
        {
            var resultado = new IncluirCargoResultado();

            resultado.IdCargo = cargoRepositorio.IncluirCargo(requisicao.NomeCargo, requisicao.SiglaCargo, requisicao.IdCusto);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Cargo {resultado.IdCargo} - Nome {requisicao.NomeCargo}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}