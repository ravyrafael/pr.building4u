﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarEstado;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarEstado
{
    public class IncluirEstadoExecutor : IExecutor<IncluirEstadoRequisicao, IncluirEstadoResultado>
    {
        private readonly IEstadoRepositorio estadoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirEstadoExecutor(IEstadoRepositorio estadoRepositorio, ILogRepositorio logRepositorio)
        {
            this.estadoRepositorio = estadoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirEstadoResultado Executar(IncluirEstadoRequisicao requisicao)
        {
            var resultado = new IncluirEstadoResultado();

            resultado.IdEstado = estadoRepositorio.IncluirEstado(requisicao.SiglaEstado, requisicao.NomeEstado, requisicao.IdPais);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Estado {resultado.IdEstado} - Nome {requisicao.NomeEstado}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}
