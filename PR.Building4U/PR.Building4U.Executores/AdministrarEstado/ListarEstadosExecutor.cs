﻿using AutoMapper;
using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarEstado;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarEstado
{
    public class ListarEstadosExecutor : IExecutor<ListarEstadosRequisicao, ListarEstadosResultado>
    {
        private readonly IEstadoRepositorio estadoRepositorio;

        public ListarEstadosExecutor(IEstadoRepositorio estadoRepositorio)
        {
            this.estadoRepositorio = estadoRepositorio;
        }

        [Transacao]
        public ListarEstadosResultado Executar(ListarEstadosRequisicao requisicao)
        {
            var resultado = new ListarEstadosResultado();

            var estados = estadoRepositorio.ListarEstados(requisicao.CodigoOuNome);
            resultado.Estados = Mapper.Map<List<EstadoDto>>(estados) ?? new List<EstadoDto>(); 

            return resultado;
        }
    }
}
