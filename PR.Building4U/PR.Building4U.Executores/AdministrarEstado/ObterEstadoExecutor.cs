﻿using AutoMapper;
using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarEstado;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarEstado 
{
	public class ObterEstadoExecutor : IExecutor<ObterEstadoRequisicao, ObterEstadoResultado>
	{
        private readonly IEstadoRepositorio estadoRepositorio;

        public ObterEstadoExecutor(IEstadoRepositorio estadoRepositorio)
        {
            this.estadoRepositorio = estadoRepositorio;
        }

        [Transacao]
        public ObterEstadoResultado Executar(ObterEstadoRequisicao requisicao)
        {
            var resultado = new ObterEstadoResultado();

            var estado = estadoRepositorio.ObterEstado(requisicao.IdEstado);
            resultado.Estado = Mapper.Map<EstadoDto>(estado);

            return resultado;
        }
	}
}
