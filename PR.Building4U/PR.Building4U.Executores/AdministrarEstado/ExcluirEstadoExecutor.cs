﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarEstado;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarEstado
{
    public class ExcluirEstadoExecutor : IExecutor<ExcluirEstadoRequisicao, ExcluirEstadoResultado>
    {
        private readonly IEstadoRepositorio estadoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirEstadoExecutor(IEstadoRepositorio estadoRepositorio, ILogRepositorio logRepositorio)
        {
            this.estadoRepositorio = estadoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirEstadoResultado Executar(ExcluirEstadoRequisicao requisicao)
        {
            var resultado = new ExcluirEstadoResultado();

            estadoRepositorio.ExcluirEstado(requisicao.IdEstado);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Estado {requisicao.IdEstado}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}
