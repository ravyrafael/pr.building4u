﻿using AutoMapper;
using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarEstado;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarEstado 
{
	public class ListarEstadosPorPaisExecutor : IExecutor<ListarEstadosPorPaisRequisicao, ListarEstadosPorPaisResultado>
	{
        private readonly IEstadoRepositorio estadoRepositorio;

        public ListarEstadosPorPaisExecutor(IEstadoRepositorio estadoRepositorio)
        {
            this.estadoRepositorio = estadoRepositorio;
        }

        [Transacao]
        public ListarEstadosPorPaisResultado Executar(ListarEstadosPorPaisRequisicao requisicao)
        {
            var resultado = new ListarEstadosPorPaisResultado();

            var estados = estadoRepositorio.ListarEstadosPorPais(requisicao.IdPais, requisicao.CodigoOuNome);
            resultado.Estados = Mapper.Map<List<EstadoDto>>(estados) ?? new List<EstadoDto>();

            return resultado;
        }
	}
}
