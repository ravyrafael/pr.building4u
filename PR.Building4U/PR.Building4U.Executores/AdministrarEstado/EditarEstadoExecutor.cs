﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarEstado;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarEstado
{
    public class EditarEstadoExecutor : IExecutor<EditarEstadoRequisicao, EditarEstadoResultado>
    {
        private readonly IEstadoRepositorio estadoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarEstadoExecutor(IEstadoRepositorio estadoRepositorio, ILogRepositorio logRepositorio)
        {
            this.estadoRepositorio = estadoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarEstadoResultado Executar(EditarEstadoRequisicao requisicao)
        {
            var resultado = new EditarEstadoResultado();

            estadoRepositorio.EditarEstado(requisicao.IdEstado, requisicao.NomeEstado, requisicao.IdPais);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Estado {requisicao.IdEstado} - Nome {requisicao.NomeEstado}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}
