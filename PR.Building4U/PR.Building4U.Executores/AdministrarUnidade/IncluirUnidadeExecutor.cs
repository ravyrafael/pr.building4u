using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarUnidade;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarUnidade
{
    public class IncluirUnidadeExecutor : IExecutor<IncluirUnidadeRequisicao, IncluirUnidadeResultado>
    {
        private readonly IUnidadeRepositorio unidadeRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirUnidadeExecutor(IUnidadeRepositorio unidadeRepositorio, ILogRepositorio logRepositorio)
        {
            this.unidadeRepositorio = unidadeRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirUnidadeResultado Executar(IncluirUnidadeRequisicao requisicao)
        {
            var resultado = new IncluirUnidadeResultado();

            resultado.IdUnidade = unidadeRepositorio.IncluirUnidade(requisicao.SiglaUnidade, requisicao.NomeUnidade);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Unidade {resultado.IdUnidade} - Nome {requisicao.NomeUnidade}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}