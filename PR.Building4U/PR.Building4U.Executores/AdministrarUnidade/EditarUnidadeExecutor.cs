using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarUnidade;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarUnidade
{
    public class EditarUnidadeExecutor : IExecutor<EditarUnidadeRequisicao, EditarUnidadeResultado>
    {
        private readonly IUnidadeRepositorio unidadeRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarUnidadeExecutor(IUnidadeRepositorio unidadeRepositorio, ILogRepositorio logRepositorio)
        {
            this.unidadeRepositorio = unidadeRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarUnidadeResultado Executar(EditarUnidadeRequisicao requisicao)
        {
            var resultado = new EditarUnidadeResultado();

            unidadeRepositorio.EditarUnidade(requisicao.IdUnidade, requisicao.SiglaUnidade, requisicao.NomeUnidade);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Unidade {requisicao.IdUnidade} - Nome {requisicao.NomeUnidade}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}