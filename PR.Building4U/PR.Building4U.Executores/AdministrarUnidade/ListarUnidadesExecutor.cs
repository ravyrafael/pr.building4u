using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarUnidade;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarUnidade
{
    public class ListarUnidadesExecutor : IExecutor<ListarUnidadesRequisicao, ListarUnidadesResultado>
    {
        private readonly IUnidadeRepositorio unidadeRepositorio;

        public ListarUnidadesExecutor(IUnidadeRepositorio unidadeRepositorio)
        {
            this.unidadeRepositorio = unidadeRepositorio;
        }

        [Transacao]
        public ListarUnidadesResultado Executar(ListarUnidadesRequisicao requisicao)
        {
            var resultado = new ListarUnidadesResultado();

            var unidades = unidadeRepositorio.ListarUnidades(requisicao.CodigoOuNome);
            resultado.Unidades = Mapper.Map<List<UnidadeDto>>(unidades) ?? new List<UnidadeDto>();

            return resultado;
        }
    }
}