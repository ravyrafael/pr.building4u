using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarUnidade;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;

namespace PR.Building4U.Executores.AdministrarUnidade
{
    public class ObterUnidadeExecutor : IExecutor<ObterUnidadeRequisicao, ObterUnidadeResultado>
    {
        private readonly IUnidadeRepositorio unidadeRepositorio;

        public ObterUnidadeExecutor(IUnidadeRepositorio unidadeRepositorio)
        {
            this.unidadeRepositorio = unidadeRepositorio;
        }

        [Transacao]
        public ObterUnidadeResultado Executar(ObterUnidadeRequisicao requisicao)
        {
            var resultado = new ObterUnidadeResultado();

            var unidade = unidadeRepositorio.ObterUnidade(requisicao.IdUnidade);
            resultado.Unidade = Mapper.Map<UnidadeDto>(unidade);

            return resultado;
        }
    }
}