using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarUnidade;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarUnidade
{
    public class ExcluirUnidadeExecutor : IExecutor<ExcluirUnidadeRequisicao, ExcluirUnidadeResultado>
    {
        private readonly IUnidadeRepositorio unidadeRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirUnidadeExecutor(IUnidadeRepositorio unidadeRepositorio, ILogRepositorio logRepositorio)
        {
            this.unidadeRepositorio = unidadeRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirUnidadeResultado Executar(ExcluirUnidadeRequisicao requisicao)
        {
            var resultado = new ExcluirUnidadeResultado();

            unidadeRepositorio.ExcluirUnidade(requisicao.IdUnidade);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Unidade {requisicao.IdUnidade}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}