using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarServico;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;

namespace PR.Building4U.Executores.AdministrarServico
{
    public class ObterServicoExecutor : IExecutor<ObterServicoRequisicao, ObterServicoResultado>
    {
        private readonly IServicoRepositorio servicoRepositorio;

        public ObterServicoExecutor(IServicoRepositorio servicoRepositorio)
        {
            this.servicoRepositorio = servicoRepositorio;
        }

        [Transacao]
        public ObterServicoResultado Executar(ObterServicoRequisicao requisicao)
        {
            var resultado = new ObterServicoResultado();

            var servico = servicoRepositorio.ObterServico(requisicao.IdServico, null);
            resultado.Servico = Mapper.Map<ServicoDto>(servico);

            return resultado;
        }
    }
}