using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarServico;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarServico
{
    public class IncluirServicoExecutor : IExecutor<IncluirServicoRequisicao, IncluirServicoResultado>
    {
        private readonly IServicoRepositorio servicoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirServicoExecutor(IServicoRepositorio servicoRepositorio, ILogRepositorio logRepositorio)
        {
            this.servicoRepositorio = servicoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirServicoResultado Executar(IncluirServicoRequisicao requisicao)
        {
            var resultado = new IncluirServicoResultado();

            resultado.IdServico = servicoRepositorio.IncluirServico(requisicao.CodigoServico, requisicao.SiglaServico, requisicao.NomeServico, requisicao.IdUnidade);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Servico {resultado.IdServico} - Nome {requisicao.NomeServico}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}