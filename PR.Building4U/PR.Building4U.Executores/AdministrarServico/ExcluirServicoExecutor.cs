using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarServico;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarServico
{
    public class ExcluirServicoExecutor : IExecutor<ExcluirServicoRequisicao, ExcluirServicoResultado>
    {
        private readonly IServicoRepositorio servicoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirServicoExecutor(IServicoRepositorio servicoRepositorio, ILogRepositorio logRepositorio)
        {
            this.servicoRepositorio = servicoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirServicoResultado Executar(ExcluirServicoRequisicao requisicao)
        {
            var resultado = new ExcluirServicoResultado();

            servicoRepositorio.ExcluirServico(requisicao.IdServico);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Servico {requisicao.IdServico}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}