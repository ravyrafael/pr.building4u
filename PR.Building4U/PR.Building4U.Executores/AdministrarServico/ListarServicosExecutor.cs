using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarServico;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarServico
{
    public class ListarServicosExecutor : IExecutor<ListarServicosRequisicao, ListarServicosResultado>
    {
        private readonly IServicoRepositorio servicoRepositorio;

        public ListarServicosExecutor(IServicoRepositorio servicoRepositorio)
        {
            this.servicoRepositorio = servicoRepositorio;
        }

        [Transacao]
        public ListarServicosResultado Executar(ListarServicosRequisicao requisicao)
        {
            var resultado = new ListarServicosResultado();

            var servicos = servicoRepositorio.ListarServicos(requisicao.CodigoOuNome);
            resultado.Servicos = Mapper.Map<List<ServicoDto>>(servicos) ?? new List<ServicoDto>();

            return resultado;
        }
    }
}