using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarServico;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarServico
{
    public class EditarServicoExecutor : IExecutor<EditarServicoRequisicao, EditarServicoResultado>
    {
        private readonly IServicoRepositorio servicoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarServicoExecutor(IServicoRepositorio servicoRepositorio, ILogRepositorio logRepositorio)
        {
            this.servicoRepositorio = servicoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarServicoResultado Executar(EditarServicoRequisicao requisicao)
        {
            var resultado = new EditarServicoResultado();

            servicoRepositorio.EditarServico(requisicao.IdServico, requisicao.SiglaServico, requisicao.NomeServico, requisicao.IdUnidade);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Servico {requisicao.IdServico} - Nome {requisicao.NomeServico}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}