﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarSituacao;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarSituacao
{
    public class EditarSituacaoExecutor : IExecutor<EditarSituacaoRequisicao, EditarSituacaoResultado>
    {
        private readonly ISituacaoRepositorio situacaoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarSituacaoExecutor(ISituacaoRepositorio situacaoRepositorio, ILogRepositorio logRepositorio)
        {
            this.situacaoRepositorio = situacaoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarSituacaoResultado Executar(EditarSituacaoRequisicao requisicao)
        {
            var resultado = new EditarSituacaoResultado();

            situacaoRepositorio.EditarSituacao(requisicao.IdSituacao, requisicao.NomeSituacao);

            resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Situação {requisicao.IdSituacao} - Nome {requisicao.NomeSituacao}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}
