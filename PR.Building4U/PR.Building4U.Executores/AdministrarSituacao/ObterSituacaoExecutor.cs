﻿using AutoMapper;
using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarSituacao;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarSituacao 
{
	public class ObterSituacaoExecutor : IExecutor<ObterSituacaoRequisicao, ObterSituacaoResultado>
	{
        private readonly ISituacaoRepositorio situacaoRepositorio;

        public ObterSituacaoExecutor(ISituacaoRepositorio situacaoRepositorio)
        {
            this.situacaoRepositorio = situacaoRepositorio;
        }

        [Transacao]
        public ObterSituacaoResultado Executar(ObterSituacaoRequisicao requisicao)
        {
            var resultado = new ObterSituacaoResultado();

            var situacao = situacaoRepositorio.ObterSituacao(requisicao.IdSituacao);
            resultado.Situacao = Mapper.Map<SituacaoDto>(situacao);

            return resultado;
        }
	}
}
