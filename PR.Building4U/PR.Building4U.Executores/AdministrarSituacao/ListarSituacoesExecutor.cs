﻿using AutoMapper;
using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarSituacao;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarSituacao 
{
	public class ListarSituacoesExecutor : IExecutor<ListarSituacoesRequisicao, ListarSituacoesResultado>
	{
        private readonly ISituacaoRepositorio situacaoRepositorio;

		public ListarSituacoesExecutor(ISituacaoRepositorio situacaoRepositorio)
        {
            this.situacaoRepositorio = situacaoRepositorio;
        }

        [Transacao]
        public ListarSituacoesResultado Executar(ListarSituacoesRequisicao requisicao)
        {
            var resultado = new ListarSituacoesResultado();

            var situacoes = situacaoRepositorio.ListarSituacoes(requisicao.CodigoOuNome);
            resultado.Situacoes = Mapper.Map<List<SituacaoDto>>(situacoes) ?? new List<SituacaoDto>();

            return resultado;
        }
	}
}
