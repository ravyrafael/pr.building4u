using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarPlanilha;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarPlanilha
{
    public class SalvarMedicaoExecutor : IExecutor<SalvarMedicaoRequisicao, SalvarMedicaoResultado>
    {
        private readonly IPlanilhaRepositorio planilhaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public SalvarMedicaoExecutor(IPlanilhaRepositorio planilhaRepositorio, ILogRepositorio logRepositorio)
        {
            this.planilhaRepositorio = planilhaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public SalvarMedicaoResultado Executar(SalvarMedicaoRequisicao requisicao)
        {
            var resultado = new SalvarMedicaoResultado();

            foreach (var item in requisicao.Planilha.Itens)
            {
                foreach (var medicao in item.Medicoes)
                {
                    if (medicao.IdPlanilhaItemMedicao == 0)
                    {
                        var idPlanilhaItemMedicao = planilhaRepositorio.IncluirMedicao(medicao.IdPlanilhaItem, medicao.PeriodoMedicao, medicao.QuantidadeMedicao.Value, medicao.ValorMedicao);
                        medicao.IdPlanilhaItemMedicao = idPlanilhaItemMedicao;
                    }
                }
            }

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Planilha {requisicao.Planilha.IdPlanilha} - Medi��o {requisicao.PeriodoMedicao.ToString("dd/MM/yyyy")}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}