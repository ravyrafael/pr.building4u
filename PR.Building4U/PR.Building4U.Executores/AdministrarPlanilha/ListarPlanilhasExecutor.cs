using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarPlanilha;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace PR.Building4U.Executores.AdministrarPlanilha
{
    public class ListarPlanilhasExecutor : IExecutor<ListarPlanilhasRequisicao, ListarPlanilhasResultado>
    {
        private readonly IPlanilhaRepositorio planilhaRepositorio;

        public ListarPlanilhasExecutor(IPlanilhaRepositorio planilhaRepositorio)
        {
            this.planilhaRepositorio = planilhaRepositorio;
        }

        [Transacao]
        public ListarPlanilhasResultado Executar(ListarPlanilhasRequisicao requisicao)
        {
            var resultado = new ListarPlanilhasResultado();

            var planilhas = planilhaRepositorio.ListarPlanilhas(requisicao.IdCronograma);
            foreach (var planilha in planilhas)
                planilha.Itens = planilhaRepositorio.ListarItens(planilha.IdPlanilha).ToList();
            resultado.Planilhas = Mapper.Map<List<PlanilhaDto>>(planilhas) ?? new List<PlanilhaDto>();

            return resultado;
        }
    }
}