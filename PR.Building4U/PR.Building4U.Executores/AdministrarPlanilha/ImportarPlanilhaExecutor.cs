using AutoMapper;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarPlanilha;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PR.Building4U.Executores.AdministrarPlanilha
{
    public class ImportarPlanilhaExecutor : IExecutor<ImportarPlanilhaRequisicao, ImportarPlanilhaResultado>
    {
        private readonly IUnidadeRepositorio unidadeRepositorio;
        private readonly IServicoRepositorio servicoRepositorio;

        public ImportarPlanilhaExecutor(IUnidadeRepositorio unidadeRepositorio, IServicoRepositorio servicoRepositorio)
        {
            this.unidadeRepositorio = unidadeRepositorio;
            this.servicoRepositorio = servicoRepositorio;
        }

        [Transacao]
        public ImportarPlanilhaResultado Executar(ImportarPlanilhaRequisicao requisicao)
        {
            var resultado = new ImportarPlanilhaResultado();

            try
            {
                using (var mem = new MemoryStream())
                {
                    mem.Write(requisicao.arquivo, 0, requisicao.arquivo.Length);

                    var planilha = new PlanilhaDto
                    {
                        IdCronograma = requisicao.IdCronograma,
                        CpPlanilha = requisicao.CpPlanilha,
                        RevPlanilha = requisicao.RevPlanilha,
                        Itens = new List<PlanilhaItemDto>()
                    };

                    using (var spreadSheetDocument = SpreadsheetDocument.Open(mem, false))
                    {
                        var sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                        var relationshipId = sheets.First().Id.Value;
                        var worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                        var workSheet = worksheetPart.Worksheet;
                        var sheetData = workSheet.GetFirstChild<SheetData>();
                        var rows = sheetData.Descendants<Row>().Skip(1);

                        foreach (var row in rows)
                        {
                            var item = new PlanilhaItemDto();

                            foreach (var cell in row.Descendants<Cell>())
                            {
                                var i = char.Parse(cell.CellReference.Value.Substring(0, 1)) - 65;
                                var valor = ObterValor(spreadSheetDocument, cell);

                                switch (i)
                                {
                                    case 0: item.EhItemControle = !string.IsNullOrEmpty(valor); break;
                                    case 1:
                                        item.IdServicoAux = valor;
                                        item.NomeCronogramaServico = ObterServico(valor);
                                        break;
                                    case 2: item.CodigoItem = valor; break;
                                    case 3: item.DescricaoItem = valor; break;
                                    case 4: item.Unidade = ObterUnidade(valor); break;
                                    case 5: item.PrecoItem = decimal.Parse(valor.Replace(".", ",")); break;
                                    case 6: item.QuantidadeItem = decimal.Parse(valor.Replace(".", ",")); break;
                                }
                            }

                            item.EhTituloItem = !item.QuantidadeItem.HasValue;
                            planilha.Itens.Add(item);
                        }
                    }

                    resultado.TemInconsistencia = VerificarInconsistencias(planilha);
                    resultado.Sucesso = true;
                    resultado.Planilha = planilha;
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().IsAssignableFrom(typeof(InvalidCastException)) || ex.GetType().IsAssignableFrom(typeof(FormatException)))
                {
                    resultado.Sucesso = false;
                    resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI023);

                    return resultado;
                }
                else
                    throw;
            }

            return resultado;
        }

        private string ObterValor(SpreadsheetDocument document, Cell cell)
        {
            var stringTablePart = document.WorkbookPart.SharedStringTablePart;
            var value = cell.CellValue.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                return stringTablePart.SharedStringTable.ChildElements[int.Parse(value)].InnerText;
            else
                return value;
        }

        private string ObterServico(string codigoServico)
        {
            string nomeServico;

            if (codigoServico.Contains("."))
            {
                var partes = codigoServico.Split('.');
                codigoServico = partes[0];
                var servico = Mapper.Map<ServicoDto>(servicoRepositorio.ObterServico(null, int.Parse(codigoServico)));
                nomeServico = servico?.SiglaServico + $" [{partes[1]}]";
            }
            else
            {
                var servico = Mapper.Map<ServicoDto>(servicoRepositorio.ObterServico(null, int.Parse(codigoServico)));
                nomeServico = servico?.SiglaServico;
            }

            return nomeServico;
        }

        private UnidadeDto ObterUnidade(string siglaUnidade)
        {
            var unidades = unidadeRepositorio.ListarUnidades(siglaUnidade);
            var unidade = Mapper.Map<UnidadeDto>(unidades.FirstOrDefault(u => u.SiglaUnidade.ToLower() == siglaUnidade.ToLower()));

            return unidade;
        }

        private bool VerificarInconsistencias(PlanilhaDto planilha)
        {
            var verifica = false;

            foreach (var item in planilha.Itens)
            {
                if (string.IsNullOrEmpty(item.DescricaoItem) || string.IsNullOrEmpty(item.CodigoItem))
                    verifica = true;

                if (!item.EhTituloItem && (string.IsNullOrEmpty(item.IdServicoAux) || item.Unidade == null))
                    verifica = true;

                if (planilha.Itens.Count(i => i.CodigoItem == item.CodigoItem) > 1)
                    verifica = true;
            }

            return verifica;
        }
    }
}