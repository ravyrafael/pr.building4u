using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarPlanilha;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarPlanilha
{
    public class ExcluirMedicaoExecutor : IExecutor<ExcluirMedicaoRequisicao, ExcluirMedicaoResultado>
    {
        private readonly IPlanilhaRepositorio planilhaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirMedicaoExecutor(IPlanilhaRepositorio planilhaRepositorio, ILogRepositorio logRepositorio)
        {
            this.planilhaRepositorio = planilhaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirMedicaoResultado Executar(ExcluirMedicaoRequisicao requisicao)
        {
            var resultado = new ExcluirMedicaoResultado();

            planilhaRepositorio.ExcluirMedicao(requisicao.IdPlanilha, requisicao.PeriodoMedicao);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Planilha {requisicao.IdPlanilha} - Medi��o {requisicao.PeriodoMedicao.ToString("dd/MM/yyyy")}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}