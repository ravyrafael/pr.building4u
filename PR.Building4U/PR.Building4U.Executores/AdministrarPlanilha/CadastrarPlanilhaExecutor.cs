using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarPlanilha;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarPlanilha
{
	public class CadastrarPlanilhaExecutor : IExecutor<CadastrarPlanilhaRequisicao, CadastrarPlanilhaResultado>
	{
				private readonly IPlanilhaRepositorio planilhaRepositorio;
				private readonly ILogRepositorio logRepositorio;
				
				public CadastrarPlanilhaExecutor(IPlanilhaRepositorio planilhaRepositorio, ILogRepositorio logRepositorio)
        {
			this.planilhaRepositorio = planilhaRepositorio;
            			this.logRepositorio = logRepositorio;
			        }

		[Transacao]
		public CadastrarPlanilhaResultado Executar(CadastrarPlanilhaRequisicao requisicao)
        {
            var resultado = new CadastrarPlanilhaResultado();
            
			            resultado.IdPlanilha = planilhaRepositorio.IncluirPlanilha();

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.sucesso, Mensagens.SUC003);

			logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Planilha {resultado.IdPlanilha} - Nome {requisicao.NomePlanilha}.", TipoLog.Sucesso);
			
            return resultado;
        }
	}
}