using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarPlanilha;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace PR.Building4U.Executores.AdministrarPlanilha
{
    public class SalvarPlanilhaExecutor : IExecutor<SalvarPlanilhaRequisicao, SalvarPlanilhaResultado>
    {
        private readonly IPlanilhaRepositorio planilhaRepositorio;
        private readonly IServicoRepositorio servicoRepositorio;
        private readonly ICronogramaRepositorio cronogramaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public SalvarPlanilhaExecutor(IPlanilhaRepositorio planilhaRepositorio, IServicoRepositorio servicoRepositorio, ICronogramaRepositorio cronogramaRepositorio, ILogRepositorio logRepositorio)
        {
            this.planilhaRepositorio = planilhaRepositorio;
            this.servicoRepositorio = servicoRepositorio;
            this.cronogramaRepositorio = cronogramaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public SalvarPlanilhaResultado Executar(SalvarPlanilhaRequisicao requisicao)
        {
            var resultado = new SalvarPlanilhaResultado();

            var idPlanilha = planilhaRepositorio.IncluirPlanilha(requisicao.Planilha.IdCronograma, requisicao.Planilha.CpPlanilha, requisicao.Planilha.RevPlanilha);
            requisicao.Planilha.IdPlanilha = idPlanilha;

            var listaServicos = requisicao.Planilha.Itens.Where(i => !string.IsNullOrEmpty(i.IdServicoAux)).Select(i => i.IdServicoAux).Distinct();
            var relacaoServico = new Dictionary<string, int>();
            var ordemServico = 0;
            ServicoDto servicoDto;
            string nomeServico;

            foreach (var servico in listaServicos)
            {
                ordemServico++;

                if (servico.Contains("."))
                {
                    var partes = servico.Split('.');
                    var codigoServico = partes[0];

                    servicoDto = Mapper.Map<ServicoDto>(servicoRepositorio.ObterServico(null, int.Parse(codigoServico)));
                    nomeServico = $" [{partes[1]}]";
                }
                else
                {
                    servicoDto = Mapper.Map<ServicoDto>(servicoRepositorio.ObterServico(null, int.Parse(servico)));
                    nomeServico = null;
                }

                var id = cronogramaRepositorio.IncluirServico(requisicao.Planilha.IdCronograma, servicoDto.IdServico, ordemServico,
                    nomeServico, 0, 0, null, 0, 0, null, 0, 0, 0, null, null, null, null, null, null, null, 0);
                relacaoServico.Add(servico, id);
            }

            foreach (var item in requisicao.Planilha.Itens)
            {
                var idCronogramaServico = string.IsNullOrEmpty(item.IdServicoAux) ? (int?)null : relacaoServico[item.IdServicoAux];

                var idPlanilhaItem = planilhaRepositorio.IncluirItem(idCronogramaServico, idPlanilha, item.Unidade?.IdUnidade,
                    item.CodigoItem, item.DescricaoItem, item.PrecoItem, item.QuantidadeItem, item.QuantidadeAcertoItem,
                    item.EhTituloItem, item.EhItemControle);
                item.IdPlanilhaItem = idPlanilhaItem;
            }

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Planilha {requisicao.Planilha.IdPlanilha} - Cronograma {requisicao.Planilha.IdCronograma}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}