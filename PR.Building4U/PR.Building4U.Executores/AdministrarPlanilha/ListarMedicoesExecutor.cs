using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarPlanilha;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarPlanilha
{
    public class ListarMedicoesExecutor : IExecutor<ListarMedicoesRequisicao, ListarMedicoesResultado>
    {
        private readonly IPlanilhaRepositorio planilhaRepositorio;

        public ListarMedicoesExecutor(IPlanilhaRepositorio planilhaRepositorio)
        {
            this.planilhaRepositorio = planilhaRepositorio;
        }

        [Transacao]
        public ListarMedicoesResultado Executar(ListarMedicoesRequisicao requisicao)
        {
            var resultado = new ListarMedicoesResultado();

            var medicoes = planilhaRepositorio.ListarMedicoes(requisicao.IdPlanilha);
            resultado.Medicoes = Mapper.Map<List<MedicaoDto>>(medicoes) ?? new List<MedicaoDto>();

            return resultado;
        }
    }
}