using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarPlanilha;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarPlanilha
{
    public class ExcluirPlanilhaExecutor : IExecutor<ExcluirPlanilhaRequisicao, ExcluirPlanilhaResultado>
    {
        private readonly IPlanilhaRepositorio planilhaRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirPlanilhaExecutor(IPlanilhaRepositorio planilhaRepositorio, ILogRepositorio logRepositorio)
        {
            this.planilhaRepositorio = planilhaRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirPlanilhaResultado Executar(ExcluirPlanilhaRequisicao requisicao)
        {
            var resultado = new ExcluirPlanilhaResultado();

            planilhaRepositorio.ExcluirItens(requisicao.IdPlanilha);
            planilhaRepositorio.ExcluirPlanilha(requisicao.IdPlanilha);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Planilha {requisicao.IdPlanilha}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}