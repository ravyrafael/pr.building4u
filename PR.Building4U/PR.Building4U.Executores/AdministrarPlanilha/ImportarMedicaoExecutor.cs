using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarPlanilha;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;
using AutoMapper;
using System.Collections.Generic;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Linq;
using System;

namespace PR.Building4U.Executores.AdministrarPlanilha
{
    public class ImportarMedicaoExecutor : IExecutor<ImportarMedicaoRequisicao, ImportarMedicaoResultado>
    {
        private readonly IPlanilhaRepositorio planilhaRepositorio;

        public ImportarMedicaoExecutor(IPlanilhaRepositorio planilhaRepositorio)
        {
            this.planilhaRepositorio = planilhaRepositorio;
        }

        [Transacao]
        public ImportarMedicaoResultado Executar(ImportarMedicaoRequisicao requisicao)
        {
            var resultado = new ImportarMedicaoResultado();

            try
            {
                using (var mem = new MemoryStream())
                {
                    mem.Write(requisicao.arquivo, 0, requisicao.arquivo.Length);

                    var planilha = ObterPlanilha(requisicao.IdPlanilha);
                    var itensMedicao = new List<PlanilhaItemMedicaoDto>();

                    using (var spreadSheetDocument = SpreadsheetDocument.Open(mem, false))
                    {
                        var sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                        var relationshipId = sheets.First().Id.Value;
                        var worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                        var workSheet = worksheetPart.Worksheet;
                        var sheetData = workSheet.GetFirstChild<SheetData>();
                        var rows = sheetData.Descendants<Row>().Skip(1);

                        foreach (var row in rows)
                        {
                            var item = new PlanilhaItemMedicaoDto { PeriodoMedicao = new DateTime(requisicao.PeriodoMedicao.Year, requisicao.PeriodoMedicao.Month, 1) };
                            var itemPlanilha = new PlanilhaItemDto();

                            foreach (var cell in row.Descendants<Cell>())
                            {
                                var i = char.Parse(cell.CellReference.Value.Substring(0, 1)) - 65;
                                var valor = ObterValor(spreadSheetDocument, cell);

                                switch (i)
                                {
                                    case 0:
                                        itemPlanilha = ObterItemPlanilha(valor, planilha);
                                        item.IdPlanilhaItem = itemPlanilha?.IdPlanilhaItem ?? 0;
                                        break;
                                    case 3:
                                        item.QuantidadeMedicao = decimal.Parse(valor.Replace(".", ","));
                                        item.ValorMedicao = (itemPlanilha?.PrecoItem ?? 0) * (item.QuantidadeMedicao ?? 0);
                                        break;
                                }
                            }

                            if (itemPlanilha != null && item.QuantidadeMedicao.HasValue)
                                itensMedicao.Add(item);
                        }
                    }

                    foreach (var item in planilha.Itens)
                    {
                        if (item.Medicoes == null)
                            item.Medicoes = new List<PlanilhaItemMedicaoDto>();
                        var medicao = itensMedicao.FirstOrDefault(i => i.IdPlanilhaItem == item.IdPlanilhaItem);
                        if (medicao != null)
                            item.Medicoes.Add(medicao);
                    }

                    resultado.TemInconsistencia = VerificarInconsistencias(itensMedicao);
                    resultado.Sucesso = true;
                    resultado.Planilha = planilha;
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().IsAssignableFrom(typeof(InvalidCastException)) || ex.GetType().IsAssignableFrom(typeof(FormatException)))
                {
                    resultado.Sucesso = false;
                    resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI023);

                    return resultado;
                }
                else
                    throw;
            }

            return resultado;
        }

        private PlanilhaDto ObterPlanilha(int idPlanilha)
        {
            var planilha = planilhaRepositorio.ObterPlanilha(idPlanilha);
            planilha.Itens = planilhaRepositorio.ListarItens(planilha.IdPlanilha).ToList();
            var planilhaDto = Mapper.Map<PlanilhaDto>(planilha) ?? new PlanilhaDto();

            return planilhaDto;
        }

        private string ObterValor(SpreadsheetDocument document, Cell cell)
        {
            var stringTablePart = document.WorkbookPart.SharedStringTablePart;
            var value = cell.CellValue.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                return stringTablePart.SharedStringTable.ChildElements[int.Parse(value)].InnerText;
            else
                return value;
        }

        private PlanilhaItemDto ObterItemPlanilha(string codigoItem, PlanilhaDto planilha)
        {
            return planilha.Itens.FirstOrDefault(i => i.CodigoItem == codigoItem);
        }

        private bool VerificarInconsistencias(List<PlanilhaItemMedicaoDto> itens)
        {
            var verifica = false;

            foreach (var item in itens)
            {
                if (item.IdPlanilhaItem == 0)
                    verifica = true;

                if (item.PeriodoMedicao == DateTime.MinValue)
                    verifica = true;
            }

            return verifica;
        }
    }
}