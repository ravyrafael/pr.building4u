using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAditivo;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.SDK.Transacao;
using AutoMapper;
using System.Collections.Generic;

namespace PR.Building4U.Executores.AdministrarAditivo
{
    public class ListarAditivosPorContratoExecutor : IExecutor<ListarAditivosPorContratoRequisicao, ListarAditivosPorContratoResultado>
    {
        private readonly IAditivoRepositorio aditivoRepositorio;

        public ListarAditivosPorContratoExecutor(IAditivoRepositorio aditivoRepositorio)
        {
            this.aditivoRepositorio = aditivoRepositorio;
        }

        [Transacao]
        public ListarAditivosPorContratoResultado Executar(ListarAditivosPorContratoRequisicao requisicao)
        {
            var resultado = new ListarAditivosPorContratoResultado();

            var aditivos = aditivoRepositorio.ListarAditivosPorContrato(requisicao.IdContrato);
            resultado.Aditivos = Mapper.Map<List<AditivoDto>>(aditivos) ?? new List<AditivoDto>();

            return resultado;
        }
    }
}