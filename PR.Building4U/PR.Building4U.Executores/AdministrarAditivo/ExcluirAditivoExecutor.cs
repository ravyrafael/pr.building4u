using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAditivo;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarAditivo
{
    public class ExcluirAditivoExecutor : IExecutor<ExcluirAditivoRequisicao, ExcluirAditivoResultado>
    {
        private readonly IAditivoRepositorio aditivoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirAditivoExecutor(IAditivoRepositorio aditivoRepositorio, ILogRepositorio logRepositorio)
        {
            this.aditivoRepositorio = aditivoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirAditivoResultado Executar(ExcluirAditivoRequisicao requisicao)
        {
            var resultado = new ExcluirAditivoResultado();

            aditivoRepositorio.ExcluirAditivo(requisicao.IdAditivo);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Aditivo {requisicao.IdAditivo}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}