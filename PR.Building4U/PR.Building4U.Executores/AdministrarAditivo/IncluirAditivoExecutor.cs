using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAditivo;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarAditivo
{
    public class IncluirAditivoExecutor : IExecutor<IncluirAditivoRequisicao, IncluirAditivoResultado>
    {
        private readonly IAditivoRepositorio aditivoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirAditivoExecutor(IAditivoRepositorio aditivoRepositorio, ILogRepositorio logRepositorio)
        {
            this.aditivoRepositorio = aditivoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirAditivoResultado Executar(IncluirAditivoRequisicao requisicao)
        {
            var resultado = new IncluirAditivoResultado();

            resultado.IdAditivo = aditivoRepositorio.IncluirAditivo(requisicao.IdContrato, requisicao.CodigoAditivo, requisicao.DataAditivo, requisicao.ValorAditivo, requisicao.PrazoAditivo, requisicao.EscopoAditivo);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Aditivo {resultado.IdAditivo} - Contrato {requisicao.IdContrato}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}