using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAluguel;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Util;
using System;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarAluguel
{
    public class DesfazerTransferenciaEquipExecutor : IExecutor<DesfazerTransferenciaEquipRequisicao, DesfazerTransferenciaEquipResultado>
    {
        private readonly IAluguelRepositorio aluguelRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public DesfazerTransferenciaEquipExecutor(IAluguelRepositorio aluguelRepositorio, ILogRepositorio logRepositorio)
        {
            this.aluguelRepositorio = aluguelRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public DesfazerTransferenciaEquipResultado Executar(DesfazerTransferenciaEquipRequisicao requisicao)
        {
            var resultado = new DesfazerTransferenciaEquipResultado();

            var aluguel = aluguelRepositorio.ObterAluguelPorEquipamento(requisicao.IdEquipamento);

            if (aluguel.InicioAluguel < DateTime.Now.Date)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI005);
            else
            {
                aluguelRepositorio.ReativarAluguel(requisicao.IdEquipamento, requisicao.SequenciaAluguel - 1);
                aluguelRepositorio.ExcluirAluguel(requisicao.IdEquipamento, requisicao.SequenciaAluguel);

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Equipamento {requisicao.IdEquipamento} - Sequ�ncia {requisicao.SequenciaAluguel}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}