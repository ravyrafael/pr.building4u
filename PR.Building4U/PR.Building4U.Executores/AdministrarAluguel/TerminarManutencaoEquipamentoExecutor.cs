using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAluguel;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Util;
using PR.Building4U.Util.Enumeracoes;
using System;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarAluguel
{
    public class TerminarManutencaoEquipamentoExecutor : IExecutor<TerminarManutencaoEquipamentoRequisicao, TerminarManutencaoEquipamentoResultado>
    {
        private readonly IEquipamentoRepositorio equipamentoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public TerminarManutencaoEquipamentoExecutor(IEquipamentoRepositorio equipamentoRepositorio, ILogRepositorio logRepositorio)
        {
            this.equipamentoRepositorio = equipamentoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public TerminarManutencaoEquipamentoResultado Executar(TerminarManutencaoEquipamentoRequisicao requisicao)
        {
            var resultado = new TerminarManutencaoEquipamentoResultado();

            var manutencao = equipamentoRepositorio.ObterManutencaoEquipamento(requisicao.IdEquipamento);

            if (requisicao.TerminoManutencao < manutencao.InicioManutencao.AddDays(1))
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI018);
            else if (requisicao.TerminoManutencao > DateTime.Now.Date)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI019);
            else
            {
                equipamentoRepositorio.TerminarManutencaoEquipamento(requisicao.IdEquipamento, requisicao.TerminoManutencao);
                equipamentoRepositorio.AlterarSituacaoEquipamento(requisicao.IdEquipamento, (int)Situacao.Funcionando);

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Equipamento {requisicao.IdEquipamento} - T�rmino {requisicao.TerminoManutencao:dd/MM/yyyy}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}