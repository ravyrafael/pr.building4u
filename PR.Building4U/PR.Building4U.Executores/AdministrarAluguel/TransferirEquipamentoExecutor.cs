using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAluguel;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;
using System;

namespace PR.Building4U.Executores.AdministrarAluguel
{
    public class TransferirEquipamentoExecutor : IExecutor<TransferirEquipamentoRequisicao, TransferirEquipamentoResultado>
    {
        private readonly IAluguelRepositorio aluguelRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public TransferirEquipamentoExecutor(IAluguelRepositorio aluguelRepositorio, ILogRepositorio logRepositorio)
        {
            this.aluguelRepositorio = aluguelRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public TransferirEquipamentoResultado Executar(TransferirEquipamentoRequisicao requisicao)
        {
            var resultado = new TransferirEquipamentoResultado();

            var aluguel = aluguelRepositorio.ObterAluguelPorEquipamento(requisicao.IdEquipamento);

            if (requisicao.IdObra == aluguel.IdObra)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI003);
            else if (requisicao.DataTransferencia <= aluguel.InicioAluguel)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI004);
            else
            {
                aluguelRepositorio.ExcluirReserva(requisicao.IdEquipamento);

                var tipo = "Reserva";
                if (requisicao.DataTransferencia > DateTime.Now)
                    aluguelRepositorio.IncluirReserva(requisicao.IdEquipamento, requisicao.IdObra, requisicao.DataTransferencia);
                else
                {
                    tipo = "Aloca��o";
                    aluguelRepositorio.IncluirAluguel(requisicao.IdEquipamento, requisicao.IdObra, requisicao.SequenciaAluguel + 1, requisicao.DataTransferencia);
                    aluguelRepositorio.FinalizarAluguel(requisicao.IdEquipamento, requisicao.SequenciaAluguel, requisicao.DataTransferencia.AddDays(-1));
                }

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"{tipo} - Equipamento {requisicao.IdEquipamento} - Obra {requisicao.IdObra}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}