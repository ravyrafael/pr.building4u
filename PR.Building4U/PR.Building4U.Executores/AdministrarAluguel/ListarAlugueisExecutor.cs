using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAluguel;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using System.Collections.Generic;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarAluguel
{
    public class ListarAlugueisExecutor : IExecutor<ListarAlugueisRequisicao, ListarAlugueisResultado>
    {
        private readonly IAluguelRepositorio aluguelRepositorio;

        public ListarAlugueisExecutor(IAluguelRepositorio aluguelRepositorio)
        {
            this.aluguelRepositorio = aluguelRepositorio;
        }

        [Transacao]
        public ListarAlugueisResultado Executar(ListarAlugueisRequisicao requisicao)
        {
            var resultado = new ListarAlugueisResultado();

            var alugueis = aluguelRepositorio.ListarAlugueis(requisicao.Obras, requisicao.TiposEquipamento);
            resultado.Alugueis = Mapper.Map<List<AluguelDto>>(alugueis) ?? new List<AluguelDto>();

            return resultado;
        }
    }
}