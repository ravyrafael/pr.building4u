using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAluguel;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarAluguel
{
    public class CancelarDestinoEquipExecutor : IExecutor<CancelarDestinoEquipRequisicao, CancelarDestinoEquipResultado>
    {
        private readonly IAluguelRepositorio aluguelRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public CancelarDestinoEquipExecutor(IAluguelRepositorio aluguelRepositorio, ILogRepositorio logRepositorio)
        {
            this.aluguelRepositorio = aluguelRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public CancelarDestinoEquipResultado Executar(CancelarDestinoEquipRequisicao requisicao)
        {
            var resultado = new CancelarDestinoEquipResultado();

            var temDestino = aluguelRepositorio.VerificarDestino(requisicao.IdEquipamento) > 0;

            if (!temDestino)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI020);
            else
            {
                aluguelRepositorio.ExcluirReserva(requisicao.IdEquipamento);

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Equipamento {requisicao.IdEquipamento}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}