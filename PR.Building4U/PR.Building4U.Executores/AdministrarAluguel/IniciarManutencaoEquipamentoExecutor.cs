using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarAluguel;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Util;
using PR.Building4U.Util.Enumeracoes;
using System;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarAluguel
{
    public class IniciarManutencaoEquipamentoExecutor : IExecutor<IniciarManutencaoEquipamentoRequisicao, IniciarManutencaoEquipamentoResultado>
    {
        private readonly IEquipamentoRepositorio equipamentoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IniciarManutencaoEquipamentoExecutor(IEquipamentoRepositorio equipamentoRepositorio, ILogRepositorio logRepositorio)
        {
            this.equipamentoRepositorio = equipamentoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IniciarManutencaoEquipamentoResultado Executar(IniciarManutencaoEquipamentoRequisicao requisicao)
        {
            var resultado = new IniciarManutencaoEquipamentoResultado();

            var manutencao = equipamentoRepositorio.ObterManutencaoEquipamento(requisicao.IdEquipamento);

            if (manutencao != null && requisicao.InicioManutencao < manutencao.TerminoManutencao.AddDays(1))
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI015);
            else if (requisicao.InicioManutencao > DateTime.Now.Date)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI016);
            else if (requisicao.TerminoManutencao <= DateTime.Now.Date)
                resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI017);
            else
            {
                equipamentoRepositorio.IniciarManutencaoEquipamento(requisicao.IdEquipamento, requisicao.InicioManutencao, requisicao.TerminoManutencao);
                equipamentoRepositorio.AlterarSituacaoEquipamento(requisicao.IdEquipamento, (int)Situacao.Manutencao);

                resultado.Sucesso = true;
                resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC004);

                logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Equipamento {requisicao.IdEquipamento} - In�cio {requisicao.InicioManutencao:dd/MM/yyyy}.", TipoLog.Sucesso);
            }

            return resultado;
        }
    }
}