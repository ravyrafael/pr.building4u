using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarTipoEquipamento
{
    public class ObterTipoEquipamentoExecutor : IExecutor<ObterTipoEquipamentoRequisicao, ObterTipoEquipamentoResultado>
    {
        private readonly ITipoEquipamentoRepositorio tipoEquipamentoRepositorio;

        public ObterTipoEquipamentoExecutor(ITipoEquipamentoRepositorio tipoEquipamentoRepositorio)
        {
            this.tipoEquipamentoRepositorio = tipoEquipamentoRepositorio;
        }

        [Transacao]
        public ObterTipoEquipamentoResultado Executar(ObterTipoEquipamentoRequisicao requisicao)
        {
            var resultado = new ObterTipoEquipamentoResultado();

            var tipoEquipamento = tipoEquipamentoRepositorio.ObterTipoEquipamento(requisicao.IdTipoEquipamento);
            resultado.TipoEquipamento = Mapper.Map<TipoEquipamentoDto>(tipoEquipamento);

            return resultado;
        }
    }
}