using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Fronteiras.Dtos;
using AutoMapper;
using System.Collections.Generic;
using PR.Building4U.SDK.Transacao;

namespace PR.Building4U.Executores.AdministrarTipoEquipamento
{
    public class ListarTiposEquipamentoExecutor : IExecutor<ListarTiposEquipamentoRequisicao, ListarTiposEquipamentoResultado>
    {
        private readonly ITipoEquipamentoRepositorio tipoEquipamentoRepositorio;

        public ListarTiposEquipamentoExecutor(ITipoEquipamentoRepositorio tipoEquipamentoRepositorio)
        {
            this.tipoEquipamentoRepositorio = tipoEquipamentoRepositorio;
        }

        [Transacao]
        public ListarTiposEquipamentoResultado Executar(ListarTiposEquipamentoRequisicao requisicao)
        {
            var resultado = new ListarTiposEquipamentoResultado();

            var tipoEquipamentos = tipoEquipamentoRepositorio.ListarTipoEquipamentos(requisicao.CodigoOuNome);
            resultado.TiposEquipamento = Mapper.Map<List<TipoEquipamentoDto>>(tipoEquipamentos) ?? new List<TipoEquipamentoDto>();

            return resultado;
        }
    }
}