using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarTipoEquipamento
{
    public class EditarTipoEquipamentoExecutor : IExecutor<EditarTipoEquipamentoRequisicao, EditarTipoEquipamentoResultado>
    {
        private readonly ITipoEquipamentoRepositorio tipoEquipamentoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public EditarTipoEquipamentoExecutor(ITipoEquipamentoRepositorio tipoEquipamentoRepositorio, ILogRepositorio logRepositorio)
        {
            this.tipoEquipamentoRepositorio = tipoEquipamentoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public EditarTipoEquipamentoResultado Executar(EditarTipoEquipamentoRequisicao requisicao)
        {
            var resultado = new EditarTipoEquipamentoResultado();

            tipoEquipamentoRepositorio.EditarTipoEquipamento(requisicao.IdTipoEquipamento, requisicao.NomeTipoEquipamento, requisicao.IdCusto);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC001);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Tipo {requisicao.IdTipoEquipamento} - Nome {requisicao.NomeTipoEquipamento}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}