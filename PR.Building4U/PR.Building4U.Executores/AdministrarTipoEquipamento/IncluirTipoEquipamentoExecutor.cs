using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarTipoEquipamento
{
    public class IncluirTipoEquipamentoExecutor : IExecutor<IncluirTipoEquipamentoRequisicao, IncluirTipoEquipamentoResultado>
    {
        private readonly ITipoEquipamentoRepositorio tipoEquipamentoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public IncluirTipoEquipamentoExecutor(ITipoEquipamentoRepositorio tipoEquipamentoRepositorio, ILogRepositorio logRepositorio)
        {
            this.tipoEquipamentoRepositorio = tipoEquipamentoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public IncluirTipoEquipamentoResultado Executar(IncluirTipoEquipamentoRequisicao requisicao)
        {
            var resultado = new IncluirTipoEquipamentoResultado();

            resultado.IdTipoEquipamento = tipoEquipamentoRepositorio.IncluirTipoEquipamento(requisicao.NomeTipoEquipamento, requisicao.IdCusto);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC003);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Tipo {resultado.IdTipoEquipamento} - Nome {requisicao.NomeTipoEquipamento}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}