using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.SDK.Transacao;
using PR.Building4U.Util;

namespace PR.Building4U.Executores.AdministrarTipoEquipamento
{
    public class ExcluirTipoEquipamentoExecutor : IExecutor<ExcluirTipoEquipamentoRequisicao, ExcluirTipoEquipamentoResultado>
    {
        private readonly ITipoEquipamentoRepositorio tipoEquipamentoRepositorio;
        private readonly ILogRepositorio logRepositorio;

        public ExcluirTipoEquipamentoExecutor(ITipoEquipamentoRepositorio tipoEquipamentoRepositorio, ILogRepositorio logRepositorio)
        {
            this.tipoEquipamentoRepositorio = tipoEquipamentoRepositorio;
            this.logRepositorio = logRepositorio;
        }

        [Transacao]
        public ExcluirTipoEquipamentoResultado Executar(ExcluirTipoEquipamentoRequisicao requisicao)
        {
            var resultado = new ExcluirTipoEquipamentoResultado();

            tipoEquipamentoRepositorio.ExcluirTipoEquipamento(requisicao.IdTipoEquipamento);

            resultado.Sucesso = true;
            resultado.Mensagem = new Mensagem(TipoMensagem.Sucesso, Mensagens.SUC002);

            logRepositorio.IncluirLog(requisicao.InformacoesLog, $"Tipo {requisicao.IdTipoEquipamento}.", TipoLog.Sucesso);

            return resultado;
        }
    }
}