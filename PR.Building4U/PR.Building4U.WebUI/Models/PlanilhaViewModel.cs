﻿using PR.Building4U.Fronteiras.Dtos;

namespace PR.Building4U.WebUI.Models
{
    public class PlanilhaViewModel
    {
        public PlanilhaDto Planilha { get; set; }
    }
}