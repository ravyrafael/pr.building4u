﻿using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;

namespace PR.Building4U.WebUI.Models
{
    public class NotificacoesViewModel
    {
        public NotificacoesViewModel()
        {
            Notificacoes = new List<NotificacaoDto>();
        }

        public List<NotificacaoDto> Notificacoes { get; set; }
    }
}