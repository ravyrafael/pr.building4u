﻿namespace PR.Building4U.WebUI.Models
{
    public class ButtonViewModel
    {
        public string Id { get; set; }
        public string Click { get; set; }
        public string Text { get; set; }
        public string Icon { get; set; }
        public string Color { get; set; }
        public string Classe { get; set; }
    }
}