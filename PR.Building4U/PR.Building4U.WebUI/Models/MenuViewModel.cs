﻿using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;

namespace PR.Building4U.WebUI.Models
{
    public class MenuViewModel
    {
        public MenuViewModel()
        {
            ItensMenu = new List<RecursoDto>();
        }

        public List<RecursoDto> ItensMenu { get; set; }
    }
}