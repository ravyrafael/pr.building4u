﻿using PR.Building4U.Fronteiras.Dtos;
using System.Collections.Generic;

namespace PR.Building4U.WebUI.Models
{
    public class CronogramaViewModel
    {
        public List<CronogramaDto> Cronogramas { get; set; }

        public int? IdCronograma { get; set; }
    }
}