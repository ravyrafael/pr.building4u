﻿using PR.Building4U.Fronteiras.Dtos;
using System;

namespace PR.Building4U.WebUI.Models
{
    public class MedicaoViewModel
    {
        public PlanilhaDto Planilha { get; set; }
        public DateTime PeriodoMedicao { get; set; }
    }
}