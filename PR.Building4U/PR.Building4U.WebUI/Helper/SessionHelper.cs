﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarUsuario;
using PR.Building4U.SDK.InversaoControle;
using System;
using System.Web;

namespace PR.Building4U.WebUI.Helper
{
    public static class SessionHelper
    {
        public static bool Logado
        {
            get { return HttpContext.Current.Session["Usuario"] != null; }
        }

        public static UsuarioDto Usuario
        {
            get { return ObterUsuario(HttpContext.Current.Session["Usuario"].ToString()); }
        }

        internal static bool PossuiPermissao(string controller, string[] actions)
        {
            bool autorizado = false;

            if (!Logado)
                return false;
            else
            {
                var permissao = Usuario.Permissao.IdPermissao;

                var requisicao = new VerificarPermissaoUsuarioRequisicao {
                    Permissao = permissao,
                    Controller = controller,
                    Actions = actions
                };
                var executor = ResolvedorDeDependencias.Instance().ObterInstanciaDe<IExecutor<VerificarPermissaoUsuarioRequisicao, VerificarPermissaoUsuarioResultado>>();
                var resultado = executor.Executar(requisicao);

                autorizado = resultado.TemPermissao;
            }

            return autorizado;
        }

        public static void CarregarUsuarioNaSessao(string loginUsuario, string senhaUsuario)
        {
            string chaveAutenticacao = System.Configuration.ConfigurationManager.AppSettings["ChaveCripto"];

            if (AutenticarUsuario(loginUsuario, senhaUsuario, chaveAutenticacao))
                HttpContext.Current.Session["Usuario"] = loginUsuario;
        }

        public static void InicializarUsuario()
        {
            var loginUsuario = System.Configuration.ConfigurationManager.AppSettings["UsuarioPadrao"];
            HttpContext.Current.Session["Usuario"] = loginUsuario;
        }

        private static UsuarioDto ObterUsuario(string login)
        {
            var requisicao = new ObterUsuarioPorCodigoRequisicao { LoginUsuario = login };
            var executor = ResolvedorDeDependencias.Instance().ObterInstanciaDe<IExecutor<ObterUsuarioPorCodigoRequisicao, ObterUsuarioPorCodigoResultado>>();
            var resultado = executor.Executar(requisicao);

            if (resultado.Usuario == null)
                throw new AccessViolationException($"O usuário {login} não está cadastrado.");

            return resultado.Usuario;
        }

        private static bool AutenticarUsuario(string login, string senha, string chave)
        {
            var requisicao = new AutenticarUsuarioRequisicao { LoginUsuario = login, SenhaUsuario = senha, ChaveAutenticacao = chave };
            var executor = ResolvedorDeDependencias.Instance().ObterInstanciaDe<IExecutor<AutenticarUsuarioRequisicao, AutenticarUsuarioResultado>>();
            var resultado = executor.Executar(requisicao);

            return resultado.IdUsuario.HasValue;
        }
    }
}