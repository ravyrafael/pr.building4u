﻿using PR.Building4U.Fronteiras;
using PR.Building4U.SDK.InversaoControle;
using PR.Building4U.Util;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Helper
{
    public static class ControllersHelper
    {
        public static string RenderRazorViewToString(this Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        private static bool CheckPropertyValue(object instance, List<string> columns, string word)
        {
            if (instance != null && columns.Any() && word != null)
            {
                foreach (var column in columns)
                {
                    if (column.Contains(".")) // complex type nested
                    {
                        var columnSplit = column.Split(new char[] { '.' }, 2);
                        var secondInstance = instance.GetType().GetProperty(columnSplit[0]).GetValue(instance, null);
                        var secondColumns = new List<string>() { columnSplit[1] };

                        if (CheckPropertyValue(secondInstance, secondColumns, word))
                            return true;
                    }
                    else
                    {
                        var property = instance.GetType().GetProperty(column);
                        var value = property.GetValue(instance, null)?.ToString();

                        if (!string.IsNullOrEmpty(value) && value.IndexOf(word, StringComparison.CurrentCultureIgnoreCase) >= 0)
                            return true;
                    }
                }
            }

            return false;
        }

        public static object RenderDatatableToView<T>(this Controller controller, DataTableAjaxPostModel model, IEnumerable<T> lista)
        {
            var totalCount = lista.Count();

            #region Filtering

            if (!string.IsNullOrEmpty(model.search?.value))
            {
                var columns = model.columns.Where(x => x.data != null).Select(x => x.data).ToList();
                var value = model.search.value.Trim();

                lista = lista.Where(m => CheckPropertyValue(m, columns, value)).ToList();
            }

            var filteredCount = lista.Count();

            #endregion

            #region Sorting

            if (model.order != null)
            {
                var sortedColumns = model.order;
                var orderByString = new StringBuilder();

                foreach (var column in sortedColumns)
                {
                    orderByString.Append(orderByString.Length > 0 ? "," : "");
                    orderByString.Append(model.columns[column.column].data + " " + column.dir);
                }

                var orderDefault = model.columns[0].data + " asc";

                lista = lista.OrderBy(orderByString.Length > 0 ? orderByString.ToString() : orderDefault);
            }

            #endregion

            #region Paging

            lista = lista.Skip(model.start).Take(model.length);

            #endregion

            return new
            {
                //draw = model.draw, // desabilitado para funcionar o reload
                recordsTotal = totalCount,
                recordsFiltered = filteredCount,
                data = lista
            };
        }

        public static InformacoesLog GetInformacoesLog(this Controller controller)
        {
            return new InformacoesLog
            {
                IdUsuario = SessionHelper.Usuario.IdUsuario,
                LoginUsuario = SessionHelper.Usuario.LoginUsuario,
                Action = controller.ControllerContext.RouteData.Values["action"].ToString(),
                Controller = controller.ControllerContext.RouteData.Values["controller"].ToString(),
                DataLog = DateTime.Now
            };
        }

        public static T ObterInstanciaDe<T>(this Controller controller)
        {
            return ResolvedorDeDependencias.Instance().ObterInstanciaDe<T>();
        }

        public static IExecutor<Requisicao, Resultado> CriarExecutor<Requisicao, Resultado>(this Controller controller) where Resultado : IResultado
        {
            return ObterInstanciaDe<IExecutor<Requisicao, Resultado>>(controller);
        }
    }
}