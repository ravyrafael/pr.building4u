﻿using PR.Building4U.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace PR.Building4U.WebUI.Helper
{
    public static class ComponentHelper
    {
        #region Métodos Internos

        internal static RouteValueDictionary PreencherAtributos(object atributos)
        {
            var dicionarioAtributos = HtmlHelper.AnonymousObjectToHtmlAttributes(atributos);
            return dicionarioAtributos;
        }

        internal static TagBuilder CriarTag(string tipoTag, string conteudo = "", string classe = "", Dictionary<string, string> dicionarioAtributos = null)
        {
            var tag = new TagBuilder(tipoTag);
            tag.AddCssClass(classe);
            tag.InnerHtml = conteudo;

            if (dicionarioAtributos != null)
            {
                foreach (KeyValuePair<string, string> par in dicionarioAtributos)
                {
                    tag.Attributes.Add(par.Key, par.Value);
                }
            }

            return tag;
        }

        internal static TagBuilder CriarTagLabel(string label, bool obrigatorio, string nome, string classe)
        {
            var formato = "{0}{1}{2}";
            var conteudo = string.Empty;
            if (string.IsNullOrWhiteSpace(label))
                conteudo = label;
            else
                conteudo = string.Format(formato, label, obrigatorio ? CriarTag("span", "*", "text-danger").ToString() : "", ":");

            var dicionarioAtributos = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(nome))
                dicionarioAtributos.Add("for", nome);

            var tagLabel = CriarTag("label", conteudo, classe, dicionarioAtributos);

            return tagLabel;
        }

        internal static TagBuilder CriarTagLabelSimples(string label, string nome, string classe)
        {
            var dicionarioAtributos = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(nome))
                dicionarioAtributos.Add("for", nome);

            var tagLabel = CriarTag("label", label, classe, dicionarioAtributos);

            return tagLabel;
        }

        internal static MvcHtmlString CriarDivSemLabel(MvcHtmlString elemento, bool addOn, string icone)
        {
            TagBuilder tagInput;

            if (addOn)
            {
                var tagI = CriarTag("i", "", "ti-" + icone);
                var tagSpan = CriarTag("span", tagI.ToString(), "input-group-text");
                var tagDiv = CriarTag("div", tagSpan.ToString(), "input-group-prepend");
                var divGroup = CriarTag("div", tagDiv.ToString() + elemento.ToString(), "input-group");
                tagInput = CriarTag("div", divGroup.ToString(), "form-group");
            }
            else
                tagInput = CriarTag("div", elemento.ToString(), "form-group");

            return new MvcHtmlString(tagInput.ToString());
        }

        internal static MvcHtmlString CriarDivComLabel(MvcHtmlString elemento, string nome, string label, bool obrigatorio, bool addOn, string icone)
        {
            TagBuilder tagInput;
            TagBuilder tagLabel = CriarTagLabel(label, obrigatorio, nome, null);

            if (addOn)
            {
                var tagI = CriarTag("i", "", "ti-" + icone);
                var tagSpan = CriarTag("span", tagI.ToString(), "input-group-text");
                var tagDiv = CriarTag("div", tagSpan.ToString(), "input-group-prepend");
                var divGroup = CriarTag("div", tagDiv.ToString() + elemento.ToString(), "input-group");
                tagInput = CriarTag("div", tagLabel.ToString() + divGroup.ToString(), "form-group");
            }
            else
                tagInput = CriarTag("div", tagLabel.ToString() + elemento.ToString(), "form-group");

            return new MvcHtmlString(tagInput.ToString());
        }

        internal static MvcHtmlString CriarDivComLabelAoLado(MvcHtmlString elemento, string nome, string label, bool obrigatorio, string colLabel, string colInput, bool addOn, string icone)
        {
            TagBuilder tagInput;
            TagBuilder divInput;
            TagBuilder tagLabel = CriarTagLabel(label, obrigatorio, nome, "control-label text-right " + colLabel);

            if (addOn)
            {
                var tagI = CriarTag("i", "", "ti-" + icone);
                var tagSpan = CriarTag("span", tagI.ToString(), "input-group-text");
                var tagDiv = CriarTag("div", tagSpan.ToString(), "input-group-prepend");
                var divGroup = CriarTag("div", tagDiv.ToString() + elemento.ToString(), "input-group");
                divInput = CriarTag("div", divGroup.ToString(), colInput);
                tagInput = CriarTag("div", tagLabel.ToString() + divInput.ToString(), "form-group row");
            }
            else
            {
                divInput = CriarTag("div", elemento.ToString(), colInput);
                tagInput = CriarTag("div", tagLabel.ToString() + divInput.ToString(), "form-group row");
            }

            return new MvcHtmlString(tagInput.ToString());
        }

        internal static MvcHtmlString CriarDivCheck(MvcHtmlString elemento, string nome, string label)
        {
            TagBuilder tagLabel = CriarTagLabelSimples(label, nome, null);
            TagBuilder tagInput = CriarTag("div", elemento.ToString() + tagLabel.ToString(), "form-group");

            return new MvcHtmlString(tagInput.ToString());
        }

        #endregion

        #region Métodos Helper

        public static MvcHtmlString Modal(this HtmlHelper helper, string idModal, string titulo, string corpo = "", bool temRodape = true, List<ButtonViewModel> botoes = null)
        {
            botoes = botoes ?? new List<ButtonViewModel>();

            var tagSpan = CriarTag("span", "&times;", "", new Dictionary<string, string>() { { "aria-hidden", "true" } });
            var tagButton = CriarTag("button", tagSpan.ToString(), "close", new Dictionary<string, string>() { { "type", "button" }, { "data-dismiss", "modal" }, { "aria-label", "Close" } });
            var tagH4 = CriarTag("h4", titulo, "modal-title");

            var divHeader = CriarTag("div", tagH4.ToString() + tagButton.ToString(), "modal-header").ToString();
            var divBody = CriarTag("div", corpo, "modal-body").ToString();

            var buttons = new StringBuilder();
            foreach (var botao in botoes)
                buttons.Append($"<button type='button' class='btn btn-{botao.Color} {botao.Classe ?? string.Empty}' id='{botao.Id}' onclick='{botao.Click}'><i class='{botao.Icon}'></i> {botao.Text}</button>");
            var divFooter = CriarTag("div", buttons.ToString(), "modal-footer").ToString();

            var divContent = CriarTag("div", divHeader + divBody + (temRodape ? divFooter : ""), "modal-content");
            var divDialog = CriarTag("div", divContent.ToString(), "modal-dialog", new Dictionary<string, string>() { { "role", "document" } });

            var divModal = CriarTag("div", divDialog.ToString(), "modal fade", new Dictionary<string, string>() { { "id", idModal }, { "tabindex", "-1" }, { "role", "dialog" } });

            return new MvcHtmlString(divModal.ToString());
        }

        public static MvcHtmlString ModalFrame(this HtmlHelper helper, string idModal, string titulo, string urlFrame = "", bool temRodape = true, List<ButtonViewModel> botoes = null)
        {
            botoes = botoes ?? new List<ButtonViewModel>();

            var tagSpan = CriarTag("span", "&times;", "", new Dictionary<string, string>() { { "aria-hidden", "true" } });
            var tagButton = CriarTag("button", tagSpan.ToString(), "close", new Dictionary<string, string>() { { "type", "button" }, { "data-dismiss", "modal" }, { "aria-label", "Close" } });
            var tagH4 = CriarTag("h4", titulo, "modal-title");

            var divHeader = CriarTag("div", tagH4.ToString() + tagButton.ToString(), "modal-header").ToString();
            var divFrame = CriarTag("iframe", "", "", new Dictionary<string, string>() { { "src", urlFrame }, { "width", "100%" }, { "height", "100%" }, { "style", "border:none;" } });
            var divBody = CriarTag("div", divFrame.ToString(), "modal-body").ToString();

            var buttons = new StringBuilder();
            foreach (var botao in botoes)
                buttons.Append($"<button type='button' class='btn btn-{botao.Color} {botao.Classe ?? string.Empty}' id='{botao.Id}' onclick='{botao.Click}'><i class='{botao.Icon}'></i> {botao.Text}</button>");
            var divFooter = CriarTag("div", buttons.ToString(), "modal-footer").ToString();

            var divContent = CriarTag("div", divHeader + divBody + (temRodape ? divFooter : ""), "modal-content");
            var divDialog = CriarTag("div", divContent.ToString(), "modal-dialog", new Dictionary<string, string>() { { "role", "document" } });

            var divModal = CriarTag("div", divDialog.ToString(), "modal fade", new Dictionary<string, string>() { { "id", idModal }, { "tabindex", "-1" }, { "role", "dialog" } });

            return new MvcHtmlString(divModal.ToString());
        }

        public static MvcHtmlString Input(this HtmlHelper helper, string nome, string valor = "", bool obrigatorio = false, string label = "", string placeholder = "", object atributos = null, bool temLabel = true, bool labelAoLado = false, string colLabel = "col-md-3", string colInput = "col-md-9")
        {
            var dicionarioAtributos = PreencherAtributos(atributos);
            if (obrigatorio)
                dicionarioAtributos.Add("required", "required");
            if (!string.IsNullOrEmpty(placeholder))
                dicionarioAtributos.Add("placeholder", placeholder);

            var textBox = helper.TextBox(nome, valor, dicionarioAtributos);

            if (!temLabel)
                return CriarDivSemLabel(textBox, false, null);
            else if (!labelAoLado)
                return CriarDivComLabel(textBox, nome, label, obrigatorio, false, null);
            else
                return CriarDivComLabelAoLado(textBox, nome, label, obrigatorio, colLabel, colInput, false, null);
        }

        public static MvcHtmlString InputNumerico(this HtmlHelper helper, string nome, string valor = "", bool obrigatorio = false, string label = "", string placeholder = "", object atributos = null, bool temLabel = true, bool labelAoLado = false, string colLabel = "col-md-3", string colInput = "col-md-9")
        {
            var dicionarioAtributos = PreencherAtributos(atributos);
            if (obrigatorio)
                dicionarioAtributos.Add("required", "required");
            if (!string.IsNullOrEmpty(placeholder))
                dicionarioAtributos.Add("placeholder", placeholder);

            if (!dicionarioAtributos.ContainsKey("class"))
                dicionarioAtributos.Add("class", "form-control numerico");
            else
            {
                dicionarioAtributos.TryGetValue("class", out object classe);
                dicionarioAtributos.Remove("class");
                var classeTexto = classe.ToString();
                dicionarioAtributos.Add("class", classeTexto + " numerico");
            }

            var textBox = helper.TextBox(nome, valor, dicionarioAtributos);

            if (!temLabel)
                return CriarDivSemLabel(textBox, false, null);
            else if (!labelAoLado)
                return CriarDivComLabel(textBox, nome, label, obrigatorio, false, null);
            else
                return CriarDivComLabelAoLado(textBox, nome, label, obrigatorio, colLabel, colInput, false, null);
        }

        public static MvcHtmlString InputSenha(this HtmlHelper helper, string nome, string valor = "", bool obrigatorio = false, string label = "", string placeholder = "", object atributos = null, bool temLabel = true, bool labelAoLado = false, string colLabel = "col-md-3", string colInput = "col-md-9")
        {
            var dicionarioAtributos = PreencherAtributos(atributos);
            if (obrigatorio)
                dicionarioAtributos.Add("required", "required");
            if (!string.IsNullOrEmpty(placeholder))
                dicionarioAtributos.Add("placeholder", placeholder);

            var password = helper.Password(nome, valor, dicionarioAtributos);

            if (!temLabel)
                return CriarDivSemLabel(password, false, null);
            else if (!labelAoLado)
                return CriarDivComLabel(password, nome, label, obrigatorio, false, null);
            else
                return CriarDivComLabelAoLado(password, nome, label, obrigatorio, colLabel, colInput, false, null);
        }

        public static MvcHtmlString Select(this HtmlHelper helper, string nome, IEnumerable<SelectListItem> lista = null, bool obrigatorio = false, string label = "", object atributos = null, bool temLabel = true, bool labelAoLado = false, string colLabel = "col-md-3", string colInput = "col-md-9")
        {
            var dicionarioAtributos = PreencherAtributos(atributos);
            if (obrigatorio)
                dicionarioAtributos.Add("required", "");

            lista = lista ?? new List<SelectListItem>();

            var dropdownlist = helper.DropDownList(nome, lista, dicionarioAtributos);

            if (!temLabel)
                return CriarDivSemLabel(dropdownlist, false, null);
            else if (!labelAoLado)
                return CriarDivComLabel(dropdownlist, nome, label, obrigatorio, false, null);
            else
                return CriarDivComLabelAoLado(dropdownlist, nome, label, obrigatorio, colLabel, colInput, false, null);
        }

        public static MvcHtmlString Botao(this HtmlHelper helper, string nome, string label = "", string tipo = "button", string classe = "")
        {
            var botao = CriarTag("button", label, classe);
            botao.Attributes.Add("id", nome);
            botao.Attributes.Add("type", tipo);

            return new MvcHtmlString(botao.ToString());
        }

        public static MvcHtmlString InputData(this HtmlHelper helper, string nome, DateTime? data = null, bool obrigatorio = false, string label = "", string placeholder = "", object atributos = null, bool temLabel = true, bool labelAoLado = false, string colLabel = "col-md-3", string colInput = "col-md-9")
        {
            var dicionarioAtributos = PreencherAtributos(atributos);
            if (obrigatorio)
                dicionarioAtributos.Add("required", "required");
            if (!string.IsNullOrEmpty(placeholder))
                dicionarioAtributos.Add("placeholder", placeholder);

            if (!dicionarioAtributos.ContainsKey("class"))
                dicionarioAtributos.Add("class", "form-control datepicker date-inputmask");
            else
            {
                dicionarioAtributos.TryGetValue("class", out object classe);
                dicionarioAtributos.Remove("class");
                var classeTexto = classe.ToString();
                dicionarioAtributos.Add("class", classeTexto + " datepicker date-inputmask");
            }

            if (dicionarioAtributos.ContainsKey("maxlength"))
                dicionarioAtributos.Remove("maxlength");

            var dataTexto = string.Empty;
            if (data.HasValue)
                dataTexto = data.Value.ToString("dd/MM/yyyy");

            var textBox = helper.TextBox(nome, dataTexto, dicionarioAtributos);

            if (!temLabel)
                return CriarDivSemLabel(textBox, true, "calendar");
            else if (!labelAoLado)
                return CriarDivComLabel(textBox, nome, label, obrigatorio, true, "calendar");
            else
                return CriarDivComLabelAoLado(textBox, nome, label, obrigatorio, colLabel, colInput, true, "calendar");
        }

        public static MvcHtmlString InputEmail(this HtmlHelper helper, string nome, string valor = "", bool obrigatorio = false, string label = "", string placeholder = "", object atributos = null, bool temLabel = true, bool labelAoLado = false, string colLabel = "col-md-3", string colInput = "col-md-9")
        {
            var dicionarioAtributos = PreencherAtributos(atributos);
            if (obrigatorio)
                dicionarioAtributos.Add("required", "required");
            if (!string.IsNullOrEmpty(placeholder))
                dicionarioAtributos.Add("placeholder", placeholder);

            if (!dicionarioAtributos.ContainsKey("class"))
                dicionarioAtributos.Add("class", "form-control email-inputmask");
            else
            {
                dicionarioAtributos.TryGetValue("class", out object classe);
                dicionarioAtributos.Remove("class");
                var classeTexto = classe.ToString();
                dicionarioAtributos.Add("class", classeTexto + " email-inputmask");
            }

            var textBox = helper.TextBox(nome, valor, dicionarioAtributos);

            if (!temLabel)
                return CriarDivSemLabel(textBox, true, "email");
            else if (!labelAoLado)
                return CriarDivComLabel(textBox, nome, label, obrigatorio, true, "email");
            else
                return CriarDivComLabelAoLado(textBox, nome, label, obrigatorio, colLabel, colInput, true, "email");
        }

        public static MvcHtmlString InputTelefone(this HtmlHelper helper, string nome, string valor = "", bool obrigatorio = false, string label = "", string placeholder = "", object atributos = null, bool temLabel = true, bool labelAoLado = false, string colLabel = "col-md-3", string colInput = "col-md-9")
        {
            var dicionarioAtributos = PreencherAtributos(atributos);
            if (obrigatorio)
                dicionarioAtributos.Add("required", "required");
            if (!string.IsNullOrEmpty(placeholder))
                dicionarioAtributos.Add("placeholder", placeholder);

            if (!dicionarioAtributos.ContainsKey("class"))
                dicionarioAtributos.Add("class", "form-control phone-inputmask");
            else
            {
                dicionarioAtributos.TryGetValue("class", out object classe);
                dicionarioAtributos.Remove("class");
                var classeTexto = classe.ToString();
                dicionarioAtributos.Add("class", classeTexto + " phone-inputmask");
            }

            if (dicionarioAtributos.ContainsKey("maxlength"))
                dicionarioAtributos.Remove("maxlength");

            var textBox = helper.TextBox(nome, valor, dicionarioAtributos);

            if (!temLabel)
                return CriarDivSemLabel(textBox, true, "mobile");
            else if (!labelAoLado)
                return CriarDivComLabel(textBox, nome, label, obrigatorio, true, "mobile");
            else
                return CriarDivComLabelAoLado(textBox, nome, label, obrigatorio, colLabel, colInput, true, "mobile");
        }

        public static MvcHtmlString InputCep(this HtmlHelper helper, string nome, string valor = "", bool obrigatorio = false, string label = "", string placeholder = "", object atributos = null, bool temLabel = true, bool labelAoLado = false, string colLabel = "col-md-3", string colInput = "col-md-9")
        {
            var dicionarioAtributos = PreencherAtributos(atributos);
            if (obrigatorio)
                dicionarioAtributos.Add("required", "required");
            if (!string.IsNullOrEmpty(placeholder))
                dicionarioAtributos.Add("placeholder", placeholder);

            if (!dicionarioAtributos.ContainsKey("class"))
                dicionarioAtributos.Add("class", "form-control cep-inputmask");
            else
            {
                dicionarioAtributos.TryGetValue("class", out object classe);
                dicionarioAtributos.Remove("class");
                var classeTexto = classe.ToString();
                dicionarioAtributos.Add("class", classeTexto + " cep-inputmask");
            }

            if (dicionarioAtributos.ContainsKey("maxlength"))
                dicionarioAtributos.Remove("maxlength");

            var textBox = helper.TextBox(nome, valor, dicionarioAtributos);

            if (!temLabel)
                return CriarDivSemLabel(textBox, true, "location-pin");
            else if (!labelAoLado)
                return CriarDivComLabel(textBox, nome, label, obrigatorio, true, "location-pin");
            else
                return CriarDivComLabelAoLado(textBox, nome, label, obrigatorio, colLabel, colInput, true, "location-pin");
        }

        public static MvcHtmlString InputCpfCnpj(this HtmlHelper helper, string nome, string valor = "", bool obrigatorio = false, string label = "", string placeholder = "", object atributos = null, bool temLabel = true, bool labelAoLado = false, string colLabel = "col-md-3", string colInput = "col-md-9")
        {
            var dicionarioAtributos = PreencherAtributos(atributos);
            if (obrigatorio)
                dicionarioAtributos.Add("required", "required");
            if (!string.IsNullOrEmpty(placeholder))
                dicionarioAtributos.Add("placeholder", placeholder);

            if (!dicionarioAtributos.ContainsKey("class"))
                dicionarioAtributos.Add("class", "form-control cpfcnpj-inputmask");
            else
            {
                dicionarioAtributos.TryGetValue("class", out object classe);
                dicionarioAtributos.Remove("class");
                var classeTexto = classe.ToString();
                dicionarioAtributos.Add("class", classeTexto + " cpfcnpj-inputmask");
            }

            if (dicionarioAtributos.ContainsKey("maxlength"))
                dicionarioAtributos.Remove("maxlength");

            var textBox = helper.TextBox(nome, valor, dicionarioAtributos);

            if (!temLabel)
                return CriarDivSemLabel(textBox, true, "user");
            else if (!labelAoLado)
                return CriarDivComLabel(textBox, nome, label, obrigatorio, true, "user");
            else
                return CriarDivComLabelAoLado(textBox, nome, label, obrigatorio, colLabel, colInput, true, "user");
        }

        public static MvcHtmlString InputDinheiro(this HtmlHelper helper, string nome, string valor = "", bool obrigatorio = false, string label = "", string placeholder = "", object atributos = null, bool temLabel = true, bool labelAoLado = false, string colLabel = "col-md-3", string colInput = "col-md-9")
        {
            var dicionarioAtributos = PreencherAtributos(atributos);
            if (obrigatorio)
                dicionarioAtributos.Add("required", "required");
            if (!string.IsNullOrEmpty(placeholder))
                dicionarioAtributos.Add("placeholder", placeholder);

            if (!dicionarioAtributos.ContainsKey("class"))
                dicionarioAtributos.Add("class", "form-control money-inputmask");
            else
            {
                dicionarioAtributos.TryGetValue("class", out object classe);
                dicionarioAtributos.Remove("class");
                var classeTexto = classe.ToString();
                dicionarioAtributos.Add("class", classeTexto + " money-inputmask");
            }

            if (!dicionarioAtributos.ContainsKey("style"))
                dicionarioAtributos.Add("style", "text-align: left;");
            else
            {
                dicionarioAtributos.TryGetValue("style", out object classe);
                dicionarioAtributos.Remove("style");
                var classeTexto = classe.ToString();
                dicionarioAtributos.Add("style", classeTexto + " text-align: left;");
            }

            if (dicionarioAtributos.ContainsKey("maxlength"))
                dicionarioAtributos.Remove("maxlength");

            var textBox = helper.TextBox(nome, valor, dicionarioAtributos);

            if (!temLabel)
                return CriarDivSemLabel(textBox, true, "money");
            else if (!labelAoLado)
                return CriarDivComLabel(textBox, nome, label, obrigatorio, true, "money");
            else
                return CriarDivComLabelAoLado(textBox, nome, label, obrigatorio, colLabel, colInput, true, "money");
        }

        public static MvcHtmlString CheckBox(this HtmlHelper helper, string nome, bool valor = false, string label = "", object atributos = null)
        {
            var dicionarioAtributos = PreencherAtributos(atributos);

            dicionarioAtributos.Add("type", "checkbox");

            if (!dicionarioAtributos.ContainsKey("class"))
                dicionarioAtributos.Add("class", "form-control filled-in");
            else
            {
                dicionarioAtributos.TryGetValue("class", out object classe);
                dicionarioAtributos.Remove("class");
                var classeTexto = classe.ToString();
                dicionarioAtributos.Add("class", classeTexto + " filled-in");
            }

            var checkBox = helper.TextBox(nome, valor, dicionarioAtributos);

            return CriarDivCheck(checkBox, nome, label);
        }

        public static MvcHtmlString TextArea(this HtmlHelper helper, string nome, string valor = "", int linhas = 3, int colunas = 0, bool obrigatorio = false, string label = "", string placeholder = "", object atributos = null, bool temLabel = true, bool labelAoLado = false, string colLabel = "col-md-3", string colInput = "col-md-9")
        {
            var dicionarioAtributos = PreencherAtributos(atributos);
            if (obrigatorio)
                dicionarioAtributos.Add("required", "required");
            if (!string.IsNullOrEmpty(placeholder))
                dicionarioAtributos.Add("placeholder", placeholder);

            var textBox = helper.TextArea(nome, valor, linhas, colunas, dicionarioAtributos);

            if (!temLabel)
                return CriarDivSemLabel(textBox, false, null);
            else if (!labelAoLado)
                return CriarDivComLabel(textBox, nome, label, obrigatorio, false, null);
            else
                return CriarDivComLabelAoLado(textBox, nome, label, obrigatorio, colLabel, colInput, false, null);
        }

        #endregion
    }
}