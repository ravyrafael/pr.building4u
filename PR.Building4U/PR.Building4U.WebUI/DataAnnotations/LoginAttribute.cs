﻿using PR.Building4U.WebUI.Helper;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PR.Building4U.WebUI.DataAnnotations
{
    public class LoginAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
#if DEBUG
            if (!SessionHelper.Logado)
                SessionHelper.InicializarUsuario();
#endif
            return SessionHelper.Logado;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Login", action = "Index" }));
        }
    }
}