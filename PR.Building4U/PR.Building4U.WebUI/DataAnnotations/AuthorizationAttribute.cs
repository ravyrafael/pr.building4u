﻿using PR.Building4U.WebUI.Helper;
using System.Web;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.DataAnnotations
{
    public class AuthorizationAttribute : AuthorizeAttribute
    {
        private readonly string[] _actions;

        public AuthorizationAttribute(string[] actions = null)
        {
            _actions = actions;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var routedata = httpContext.Request.RequestContext.RouteData;
            var actions = _actions ?? new[] { routedata.GetRequiredString("action") };
            var controller = routedata.GetRequiredString("controller");

            return SessionHelper.PossuiPermissao(controller, actions);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
            filterContext.Result = new HttpStatusCodeResult(403);
        }
    }
}