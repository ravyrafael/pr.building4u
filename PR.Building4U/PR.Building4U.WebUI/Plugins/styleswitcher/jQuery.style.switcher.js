// Theme color settings
$(document).ready(function () {
    function store(name, val) {
        if (typeof (Storage) !== "undefined")
            localStorage.setItem(name, val);
        else
            window.alert('Please use a modern browser to properly view this template!');
    }

    $("*[data-theme]").click(function (e) {
        e.preventDefault();
        var theme = $(this).attr('data-theme');
        store('theme', theme);
        $('#theme').attr({ href: '/Content/colors/' + theme + '.css' });
    });
    $("*[data-style]").click(function (e) {
        e.preventDefault();
        var style = $(this).attr('data-style');
        store('style', style);
        $('#style').attr({ href: '/Content/' + style + '.css' });
    });

    var currentTheme = localStorage.getItem('theme');
    var currentStyle = localStorage.getItem('style');
    if (currentTheme) {
        $('#theme').attr({ href: '/Content/colors/' + currentTheme + '.css' });
        $('#themecolors li a').removeClass('working');
        $("[data-theme='" + currentTheme + "']").addClass('working');
    }
    if (currentStyle) {
        $('#style').attr({ href: '/Content/' + currentStyle + '.css' });
        $('#stylecolors li a').removeClass('working');
        $("[data-style='" + currentStyle + "']").addClass('working');
    }

    // color selector
    $('#themecolors').on('click', 'a', function () {
        $('#themecolors li a').removeClass('working');
        $(this).addClass('working');
    });
    $('#stylecolors').on('click', 'a', function () {
        $('#stylecolors li a').removeClass('working');
        $(this).addClass('working');
    });
});