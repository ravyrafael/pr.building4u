function Fab(json) {
    var __element = document.querySelector(json.selector);
    var positions = ["top-left", "top-right", "bottom-left", "bottom-right"], countposition = 0, status = false;
    if (!json.selector || !__element) {
        console.error("There is no selector or you have not written one, write one in the option 'selector' in the json");
        return false;
    }
    $.each(positions, function (i, e) {
        if (positions[i] != json.position) {
            countposition++;
        }
        if (countposition == 4) {
            __element.classList.add("default");
        }
    });
    __element.classList.add("fab_contenedor");
    __element.classList.add(json.position);
    __element.classList.add(json.direction);

    __element.insertAdjacentHTML('beforeend',
        '<button class="fab primary ' + json.button + '">' +
        '   <i class="' + json.icon + '"></i>' +
        '</button>');

    __element.insertAdjacentHTML('afterbegin', '<dl></dl>');

    $.each(json.buttons, function (i, e) {
        __element.querySelector('dl').insertAdjacentHTML('afterbegin',
            '<dt>' +
            '    <span class="fab-label">' + e.label + '</span>' +
            '    <button class="fab ' + e.button + ' btn' + i + '">' +
            '        <i class="' + e.icon + '"></i>' +
            '    </button>' +
            '</dt>');
        __element.querySelector("dl .fab.btn" + i).addEventListener("click", function () {
            e.onClick();
            __element.querySelector('dl').classList.remove("visible");
        });
    });

    var button = __element.querySelectorAll('dl dt button');
    var span = __element.querySelectorAll('dl dt span');

    $(json.selector + " .fab.primary").hover(function () {
        __element.querySelector('dl').classList.add("visible");
        $.each(button, function (i, e) {
            button[i].classList.add("transform");
        });
        $.each(span, function (i, e) {
            span[i].classList.add("transform");
        });
        $(json.selector).hover(function () { }, function () {
            __element.querySelector('dl').classList.remove("visible");
            $.each(button, function (i, e) {
                button[i].classList.remove("transform");
            });
            $.each(span, function (i, e) {
                span[i].classList.remove("transform");
            });
        });
    });
}
