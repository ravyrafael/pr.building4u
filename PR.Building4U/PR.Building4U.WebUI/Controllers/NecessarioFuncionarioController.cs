using PR.Building4U.Fronteiras.Executores.AdministrarCargo;
using PR.Building4U.Fronteiras.Executores.AdministrarNecessario;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class NecessarioFuncionarioController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model, int? idObra)
        {
            var requisicao = new ListarNecessarioAprovRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra ?? 0
            };
            var executor = this.CriarExecutor<ListarNecessarioAprovRequisicao, ListarNecessarioAprovResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Necessarios);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Atualizar(int idObra, int idCargo, int quantidadeNecessario)
        {
            var requisicao = new AtualizarNecessarioAprovRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra,
                IdCargo = idCargo,
                QuantidadeNecessario = quantidadeNecessario
            };
            var executor = this.CriarExecutor<AtualizarNecessarioAprovRequisicao, AtualizarNecessarioAprovResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Atualizar" })]
        public ActionResult Enviar(int idObra)
        {
            var requisicao = new EnviarNecessarioAprovRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra
            };
            var executor = this.CriarExecutor<EnviarNecessarioAprovRequisicao, EnviarNecessarioAprovResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Atualizar" })]
        public ActionResult ListarCargos(string codigoOuNome)
        {
            var requisicao = new ListarCargosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarCargosRequisicao, ListarCargosResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Cargos = resultado.Cargos.OrderBy("NomeCargo asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var cargo in resultado.Cargos)
            {
                selectList.Add(new SelectListItem
                {
                    Value = cargo.IdCargo.ToString(),
                    Text = $"{cargo.NomeCargo} ({cargo.Custo.SiglaCusto})"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Index" })]
        public ActionResult ListarObras(string codigoOuNome)
        {
            var requisicao = new ListarObrasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome,
                SomenteAtivas = true,
                SomenteObras = true
            };
            var executor = this.CriarExecutor<ListarObrasRequisicao, ListarObrasResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Obras = resultado.Obras.OrderBy("NomeObra asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var obra in resultado.Obras)
            {
                selectList.Add(new SelectListItem
                {
                    Value = obra.IdObra.ToString(),
                    Text = $"{obra.CodigoObra.PadLeft(3, '0')} - {obra.NomeObra}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }
    }
}
