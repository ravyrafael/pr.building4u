using PR.Building4U.Fronteiras.Executores.AdministrarAluguel;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class AlocacaoEquipamentoController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model, int[] obras, int[] tiposEquipamento)
        {
            obras = obras?[0] == 0 ? null : obras;
            tiposEquipamento = tiposEquipamento?[0] == 0 ? null : tiposEquipamento;

            var requisicao = new ListarAlugueisRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                Obras = obras,
                TiposEquipamento = tiposEquipamento
            };
            var executor = this.CriarExecutor<ListarAlugueisRequisicao, ListarAlugueisResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Alugueis);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Transferir(int sequenciaAluguel, int idEquipamento, int idObra, DateTime dataTransferencia)
        {
            var requisicao = new TransferirEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdEquipamento = idEquipamento,
                SequenciaAluguel = sequenciaAluguel,
                IdObra = idObra,
                DataTransferencia = dataTransferencia
            };
            var executor = this.CriarExecutor<TransferirEquipamentoRequisicao, TransferirEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Desfazer(int sequenciaAluguel, int idEquipamento)
        {
            var requisicao = new DesfazerTransferenciaEquipRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdEquipamento = idEquipamento,
                SequenciaAluguel = sequenciaAluguel
            };
            var executor = this.CriarExecutor<DesfazerTransferenciaEquipRequisicao, DesfazerTransferenciaEquipResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Cancelar(int idEquipamento)
        {
            var requisicao = new CancelarDestinoEquipRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdEquipamento = idEquipamento
            };
            var executor = this.CriarExecutor<CancelarDestinoEquipRequisicao, CancelarDestinoEquipResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Arrumar(int idEquipamento, DateTime? inicioManutencao, DateTime terminoManutencao)
        {
            if (inicioManutencao.HasValue)
            {
                var requisicao = new IniciarManutencaoEquipamentoRequisicao
                {
                    InformacoesLog = this.GetInformacoesLog(),
                    IdEquipamento = idEquipamento,
                    InicioManutencao = inicioManutencao.Value,
                    TerminoManutencao = terminoManutencao
                };
                var executor = this.CriarExecutor<IniciarManutencaoEquipamentoRequisicao, IniciarManutencaoEquipamentoResultado>();
                var resultado = executor.Executar(requisicao);

                return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var requisicao = new TerminarManutencaoEquipamentoRequisicao
                {
                    InformacoesLog = this.GetInformacoesLog(),
                    IdEquipamento = idEquipamento,
                    TerminoManutencao = terminoManutencao
                };
                var executor = this.CriarExecutor<TerminarManutencaoEquipamentoRequisicao, TerminarManutencaoEquipamentoResultado>();
                var resultado = executor.Executar(requisicao);

                return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorization(new[] { "Index", "Transferir" })]
        public ActionResult ListarObras(string codigoOuNome)
        {
            var requisicao = new ListarObrasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome,
                SomenteAtivas = true,
                SomenteObras = false
            };
            var executor = this.CriarExecutor<ListarObrasRequisicao, ListarObrasResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Obras = resultado.Obras.OrderBy("NomeObra asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var obra in resultado.Obras)
            {
                selectList.Add(new SelectListItem
                {
                    Value = obra.IdObra.ToString(),
                    Text = $"{obra.CodigoObra.PadLeft(3, '0')} - {obra.NomeObra}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Index" })]
        public ActionResult ListarTiposEquipamento(string codigoOuNome)
        {
            var requisicao = new ListarTiposEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarTiposEquipamentoRequisicao, ListarTiposEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.TiposEquipamento = resultado.TiposEquipamento.OrderBy("NomeTipoEquipamento asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var tipoEquipamento in resultado.TiposEquipamento)
            {
                selectList.Add(new SelectListItem
                {
                    Value = tipoEquipamento.IdTipoEquipamento.ToString(),
                    Text = $"{tipoEquipamento.NomeTipoEquipamento} ({tipoEquipamento.Custo.SiglaCusto})"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }
    }
}
