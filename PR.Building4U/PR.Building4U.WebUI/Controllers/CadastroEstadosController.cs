﻿using PR.Building4U.Fronteiras.Executores.AdministrarEstado;
using PR.Building4U.Fronteiras.Executores.AdministrarPais;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroEstadosController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarEstadosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarEstadosRequisicao, ListarEstadosResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Estados);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(string siglaEstado, string nomeEstado, int idPais)
        {
            var requisicao = new IncluirEstadoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                SiglaEstado = siglaEstado,
                NomeEstado = nomeEstado,
                IdPais = idPais
            };
            var executor = this.CriarExecutor<IncluirEstadoRequisicao, IncluirEstadoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idEstado, string siglaEstado, string nomeEstado, int idPais)
        {
            var requisicao = new EditarEstadoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdEstado = idEstado,
                SiglaEstado = siglaEstado,
                NomeEstado = nomeEstado,
                IdPais = idPais
            };
            var executor = this.CriarExecutor<EditarEstadoRequisicao, EditarEstadoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Excluir(int idEstado)
        {
            var requisicao = new ExcluirEstadoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdEstado = idEstado
            };
            var executor = this.CriarExecutor<ExcluirEstadoRequisicao, ExcluirEstadoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idEstado)
        {
            var requisicao = new ObterEstadoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdEstado = idEstado
            };
            var executor = this.CriarExecutor<ObterEstadoRequisicao, ObterEstadoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarPaises(string codigoOuNome)
        {
            var requisicao = new ListarPaisesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarPaisesRequisicao, ListarPaisesResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Paises = resultado.Paises.OrderBy("NomePais asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var pais in resultado.Paises)
            {
                selectList.Add(new SelectListItem
                {
                    Value = pais.IdPais.ToString(),
                    Text = $"{pais.CodigoPais.ToString().PadLeft(4, '0')} - {pais.NomePais}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }
    }
}