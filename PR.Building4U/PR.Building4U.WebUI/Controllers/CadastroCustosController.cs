using PR.Building4U.Fronteiras.Executores.AdministrarCusto;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroCustosController : Controller
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarCustosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarCustosRequisicao, ListarCustosResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Custos);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(string siglaCusto, string nomeCusto)
        {
            var requisicao = new IncluirCustoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                SiglaCusto = siglaCusto,
                NomeCusto = nomeCusto
            };
            var executor = this.CriarExecutor<IncluirCustoRequisicao, IncluirCustoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idCusto, string nomeCusto)
        {
            var requisicao = new EditarCustoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCusto = idCusto,
                NomeCusto = nomeCusto
            };
            var executor = this.CriarExecutor<EditarCustoRequisicao, EditarCustoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Excluir(int idCusto)
        {
            var requisicao = new ExcluirCustoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCusto = idCusto
            };
            var executor = this.CriarExecutor<ExcluirCustoRequisicao, ExcluirCustoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idCusto)
        {
            var requisicao = new ObterCustoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCusto = idCusto
            };
            var executor = this.CriarExecutor<ObterCustoRequisicao, ObterCustoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}
