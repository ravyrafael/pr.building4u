﻿using PR.Building4U.WebUI.Helper;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ValidarUsuario(string usuario, string senha)
        {
            SessionHelper.CarregarUsuarioNaSessao(usuario, senha);

            return Json(new { SessionHelper.Logado }, JsonRequestBehavior.AllowGet);
        }
    }
}