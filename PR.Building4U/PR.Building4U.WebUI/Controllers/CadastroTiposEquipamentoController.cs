using PR.Building4U.Fronteiras.Executores.AdministrarCusto;
using PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroTiposEquipamentoController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            ViewBag.ListaCustos = ListarCustos();

            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarTiposEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarTiposEquipamentoRequisicao, ListarTiposEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.TiposEquipamento);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(string nomeTipoEquipamento, int idCusto)
        {
            var requisicao = new IncluirTipoEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                NomeTipoEquipamento = nomeTipoEquipamento,
                IdCusto = idCusto
            };
            var executor = this.CriarExecutor<IncluirTipoEquipamentoRequisicao, IncluirTipoEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idTipoEquipamento, string nomeTipoEquipamento, int idCusto)
        {
            var requisicao = new EditarTipoEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdTipoEquipamento = idTipoEquipamento,
                NomeTipoEquipamento = nomeTipoEquipamento,
                IdCusto = idCusto
            };
            var executor = this.CriarExecutor<EditarTipoEquipamentoRequisicao, EditarTipoEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Excluir(int idTipoEquipamento)
        {
            var requisicao = new ExcluirTipoEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdTipoEquipamento = idTipoEquipamento
            };
            var executor = this.CriarExecutor<ExcluirTipoEquipamentoRequisicao, ExcluirTipoEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idTipoEquipamento)
        {
            var requisicao = new ObterTipoEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdTipoEquipamento = idTipoEquipamento
            };
            var executor = this.CriarExecutor<ObterTipoEquipamentoRequisicao, ObterTipoEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> ListarCustos()
        {
            var requisicao = new ListarCustosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
            };
            var executor = this.CriarExecutor<ListarCustosRequisicao, ListarCustosResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Custos = resultado.Custos.OrderBy("NomeCusto asc").ToList();

            var selectList = resultado.Custos.Select(x => new SelectListItem
            {
                Text = x.NomeCusto,
                Value = x.IdCusto.ToString()
            }).ToList();

            return selectList;
        }
    }
}
