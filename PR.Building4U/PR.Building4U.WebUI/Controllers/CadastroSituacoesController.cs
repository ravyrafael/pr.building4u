﻿using PR.Building4U.Fronteiras.Executores.AdministrarSituacao;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroSituacoesController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            ViewBag.ListaTipos = ListarTipos();

            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarSituacoesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarSituacoesRequisicao, ListarSituacoesResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Situacoes);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idSituacao, string nomeSituacao)
        {
            var requisicao = new EditarSituacaoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdSituacao = idSituacao,
                NomeSituacao = nomeSituacao
            };
            var executor = this.CriarExecutor<EditarSituacaoRequisicao, EditarSituacaoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idSituacao)
        {
            var requisicao = new ObterSituacaoRequisicao()
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdSituacao = idSituacao
            };
            var executor = this.CriarExecutor<ObterSituacaoRequisicao, ObterSituacaoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Listar" })]
        public ActionResult ObterEnum(int codigo)
        {
            var tipo = ((Util.TipoSituacao)codigo).ToString();
            return Json(tipo, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> ListarTipos()
        {
            return Enum.GetValues(typeof(Util.TipoSituacao)).
                Cast<Util.TipoSituacao>().
                Select(x => new SelectListItem
                {
                    Text = x.ToString(),
                    Value = ((int)x).ToString()
                }).ToList();
        }
    }
}