using PR.Building4U.Fronteiras.Executores.AdministrarAditivo;
using PR.Building4U.Fronteiras.Executores.AdministrarCliente;
using PR.Building4U.Fronteiras.Executores.AdministrarContrato;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroContratosController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarContratosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarContratosRequisicao, ListarContratosResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Contratos);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(int idCliente, string codigoContrato, DateTime dataContrato, decimal valorContrato, int prazoContrato, string escopoContrato)
        {
            var requisicao = new IncluirContratoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCliente = idCliente,
                CodigoContrato = codigoContrato,
                DataContrato = dataContrato,
                ValorContrato = valorContrato,
                PrazoContrato = prazoContrato,
                EscopoContrato = escopoContrato
            };
            var executor = this.CriarExecutor<IncluirContratoRequisicao, IncluirContratoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idContrato, string escopoContrato)
        {
            var requisicao = new EditarContratoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdContrato = idContrato,
                EscopoContrato = escopoContrato
            };
            var executor = this.CriarExecutor<EditarContratoRequisicao, EditarContratoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Excluir(int idContrato)
        {
            var requisicao = new ExcluirContratoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdContrato = idContrato
            };
            var executor = this.CriarExecutor<ExcluirContratoRequisicao, ExcluirContratoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idContrato)
        {
            var requisicao = new ObterContratoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdContrato = idContrato
            };
            var executor = this.CriarExecutor<ObterContratoRequisicao, ObterContratoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir" })]
        public ActionResult ListarClientes(string codigoOuNome)
        {
            var requisicao = new ListarClientesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarClientesRequisicao, ListarClientesResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Clientes = resultado.Clientes.OrderBy("NomeCliente asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var cliente in resultado.Clientes)
            {
                selectList.Add(new SelectListItem
                {
                    Value = cliente.IdCliente.ToString(),
                    Text = $"{cliente.NomeCliente} ({(cliente.CnpjCliente.Length == 14 ? Convert.ToUInt64(cliente.CnpjCliente).ToString(@"00\.000\.000\/0000\-00") : Convert.ToUInt64(cliente.CnpjCliente).ToString(@"000\.000\.000\-00"))})"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Index" })]
        public ActionResult ListarAditivos(DataTableAjaxPostModel model, int? idContrato)
        {
            var requisicao = new ListarAditivosPorContratoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdContrato = idContrato ?? 0
            };
            var executor = this.CriarExecutor<ListarAditivosPorContratoRequisicao, ListarAditivosPorContratoResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Aditivos);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult IncluirAditivo(int idContrato, string codigoAditivo, DateTime dataAditivo, decimal valorAditivo, int prazoAditivo, string escopoAditivo)
        {
            var requisicao = new IncluirAditivoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdContrato = idContrato,
                CodigoAditivo = codigoAditivo,
                DataAditivo = dataAditivo,
                ValorAditivo = valorAditivo,
                PrazoAditivo = prazoAditivo,
                EscopoAditivo = escopoAditivo
            };
            var executor = this.CriarExecutor<IncluirAditivoRequisicao, IncluirAditivoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ExcluirAditivo(int idAditivo)
        {
            var requisicao = new ExcluirAditivoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdAditivo = idAditivo
            };
            var executor = this.CriarExecutor<ExcluirAditivoRequisicao, ExcluirAditivoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }
    }
}
