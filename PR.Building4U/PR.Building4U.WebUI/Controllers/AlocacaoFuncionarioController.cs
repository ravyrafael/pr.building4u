using PR.Building4U.Fronteiras.Executores.AdministrarAlocacao;
using PR.Building4U.Fronteiras.Executores.AdministrarCargo;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class AlocacaoFuncionarioController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model, int[] obras, int[] cargos)
        {
            obras = obras?[0] == 0 ? null : obras;
            cargos = cargos?[0] == 0 ? null : cargos;

            var requisicao = new ListarAlocacoesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                Obras = obras,
                Cargos = cargos
            };
            var executor = this.CriarExecutor<ListarAlocacoesRequisicao, ListarAlocacoesResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Alocacoes);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Transferir(int sequenciaAlocacao, int idFuncionario, int idObra, DateTime dataTransferencia)
        {
            var requisicao = new TransferirFuncionarioRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdFuncionario = idFuncionario,
                SequenciaAlocacao = sequenciaAlocacao,
                IdObra = idObra,
                DataTransferencia = dataTransferencia
            };
            var executor = this.CriarExecutor<TransferirFuncionarioRequisicao, TransferirFuncionarioResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Desfazer(int sequenciaAlocacao, int idFuncionario)
        {
            var requisicao = new DesfazerTransferenciaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdFuncionario = idFuncionario,
                SequenciaAlocacao = sequenciaAlocacao
            };
            var executor = this.CriarExecutor<DesfazerTransferenciaRequisicao, DesfazerTransferenciaResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Cancelar(int idFuncionario)
        {
            var requisicao = new CancelarDestinoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdFuncionario = idFuncionario
            };
            var executor = this.CriarExecutor<CancelarDestinoRequisicao, CancelarDestinoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Folgar(int idFuncionario, DateTime? inicioFerias, DateTime terminoFerias)
        {
            if (inicioFerias.HasValue)
            {
                var requisicao = new IniciarFeriasFuncionarioRequisicao
                {
                    InformacoesLog = this.GetInformacoesLog(),
                    IdFuncionario = idFuncionario,
                    InicioFerias = inicioFerias.Value,
                    TerminoFerias = terminoFerias
                };
                var executor = this.CriarExecutor<IniciarFeriasFuncionarioRequisicao, IniciarFeriasFuncionarioResultado>();
                var resultado = executor.Executar(requisicao);

                return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var requisicao = new TerminarFeriasFuncionarioRequisicao
                {
                    InformacoesLog = this.GetInformacoesLog(),
                    IdFuncionario = idFuncionario,
                    TerminoFerias = terminoFerias
                };
                var executor = this.CriarExecutor<TerminarFeriasFuncionarioRequisicao, TerminarFeriasFuncionarioResultado>();
                var resultado = executor.Executar(requisicao);

                return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorization(new[] { "Index", "Transferir" })]
        public ActionResult ListarObras(string codigoOuNome)
        {
            var requisicao = new ListarObrasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome,
                SomenteAtivas = true,
                SomenteObras = false
            };
            var executor = this.CriarExecutor<ListarObrasRequisicao, ListarObrasResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Obras = resultado.Obras.OrderBy("NomeObra asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var obra in resultado.Obras)
            {
                selectList.Add(new SelectListItem
                {
                    Value = obra.IdObra.ToString(),
                    Text = $"{obra.CodigoObra.PadLeft(3, '0')} - {obra.NomeObra}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Index" })]
        public ActionResult ListarCargos(string codigoOuNome)
        {
            var requisicao = new ListarCargosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarCargosRequisicao, ListarCargosResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Cargos = resultado.Cargos.OrderBy("NomeCargo asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var cargo in resultado.Cargos)
            {
                selectList.Add(new SelectListItem
                {
                    Value = cargo.IdCargo.ToString(),
                    Text = $"{cargo.NomeCargo} ({cargo.Custo.SiglaCusto})"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }
    }
}
