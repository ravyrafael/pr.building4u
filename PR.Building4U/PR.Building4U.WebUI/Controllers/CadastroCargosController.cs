using PR.Building4U.Fronteiras.Executores.AdministrarCargo;
using PR.Building4U.Fronteiras.Executores.AdministrarCusto;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroCargosController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            ViewBag.ListaCustos = ListarCustos();

            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarCargosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarCargosRequisicao, ListarCargosResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Cargos);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(string siglaCargo, string nomeCargo, int idCusto)
        {
            var requisicao = new IncluirCargoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                SiglaCargo = siglaCargo.ToUpper(),
                NomeCargo = nomeCargo,
                IdCusto = idCusto
            };
            var executor = this.CriarExecutor<IncluirCargoRequisicao, IncluirCargoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idCargo, string siglaCargo, string nomeCargo, int idCusto)
        {
            var requisicao = new EditarCargoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCargo = idCargo,
                SiglaCargo = siglaCargo,
                NomeCargo = nomeCargo,
                IdCusto = idCusto
            };
            var executor = this.CriarExecutor<EditarCargoRequisicao, EditarCargoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Excluir(int idCargo)
        {
            var requisicao = new ExcluirCargoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCargo = idCargo
            };
            var executor = this.CriarExecutor<ExcluirCargoRequisicao, ExcluirCargoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idCargo)
        {
            var requisicao = new ObterCargoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCargo = idCargo
            };
            var executor = this.CriarExecutor<ObterCargoRequisicao, ObterCargoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> ListarCustos()
        {
            var requisicao = new ListarCustosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
            };
            var executor = this.CriarExecutor<ListarCustosRequisicao, ListarCustosResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Custos = resultado.Custos.OrderBy("NomeCusto asc").ToList();

            var selectList = resultado.Custos.Select(x => new SelectListItem
            {
                Text = x.NomeCusto,
                Value = x.IdCusto.ToString()
            }).ToList();

            return selectList;
        }
    }
}
