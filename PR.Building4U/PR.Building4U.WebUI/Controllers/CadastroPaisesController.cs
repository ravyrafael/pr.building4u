﻿using PR.Building4U.Fronteiras.Executores.AdministrarPais;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Models;
using PR.Building4U.WebUI.Helper;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroPaisesController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarPaisesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarPaisesRequisicao, ListarPaisesResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Paises);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(int codigoPais, string nomePais)
        {
            var requisicao = new IncluirPaisRequisicao()
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoPais = codigoPais,
                NomePais = nomePais
            };
            var executor = this.CriarExecutor<IncluirPaisRequisicao, IncluirPaisResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idPais, int codigoPais, string nomePais)
        {
            var requisicao = new EditarPaisRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdPais = idPais,
                CodigoPais = codigoPais,
                NomePais = nomePais
            };
            var executor = this.CriarExecutor<EditarPaisRequisicao, EditarPaisResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Excluir(int idPais)
        {
            var requisicao = new ExcluirPaisRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdPais = idPais
            };
            var executor = this.CriarExecutor<ExcluirPaisRequisicao, ExcluirPaisResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idPais)
        {
            var requisicao = new ObterPaisRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdPais = idPais
            };
            var executor = this.CriarExecutor<ObterPaisRequisicao, ObterPaisResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}