﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarMenu;
using PR.Building4U.Fronteiras.Executores.AdministrarNotificacao;
using PR.Building4U.SDK.InversaoControle;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SemAutenticacao()
        {
            return View("Index");
        }

        public PartialViewResult Menu()
        {
            var requisicao = new ObterMenuPorPermissaoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                PermissaoUsuario = SessionHelper.Usuario.Permissao.IdPermissao
            };
            var executor = this.CriarExecutor<ObterMenuPorPermissaoRequisicao, ObterMenuPorPermissaoResultado>();
            var resultado = executor.Executar(requisicao);

            var menu = new MenuViewModel { ItensMenu = resultado.Menu };

            return PartialView("Estrutura/_PainelEsquerdo", menu);
        }

        public PartialViewResult Notificacoes()
        {
            var notificacoes = ListarNotificacoes();

            return PartialView("Estrutura/_CentralNotificacao", notificacoes);
        }
    }
}