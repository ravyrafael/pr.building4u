﻿using PR.Building4U.Fronteiras.Executores.AdministrarNotificacao;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    [Login]
    public class BaseController : Controller
    {
        public ActionResult PossuiPermissao(string controller, string action)
        {
            return Json(SessionHelper.PossuiPermissao(controller, new[] { action }), JsonRequestBehavior.AllowGet);
        }

        public NotificacoesViewModel ListarNotificacoes()
        {
            var requisicao = new ListarNotificacoesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdFuncionario = SessionHelper.Usuario.Funcionario.IdFuncionario
            };
            var executor = this.CriarExecutor<ListarNotificacoesRequisicao, ListarNotificacoesResultado>();
            var resultado = executor.Executar(requisicao);

            return new NotificacoesViewModel { Notificacoes = resultado.Notificacoes };
        }

        public ActionResult MarcarNotificacao(int? idNotificacao)
        {
            var requisicao = new MarcarNotificacaoComoLidaRequisicao
            {
                IdNotificacao = idNotificacao,
                IdFuncionario = !idNotificacao.HasValue ? SessionHelper.Usuario.Funcionario.IdFuncionario : (int?)null,
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<MarcarNotificacaoComoLidaRequisicao, MarcarNotificacaoComoLidaResultado>();
            var resultado = executor.Executar(requisicao);

            var notificacoes = ListarNotificacoes();

            var html = this.RenderRazorViewToString("Estrutura/_CentralNotificacao", notificacoes);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem, html }, JsonRequestBehavior.AllowGet);
        }
    }
}