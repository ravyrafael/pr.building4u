using PR.Building4U.Fronteiras.Executores.AdministrarGerencia;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
	public class CadastroGerenciasController : BaseController
	{
		[Authorization]
		public ActionResult Index()
		{
			return View();
		}

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarGerenciasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarGerenciasRequisicao, ListarGerenciasResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Gerencias);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(string nomeGerencia, string siglaGerencia)
        {
            var requisicao = new IncluirGerenciaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                NomeGerencia = nomeGerencia,
                SiglaGerencia = siglaGerencia
            };
            var executor = this.CriarExecutor<IncluirGerenciaRequisicao, IncluirGerenciaResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Excluir(int idGerencia)
        {
            var requisicao = new ExcluirGerenciaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdGerencia = idGerencia
            };
            var executor = this.CriarExecutor<ExcluirGerenciaRequisicao, ExcluirGerenciaResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }
    }
}
