﻿using PR.Building4U.Fronteiras.Executores.AdministrarEstado;
using PR.Building4U.Fronteiras.Executores.AdministrarMunicipio;
using PR.Building4U.Fronteiras.Executores.AdministrarPais;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroMunicipiosController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarMunicipiosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarMunicipiosRequisicao, ListarMunicipiosResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Municipios);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(int codigoMunicipio, string nomeMunicipio, int idEstado)
        {
            var requisicao = new IncluirMunicipioRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoMunicipio = codigoMunicipio,
                NomeMunicipio = nomeMunicipio,
                IdEstado = idEstado
            };
            var executor = this.CriarExecutor<IncluirMunicipioRequisicao, IncluirMunicipioResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idMunicipio, int codigoMunicipio, string nomeMunicipio, int idEstado)
        {
            var requisicao = new EditarMunicipioRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdMunicipio = idMunicipio,
                CodigoMunicipio = codigoMunicipio,
                NomeMunicipio = nomeMunicipio,
                IdEstado = idEstado
            };
            var executor = this.CriarExecutor<EditarMunicipioRequisicao, EditarMunicipioResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Excluir(int idMunicipio)
        {
            var requisicao = new ExcluirMunicipioRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdMunicipio = idMunicipio
            };
            var executor = this.CriarExecutor<ExcluirMunicipioRequisicao, ExcluirMunicipioResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idMunicipio)
        {
            var requisicao = new ObterMunicipioRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdMunicipio = idMunicipio
            };
            var executor = this.CriarExecutor<ObterMunicipioRequisicao, ObterMunicipioResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarPaises(string codigoOuNome)
        {
            var requisicao = new ListarPaisesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarPaisesRequisicao, ListarPaisesResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Paises = resultado.Paises.OrderBy("NomePais asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var pais in resultado.Paises)
            {
                selectList.Add(new SelectListItem
                {
                    Value = pais.IdPais.ToString(),
                    Text = $"{pais.CodigoPais.ToString().PadLeft(4, '0')} - {pais.NomePais}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarEstados(string codigoOuNome, string idPais)
        {
            var requisicao = new ListarEstadosPorPaisRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdPais = int.Parse(idPais),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarEstadosPorPaisRequisicao, ListarEstadosPorPaisResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Estados = resultado.Estados.OrderBy("NomeEstado asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var estado in resultado.Estados)
            {
                selectList.Add(new SelectListItem
                {
                    Value = estado.IdEstado.ToString(),
                    Text = $"{estado.SiglaEstado} - {estado.NomeEstado}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }
    }
}