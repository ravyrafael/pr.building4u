using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarCronograma;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
	public class CadastroCronogramasController : BaseController
	{
		[Authorization]
		public ActionResult Index()
		{
			return View();
		}

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model, int? idObra)
        {
            var cronogramas = ListarCronogramas(idObra);

            var datatable = this.RenderDatatableToView(model, cronogramas);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(int idObra, string nomeCronograma, int diaMedicaoCronograma)
        {
            var cronogramas = ListarCronogramas(idObra);
            var ordemCronograma = cronogramas.Max(c => c.OrdemCronograma) + 1;

            var requisicao = new IncluirCronogramaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra,
                OrdemCronograma = ordemCronograma,
                NomeCronograma = nomeCronograma,
                DiaMedicaoCronograma = diaMedicaoCronograma
            };
            var executor = this.CriarExecutor<IncluirCronogramaRequisicao, IncluirCronogramaResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idCronograma, string nomeCronograma, int diaMedicaoCronograma)
        {
            var requisicao = new EditarCronogramaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCronograma = idCronograma,
                NomeCronograma = nomeCronograma,
                DiaMedicaoCronograma = diaMedicaoCronograma
            };
            var executor = this.CriarExecutor<EditarCronogramaRequisicao, EditarCronogramaResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idCronograma)
        {
            var requisicao = new ObterCronogramaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCronograma = idCronograma
            };
            var executor = this.CriarExecutor<ObterCronogramaRequisicao, ObterCronogramaResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        private List<CronogramaDto> ListarCronogramas(int? idObra)
        {
            var requisicao = new ListarCronogramasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra ?? 0
            };
            var executor = this.CriarExecutor<ListarCronogramasRequisicao, ListarCronogramasResultado>();
            var resultado = executor.Executar(requisicao);

            return resultado.Cronogramas;
        }
    }
}
