using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarCliente;
using PR.Building4U.Fronteiras.Executores.AdministrarEstado;
using PR.Building4U.Fronteiras.Executores.AdministrarMunicipio;
using PR.Building4U.Fronteiras.Executores.AdministrarPais;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroClientesController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarClientesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarClientesRequisicao, ListarClientesResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Clientes);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(ClienteDto cliente)
        {
            var requisicao = new IncluirClienteRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CnpjCliente = cliente.CnpjCliente,
                RazaoSocialCliente = cliente.RazaoSocialCliente,
                NomeCliente = cliente.NomeCliente,
                LogradouroEndereco = cliente.Endereco.LogradouroEndereco,
                NumeroEndereco = cliente.Endereco.NumeroEndereco,
                ComplementoEndereco = cliente.Endereco.ComplementoEndereco,
                CepEndereco = cliente.Endereco.CepEndereco,
                DistritoEndereco = cliente.Endereco.DistritoEndereco,
                IdMunicipio = cliente.Endereco.Municipio.IdMunicipio
            };
            var executor = this.CriarExecutor<IncluirClienteRequisicao, IncluirClienteResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(ClienteDto cliente)
        {
            var requisicao = new EditarClienteRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCliente = cliente.IdCliente,
                RazaoSocialCliente = cliente.RazaoSocialCliente,
                NomeCliente = cliente.NomeCliente,
                IdEndereco = cliente.Endereco.IdEndereco,
                LogradouroEndereco = cliente.Endereco.LogradouroEndereco,
                NumeroEndereco = cliente.Endereco.NumeroEndereco,
                ComplementoEndereco = cliente.Endereco.ComplementoEndereco,
                CepEndereco = cliente.Endereco.CepEndereco,
                DistritoEndereco = cliente.Endereco.DistritoEndereco,
                IdMunicipio = cliente.Endereco.Municipio.IdMunicipio
            };
            var executor = this.CriarExecutor<EditarClienteRequisicao, EditarClienteResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Desativar(int idCliente, DateTime dataDesativado)
        {
            var requisicao = new DesativarClienteRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCliente = idCliente,
                DataDesativado = dataDesativado
            };
            var executor = this.CriarExecutor<DesativarClienteRequisicao, DesativarClienteResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idCliente)
        {
            var requisicao = new ObterClienteRequisicao()
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCliente = idCliente
            };
            var executor = this.CriarExecutor<ObterClienteRequisicao, ObterClienteResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarPaises(string codigoOuNome)
        {
            var requisicao = new ListarPaisesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarPaisesRequisicao, ListarPaisesResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Paises = resultado.Paises.OrderBy("NomePais asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var pais in resultado.Paises)
            {
                selectList.Add(new SelectListItem
                {
                    Value = pais.IdPais.ToString(),
                    Text = $"{pais.CodigoPais.ToString().PadLeft(4, '0')} - {pais.NomePais}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarEstados(string codigoOuNome, string idPais)
        {
            var requisicao = new ListarEstadosPorPaisRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdPais = int.Parse(idPais),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarEstadosPorPaisRequisicao, ListarEstadosPorPaisResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Estados = resultado.Estados.OrderBy("NomeEstado asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var estado in resultado.Estados)
            {
                selectList.Add(new SelectListItem
                {
                    Value = estado.IdEstado.ToString(),
                    Text = $"{estado.SiglaEstado} - {estado.NomeEstado}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarMunicipios(string codigoOuNome, string idEstado)
        {
            var requisicao = new ListarMunicipiosPorEstadoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdEstado = int.Parse(idEstado),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarMunicipiosPorEstadoRequisicao, ListarMunicipiosPorEstadoResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Municipios = resultado.Municipios.OrderBy("NomeMunicipio asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var municipio in resultado.Municipios)
            {
                selectList.Add(new SelectListItem
                {
                    Value = municipio.IdMunicipio.ToString(),
                    Text = $"{municipio.CodigoMunicipio.ToString().PadLeft(4, '0')} - {municipio.NomeMunicipio}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }
    }
}
