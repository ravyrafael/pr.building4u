using PR.Building4U.Fronteiras.Executores.AdministrarNecessario;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class NecessAprovFuncionarioController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model, int? idObra)
        {
            var requisicao = new ListarNecessarioRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra ?? 0
            };
            var executor = this.CriarExecutor<ListarNecessarioRequisicao, ListarNecessarioResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Necessarios);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Aprovar(int idObra)
        {
            var requisicao = new AprovarNecessarioRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra
            };
            var executor = this.CriarExecutor<AprovarNecessarioRequisicao, AprovarNecessarioResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Aprovar" })]
        public ActionResult Reprovar(int idObra)
        {
            var requisicao = new ReprovarNecessarioRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra
            };
            var executor = this.CriarExecutor<ReprovarNecessarioRequisicao, ReprovarNecessarioResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Index" })]
        public ActionResult ListarObras(string codigoOuNome)
        {
            var requisicao = new ListarObrasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome,
                SomenteAtivas = true,
                SomenteObras = true
            };
            var executor = this.CriarExecutor<ListarObrasRequisicao, ListarObrasResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Obras = resultado.Obras.OrderBy("NomeObra asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var obra in resultado.Obras)
            {
                selectList.Add(new SelectListItem
                {
                    Value = obra.IdObra.ToString(),
                    Text = $"{obra.CodigoObra.PadLeft(3, '0')} - {obra.NomeObra}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }
    }
}
