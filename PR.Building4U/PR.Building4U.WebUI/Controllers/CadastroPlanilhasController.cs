﻿using Newtonsoft.Json;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarCronograma;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.Fronteiras.Executores.AdministrarPlanilha;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroPlanilhasController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model, int? idCronograma)
        {
            var requisicao = new ListarPlanilhasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCronograma = idCronograma ?? 0
            };
            var executor = this.CriarExecutor<ListarPlanilhasRequisicao, ListarPlanilhasResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Planilhas);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Index" })]
        public ActionResult ListarObras(string codigoOuNome)
        {
            var requisicao = new ListarObrasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome,
                SomenteAtivas = true,
                SomenteObras = true
            };
            var executor = this.CriarExecutor<ListarObrasRequisicao, ListarObrasResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Obras = resultado.Obras.OrderBy("NomeObra asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var obra in resultado.Obras)
            {
                selectList.Add(new SelectListItem
                {
                    Value = obra.IdObra.ToString(),
                    Text = $"{obra.CodigoObra.ToString().PadLeft(3, '0')} - {obra.NomeObra}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Index" })]
        public ActionResult ListarCronogramas(string codigoOuNome, int? idObra)
        {
            var requisicao = new ListarCronogramasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra ?? 0
            };
            var executor = this.CriarExecutor<ListarCronogramasRequisicao, ListarCronogramasResultado>();
            var resultado = executor.Executar(requisicao);

            if (!string.IsNullOrEmpty(codigoOuNome))
                resultado.Cronogramas = resultado.Cronogramas.Where(c => c.NomeCronograma.ToUpper().Contains(codigoOuNome.ToUpper())).ToList();

            resultado.Cronogramas = resultado.Cronogramas.OrderBy("NomeCronograma asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var cronograma in resultado.Cronogramas)
            {
                selectList.Add(new SelectListItem
                {
                    Value = cronograma.IdCronograma.ToString(),
                    Text = cronograma.NomeCronograma
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Excluir(int idPlanilha)
        {
            var requisicao = new ExcluirPlanilhaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdPlanilha = idPlanilha
            };
            var executor = this.CriarExecutor<ExcluirPlanilhaRequisicao, ExcluirPlanilhaResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir" })]
        public ActionResult Enviar(int? idCronograma, string cpPlanilha, int? revPlanilha)
        {
            var arquivoPlanilha = Request.Files["arquivoPlanilha"];

            var arquivoByte = new byte[arquivoPlanilha.ContentLength];
            arquivoPlanilha.InputStream.Read(arquivoByte, 0, arquivoByte.Length);

            var requisicao = new ImportarPlanilhaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdCronograma = idCronograma ?? 0,
                CpPlanilha = cpPlanilha,
                RevPlanilha = revPlanilha ?? 0,
                arquivo = arquivoByte
            };
            var executor = this.CriarExecutor<ImportarPlanilhaRequisicao, ImportarPlanilhaResultado>();
            var resultado = executor.Executar(requisicao);

            var vm = new PlanilhaViewModel { Planilha = resultado.Planilha };
            var html = this.RenderRazorViewToString("_PlanilhaVisao", vm);

            return Json(new
            {
                sucesso = resultado.Sucesso,
                mensagem = resultado.Mensagem,
                temInconsistencia = resultado.TemInconsistencia,
                planilha = JsonConvert.SerializeObject(resultado.Planilha),
                html
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir" })]
        public ActionResult Importar(string planilha)
        {
            var planilhaDto = JsonConvert.DeserializeObject<PlanilhaDto>(planilha);

            var requisicao = new SalvarPlanilhaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                Planilha = planilhaDto
            };
            var executor = this.CriarExecutor<SalvarPlanilhaRequisicao, SalvarPlanilhaResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Index" })]
        public ActionResult ListarMedicoes(DataTableAjaxPostModel model, int? idPlanilha)
        {
            var requisicao = new ListarMedicoesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdPlanilha = idPlanilha ?? 0
            };
            var executor = this.CriarExecutor<ListarMedicoesRequisicao, ListarMedicoesResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Medicoes);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Excluir" })]
        public ActionResult ExcluirMedicao(int idPlanilha, DateTime periodoMedicao)
        {
            var requisicao = new ExcluirMedicaoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdPlanilha = idPlanilha,
                PeriodoMedicao = periodoMedicao
            };
            var executor = this.CriarExecutor<ExcluirMedicaoRequisicao, ExcluirMedicaoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir" })]
        public ActionResult EnviarMedicao(int? idPlanilha, DateTime periodoMedicao)
        {
            var arquivoMedicao = Request.Files["arquivoMedicao"];

            var arquivoByte = new byte[arquivoMedicao.ContentLength];
            arquivoMedicao.InputStream.Read(arquivoByte, 0, arquivoByte.Length);

            var requisicao = new ImportarMedicaoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdPlanilha = idPlanilha ?? 0,
                PeriodoMedicao = periodoMedicao,
                arquivo = arquivoByte
            };
            var executor = this.CriarExecutor<ImportarMedicaoRequisicao, ImportarMedicaoResultado>();
            var resultado = executor.Executar(requisicao);

            var vm = new MedicaoViewModel { Planilha = resultado.Planilha, PeriodoMedicao = periodoMedicao };
            var html = this.RenderRazorViewToString("_MedicaoVisao", vm);

            return Json(new
            {
                sucesso = resultado.Sucesso,
                mensagem = resultado.Mensagem,
                temInconsistencia = resultado.TemInconsistencia,
                planilha = JsonConvert.SerializeObject(resultado.Planilha),
                periodo = periodoMedicao,
                html
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir" })]
        public ActionResult ImportarMedicao(string planilha, DateTime periodoMedicao)
        {
            var planilhaDto = JsonConvert.DeserializeObject<PlanilhaDto>(planilha);

            var requisicao = new SalvarMedicaoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                Planilha = planilhaDto,
                PeriodoMedicao = periodoMedicao
            };
            var executor = this.CriarExecutor<SalvarMedicaoRequisicao, SalvarMedicaoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }
    }
}