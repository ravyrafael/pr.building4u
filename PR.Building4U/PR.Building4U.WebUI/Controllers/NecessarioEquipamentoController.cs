using PR.Building4U.Fronteiras.Executores.AdministrarNecessarioEquip;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class NecessarioEquipamentoController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model, int? idObra)
        {
            var requisicao = new ListarNecessarioEquipAprovRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra ?? 0
            };
            var executor = this.CriarExecutor<ListarNecessarioEquipAprovRequisicao, ListarNecessarioEquipAprovResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Necessarios);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Atualizar(int idObra, int idTipoEquipamento, int quantidadeNecessario)
        {
            var requisicao = new AtualizarNecessarioEquipAprovRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra,
                IdTipoEquipamento = idTipoEquipamento,
                QuantidadeNecessario = quantidadeNecessario
            };
            var executor = this.CriarExecutor<AtualizarNecessarioEquipAprovRequisicao, AtualizarNecessarioEquipAprovResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Atualizar" })]
        public ActionResult Enviar(int idObra)
        {
            var requisicao = new EnviarNecessarioEquipAprovRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra
            };
            var executor = this.CriarExecutor<EnviarNecessarioEquipAprovRequisicao, EnviarNecessarioEquipAprovResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Atualizar" })]
        public ActionResult ListarTiposEquipamento(string codigoOuNome)
        {
            var requisicao = new ListarTiposEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarTiposEquipamentoRequisicao, ListarTiposEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.TiposEquipamento = resultado.TiposEquipamento.OrderBy("NomeTipoEquipamento asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var tipoEquipamento in resultado.TiposEquipamento)
            {
                selectList.Add(new SelectListItem
                {
                    Value = tipoEquipamento.IdTipoEquipamento.ToString(),
                    Text = $"{tipoEquipamento.NomeTipoEquipamento} ({tipoEquipamento.Custo.SiglaCusto})"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Index" })]
        public ActionResult ListarObras(string codigoOuNome)
        {
            var requisicao = new ListarObrasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome,
                SomenteAtivas = true,
                SomenteObras = true
            };
            var executor = this.CriarExecutor<ListarObrasRequisicao, ListarObrasResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Obras = resultado.Obras.OrderBy("NomeObra asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var obra in resultado.Obras)
            {
                selectList.Add(new SelectListItem
                {
                    Value = obra.IdObra.ToString(),
                    Text = $"{obra.CodigoObra.PadLeft(3, '0')} - {obra.NomeObra}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }
    }
}
