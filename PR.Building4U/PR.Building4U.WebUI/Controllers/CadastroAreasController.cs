using PR.Building4U.Fronteiras.Executores.AdministrarArea;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
	public class CadastroAreasController : BaseController
	{
		[Authorization]
		public ActionResult Index()
		{
			return View();
		}

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarAreasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarAreasRequisicao, ListarAreasResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Areas);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idArea, string nomeArea)
        {
            var requisicao = new EditarAreaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdArea = idArea,
                NomeArea = nomeArea
            };
            var executor = this.CriarExecutor<EditarAreaRequisicao, EditarAreaResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idArea)
        {
            var requisicao = new ObterAreaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdArea = idArea
            };
            var executor = this.CriarExecutor<ObterAreaRequisicao, ObterAreaResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}
