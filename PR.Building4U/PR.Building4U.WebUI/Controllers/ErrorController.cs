﻿using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index(int error)
        {
            Response.StatusCode = error;

            switch (error)
            {
                case 403: return View("_403");
                case 404: return View("_404");
                case 500: return View("_500");
                default: return View();
            }
        }
    }
}