using PR.Building4U.Fronteiras.Executores.AdministrarUnidade;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
	public class CadastroUnidadesController : BaseController
	{
		[Authorization]
		public ActionResult Index()
		{
			return View();
		}

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarUnidadesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarUnidadesRequisicao, ListarUnidadesResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Unidades);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(string siglaUnidade, string nomeUnidade)
        {
            var requisicao = new IncluirUnidadeRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                SiglaUnidade = siglaUnidade,
                NomeUnidade = nomeUnidade
            };
            var executor = this.CriarExecutor<IncluirUnidadeRequisicao, IncluirUnidadeResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idUnidade, string siglaUnidade, string nomeUnidade)
        {
            var requisicao = new EditarUnidadeRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdUnidade = idUnidade,
                SiglaUnidade = siglaUnidade,
                NomeUnidade = nomeUnidade
            };
            var executor = this.CriarExecutor<EditarUnidadeRequisicao, EditarUnidadeResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Excluir(int idUnidade)
        {
            var requisicao = new ExcluirUnidadeRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdUnidade = idUnidade
            };
            var executor = this.CriarExecutor<ExcluirUnidadeRequisicao, ExcluirUnidadeResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idUnidade)
        {
            var requisicao = new ObterUnidadeRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdUnidade = idUnidade
            };
            var executor = this.CriarExecutor<ObterUnidadeRequisicao, ObterUnidadeResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}
