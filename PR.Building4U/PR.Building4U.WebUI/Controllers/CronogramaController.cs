﻿using Newtonsoft.Json;
using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarCronograma;
using PR.Building4U.Fronteiras.Executores.AdministrarServico;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CronogramaController : BaseController
    {
        [Authorization]
        public ActionResult Index(int idObra, int? idCronograma)
        {
            ViewBag.IdObra = idObra;
            ViewBag.IdCronograma = idCronograma;

            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Carregar(int idObra, int? idCronograma)
        {
            var requisicao = new VisualizarCronogramasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra
            };
            var executor = this.CriarExecutor<VisualizarCronogramasRequisicao, VisualizarCronogramasResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Cronogramas = RecalcularCronogramas(resultado.Cronogramas);

            var vm = new CronogramaViewModel { Cronogramas = resultado.Cronogramas, IdCronograma = idCronograma };
            var htmlCronograma = this.RenderRazorViewToString("_Cronograma", vm);
            var htmlPlanilha = this.RenderRazorViewToString("_Planilha", vm);
            var htmlMedicao = this.RenderRazorViewToString("_Medicao", vm);
            var htmlProducao = this.RenderRazorViewToString("_Producao", vm);

            var json = Json(new
            {
                Cronogramas = JsonConvert.SerializeObject(resultado.Cronogramas),
                HtmlCronograma = htmlCronograma,
                HtmlPlanilha = htmlPlanilha,
                HtmlMedicao = htmlMedicao,
                HtmlProducao = htmlProducao
            }, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = int.MaxValue;

            return json;
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Modificar(ModificarCronogramaRequisicao requisicao)
        {
            requisicao.InformacoesLog = this.GetInformacoesLog();
            var executor = this.CriarExecutor<ModificarCronogramaRequisicao, ModificarCronogramaResultado>();
            var resultado = executor.Executar(requisicao);

            var htmlCronograma = string.Empty;
            var htmlPlanilha = string.Empty;
            var htmlMedicao = string.Empty;
            var htmlProducao = string.Empty;

            if (resultado.Cronogramas != null)
            {
                resultado.Cronogramas = RecalcularCronogramas(resultado.Cronogramas);
                var vm = new CronogramaViewModel { Cronogramas = resultado.Cronogramas, IdCronograma = requisicao.IdCronogramaJson };
                htmlCronograma = this.RenderRazorViewToString("_Cronograma", vm);
                htmlPlanilha = this.RenderRazorViewToString("_Planilha", vm);
                htmlMedicao = this.RenderRazorViewToString("_Medicao", vm);
                htmlProducao = this.RenderRazorViewToString("_Producao", vm);
            }

            var json = Json(new
            {
                Cronogramas = resultado.Cronogramas != null ? JsonConvert.SerializeObject(resultado.Cronogramas) : null,
                HtmlCronograma = htmlCronograma,
                HtmlPlanilha = htmlPlanilha,
                HtmlMedicao = htmlMedicao,
                HtmlProducao = htmlProducao,
                resultado.Mensagem,
                resultado.Sucesso
            }, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = int.MaxValue;

            return json;
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Salvar(string cronogramas)
        {
            var requisicao = new SalvarCronogramasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                Cronogramas = JsonConvert.DeserializeObject<List<CronogramaDto>>(cronogramas)
            };
            var executor = this.CriarExecutor<SalvarCronogramasRequisicao, SalvarCronogramasResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListarServicos(string codigoOuNome)
        {
            var requisicao = new ListarServicosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarServicosRequisicao, ListarServicosResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Servicos = resultado.Servicos.OrderBy("NomeServico asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var servico in resultado.Servicos)
            {
                selectList.Add(new SelectListItem
                {
                    Value = servico.IdServico.ToString(),
                    Text = $"{servico.SiglaServico}{(string.IsNullOrEmpty(servico.NomeServico) ? "" : " - " + servico.NomeServico)}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        private List<CronogramaDto> RecalcularCronogramas(List<CronogramaDto> cronogramas)
        {
            for (var i = 0; i < cronogramas.Count; i++)
            {
                var requisicao = new CalcularCronogramaRequisicao
                {
                    InformacoesLog = this.GetInformacoesLog(),
                    Cronogramas = cronogramas,
                    IdCronograma = cronogramas[i].IdCronograma
                };
                var executor = this.CriarExecutor<CalcularCronogramaRequisicao, CalcularCronogramaResultado>();
                var resultado = executor.Executar(requisicao);

                cronogramas = resultado.Cronogramas;
            }

            return cronogramas;
        }
    }
}