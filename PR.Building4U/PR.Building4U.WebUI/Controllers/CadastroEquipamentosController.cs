using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarEquipamento;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.Fronteiras.Executores.AdministrarTipoEquipamento;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroEquipamentosController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarEquipamentosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
            };
            var executor = this.CriarExecutor<ListarEquipamentosRequisicao, ListarEquipamentosResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Equipamentos);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(EquipamentoDto equipamento)
        {
            var requisicao = new IncluirEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                PatrimonioEquipamento = equipamento.PatrimonioEquipamento,
                MarcaEquipamento = equipamento.MarcaEquipamento,
                ModeloEquipamento = equipamento.ModeloEquipamento,
                AquisicaoEquipamento = equipamento.AquisicaoEquipamento,
                IndicadorTercerio = equipamento.IndicadorTerceiro,
                FornecedorEquipamento = equipamento.FornecedorEquipamento,
                IdTipoEquipamento = equipamento.TipoEquipamento.IdTipoEquipamento,
                IdObra = equipamento.Obra.IdObra
            };
            var executor = this.CriarExecutor<IncluirEquipamentoRequisicao, IncluirEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(EquipamentoDto equipamento)
        {
            var requisicao = new EditarEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdEquipamento = equipamento.IdEquipamento,
                MarcaEquipamento = equipamento.MarcaEquipamento,
                ModeloEquipamento = equipamento.ModeloEquipamento,
                FornecedorEquipamento = equipamento.FornecedorEquipamento
            };
            var executor = this.CriarExecutor<EditarEquipamentoRequisicao, EditarEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Sucatear(int idEquipamento, DateTime sucataEquipamento)
        {
            var requisicao = new SucatearEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdEquipamento = idEquipamento,
                SucataEquipamento = sucataEquipamento
            };
            var executor = this.CriarExecutor<SucatearEquipamentoRequisicao, SucatearEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idEquipamento)
        {
            var requisicao = new ObterEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdEquipamento = idEquipamento
            };
            var executor = this.CriarExecutor<ObterEquipamentoRequisicao, ObterEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarTiposEquipamento(string codigoOuNome)
        {
            var requisicao = new ListarTiposEquipamentoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarTiposEquipamentoRequisicao, ListarTiposEquipamentoResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.TiposEquipamento = resultado.TiposEquipamento.OrderBy("NomeTipoEquipamento asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var cargo in resultado.TiposEquipamento)
            {
                selectList.Add(new SelectListItem
                {
                    Value = cargo.IdTipoEquipamento.ToString(),
                    Text = cargo.NomeTipoEquipamento
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir" })]
        public ActionResult ListarObras(string codigoOuNome)
        {
            var requisicao = new ListarObrasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome,
                SomenteAtivas = true,
                SomenteObras = false
            };
            var executor = this.CriarExecutor<ListarObrasRequisicao, ListarObrasResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Obras = resultado.Obras.OrderBy("NomeObra asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var obra in resultado.Obras)
            {
                selectList.Add(new SelectListItem
                {
                    Value = obra.IdObra.ToString(),
                    Text = $"{obra.CodigoObra.PadLeft(3, '0')} - {obra.NomeObra}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }
    }
}
