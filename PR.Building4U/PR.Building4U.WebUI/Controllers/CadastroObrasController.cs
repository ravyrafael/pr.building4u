using PR.Building4U.Fronteiras.Executores.AdministrarArea;
using PR.Building4U.Fronteiras.Executores.AdministrarGerencia;
using PR.Building4U.Fronteiras.Executores.AdministrarContrato;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PR.Building4U.Fronteiras.Executores.AdministrarObraGerencia;
using PR.Building4U.Fronteiras.Executores.AdministrarFuncionario;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroObrasController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            ViewBag.ListaAreas = ListarAreas();
            ViewBag.ListaGerencias = ListarGerencias();

            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarObrasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarObrasRequisicao, ListarObrasResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Obras);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(string codigoObra, string nomeObra, int idArea, int? idContrato, DateTime? dataOrdemServico, int? prazoOrdemServico)
        {
            var requisicao = new IncluirObraRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoObra = codigoObra,
                NomeObra = nomeObra,
                IdArea = idArea,
                IdContrato = idContrato,
                DataOrdemServico = dataOrdemServico,
                PrazoOrdemServico = prazoOrdemServico
            };
            var executor = this.CriarExecutor<IncluirObraRequisicao, IncluirObraResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem, idObra = resultado.IdObra }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idObra, string codigoObra, string nomeObra, int idArea, int? idContrato, DateTime? dataOrdemServico, int? prazoOrdemServico)
        {
            var requisicao = new EditarObraRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra,
                CodigoObra = codigoObra,
                NomeObra = nomeObra,
                IdArea = idArea,
                IdContrato = idContrato,
                DataOrdemServico = dataOrdemServico,
                PrazoOrdemServico = prazoOrdemServico
            };
            var executor = this.CriarExecutor<EditarObraRequisicao, EditarObraResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Excluir(int idObra)
        {
            var requisicao = new ExcluirObraRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra
            };
            var executor = this.CriarExecutor<ExcluirObraRequisicao, ExcluirObraResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idObra)
        {
            var requisicao = new ObterObraRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra
            };
            var executor = this.CriarExecutor<ObterObraRequisicao, ObterObraResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarContratos(string codigoOuNome)
        {
            var requisicao = new ListarContratosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarContratosRequisicao, ListarContratosResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Contratos = resultado.Contratos.OrderBy("DataContrato desc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var contrato in resultado.Contratos)
            {
                selectList.Add(new SelectListItem
                {
                    Value = contrato.IdContrato.ToString(),
                    Text = $"{contrato.CodigoContrato} ({contrato.Cliente.NomeCliente})"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Index" })]
        public ActionResult ListarGerencias(DataTableAjaxPostModel model, int? idObra)
        {
            var requisicao = new ListarGerenciasPorObraRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra ?? 0
            };
            var executor = this.CriarExecutor<ListarGerenciasPorObraRequisicao, ListarGerenciasPorObraResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.ObraGerencia);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult IncluirGerencia(int idObra, int idGerencia, int idFuncionario)
        {
            var requisicao = new IncluirObraGerenciaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObra = idObra,
                IdGerencia = idGerencia,
                IdFuncionario = idFuncionario
            };
            var executor = this.CriarExecutor<IncluirObraGerenciaRequisicao, IncluirObraGerenciaResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ExcluirGerencia(int idObraGerencia)
        {
            var requisicao = new ExcluirObraGerenciaRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdObraGerencia = idObraGerencia
            };
            var executor = this.CriarExecutor<ExcluirObraGerenciaRequisicao, ExcluirObraGerenciaResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarFuncionarios(string codigoOuNome)
        {
            var requisicao = new ListarFuncionariosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarFuncionariosRequisicao, ListarFuncionariosResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Funcionarios = resultado.Funcionarios.OrderBy("NomeFuncionario asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var funcionario in resultado.Funcionarios)
            {
                selectList.Add(new SelectListItem
                {
                    Value = funcionario.IdFuncionario.ToString(),
                    Text = $"{funcionario.ChapaFuncionario} - {funcionario.NomeFuncionario}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> ListarAreas()
        {
            var requisicao = new ListarAreasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarAreasRequisicao, ListarAreasResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Areas = resultado.Areas.OrderBy("SiglaArea asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var area in resultado.Areas)
            {
                selectList.Add(new SelectListItem
                {
                    Value = area.IdArea.ToString(),
                    Text = $"{area.SiglaArea} - {area.NomeArea}"
                });
            }

            return selectList;
        }

        private List<SelectListItem> ListarGerencias()
        {
            var requisicao = new ListarGerenciasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarGerenciasRequisicao, ListarGerenciasResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Gerencias = resultado.Gerencias.OrderBy("SiglaGerencia asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var gerencia in resultado.Gerencias)
            {
                selectList.Add(new SelectListItem
                {
                    Value = gerencia.IdGerencia.ToString(),
                    Text = $"{gerencia.SiglaGerencia} - {gerencia.NomeGerencia}"
                });
            }

            return selectList;
        }
    }
}
