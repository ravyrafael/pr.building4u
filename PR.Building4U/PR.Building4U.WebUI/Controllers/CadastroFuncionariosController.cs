using PR.Building4U.Fronteiras.Dtos;
using PR.Building4U.Fronteiras.Executores.AdministrarCargo;
using PR.Building4U.Fronteiras.Executores.AdministrarEstado;
using PR.Building4U.Fronteiras.Executores.AdministrarFuncionario;
using PR.Building4U.Fronteiras.Executores.AdministrarMunicipio;
using PR.Building4U.Fronteiras.Executores.AdministrarObra;
using PR.Building4U.Fronteiras.Executores.AdministrarPais;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
    public class CadastroFuncionariosController : BaseController
    {
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarFuncionariosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarFuncionariosRequisicao, ListarFuncionariosResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Funcionarios);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(FuncionarioDto funcionario)
        {
            var requisicao = new IncluirFuncionarioRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                ChapaFuncionario = funcionario.ChapaFuncionario,
                NomeFuncionario = funcionario.NomeFuncionario,
                AdmissaoFuncionario = funcionario.AdmissaoFuncionario,
                EmailFuncionario = funcionario.EmailFuncionario,
                TelefoneFuncionario = funcionario.TelefoneFuncionario,
                IdCargo = funcionario.Cargo.IdCargo,
                IdObra = funcionario.Obra.IdObra,
                LogradouroEndereco = funcionario.Endereco.LogradouroEndereco,
                NumeroEndereco = funcionario.Endereco.NumeroEndereco,
                ComplementoEndereco = funcionario.Endereco.ComplementoEndereco,
                CepEndereco = funcionario.Endereco.CepEndereco,
                DistritoEndereco = funcionario.Endereco.DistritoEndereco,
                IdMunicipio = funcionario.Endereco.Municipio.IdMunicipio
            };
            var executor = this.CriarExecutor<IncluirFuncionarioRequisicao, IncluirFuncionarioResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(FuncionarioDto funcionario)
        {
            var requisicao = new EditarFuncionarioRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdFuncionario = funcionario.IdFuncionario,
                NomeFuncionario = funcionario.NomeFuncionario,
                EmailFuncionario = funcionario.EmailFuncionario,
                TelefoneFuncionario = funcionario.TelefoneFuncionario,
                IdCargo = funcionario.Cargo.IdCargo,
                IdEndereco = funcionario.Endereco.IdEndereco,
                LogradouroEndereco = funcionario.Endereco.LogradouroEndereco,
                NumeroEndereco = funcionario.Endereco.NumeroEndereco,
                ComplementoEndereco = funcionario.Endereco.ComplementoEndereco,
                CepEndereco = funcionario.Endereco.CepEndereco,
                DistritoEndereco = funcionario.Endereco.DistritoEndereco,
                IdMunicipio = funcionario.Endereco.Municipio.IdMunicipio
            };
            var executor = this.CriarExecutor<EditarFuncionarioRequisicao, EditarFuncionarioResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Demitir(int idFuncionario, DateTime demissaoFuncionario)
        {
            var requisicao = new DemitirFuncionarioRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdFuncionario = idFuncionario,
                DemissaoFuncionario = demissaoFuncionario
            };
            var executor = this.CriarExecutor<DemitirFuncionarioRequisicao, DemitirFuncionarioResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idFuncionario)
        {
            var requisicao = new ObterFuncionarioRequisicao()
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdFuncionario = idFuncionario
            };
            var executor = this.CriarExecutor<ObterFuncionarioRequisicao, ObterFuncionarioResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarCargos(string codigoOuNome)
        {
            var requisicao = new ListarCargosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarCargosRequisicao, ListarCargosResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Cargos = resultado.Cargos.OrderBy("NomeCargo asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var cargo in resultado.Cargos)
            {
                selectList.Add(new SelectListItem
                {
                    Value = cargo.IdCargo.ToString(),
                    Text = $"{cargo.NomeCargo} ({cargo.Custo.SiglaCusto})"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir" })]
        public ActionResult ListarObras(string codigoOuNome)
        {
            var requisicao = new ListarObrasRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome,
                SomenteAtivas = true,
                SomenteObras = false
            };
            var executor = this.CriarExecutor<ListarObrasRequisicao, ListarObrasResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Obras = resultado.Obras.OrderBy("NomeObra asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var obra in resultado.Obras)
            {
                selectList.Add(new SelectListItem
                {
                    Value = obra.IdObra.ToString(),
                    Text = $"{obra.CodigoObra.PadLeft(3, '0')} - {obra.NomeObra}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarPaises(string codigoOuNome)
        {
            var requisicao = new ListarPaisesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarPaisesRequisicao, ListarPaisesResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Paises = resultado.Paises.OrderBy("NomePais asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var pais in resultado.Paises)
            {
                selectList.Add(new SelectListItem
                {
                    Value = pais.IdPais.ToString(),
                    Text = $"{pais.CodigoPais.ToString().PadLeft(4, '0')} - {pais.NomePais}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarEstados(string codigoOuNome, string idPais)
        {
            var requisicao = new ListarEstadosPorPaisRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdPais = int.Parse(idPais),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarEstadosPorPaisRequisicao, ListarEstadosPorPaisResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Estados = resultado.Estados.OrderBy("NomeEstado asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var estado in resultado.Estados)
            {
                selectList.Add(new SelectListItem
                {
                    Value = estado.IdEstado.ToString(),
                    Text = $"{estado.SiglaEstado} - {estado.NomeEstado}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Incluir", "Editar" })]
        public ActionResult ListarMunicipios(string codigoOuNome, string idEstado)
        {
            var requisicao = new ListarMunicipiosPorEstadoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdEstado = int.Parse(idEstado),
                CodigoOuNome = codigoOuNome
            };
            var executor = this.CriarExecutor<ListarMunicipiosPorEstadoRequisicao, ListarMunicipiosPorEstadoResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Municipios = resultado.Municipios.OrderBy("NomeMunicipio asc").ToList();

            var selectList = new List<SelectListItem>();

            foreach (var municipio in resultado.Municipios)
            {
                selectList.Add(new SelectListItem
                {
                    Value = municipio.IdMunicipio.ToString(),
                    Text = $"{municipio.CodigoMunicipio.ToString().PadLeft(4, '0')} - {municipio.NomeMunicipio}"
                });
            }

            return Json(selectList, JsonRequestBehavior.AllowGet);
        }
    }
}
