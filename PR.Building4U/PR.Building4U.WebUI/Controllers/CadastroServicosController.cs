using PR.Building4U.Fronteiras.Executores.AdministrarServico;
using PR.Building4U.Fronteiras.Executores.AdministrarUnidade;
using PR.Building4U.WebUI.DataAnnotations;
using PR.Building4U.WebUI.Helper;
using PR.Building4U.WebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PR.Building4U.WebUI.Controllers
{
	public class CadastroServicosController : BaseController
	{
        [Authorization]
        public ActionResult Index()
        {
            ViewBag.ListaUnidades = ListarUnidades();

            return View();
        }

        [Authorization(new[] { "Index" })]
        public ActionResult Listar(DataTableAjaxPostModel model)
        {
            var requisicao = new ListarServicosRequisicao
            {
                InformacoesLog = this.GetInformacoesLog()
            };
            var executor = this.CriarExecutor<ListarServicosRequisicao, ListarServicosResultado>();
            var resultado = executor.Executar(requisicao);

            var datatable = this.RenderDatatableToView(model, resultado.Servicos);

            return Json(datatable, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Incluir(int codigoServico, string siglaServico, string nomeServico, int idUnidade)
        {
            var requisicao = new IncluirServicoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                CodigoServico = codigoServico,
                SiglaServico = siglaServico,
                NomeServico = nomeServico,
                IdUnidade = idUnidade
            };
            var executor = this.CriarExecutor<IncluirServicoRequisicao, IncluirServicoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Editar(int idServico, string siglaServico, string nomeServico, int idUnidade)
        {
            var requisicao = new EditarServicoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdServico = idServico,
                SiglaServico = siglaServico,
                NomeServico = nomeServico,
                IdUnidade = idUnidade
            };
            var executor = this.CriarExecutor<EditarServicoRequisicao, EditarServicoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization]
        public ActionResult Excluir(int idServico)
        {
            var requisicao = new ExcluirServicoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdServico = idServico
            };
            var executor = this.CriarExecutor<ExcluirServicoRequisicao, ExcluirServicoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(new { sucesso = resultado.Sucesso, mensagem = resultado.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Authorization(new[] { "Editar" })]
        public ActionResult Obter(int idServico)
        {
            var requisicao = new ObterServicoRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
                IdServico = idServico
            };
            var executor = this.CriarExecutor<ObterServicoRequisicao, ObterServicoResultado>();
            var resultado = executor.Executar(requisicao);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> ListarUnidades()
        {
            var requisicao = new ListarUnidadesRequisicao
            {
                InformacoesLog = this.GetInformacoesLog(),
            };
            var executor = this.CriarExecutor<ListarUnidadesRequisicao, ListarUnidadesResultado>();
            var resultado = executor.Executar(requisicao);

            resultado.Unidades = resultado.Unidades.OrderBy("NomeUnidade asc").ToList();

            var selectList = resultado.Unidades.Select(x => new SelectListItem
            {
                Text = $"{x.SiglaUnidade} ({x.NomeUnidade})",
                Value = x.IdUnidade.ToString()
            }).ToList();

            return selectList;
        }
    }
}
