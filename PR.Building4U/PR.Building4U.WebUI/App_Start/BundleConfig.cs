﻿using System.Web.Optimization;

namespace PR.Building4U.WebUI
{
    public static class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Method intentionally left empty.
        }
    }
}