﻿"use strict";

let cadastroCronogramas = new CadastroCronogramas();

function CadastroCronogramas() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroCronogramas", "Incluir");
        let editar = PossuiPermissao("CadastroCronogramas", "Editar");
        let visualizar = PossuiPermissao("Cronograma", "Index");

        if (incluir || editar || visualizar) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroCronogramas.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroCronogramas.Editar };
            let btnCancelar = { button: "medium red", label: "Cancelar", icon: "mdi mdi-close", onClick: cadastroCronogramas.CancelarTab };
            let btnVisualizar = { button: "medium purple", label: "Visualizar", icon: "mdi mdi-eye", onClick: cadastroCronogramas.Visualizar };

            botoes = visualizar ? botoes.concat(btnVisualizar) : botoes;
            botoes = botoes.concat(btnCancelar);
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdCronograma", searchable: false, orderable: false, visible: false },
            { title: "Ordem", data: "OrdemCronograma", searchable: true, orderable: true },
            { title: "Nome", data: "NomeCronograma", searchable: true, orderable: true },
            { title: "1º Dia Med.", data: "DiaMedicaoCronograma", searchable: true, orderable: true },
            {
                title: "Início", data: "InicioCronograma", searchable: false, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            {
                title: "Término", data: "TerminoCronograma", searchable: false, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            {
                title: "Atualização", data: "AtualizacaoCronograma", searchable: false, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            }
        ];
        let ordem = [
            [1, "asc"]
        ];
        let parametros = function (p) {
            p.idObra = $("#selIdObra").val();
            return p;
        };

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true, parametros);
    };

    this.CriarSelect2 = function () {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };

        CriarSelect2Dinamico("selIdObra", urlListarObras, "Informe código ou nome...", null, 2, paramSelect, false);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdCronograma");

            MostrarModal("modalCadastro", "80%");
            LimparModal("modalCadastro");

            let parametros = {
                idCronograma: id
            };
            let sucesso = function (data) {
                if (data.Cronograma) {
                    $("#hidIdCronograma").val(data.Cronograma.IdCronograma);
                    $("#inpNomeCronograma").val(data.Cronograma.NomeCronograma);
                    $("#inpMedicaoCronograma").val(data.Cronograma.DiaMedicaoCronograma);
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idObra: $("#selIdObra").val(),
                idCronograma: $("#hidIdCronograma").val(),
                nomeCronograma: $("#inpNomeCronograma").val(),
                diaMedicaoCronograma: $("#inpMedicaoCronograma").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdCronograma").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };

    this.CancelarTab = function () {
        $("#selIdObra").prop("disabled", false);
        $("#selIdObra").val("").change();
    };

    this.Visualizar = function () {
        let data = {
            idObra: $("#selIdObra").val(),
            idCronograma: ObterCampoTabela("tabelaCadastro", "IdCronograma")
        };

        AbrirJanelaPost(urlVisuzalizar, data);
    };
}

$(document).ready(function () {
    cadastroCronogramas.CriarTabela();
    cadastroCronogramas.CriarSelect2();

    $("#selIdObra").change(function (e) {
        if (e.currentTarget.value) {
            $("#rowTabela").css("display", "block");
            $("#menu-action").html("");
            $("#selIdObra").prop("disabled", true);
            cadastroCronogramas.CriarMenu();
            AtualizarDatatable("tabelaCadastro");
        }
        else {
            $("#rowTabela").css("display", "none");
            $("#menu-action").html("");
        }
    });

    $("#inpMedicaoCronograma").change(function (e) {
        if (e.currentTarget.value && (e.currentTarget.value > 28 || e.currentTarget.value == 0))
            e.currentTarget.value = "";
    });
});
