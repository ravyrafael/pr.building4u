﻿"use strict";

let cadastroGerencias = new CadastroGerencias();

function CadastroGerencias() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroGerencias", "Incluir");
        let excluir = PossuiPermissao("CadastroGerencias", "Excluir");

        if (incluir || excluir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroGerencias.Incluir };
            let btnExcluir = { button: "medium red", label: "Excluir", icon: "mdi mdi-delete", onClick: cadastroGerencias.Excluir };

            botoes = excluir ? botoes.concat(btnExcluir) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        var colunas = [
            { title: "Id", data: "IdGerencia", searchable: false, orderable: false, visible: false },
            { title: "Nome", data: "NomeGerencia", searchable: true, orderable: true },
            { title: "Sigla", data: "SiglaGerencia", searchable: true, orderable: true }
        ];
        var ordem = [
            [1, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        LimparModal("modalCadastro");
    };

    this.Excluir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdGerencia");

            let confirma = function () {
                let parametros = {
                    idGerencia: id
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlExcluir, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idGerencia: $("#hidIdGerencia").val(),
                nomeGerencia: $("#inpNomeGerencia").val(),
                siglaGerencia: $("#inpSiglaGerencia").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };
}

$(document).ready(function () {
    cadastroGerencias.CriarMenu();
    cadastroGerencias.CriarTabela();
});
