﻿"use strict";

let login = new Login();

function Login() {
    this.ValidarTamanhoSenha = function () {
        if ($("#inpSenha").val().length < 6) {
            AdicionarClasseAviso("inpSenha", true);
            NotificacaoAviso("A senha deve ter no mínimo 6 dígitos.");
            return false;
        }
        return true;
    };

    this.ValidarUsuario = function () {
        let parametros = {
            usuario: $("#inpUsuario").val(),
            senha: $("#inpSenha").val()
        };
        let sucesso = function (data) {
            if (data.Logado)
                window.location.replace(urlHome);
            else
                NotificacaoAviso("Usuário ou senha estão incorretos.");
        };

        RequisicaoAjax(urlValidarUsuario, "json", parametros, "POST", true, true, sucesso);
    };

    this.ValidarLogin = function () {
        if (ValidarCamposObrigatorios())
            if (login.ValidarTamanhoSenha())
                login.ValidarUsuario();
    };
}

$(document).ready(function () {
    $("#inpUsuario, #inpSenha").keypress(function (e) {
        if (e.which == 10 || e.which == 13)
            login.ValidarLogin();
    });

    $("#btnEntrar").click(function () {
        login.ValidarLogin();
    });
});
