﻿"use strict";

let necessarioEquipamento = new NecessarioEquipamento();

function NecessarioEquipamento() {
    this.CriarMenu = function () {
        let atualizar = PossuiPermissao("NecessarioEquipamento", "Atualizar");

        if (atualizar) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: necessarioEquipamento.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: necessarioEquipamento.Editar };
            let btnCancelar = { button: "medium red", label: "Cancelar", icon: "mdi mdi-close", onClick: necessarioEquipamento.CancelarNec };
            let btnEnviar = { button: "medium purple", label: "Salvar e Enviar", icon: "mdi mdi-send", onClick: necessarioEquipamento.Enviar };

            botoes = botoes.concat(btnEnviar);
            botoes = botoes.concat(btnCancelar);
            botoes = botoes.concat(btnEditar);
            botoes = botoes.concat(btnIncluir);

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdNecessario", searchable: false, orderable: false, visible: false },
            { title: "Id Equipamento", data: "TipoEquipamento.IdTipoEquipamento", searchable: false, orderable: false, visible: false },
            { title: "Sigla Custo", data: "TipoEquipamento.Custo.NomeCusto", searchable: false, orderable: false, visible: false },
            { title: "Custo", data: "TipoEquipamento.Custo.NomeCusto", searchable: true, orderable: true },
            { title: "Equipamento", data: "TipoEquipamento.NomeTipoEquipamento", searchable: true, orderable: true },
            { title: "Quantidade", data: "QuantidadeNecessario", searchable: true, orderable: true }
        ];
        let ordem = [
            [3, "asc"], [4, "asc"]
        ];
        let parametros = function (p) {
            p.idObra = $("#selIdObra").val();
            return p;
        };

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true, parametros);
    };

    this.CriarSelect2 = function () {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };

        CriarSelect2Dinamico("selIdObra", urlListarObras, "Informe código ou nome...", null, 2, paramSelect, false);
        CriarSelect2Dinamico("selIdTipoEquipamento", urlListarTiposEquipamento, "Informe nome...", "modalCadastro", 2, paramSelect, false);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        $("#selIdTipoEquipamento").prop("disabled", false);
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let idNecessario = ObterCampoTabela("tabelaCadastro", "IdNecessario");
            let idTipoEquipamento = ObterCampoTabela("tabelaCadastro", "TipoEquipamento.IdTipoEquipamento");
            let siglaCusto = ObterCampoTabela("tabelaCadastro", "TipoEquipamento.Custo.SiglaCusto");
            let nomeTipoEquipamento = ObterCampoTabela("tabelaCadastro", "TipoEquipamento.NomeTipoEquipamento");
            let quantidade = ObterCampoTabela("tabelaCadastro", "QuantidadeNecessario");

            MostrarModal("modalCadastro", "80%");
            $("#selIdTipoEquipamento").prop("disabled", true);
            LimparModal("modalCadastro");

            $("#hidIdNecessario").val(idNecessario);
            $("#inpQuantidadeNecessario").val(quantidade);
            DefinirValorSelect2Dinamico("selIdTipoEquipamento", idTipoEquipamento, nomeTipoEquipamento + " (" + siglaCusto + ")");
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idObra: $("#selIdObra").val(),
                idTipoEquipamento: $("#selIdTipoEquipamento").val(),
                quantidadeNecessario: $("#inpQuantidadeNecessario").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            RequisicaoAjax(urlAtualizar, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };

    this.CancelarNec = function () {
        let confirma = function () {
            $("#selIdObra").prop("disabled", false);
            $("#selIdObra").val("").change();
        };

        AlertaConfirmacao("Tem certeza?", "Os dados alterados serão perdidos.", confirma);
    };

    this.Enviar = function () {
        let idObra = $("#selIdObra").val();

        let confirma = function () {
            let parametros = {
                idObra: idObra
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    $("#selIdObra").prop("disabled", false);
                    $("#selIdObra").val("").change();
                }
            };

            RequisicaoAjax(urlEnviar, "json", parametros, "POST", true, true, sucesso);
        };

        AlertaConfirmacao("Tem certeza?", "A solicitação será salva e enviada ao responsável.", confirma);
    };
}

$(document).ready(function () {
    necessarioEquipamento.CriarTabela();
    necessarioEquipamento.CriarSelect2();

    $("#selIdObra").change(function (e) {
        if (e.currentTarget.value) {
            $("#rowTabela").css("display", "block");
            $("#menu-action").html("");
            $("#selIdObra").prop("disabled", true);
            necessarioEquipamento.CriarMenu();
            AtualizarDatatable("tabelaCadastro");
        }
        else {
            $("#rowTabela").css("display", "none");
            $("#menu-action").html("");
        }
    });
});
