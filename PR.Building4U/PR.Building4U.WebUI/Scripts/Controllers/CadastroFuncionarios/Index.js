﻿"use strict";

let cadastroFuncionarios = new CadastroFuncionarios();

function CadastroFuncionarios() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroFuncionarios", "Incluir");
        let editar = PossuiPermissao("CadastroFuncionarios", "Editar");
        let demitir = PossuiPermissao("CadastroFuncionarios", "Demitir");

        if (incluir || editar || demitir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroFuncionarios.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroFuncionarios.Editar };
            let btnDemitir = { button: "medium red", label: "Demitir", icon: "mdi mdi-account-remove", onClick: cadastroFuncionarios.Demitir };

            botoes = demitir ? botoes.concat(btnDemitir) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdFuncionario", searchable: false, orderable: false, visible: false },
            { title: "Chapa", data: "ChapaFuncionario", searchable: true, orderable: true },
            { title: "Nome", data: "NomeFuncionario", searchable: true, orderable: true },
            {
                title: "Admissão", data: "AdmissaoFuncionario", searchable: false, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            {
                title: "Demissão", data: "DemissaoFuncionario", searchable: false, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            { title: "Cargo", data: "Cargo.NomeCargo", searchable: true, orderable: true },
            { title: "Situação", data: "Situacao.NomeSituacao", searchable: true, orderable: true }
        ];
        let ordem = [
            [2, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.CriarSelect2 = function () {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };
        let paramSelectEstado = function (term) {
            return {
                codigoOuNome: term.term,
                idPais: $("#selIdPais").val()
            };
        };
        let paramSelectMunicipio = function (term) {
            return {
                codigoOuNome: term.term,
                idEstado: $("#selIdEstado").val()
            };
        };

        CriarSelect2Dinamico("selIdCargo", urlListarCargos, "Informe nome...", "modalCadastro", 2, paramSelect, false);
        CriarSelect2Dinamico("selIdObra", urlListarObras, "Informe código ou nome...", "modalCadastro", 2, paramSelect, false);
        CriarSelect2Dinamico("selIdPais", urlListarPaises, "Informe código ou nome...", "modalCadastro", 2, paramSelect, false);
        CriarSelect2Dinamico("selIdEstado", urlListarEstados, "Informe sigla ou nome...", "modalCadastro", 2, paramSelectEstado, false);
        CriarSelect2Dinamico("selIdMunicipio", urlListarMunicipios, "Informe código ou nome...", "modalCadastro", 2, paramSelectMunicipio, false);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        $("#inpChapaFuncionario").prop("disabled", false);
        $("#inpAdmissaoFuncionario").prop("disabled", false);
        $("#selIdObra").prop("disabled", false);
        $("#selIdEstado").prop("disabled", true);
        $("#selIdMunicipio").prop("disabled", true);
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdFuncionario");

            MostrarModal("modalCadastro", "80%");
            $("#inpChapaFuncionario").prop("disabled", true);
            $("#inpAdmissaoFuncionario").prop("disabled", true);
            $("#selIdObra").prop("disabled", true);
            $("#selIdEstado").prop("disabled", false);
            $("#selIdMunicipio").prop("disabled", false);
            LimparModal("modalCadastro");

            let parametros = {
                idFuncionario: id
            };
            let sucesso = function (data) {
                if (data.Funcionario) {
                    $("#hidIdFuncionario").val(data.Funcionario.IdFuncionario);
                    $("#inpChapaFuncionario").val(data.Funcionario.ChapaFuncionario);
                    $("#inpNomeFuncionario").val(data.Funcionario.NomeFuncionario);
                    $("#inpAdmissaoFuncionario").val(DateJsonToDateString(data.Funcionario.AdmissaoFuncionario)).change();
                    $("#inpEmailFuncionario").val(data.Funcionario.EmailFuncionario).change();
                    $("#inpTelefoneFuncionario").val(data.Funcionario.TelefoneFuncionario).change();
                    $("#hidIdEndereco").val(data.Funcionario.Endereco.IdEndereco);
                    $("#inpLogradouroEndereco").val(data.Funcionario.Endereco.LogradouroEndereco);
                    $("#inpNumeroEndereco").val(data.Funcionario.Endereco.NumeroEndereco);
                    $("#inpCepEndereco").val(data.Funcionario.Endereco.CepEndereco).change();
                    $("#inpComplementoEndereco").val(data.Funcionario.Endereco.ComplementoEndereco);
                    $("#inpDistritoEndereco").val(data.Funcionario.Endereco.DistritoEndereco);
                    DefinirValorSelect2Dinamico("selIdCargo", data.Funcionario.Cargo.IdCargo, data.Funcionario.Cargo.NomeCargo);
                    DefinirValorSelect2Dinamico("selIdObra", data.Funcionario.Obra.IdObra, PadLeft(data.Funcionario.Obra.CodigoObra, "0", 3) + " - " + data.Funcionario.Obra.NomeObra);
                    DefinirValorSelect2Dinamico("selIdPais", data.Funcionario.Endereco.Municipio.Estado.Pais.IdPais, PadLeft(data.Funcionario.Endereco.Municipio.Estado.Pais.CodigoPais.toString(), "0", 4) + " - " + data.Funcionario.Endereco.Municipio.Estado.Pais.NomePais);
                    DefinirValorSelect2Dinamico("selIdEstado", data.Funcionario.Endereco.Municipio.Estado.IdEstado, data.Funcionario.Endereco.Municipio.Estado.SiglaEstado + " - " + data.Funcionario.Endereco.Municipio.Estado.NomeEstado);
                    DefinirValorSelect2Dinamico("selIdMunicipio", data.Funcionario.Endereco.Municipio.IdMunicipio, PadLeft(data.Funcionario.Endereco.Municipio.CodigoMunicipio.toString(), "0", 4) + " - " + data.Funcionario.Endereco.Municipio.NomeMunicipio);
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Demitir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdFuncionario");
            let nome = ObterCampoTabela("tabelaCadastro", "NomeFuncionario");

            MostrarModal("modalDemissao");
            LimparModal("modalDemissao");
            $("#hidIdFuncionarioDemissao").val(id);
            $("#inpDemissaoFuncionario").val(DateToString(new Date())).change();
            $("#spnNomeFuncionario").html(nome);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let municipio = {
                IdMunicipio: $("#selIdMunicipio").val()
            };
            let endereco = {
                IdEndereco: $("#hidIdEndereco").val(),
                LogradouroEndereco: $("#inpLogradouroEndereco").val(),
                NumeroEndereco: $("#inpNumeroEndereco").val(),
                ComplementoEndereco: $("#inpComplementoEndereco").val(),
                CepEndereco: $("#inpCepEndereco").val(),
                DistritoEndereco: $("#inpDistritoEndereco").val(),
                Municipio: municipio
            };
            let cargo = {
                IdCargo: $("#selIdCargo").val()
            };
            let obra = {
                IdObra: $("#selIdObra").val()
            };
            let parametros = {
                IdFuncionario: $("#hidIdFuncionario").val(),
                ChapaFuncionario: $("#inpChapaFuncionario").val(),
                NomeFuncionario: $("#inpNomeFuncionario").val(),
                AdmissaoFuncionario: $("#inpAdmissaoFuncionario").val(),
                EmailFuncionario: $("#inpEmailFuncionario").val(),
                TelefoneFuncionario: $("#inpTelefoneFuncionario").val(),
                Endereco: endereco,
                Cargo: cargo,
                Obra: obra
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdFuncionario").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };

    this.CancelarDemissao = function () {
        LimparModal("modalDemissao");
        EsconderModal("modalDemissao");
    };

    this.ConfirmarDemissao = function () {
        if (ValidarCamposObrigatorios("modalDemissao")) {
            let parametros = {
                idFuncionario: $("#hidIdFuncionarioDemissao").val(),
                demissaoFuncionario: $("#inpDemissaoFuncionario").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalDemissao");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            RequisicaoAjax(urlDemitir, "json", parametros, "POST", true, true, sucesso);
        }
    };
}

$(document).ready(function () {
    cadastroFuncionarios.CriarMenu();
    cadastroFuncionarios.CriarTabela();
    cadastroFuncionarios.CriarSelect2();

    $("#selIdPais").change(function (e) {
        $("#selIdEstado").val("").change();
        if (e.currentTarget.value)
            $("#selIdEstado").prop("disabled", false);
        else
            $("#selIdEstado").prop("disabled", true);
    });

    $("#selIdEstado").change(function (e) {
        $("#selIdMunicipio").val("").change();
        if (e.currentTarget.value)
            $("#selIdMunicipio").prop("disabled", false);
        else
            $("#selIdMunicipio").prop("disabled", true);
    });
});
