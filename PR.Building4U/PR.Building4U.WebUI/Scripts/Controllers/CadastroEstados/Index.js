﻿"use strict";

let cadastroEstados = new CadastroEstados();

function CadastroEstados() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroEstados", "Incluir");
        let editar = PossuiPermissao("CadastroEstados", "Editar");
        let excluir = PossuiPermissao("CadastroEstados", "Excluir");

        if (incluir || editar || excluir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroEstados.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroEstados.Editar };
            let btnExcluir = { button: "medium red", label: "Excluir", icon: "mdi mdi-delete", onClick: cadastroEstados.Excluir };

            botoes = excluir ? botoes.concat(btnExcluir) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        var colunas = [
            { title: "Id", data: "IdEstado", searchable: false, orderable: false, visible: false },
            { title: "Sigla", data: "SiglaEstado", searchable: true, orderable: true },
            { title: "Nome", data: "NomeEstado", searchable: true, orderable: true },
            { title: "País", data: "Pais.NomePais", searchable: true, orderable: true }
        ];
        var ordem = [
            [2, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.CriarSelect2 = function () {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };

        CriarSelect2Dinamico("selIdPais", urlListarPaises, "Informe código ou nome...", "modalCadastro", 2, paramSelect, false);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        $("#inpSiglaEstado").prop("disabled", false);
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdEstado");

            MostrarModal("modalCadastro", "80%");
            $("#inpSiglaEstado").prop("disabled", true);
            LimparModal("modalCadastro");

            let parametros = {
                idEstado: id
            };
            let sucesso = function (data) {
                if (data.Estado) {
                    $("#hidIdEstado").val(data.Estado.IdEstado);
                    $("#inpSiglaEstado").val(data.Estado.SiglaEstado);
                    $("#inpNomeEstado").val(data.Estado.NomeEstado);
                    DefinirValorSelect2Dinamico("selIdPais", data.Estado.Pais.IdPais, PadLeft(data.Estado.Pais.CodigoPais.toString(), "0", 4) + " - " + data.Estado.Pais.NomePais);
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Excluir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdEstado");

            let confirma = function () {
                let parametros = {
                    idEstado: id
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlExcluir, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idEstado: $("#hidIdEstado").val(),
                siglaEstado: $("#inpSiglaEstado").val(),
                nomeEstado: $("#inpNomeEstado").val(),
                idPais: $("#selIdPais").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdEstado").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };
}

$(document).ready(function () {
    cadastroEstados.CriarMenu();
    cadastroEstados.CriarTabela();
    cadastroEstados.CriarSelect2();
});
