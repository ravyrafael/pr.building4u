﻿"use strict";

let cadastroMunicipios = new CadastroMunicipios();

function CadastroMunicipios() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroMunicipios", "Incluir");
        let editar = PossuiPermissao("CadastroMunicipios", "Editar");
        let excluir = PossuiPermissao("CadastroMunicipios", "Excluir");

        if (incluir || editar || excluir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroMunicipios.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroMunicipios.Editar };
            let btnExcluir = { button: "medium red", label: "Excluir", icon: "mdi mdi-delete", onClick: cadastroMunicipios.Excluir };

            botoes = excluir ? botoes.concat(btnExcluir) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        var colunas = [
            { title: "Id", data: "IdMunicipio", searchable: false, orderable: false, visible: false },
            { title: "Código", data: "CodigoMunicipio", searchable: true, orderable: true },
            { title: "Nome", data: "NomeMunicipio", searchable: true, orderable: true },
            { title: "Estado", data: "Estado.NomeEstado", searchable: true, orderable: true },
            { title: "País", data: "Estado.Pais.NomePais", searchable: true, orderable: true }
        ];
        var ordem = [
            [2, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.CriarSelect2 = function () {
        let paramSelectPais = function (term) {
            return {
                codigoOuNome: term.term
            };
        };
        let paramSelectEstado = function (term) {
            return {
                codigoOuNome: term.term,
                idPais: $("#selIdPais").val()
            };
        };

        CriarSelect2Dinamico("selIdPais", urlListarPaises, "Informe código ou nome...", "modalCadastro", 2, paramSelectPais, false);
        CriarSelect2Dinamico("selIdEstado", urlListarEstados, "Informe sigla ou nome...", "modalCadastro", 2, paramSelectEstado, false);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        $("#inpCodigoMunicipio").prop("disabled", false);
        $("#selIdEstado").prop("disabled", true);
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdMunicipio");

            MostrarModal("modalCadastro", "80%");
            $("#inpCodigoMunicipio").prop("disabled", true);
            $("#selIdEstado").prop("disabled", false);
            LimparModal("modalCadastro");

            let parametros = {
                idMunicipio: id
            };
            let sucesso = function (data) {
                if (data.Municipio) {
                    $("#hidIdMunicipio").val(data.Municipio.IdMunicipio);
                    $("#inpCodigoMunicipio").val(data.Municipio.CodigoMunicipio);
                    $("#inpNomeMunicipio").val(data.Municipio.NomeMunicipio);
                    DefinirValorSelect2Dinamico("selIdPais", data.Municipio.Estado.Pais.IdPais, PadLeft(data.Municipio.Estado.Pais.CodigoPais.toString(), "0", 4) + " - " + data.Municipio.Estado.Pais.NomePais);
                    DefinirValorSelect2Dinamico("selIdEstado", data.Municipio.Estado.IdEstado, data.Municipio.Estado.SiglaEstado + " - " + data.Municipio.Estado.NomeEstado);
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Excluir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdMunicipio");

            let confirma = function () {
                let parametros = {
                    idMunicipio: id
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlExcluir, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idMunicipio: $("#hidIdMunicipio").val(),
                codigoMunicipio: $("#inpCodigoMunicipio").val(),
                nomeMunicipio: $("#inpNomeMunicipio").val(),
                idEstado: $("#selIdEstado").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdMunicipio").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };
}

$(document).ready(function () {
    cadastroMunicipios.CriarMenu();
    cadastroMunicipios.CriarTabela();
    cadastroMunicipios.CriarSelect2();

    $("#selIdPais").change(function (e) {
        $("#selIdEstado").val("").change();
        if (e.currentTarget.value)
            $("#selIdEstado").prop("disabled", false);
        else
            $("#selIdEstado").prop("disabled", true);
    });
});
