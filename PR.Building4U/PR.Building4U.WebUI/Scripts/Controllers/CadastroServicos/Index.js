﻿"use strict";

let cadastroServicos = new CadastroServicos();

function CadastroServicos() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroServicos", "Incluir");
        let editar = PossuiPermissao("CadastroServicos", "Editar");
        let excluir = PossuiPermissao("CadastroServicos", "Excluir");

        if (incluir || editar || excluir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroServicos.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroServicos.Editar };
            let btnExcluir = { button: "medium red", label: "Excluir", icon: "mdi mdi-delete", onClick: cadastroServicos.Excluir };

            botoes = excluir ? botoes.concat(btnExcluir) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdServico", searchable: false, orderable: false, visible: false },
            { title: "Código", data: "CodigoServico", searchable: true, orderable: true },
            { title: "Sigla", data: "SiglaServico", searchable: true, orderable: true },
            { title: "Unidade", data: "Unidade.SiglaUnidade", searchable: true, orderable: true },
            { title: "Nome", data: "NomeServico", searchable: true, orderable: true }
        ];
        let ordem = [
            [2, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.CriarSelect2 = function () {
        CriarSelect2("selIdUnidade", "Informe sigla ou nome...", "modalCadastro", false);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdServico");

            MostrarModal("modalCadastro", "80%");
            LimparModal("modalCadastro");

            let parametros = {
                idServico: id
            };
            let sucesso = function (data) {
                if (data.Servico) {
                    $("#hidIdServico").val(data.Servico.IdServico);
                    $("#inpCodigoServico").val(data.Servico.CodigoServico);
                    $("#inpSiglaServico").val(data.Servico.SiglaServico);
                    $("#inpNomeServico").val(data.Servico.NomeServico);
                    $("#selIdUnidade").val(data.Servico.Unidade.IdUnidade).change();
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Excluir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdServico");

            let confirma = function () {
                let parametros = {
                    idServico: id
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlExcluir, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idServico: $("#hidIdServico").val(),
                codigoServico: $("#inpCodigoServico").val(),
                siglaServico: $("#inpSiglaServico").val(),
                nomeServico: $("#inpNomeServico").val(),
                idUnidade: $("#selIdUnidade").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdServico").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };
}

$(document).ready(function () {
    cadastroServicos.CriarMenu();
    cadastroServicos.CriarTabela();
    cadastroServicos.CriarSelect2();
});
