﻿"use strict";

let cadastroSituacoes = new CadastroSituacoes();

function CadastroSituacoes() {
    this.CriarMenu = function () {
        let editar = PossuiPermissao("CadastroSituacoes", "Editar");

        if (editar) {
            let botoes = [];
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroSituacoes.Editar };

            botoes = botoes.concat(btnEditar);

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdSituacao", searchable: false, orderable: false, visible: false },
            { title: "Sigla", data: "SiglaSituacao", searchable: true, orderable: true },
            { title: "Nome", data: "NomeSituacao", searchable: true, orderable: true },
            {
                title: "Tipo", data: "TipoSituacao", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return cadastroSituacoes.ObterEnum(data);
                }
            }
        ];
        let ordem = [
            [3, "asc"], [2, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdSituacao");

            MostrarModal("modalCadastro", "80%");
            $("#inpSiglaSituacao").prop("disabled", true);
            LimparModal("modalCadastro");

            let parametros = {
                idSituacao: id
            };
            let sucesso = function (data) {
                if (data.Situacao) {
                    $("#hidIdSituacao").val(data.Situacao.IdSituacao);
                    $("#inpSiglaSituacao").val(data.Situacao.SiglaSituacao);
                    $("#inpNomeSituacao").val(data.Situacao.NomeSituacao);
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idSituacao: $("#hidIdSituacao").val(),
                nomeSituacao: $("#inpNomeSituacao").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };

    this.ObterEnum = function (codigo) {
        let tipo = "";

        let parametros = {
            codigo: codigo
        };
        let sucesso = function (data) {
            if (data)
                tipo = data;
        };

        RequisicaoAjax(urlObterEnum, "json", parametros, "POST", false, false, sucesso);

        return tipo;
    };
}

$(document).ready(function () {
    cadastroSituacoes.CriarMenu();
    cadastroSituacoes.CriarTabela();
});
