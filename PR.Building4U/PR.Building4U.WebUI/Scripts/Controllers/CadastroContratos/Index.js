﻿"use strict";

let cadastroContratos = new CadastroContratos();

function CadastroContratos() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroContratos", "Incluir");
        let editar = PossuiPermissao("CadastroContratos", "Editar");
        let excluir = PossuiPermissao("CadastroContratos", "Excluir");

        if (incluir || editar || excluir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroContratos.Incluir };
            let btnAditivo = { button: "medium brown", label: "Aditivos", icon: "mdi mdi-paperclip", onClick: cadastroContratos.Aditivo };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroContratos.Editar };
            let btnExcluir = { button: "medium red", label: "Excluir", icon: "mdi mdi-delete", onClick: cadastroContratos.Excluir };

            botoes = excluir ? botoes.concat(btnExcluir) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = (incluir || editar) ? botoes.concat(btnAditivo) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdContrato", searchable: false, orderable: false, visible: false },
            { title: "Cliente", data: "Cliente.NomeCliente", searchable: true, orderable: true },
            { title: "Código", data: "CodigoContrato", searchable: true, orderable: true },
            {
                title: "Data", data: "DataContrato", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            { title: "Prazo", data: "PrazoContrato", searchable: true, orderable: true },
            { title: "Prazo (Adt)", data: "PrazoAditivo", searchable: true, orderable: true },
            {
                title: "Vencimento", data: "VencimentoContrato", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            {
                title: "Valor", data: "ValorContrato", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return FormatarDinheiro(data, true);
                }
            },
            {
                title: "Valor (Adt)", data: "ValorAditivo", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return FormatarDinheiro(data, true);
                }
            }
        ];
        let ordem = [
            [1, "asc"], [3, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.CriarSelect2 = function () {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };

        CriarSelect2Dinamico("selIdCliente", urlListarClientes, "Informe CNPJ ou nome...", "modalCadastro", 2, paramSelect, false);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        $("#selIdCliente").prop("disabled", false);
        $("#inpCodigoContrato").prop("disabled", false);
        $("#inpDataContrato").prop("disabled", false);
        $("#inpValorContrato").prop("disabled", false);
        $("#inpPrazoContrato").prop("disabled", false);
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdContrato");

            MostrarModal("modalCadastro", "80%");
            $("#selIdCliente").prop("disabled", true);
            $("#inpCodigoContrato").prop("disabled", true);
            $("#inpDataContrato").prop("disabled", true);
            $("#inpValorContrato").prop("disabled", true);
            $("#inpPrazoContrato").prop("disabled", true);
            LimparModal("modalCadastro");

            let parametros = {
                idContrato: id
            };
            let sucesso = function (data) {
                if (data.Contrato) {
                    $("#hidIdContrato").val(data.Contrato.IdContrato);
                    $("#inpCodigoContrato").val(data.Contrato.CodigoContrato);
                    $("#inpDataContrato").val(DateJsonToDateString(data.Contrato.DataContrato));
                    $("#inpValorContrato").val(data.Contrato.ValorContrato);
                    $("#inpPrazoContrato").val(data.Contrato.PrazoContrato);
                    $("#txtEscopoContrato").val(data.Contrato.EscopoContrato);
                    DefinirValorSelect2Dinamico("selIdCliente", data.Contrato.Cliente.IdCliente, data.Contrato.Cliente.NomeCliente + " (" + (data.Contrato.Cliente.CnpjCliente.length == 14 ? data.Contrato.Cliente.CnpjCliente.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5") : data.Contrato.Cliente.CnpjCliente.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "\$1.\$2.\$3\-\$4")) + ")");
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Excluir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdContrato");

            let confirma = function () {
                let parametros = {
                    idContrato: id
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlExcluir, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idContrato: $("#hidIdContrato").val(),
                idCliente: $("#selIdCliente").val(),
                codigoContrato: $("#inpCodigoContrato").val(),
                dataContrato: $("#inpDataContrato").val(),
                valorContrato: $("#inpValorContrato").val().replace(".", ""),
                prazoContrato: $("#inpPrazoContrato").val(),
                escopoContrato: $("#txtEscopoContrato").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdContrato").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };

    this.CriarTabelaAditivo = function () {
        let colunas = [
            { title: "Id", data: "IdAditivo", searchable: false, orderable: false, visible: false },
            {
                title: "<button type='button' class='btn btn-sm btn-info btn-rounded' style='white-space:nowrap;' onclick='cadastroContratos.IncluirAditivo()'><i class='fas fa-plus'></i> Incluir</button>",
                data: "IdAditivo", searchable: false, orderable: false,
                render: function (data, type, row, meta) {
                    return "<button class='btn btn-sm btn-danger btn-rounded' onclick='cadastroContratos.ExcluirAditivo(" + data + ")'><i class='fas fa-trash'></i> Excluir</button>";
                }
            },
            { title: "Código", data: "CodigoAditivo", searchable: true, orderable: true },
            {
                title: "Data", data: "DataAditivo", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            { title: "Prazo", data: "PrazoAditivo", searchable: true, orderable: true },
            {
                title: "Valor", data: "ValorAditivo", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return FormatarDinheiro(data, true);
                }
            }
        ];
        let ordem = [
            [2, "asc"]
        ];
        let parametros = function (p) {
            p.idContrato = $("#hidIdContratoAditivo").val();
            return p;
        };

        CriarDatatableServerSide("tabelaAditivo", urlListarAditivos, colunas, ordem, true, parametros);
    };

    this.Aditivo = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdContrato");
            $("#hidIdContratoAditivo").val(id);

            AtualizarDatatable("tabelaAditivo");

            MostrarModal("modalAditivo", "80%");
        }
    };

    this.IncluirAditivo = function () {
        EsconderModal("modalAditivo");
        MostrarModal("modalCadastroAditivo", "80%");
        LimparModal("modalCadastroAditivo");
    };

    this.ExcluirAditivo = function (id) {
        let confirma = function () {
            let parametros = {
                idAditivo: id
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    AtualizarDatatable("tabelaCadastro");
                    AtualizarDatatable("tabelaAditivo");
                }
            };

            RequisicaoAjax(urlExcluirAditivo, "json", parametros, "POST", true, true, sucesso);
        };

        AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
    };

    this.SalvarAditivo = function () {
        if (ValidarCamposObrigatorios("modalCadastroAditivo")) {
            let parametros = {
                idContrato: $("#hidIdContratoAditivo").val(),
                codigoAditivo: $("#inpCodigoAditivo").val(),
                dataAditivo: $("#inpDataAditivo").val(),
                valorAditivo: $("#inpValorAditivo").val().replace(".", ""),
                prazoAditivo: $("#inpPrazoAditivo").val(),
                escopoAditivo: $("#txtEscopoAditivo").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastroAditivo");
                    AtualizarDatatable("tabelaCadastro");
                    AtualizarDatatable("tabelaAditivo");
                    MostrarModal("modalAditivo", "80%");
                }
            };

            RequisicaoAjax(urlIncluirAditivo, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.CancelarAditivo = function () {
        LimparModal("modalCadastroAditivo");
        EsconderModal("modalCadastroAditivo");
        MostrarModal("modalAditivo", "80%");
    };
}

$(document).ready(function () {
    cadastroContratos.CriarMenu();
    cadastroContratos.CriarTabela();
    cadastroContratos.CriarSelect2();
    cadastroContratos.CriarTabelaAditivo();
});
