﻿"use strict";

let alocacaoEquipamento = new AlocacaoEquipamento();

function AlocacaoEquipamento() {
    this.CriarMenu = function () {
        let transferir = PossuiPermissao("AlocacaoEquipamento", "Transferir");
        let desfazer = PossuiPermissao("AlocacaoEquipamento", "Desfazer");
        let cancelar = PossuiPermissao("AlocacaoEquipamento", "Cancelar");
        let arrumar = PossuiPermissao("AlocacaoEquipamento", "Arrumar");

        if (transferir || desfazer || cancelar || arrumar) {
            let botoes = [];
            let btnTransferir = { button: "medium blue", label: "Transferir", icon: "mdi mdi-transit-transfer", onClick: alocacaoEquipamento.Transferir };
            let btnDesfazer = { button: "medium red", label: "Desfazer Transferência", icon: "mdi mdi-backup-restore", onClick: alocacaoEquipamento.Desfazer };
            let btnCancelar = { button: "medium purple", label: "Cancelar Destino", icon: "mdi mdi-close", onClick: alocacaoEquipamento.Cancelar };
            let btnArrumar = { button: "medium green", label: "Manutenção", icon: "mdi mdi-car-wash", onClick: alocacaoEquipamento.Arrumar };

            botoes = arrumar ? botoes.concat(btnArrumar) : botoes;
            botoes = cancelar ? botoes.concat(btnCancelar) : botoes;
            botoes = desfazer ? botoes.concat(btnDesfazer) : botoes;
            botoes = transferir ? botoes.concat(btnTransferir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdAluguel", searchable: false, orderable: false, visible: false },
            { title: "Sequência", data: "SequenciaAluguel", searchable: false, orderable: false, visible: false },
            { title: "Equipamento", data: "IdEquipamento", searchable: false, orderable: false, visible: false },
            { title: "Patrimônio", data: "PatrimonioEquipamento", searchable: true, orderable: true },
            { title: "Equipamento", data: "TipoEquipamento", searchable: true, orderable: true },
            { title: "Marca", data: "MarcaEquipamento", searchable: true, orderable: true },
            { title: "Modelo", data: "ModeloEquipamento", searchable: true, orderable: true },
            { title: "Custo", data: "NomeCusto", searchable: true, orderable: true },
            { title: "Obra", data: "NomeObra", searchable: true, orderable: true },
            {
                title: "Mobilização", data: "InicioAluguel", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            {
                title: "Disponib.", data: "InicioReservaAluguel", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            { title: "Obra Origem", data: "ObraOrigem", searchable: true, orderable: true },
            { title: "Obra Destino", data: "ObraDestino", searchable: true, orderable: true },
            {
                title: "Situação", data: "SituacaoEquipamento", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    if (row["SiglaSituacaoEquipamento"] == "MN")
                        return data + " (até " + DateJsonToDateString(row["TerminoManutencao"]) + ")";
                    else
                        return data;
                }
            },
            { title: "Sigla Situação", data: "SiglaSituacaoEquipamento", searchable: true, orderable: true, visible: false }
        ];
        let ordem = [
            [4, "asc"]
        ];
        let rowCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData["SituacaoObra"] == 'TM') {
                $("td", nRow).css("color", "darkred");
            }
        };
        let parametros = function (p) {
            p.obras = $("#selFiltroObra").val();
            p.tiposEquipamento = $("#selFiltroTipoEquipamento").val();
            return p;
        };

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true, parametros, rowCallback);
    };

    this.CriarSelect2 = function () {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };

        CriarSelect2Dinamico("selIdObra", urlListarObras, "Informe código ou nome...", "modalTransferencia", 2, paramSelect, false);
        CriarSelect2Dinamico("selFiltroObra", urlListarObras, "Informe código ou nome...", null, 2, paramSelect, true);
        CriarSelect2Dinamico("selFiltroTipoEquipamento", urlListarTiposEquipamento, "Informe nome...", null, 2, paramSelect, true);
    };

    this.Transferir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let sequenciaAluguel = ObterCampoTabela("tabelaCadastro", "SequenciaAluguel");
            let idEquipamento = ObterCampoTabela("tabelaCadastro", "IdEquipamento");
            let tipoEquipamento = ObterCampoTabela("tabelaCadastro", "TipoEquipamento");
            let marcaEquipamento = ObterCampoTabela("tabelaCadastro", "MarcaEquipamento");
            let modeloEquipamento = ObterCampoTabela("tabelaCadastro", "ModeloEquipamento");

            MostrarModal("modalTransferencia", "80%");
            LimparModal("modalTransferencia");

            $("#hidSequenciaAluguel").val(sequenciaAluguel);
            $("#hidIdEquipamento").val(idEquipamento);
            $("#inpTipoEquipamento").val(tipoEquipamento);
            $("#inpMarcaEquipamento").val(marcaEquipamento);
            $("#inpModeloEquipamento").val(modeloEquipamento);
            $("#inpDataTransferencia").val(DateToString(new Date()));
        }
    };

    this.Desfazer = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let idEquipamento = ObterCampoTabela("tabelaCadastro", "IdEquipamento");
            let sequenciaAluguel = ObterCampoTabela("tabelaCadastro", "SequenciaAluguel");

            let confirma = function () {
                let parametros = {
                    idEquipamento: idEquipamento,
                    sequenciaAluguel: sequenciaAluguel
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlDesfazer, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar a transferência.", confirma);
        }
    };

    this.Cancelar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let idEquipamento = ObterCampoTabela("tabelaCadastro", "IdEquipamento");

            let confirma = function () {
                let parametros = {
                    idEquipamento: idEquipamento
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlCancelar, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar a reserva.", confirma);
        }
    };

    this.Arrumar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let idEquipamento = ObterCampoTabela("tabelaCadastro", "IdEquipamento");
            let patrimonioEquipamento = ObterCampoTabela("tabelaCadastro", "PatrimonioEquipamento");
            let situacaoEquipamento = ObterCampoTabela("tabelaCadastro", "SiglaSituacaoEquipamento");

            MostrarModal("modalManutencao");
            LimparModal("modalManutencao");
            $("#hidIdEquipamentoManutencao").val(idEquipamento);
            $("#hidSituacaoEquipamento").val(situacaoEquipamento);
            $("#spnPatrimonioEquipamento").html(patrimonioEquipamento);

            if (situacaoEquipamento == "MN") {
                $("#divInicioManutencao").hide();
                $("#inpInicioManutencao").removeAttr("required");
                $("#spnTipoManutencao").html("término");

                $("#inpTerminoManutencao").val(DateToString(new Date()));
            }
            else {
                $("#divInicioManutencao").show();
                $("#inpInicioManutencao").attr("required", "required");
                $("#spnTipoManutencao").html("início");

                $("#inpInicioManutencao").val(DateToString(new Date()));
                $("#inpTerminoManutencao").val(DateToString(new Date().addDays(7)));
            }
        }
    };

    this.SalvarTransferencia = function () {
        if (ValidarCamposObrigatorios("modalTransferencia")) {
            let parametros = {
                sequenciaAluguel: $("#hidSequenciaAluguel").val(),
                idEquipamento: $("#hidIdEquipamento").val(),
                idObra: $("#selIdObra").val(),
                dataTransferencia: $("#inpDataTransferencia").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalTransferencia");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            RequisicaoAjax(urlTransferir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.CancelarTransferencia = function () {
        LimparModal("modalTransferencia");
        EsconderModal("modalTransferencia");
    };

    this.SalvarManutencao = function () {
        if (ValidarCamposObrigatorios("modalManutencao")) {
            let parametros = {
                idEquipamento: $("#hidIdEquipamentoManutencao").val(),
                inicioManutencao: $("#inpInicioManutencao").val(),
                terminoManutencao: $("#inpTerminoManutencao").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalManutencao");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidSituacaoEquipamento").val() == "MN")
                RequisicaoAjax(urlArrumar, "json", parametros, "POST", true, true, sucesso);
            else if (ValidarDataMaior("inpInicioManutencao", "inpTerminoManutencao"))
                RequisicaoAjax(urlArrumar, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.CancelarManutencao = function () {
        LimparModal("modalManutencao");
        EsconderModal("modalManutencao");
    };
}

$(document).ready(function () {
    alocacaoEquipamento.CriarMenu();
    alocacaoEquipamento.CriarTabela();
    alocacaoEquipamento.CriarSelect2();

    $("#selFiltroObra, #selFiltroTipoEquipamento").change(function () {
        AtualizarDatatable("tabelaCadastro");
    });
});
