﻿"use strict";

let cadastroPlanilhas = new CadastroPlanilhas();

function CadastroPlanilhas() {
    this.Arquivos = null;
    this.Planilha = null;
    this.PeriodoMedicao = null;

    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroPlanilhas", "Incluir");
        let excluir = PossuiPermissao("CadastroPlanilhas", "Excluir");

        if (incluir || excluir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroPlanilhas.Incluir };
            let btnMedicao = { button: "medium brown", label: "Medições", icon: "mdi mdi-paperclip", onClick: cadastroPlanilhas.Medicao };
            let btnExcluir = { button: "medium red", label: "Excluir", icon: "mdi mdi-delete", onClick: cadastroPlanilhas.Excluir };

            botoes = excluir ? botoes.concat(btnExcluir) : botoes;
            botoes = incluir ? botoes.concat(btnMedicao) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdPlanilha", searchable: false, orderable: false, visible: false },
            { title: "CP", data: "CpPlanilha", searchable: true, orderable: true },
            { title: "Rev", data: "RevPlanilha", searchable: true, orderable: true }
        ];
        let ordem = [
            [1, "asc"]
        ];
        let parametros = function (p) {
            p.idCronograma = $("#selIdCronograma").val();
            return p;
        };

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true, parametros);
    };

    this.CriarSelect2 = function () {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };

        let paramSelectCron = function (term) {
            return {
                codigoOuNome: term.term,
                idObra: $("#selIdObra").val()
            };
        };

        CriarSelect2Dinamico("selIdObra", urlListarObras, "Informe código ou nome...", null, 2, paramSelect, false);
        CriarSelect2Dinamico("selIdCronograma", urlListarCronogramas, "Informe nome...", null, 0, paramSelectCron, false);
    };

    this.CriarDropify = function () {
        $("#inpArquivoPlanilha").dropify();
        $("#inpArquivoMedicao").dropify();
    };

    this.PrepararUpload = function (e) {
        cadastroPlanilhas.Arquivos = e.target.files;
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "40%");
        LimparModal("modalCadastro");

        let drop = $("#inpArquivoPlanilha").dropify();
        let dropEvent = drop.data('dropify');
        dropEvent.resetPreview();
        dropEvent.clearElement();
    };

    this.Excluir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdPlanilha");

            let confirma = function () {
                let parametros = {
                    idPlanilha: id
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlExcluir, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
        }
    };

    this.Enviar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = new FormData();
            $.each(cadastroPlanilhas.Arquivos, function (key, value) {
                parametros.append("arquivoPlanilha", value);
            });
            parametros.append("idCronograma", $("#selIdCronograma").val());
            parametros.append("cpPlanilha", $("#inpCpPlanilha").val());
            parametros.append("revPlanilha", $("#inpRevPlanilha").val());

            let sucesso = function (data) {
                if (!data.sucesso)
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));
                else {
                    $("#modalVisaoPlanilha .modal-body").html(data.html);
                    cadastroPlanilhas.Planilha = data.planilha;

                    EsconderModal("modalCadastro");
                    MostrarModal("modalVisaoPlanilha", "95%");

                    if (data.temInconsistencia) {
                        $("#btnImportar").hide();
                        NotificacaoAviso("Existem inconsistências. Verifique e envie novamente!");
                    }
                    else
                        $("#btnImportar").show();
                }
            };

            RequisicaoAjaxArquivo(urlEnviar, "json", parametros, "POST", true, true, sucesso, null);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");

        let drop = $("#inpArquivoPlanilha").dropify();
        let dropEvent = drop.data('dropify');
        dropEvent.resetPreview();
        dropEvent.clearElement();
    };

    this.CancelarImportar = function () {
        EsconderModal("modalVisaoPlanilha");
        MostrarModal("modalCadastro", "40%");
    };

    this.Importar = function () {
        let parametros = {
            planilha: cadastroPlanilhas.Planilha
        };
        let sucesso = function (data) {
            Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

            if (data.sucesso) {
                EsconderModal("modalVisaoPlanilha");
                AtualizarDatatable("tabelaCadastro");
            }
        };

        RequisicaoAjax(urlImportar, "json", parametros, "POST", true, true, sucesso);
    };

    this.CriarTabelaMedicao = function () {
        let colunas = [
            {
                title: "<button type='button' class='btn btn-sm btn-info btn-rounded' style='white-space:nowrap;' onclick='cadastroPlanilhas.IncluirMedicao()'><i class='fas fa-plus'></i> Incluir</button>",
                data: "PeriodoMedicao", searchable: false, orderable: false,
                render: function (data, type, row, meta) {
                    return "<button class='btn btn-sm btn-danger btn-rounded' onclick='cadastroPlanilhas.ExcluirMedicao(" + data + ")'><i class='fas fa-trash'></i> Excluir</button>";
                }
            },
            {
                title: "Período", data: "PeriodoMedicao", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            {
                title: "Valor", data: "ValorMedicao", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return FormatarDinheiro(data, true);
                }
            }
        ];
        let ordem = [
            [1, "asc"]
        ];
        let parametros = function (p) {
            p.idPlanilha = $("#hidIdPlanilha").val();
            return p;
        };

        CriarDatatableServerSide("tabelaMedicao", urlListarMedicoes, colunas, ordem, true, parametros);
    };

    this.Medicao = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdPlanilha");
            $("#hidIdPlanilha").val(id);

            AtualizarDatatable("tabelaMedicao");

            MostrarModal("modalMedicao", "80%");
        }
    };

    this.IncluirMedicao = function () {
        EsconderModal("modalMedicao");
        MostrarModal("modalCadastroMedicao", "40%");
        LimparModal("modalCadastroMedicao");

        let drop = $("#inpArquivoMedicao").dropify();
        let dropEvent = drop.data('dropify');
        dropEvent.resetPreview();
        dropEvent.clearElement();
    };

    this.ExcluirMedicao = function (periodo) {
        let confirma = function () {
            let parametros = {
                idPlanilha: $("#hidIdPlanilha").val(),
                periodoMedicao: DateJsonToDateString(periodo.toString())
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    AtualizarDatatable("tabelaCadastro");
                    AtualizarDatatable("tabelaMedicao");
                }
            };

            RequisicaoAjax(urlExcluirMedicao, "json", parametros, "POST", true, true, sucesso);
        };

        AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
    };

    this.EnviarMedicao = function () {
        if (ValidarCamposObrigatorios("modalCadastroMedicao")) {
            let parametros = new FormData();
            $.each(cadastroPlanilhas.Arquivos, function (key, value) {
                parametros.append("arquivoMedicao", value);
            });
            parametros.append("idPlanilha", $("#hidIdPlanilha").val());
            parametros.append("periodoMedicao", $("#inpPeriodoMedicao").val());
            let sucesso = function (data) {
                if (!data.sucesso)
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));
                else {
                    $("#modalVisaoMedicao .modal-body").html(data.html);
                    cadastroPlanilhas.Planilha = data.planilha;
                    cadastroPlanilhas.PeriodoMedicao = data.periodo;

                    EsconderModal("modalCadastroMedicao");
                    MostrarModal("modalVisaoMedicao", "95%");

                    if (data.temInconsistencia) {
                        $("#btnImportarMedicao").hide();
                        NotificacaoAviso("Existem inconsistências. Verifique e envie novamente!");
                    }
                    else
                        $("#btnImportarMedicao").show();
                }
            };

            RequisicaoAjaxArquivo(urlEnviarMedicao, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.CancelarMedicao = function () {
        LimparModal("modalCadastroMedicao");
        EsconderModal("modalCadastroMedicao");
        MostrarModal("modalMedicao", "80%");

        let drop = $("#inpArquivoMedicao").dropify();
        let dropEvent = drop.data('dropify');
        dropEvent.resetPreview();
        dropEvent.clearElement();
    };

    this.CancelarImportarMedicao = function () {
        EsconderModal("modalVisaoMedicao");
        MostrarModal("modalCadastroMedicao", "40%");
    };

    this.ImportarMedicao = function () {
        let parametros = {
            planilha: cadastroPlanilhas.Planilha,
            periodoMedicao: DateJsonToDateString(cadastroPlanilhas.PeriodoMedicao)
        };
        let sucesso = function (data) {
            Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

            if (data.sucesso) {
                EsconderModal("modalVisaoMedicao");
                AtualizarDatatable("tabelaMedicao");
                MostrarModal("modalMedicao", "80%");
            }
        };

        RequisicaoAjax(urlImportarMedicao, "json", parametros, "POST", true, true, sucesso);
    };
}

$(document).ready(function () {
    cadastroPlanilhas.CriarTabela();
    cadastroPlanilhas.CriarSelect2();
    cadastroPlanilhas.CriarDropify();
    cadastroPlanilhas.CriarTabelaMedicao();

    $("input[type=file]").change(cadastroPlanilhas.PrepararUpload);

    $("#selIdObra").change(function (e) {
        if (e.currentTarget.value) {
            $("#selIdObra").prop("disabled", true);
            $("#selIdCronograma").prop("disabled", false);
            $("#selIdCronograma").val("").change();
        }
        else {
            $("#selIdObra").prop("disabled", false);
            $("#selIdCronograma").prop("disabled", true);
            $("#selIdCronograma").val("").change();
        }
    });

    $("#selIdCronograma").change(function (e) {
        if (e.currentTarget.value) {
            $("#rowTabela").css("display", "block");
            $("#menu-action").html("");
            cadastroPlanilhas.CriarMenu();
            AtualizarDatatable("tabelaCadastro");
        }
        else {
            $("#rowTabela").css("display", "none");
            $("#menu-action").html("");
        }
    });
});
