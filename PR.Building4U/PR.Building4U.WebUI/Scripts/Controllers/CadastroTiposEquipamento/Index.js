﻿"use strict";

let cadastroTiposEquipamento = new CadastroTiposEquipamento();

function CadastroTiposEquipamento() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroTiposEquipamento", "Incluir");
        let editar = PossuiPermissao("CadastroTiposEquipamento", "Editar");
        let excluir = PossuiPermissao("CadastroTiposEquipamento", "Excluir");

        if (incluir || editar || excluir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroTiposEquipamento.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroTiposEquipamento.Editar };
            let btnExcluir = { button: "medium red", label: "Excluir", icon: "mdi mdi-delete", onClick: cadastroTiposEquipamento.Excluir };

            botoes = excluir ? botoes.concat(btnExcluir) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdTipoEquipamento", searchable: false, orderable: false, visible: false },
            { title: "Nome", data: "NomeTipoEquipamento", searchable: true, orderable: true },
            { title: "Custo", data: "Custo.NomeCusto", searchable: true, orderable: true }
        ];
        let ordem = [
            [2, "asc"], [1, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.CriarSelect2 = function () {
        CriarSelect2("selIdCusto", "Informe nome...", "modalCadastro", false);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdTipoEquipamento");

            MostrarModal("modalCadastro", "80%");
            LimparModal("modalCadastro");

            let parametros = {
                idTipoEquipamento: id
            };
            let sucesso = function (data) {
                if (data.TipoEquipamento) {
                    $("#hidIdTipoEquipamento").val(data.TipoEquipamento.IdTipoEquipamento);
                    $("#inpNomeTipoEquipamento").val(data.TipoEquipamento.NomeTipoEquipamento);
                    $("#selIdCusto").val(data.TipoEquipamento.Custo.IdCusto).change();
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Excluir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdTipoEquipamento");

            let confirma = function () {
                let parametros = {
                    idTipoEquipamento: id
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlExcluir, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idTipoEquipamento: $("#hidIdTipoEquipamento").val(),
                nomeTipoEquipamento: $("#inpNomeTipoEquipamento").val(),
                idCusto: $("#selIdCusto").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdTipoEquipamento").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };
}

$(document).ready(function () {
    cadastroTiposEquipamento.CriarMenu();
    cadastroTiposEquipamento.CriarTabela();
    cadastroTiposEquipamento.CriarSelect2();
});
