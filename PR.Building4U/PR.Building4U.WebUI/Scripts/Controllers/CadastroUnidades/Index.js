﻿"use strict";

let cadastroUnidades = new CadastroUnidades();

function CadastroUnidades() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroUnidades", "Incluir");
        let editar = PossuiPermissao("CadastroUnidades", "Editar");
        let excluir = PossuiPermissao("CadastroUnidades", "Excluir");

        if (incluir || editar || excluir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroUnidades.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroUnidades.Editar };
            let btnExcluir = { button: "medium red", label: "Excluir", icon: "mdi mdi-delete", onClick: cadastroUnidades.Excluir };

            botoes = excluir ? botoes.concat(btnExcluir) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdUnidade", searchable: false, orderable: false, visible: false },
            { title: "Sigla", data: "SiglaUnidade", searchable: true, orderable: true },
            { title: "Nome", data: "NomeUnidade", searchable: true, orderable: true }
        ];
        let ordem = [
            [2, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdUnidade");

            MostrarModal("modalCadastro", "80%");
            LimparModal("modalCadastro");

            let parametros = {
                idUnidade: id
            };
            let sucesso = function (data) {
                if (data.Unidade) {
                    $("#hidIdUnidade").val(data.Unidade.IdUnidade);
                    $("#inpSiglaUnidade").val(data.Unidade.SiglaUnidade);
                    $("#inpNomeUnidade").val(data.Unidade.NomeUnidade);
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Excluir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdUnidade");

            let confirma = function () {
                let parametros = {
                    idUnidade: id
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlExcluir, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idUnidade: $("#hidIdUnidade").val(),
                siglaUnidade: $("#inpSiglaUnidade").val(),
                nomeUnidade: $("#inpNomeUnidade").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdUnidade").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };
}

$(document).ready(function () {
    cadastroUnidades.CriarMenu();
    cadastroUnidades.CriarTabela();
});
