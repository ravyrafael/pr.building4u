﻿"use strict";

let necessAprovEquipamento = new NecessAprovEquipamento();

function NecessAprovEquipamento() {
    this.CriarMenu = function () {
        let aprovar = PossuiPermissao("NecessAprovEquipamento", "Aprovar");

        if (aprovar) {
            let botoes = [];
            let btnAprovar = { button: "medium blue", label: "Aprovar", icon: "mdi mdi-check", onClick: necessAprovEquipamento.Aprovar };
            let btnReprovar = { button: "medium red", label: "Reprovar", icon: "mdi mdi-close", onClick: necessAprovEquipamento.Reprovar };

            botoes = botoes.concat(btnReprovar);
            botoes = botoes.concat(btnAprovar);

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Custo", data: "TipoEquipamento.Custo.NomeCusto", searchable: true, orderable: true },
            { title: "Equipamento", data: "TipoEquipamento.NomeTipoEquipamento", searchable: true, orderable: true },
            { title: "Atual", data: "QuantidadeNecessario", searchable: true, orderable: true, width: "10%" },
            { title: "Aprovar", data: "QuantidadeAprovacao", searchable: true, orderable: true, width: "10%" }
        ];
        let ordem = [
            [0, "asc"], [1, "asc"]
        ];
        let parametros = function (p) {
            p.idObra = $("#selIdObra").val();
            return p;
        };

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, false, parametros);
    };

    this.CriarSelect2 = function () {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };

        CriarSelect2Dinamico("selIdObra", urlListarObras, "Informe código ou nome...", null, 2, paramSelect, false);
    };

    this.Aprovar = function () {
        let idObra = $("#selIdObra").val();

        let confirma = function () {
            let parametros = {
                idObra: idObra
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso)
                    $("#selIdObra").val("").change();
            };

            RequisicaoAjax(urlAprovar, "json", parametros, "POST", true, true, sucesso);
        };

        AlertaConfirmacao("Tem certeza?", "A solicitação será aprovada e consistida na base de dados.", confirma);
    };

    this.Reprovar = function () {
        let idObra = $("#selIdObra").val();

        let confirma = function () {
            let parametros = {
                idObra: idObra
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso)
                    $("#selIdObra").val("").change();
            };

            RequisicaoAjax(urlReprovar, "json", parametros, "POST", true, true, sucesso);
        };

        AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
    };
}

$(document).ready(function () {
    necessAprovEquipamento.CriarTabela();
    necessAprovEquipamento.CriarSelect2();

    $("#selIdObra").change(function (e) {
        if (e.currentTarget.value) {
            $("#rowTabela").css("display", "block");
            $("#menu-action").html("");
            necessAprovEquipamento.CriarMenu();
            AtualizarDatatable("tabelaCadastro");
        }
        else {
            $("#rowTabela").css("display", "none");
            $("#menu-action").html("");
        }
    });
});
