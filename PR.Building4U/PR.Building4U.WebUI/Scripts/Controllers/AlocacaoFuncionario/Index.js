﻿"use strict";

let alocacaoFuncionario = new AlocacaoFuncionario();

function AlocacaoFuncionario() {
    this.CriarMenu = function () {
        let transferir = PossuiPermissao("AlocacaoFuncionario", "Transferir");
        let desfazer = PossuiPermissao("AlocacaoFuncionario", "Desfazer");
        let cancelar = PossuiPermissao("AlocacaoFuncionario", "Cancelar");
        let folgar = PossuiPermissao("AlocacaoFuncionario", "Folgar");

        if (transferir || desfazer || cancelar || folgar) {
            let botoes = [];
            let btnTransferir = { button: "medium blue", label: "Transferir", icon: "mdi mdi-transit-transfer", onClick: alocacaoFuncionario.Transferir };
            let btnDesfazer = { button: "medium red", label: "Desfazer Transferência", icon: "mdi mdi-backup-restore", onClick: alocacaoFuncionario.Desfazer };
            let btnCancelar = { button: "medium purple", label: "Cancelar Destino", icon: "mdi mdi-close", onClick: alocacaoFuncionario.Cancelar };
            let btnFolgar = { button: "medium green", label: "Férias", icon: "mdi mdi-airplane", onClick: alocacaoFuncionario.Folgar };

            botoes = folgar ? botoes.concat(btnFolgar) : botoes;
            botoes = cancelar ? botoes.concat(btnCancelar) : botoes;
            botoes = desfazer ? botoes.concat(btnDesfazer) : botoes;
            botoes = transferir ? botoes.concat(btnTransferir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdAlocacao", searchable: false, orderable: false, visible: false },
            { title: "Sequência", data: "SequenciaAlocacao", searchable: false, orderable: false, visible: false },
            { title: "Funcionário", data: "IdFuncionario", searchable: false, orderable: false, visible: false },
            { title: "Chapa", data: "ChapaFuncionario", searchable: true, orderable: true },
            { title: "Nome", data: "NomeFuncionario", searchable: true, orderable: true },
            { title: "Custo", data: "NomeCusto", searchable: true, orderable: true },
            { title: "Cargo", data: "NomeCargo", searchable: true, orderable: true },
            { title: "Obra", data: "NomeObra", searchable: true, orderable: true },
            {
                title: "Mobilização", data: "InicioAlocacao", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            {
                title: "Disponib.", data: "InicioReserva", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            { title: "Obra Origem", data: "ObraOrigem", searchable: true, orderable: true },
            { title: "Obra Destino", data: "ObraDestino", searchable: true, orderable: true },
            {
                title: "Situação", data: "SituacaoFuncionario", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    if (row["SiglaSituacaoFuncionario"] == "FR")
                        return data + " (até " + DateJsonToDateString(row["TerminoFerias"]) + ")";
                    else
                        return data;
                }
            },
            { title: "Sigla Situação", data: "SiglaSituacaoFuncionario", searchable: true, orderable: true, visible: false }
        ];
        let ordem = [
            [4, "asc"]
        ];
        let rowCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData["SituacaoObra"] == 'TM') {
                $("td", nRow).css("color", "darkred");
            }
        };
        let parametros = function (p) {
            p.obras = $("#selFiltroObra").val();
            p.cargos = $("#selFiltroCargo").val();
            return p;
        };

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true, parametros, rowCallback);
    };

    this.CriarSelect2 = function () {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };

        CriarSelect2Dinamico("selIdObra", urlListarObras, "Informe código ou nome...", "modalTransferencia", 2, paramSelect, false);
        CriarSelect2Dinamico("selFiltroObra", urlListarObras, "Informe código ou nome...", null, 2, paramSelect, true);
        CriarSelect2Dinamico("selFiltroCargo", urlListarCargos, "Informe nome...", null, 2, paramSelect, true);
    };

    this.Transferir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let sequenciaAlocacao = ObterCampoTabela("tabelaCadastro", "SequenciaAlocacao");
            let idFuncionario = ObterCampoTabela("tabelaCadastro", "IdFuncionario");
            let nomeFuncionario = ObterCampoTabela("tabelaCadastro", "NomeFuncionario");

            MostrarModal("modalTransferencia", "80%");
            LimparModal("modalTransferencia");

            $("#hidSequenciaAlocacao").val(sequenciaAlocacao);
            $("#hidIdFuncionario").val(idFuncionario);
            $("#inpNomeFuncionario").val(nomeFuncionario);
            $("#inpDataTransferencia").val(DateToString(new Date()));
        }
    };

    this.Desfazer = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let idFuncionario = ObterCampoTabela("tabelaCadastro", "IdFuncionario");
            let sequenciaAlocacao = ObterCampoTabela("tabelaCadastro", "SequenciaAlocacao");

            let confirma = function () {
                let parametros = {
                    idFuncionario: idFuncionario,
                    sequenciaAlocacao: sequenciaAlocacao
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlDesfazer, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar a transferência.", confirma);
        }
    };

    this.Cancelar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let idFuncionario = ObterCampoTabela("tabelaCadastro", "IdFuncionario");

            let confirma = function () {
                let parametros = {
                    idFuncionario: idFuncionario
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlCancelar, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar a reserva.", confirma);
        }
    };

    this.Folgar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let idFuncionario = ObterCampoTabela("tabelaCadastro", "IdFuncionario");
            let nomeFuncionario = ObterCampoTabela("tabelaCadastro", "NomeFuncionario");
            let situacaoFuncionario = ObterCampoTabela("tabelaCadastro", "SiglaSituacaoFuncionario");

            MostrarModal("modalFerias");
            LimparModal("modalFerias");
            $("#hidIdFuncionarioFerias").val(idFuncionario);
            $("#hidSituacaoFuncionario").val(situacaoFuncionario);
            $("#spnNomeFuncionario").html(nomeFuncionario);

            if (situacaoFuncionario == "FR") {
                $("#divInicioFerias").hide();
                $("#inpInicioFerias").removeAttr("required");
                $("#spnTipoFerias").html("término");

                $("#inpTerminoFerias").val(DateToString(new Date()));
            }
            else {
                $("#divInicioFerias").show();
                $("#inpInicioFerias").attr("required", "required");
                $("#spnTipoFerias").html("início");

                $("#inpInicioFerias").val(DateToString(new Date()));
                $("#inpTerminoFerias").val(DateToString(new Date().addDays(7)));
            }
        }
    };

    this.SalvarTransferencia = function () {
        if (ValidarCamposObrigatorios("modalTransferencia")) {
            let parametros = {
                sequenciaAlocacao: $("#hidSequenciaAlocacao").val(),
                idFuncionario: $("#hidIdFuncionario").val(),
                idObra: $("#selIdObra").val(),
                dataTransferencia: $("#inpDataTransferencia").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalTransferencia");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            RequisicaoAjax(urlTransferir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.CancelarTransferencia = function () {
        LimparModal("modalTransferencia");
        EsconderModal("modalTransferencia");
    };

    this.SalvarFerias = function () {
        if (ValidarCamposObrigatorios("modalFerias")) {
            let parametros = {
                idFuncionario: $("#hidIdFuncionarioFerias").val(),
                inicioFerias: $("#inpInicioFerias").val(),
                terminoFerias: $("#inpTerminoFerias").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalFerias");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidSituacaoFuncionario").val() == "FR")
                RequisicaoAjax(urlFolgar, "json", parametros, "POST", true, true, sucesso);
            else if (ValidarDataMaior("inpInicioFerias", "inpTerminoFerias"))
                RequisicaoAjax(urlFolgar, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.CancelarFerias = function () {
        LimparModal("modalFerias");
        EsconderModal("modalFerias");
    };
}

$(document).ready(function () {
    alocacaoFuncionario.CriarMenu();
    alocacaoFuncionario.CriarTabela();
    alocacaoFuncionario.CriarSelect2();

    $("#selFiltroObra, #selFiltroCargo").change(function () {
        AtualizarDatatable("tabelaCadastro");
    });
});
