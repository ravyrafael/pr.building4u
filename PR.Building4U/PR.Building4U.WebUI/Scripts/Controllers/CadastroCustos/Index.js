﻿"use strict";

let cadastroCustos = new CadastroCustos();

function CadastroCustos() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroCustos", "Incluir");
        let editar = PossuiPermissao("CadastroCustos", "Editar");
        let excluir = PossuiPermissao("CadastroCustos", "Excluir");

        if (incluir || editar || excluir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroCustos.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroCustos.Editar };
            let btnExcluir = { button: "medium red", label: "Excluir", icon: "mdi mdi-delete", onClick: cadastroCustos.Excluir };

            botoes = excluir ? botoes.concat(btnExcluir) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdCusto", searchable: false, orderable: false, visible: false },
            { title: "Sigla", data: "SiglaCusto", searchable: true, orderable: true },
            { title: "Nome", data: "NomeCusto", searchable: true, orderable: true }
        ];
        let ordem = [
            [2, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        $("#inpSiglaCusto").prop("disabled", false);
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdCusto");

            MostrarModal("modalCadastro", "80%");
            $("#inpSiglaCusto").prop("disabled", true);
            LimparModal("modalCadastro");

            let parametros = {
                idCusto: id
            };
            let sucesso = function (data) {
                if (data.Custo) {
                    $("#hidIdCusto").val(data.Custo.IdCusto);
                    $("#inpSiglaCusto").val(data.Custo.SiglaCusto);
                    $("#inpNomeCusto").val(data.Custo.NomeCusto);
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Excluir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdCusto");

            let confirma = function () {
                let parametros = {
                    idCusto: id
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlExcluir, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idCusto: $("#hidIdCusto").val(),
                siglaCusto: $("#inpSiglaCusto").val(),
                nomeCusto: $("#inpNomeCusto").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdCusto").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };
}

$(document).ready(function () {
    cadastroCustos.CriarMenu();
    cadastroCustos.CriarTabela();
});
