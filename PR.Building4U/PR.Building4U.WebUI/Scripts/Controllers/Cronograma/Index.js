﻿"use strict";

let cronograma = new Cronograma();

function Cronograma() {
    this.cronogramas = null;

    this.idObra = null;
    this.idCronograma = null;

    this.campoAlterado = null;
    this.valorAlterado = null;
    this.campoFocus = null;
    this.valorFocus = null;

    this.CriarMenu = function () {
        let botoes = [];
        let btnCancelar = { button: "medium red", label: "Cancelar", icon: "mdi mdi-close", onClick: cronograma.Cancelar };
        let btnSalvar = { button: "medium purple", label: "Salvar", icon: "mdi mdi-content-save", onClick: cronograma.Salvar };

        botoes = botoes.concat(btnCancelar);
        botoes = botoes.concat(btnSalvar);

        CriarMenuFlutuante(botoes);
    };

    this.CriarSelect2 = function (idCronograma) {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };

        CriarSelect2Dinamico("selIdServico", urlListarServicos, "Informe sigla ou nome...", "modalServico", 2, paramSelect, false);

        $("#selIdCronogramaServico").html("");
        $("#selIdCronogramaServico").append($("<option>", {
            value: null,
            text: null
        }));
        let cronogramas = JSON.parse(cronograma.cronogramas);
        for (var i = 0; i < cronogramas.length; i++) {
            if (cronogramas[i].IdCronograma == idCronograma) {
                cronogramas[i].Servicos.forEach(function (item, i) {
                    $("#selIdCronogramaServico").append($("<option>", {
                        value: item.IdCronogramaServico,
                        text: item.OrdemServico + " - " + item.Servico.SiglaServico + " " + (item.NomeServico ? item.NomeServico : "")
                    }));
                });
            }
        }

        CriarSelect2("selIdCronogramaServico", "Informe sigla ou nome...", "modalServico", false);
    };

    this.Carregar = function () {
        let parametros = {
            idObra: cronograma.idObra,
            idCronograma: cronograma.idCronograma
        };
        let sucesso = function (data) {
            if (data.Cronogramas)
                cronograma.cronogramas = data.Cronogramas;
            else
                cronograma.cronogramas = null;

            if (data.HtmlCronograma) {
                $('#tableCronograma').floatThead('destroy');
                $("#divCronograma").html(data.HtmlCronograma);
                $('#tableCronograma').floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('#divCronograma');
                    }
                });
            }
            else {
                $('#tableCronograma').floatThead('destroy');
                $("#divCronograma").html("");
            }

            if (data.HtmlPlanilha) {
                $('#tablePlanilha').floatThead('destroy');
                $("#divPlanilha").html(data.HtmlPlanilha);
                $('#tablePlanilha').floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('#divPlanilha');
                    }
                });
            }
            else {
                $('#tablePlanilha').floatThead('destroy');
                $("#divPlanilha").html("");
            }

            if (data.HtmlMedicao) {
                $('#tableMedicao').floatThead('destroy');
                $("#divMedicao").html(data.HtmlMedicao);
                $('#tableMedicao').floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('#divMedicao');
                    }
                });
            }
            else {
                $('#tableMedicao').floatThead('destroy');
                $("#divMedicao").html("");
            }

            if (data.HtmlProducao) {
                $('#tableProducao').floatThead('destroy');
                $("#divProducao").html(data.HtmlProducao);
                $('#tableProducao').floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('#divProducao');
                    }
                });
            }
            else {
                $('#tableProducao').floatThead('destroy');
                $("#divProducao").html("");
            }
        };

        RequisicaoAjax(urlCarregar, "json", parametros, "POST", true, true, sucesso);
    };

    this.ModificarInterno = function (idCronograma, idCronogramaServico, mesReferencia, dataProducao, idPlanilha, idItemPlanilha, nomePropriedade, valorPropriedade) {
        let cronScrollTop = $("#divCronograma").scrollTop();
        let cronScrollLeft = $("#divCronograma").scrollLeft();

        let planScrollTop = $("#divPlanilha").scrollTop();
        let planScrollLeft = $("#divPlanilha").scrollLeft();

        let mediScrollTop = $("#divMedicao").scrollTop();
        let mediScrollLeft = $("#divMedicao").scrollLeft();

        let prodScrollTop = $("#divProducao").scrollTop();
        let prodScrollLeft = $("#divProducao").scrollLeft();

        let parametros = {
            IdCronograma: idCronograma,
            IdCronogramaServico: idCronogramaServico,
            MesReferencia: mesReferencia,
            DataProducao: dataProducao,
            IdPlanilha: idPlanilha,
            IdItemPlanilha: idItemPlanilha,
            NomePropriedade: nomePropriedade,
            ValorPropriedade: valorPropriedade,
            CronogramasJson: cronograma.cronogramas,
            IdCronogramaJson: cronograma.idCronograma
        };
        let sucesso = function (data) {
            if (!data.Sucesso) {
                Notificacao(data.Mensagem.Texto, TipoMensagem(data.Mensagem.Tipo));
                cronograma.campoAlterado.innerText = cronograma.valorAlterado;
            }
            else {
                cronograma.cronogramas = data.Cronogramas;

                $("#tableCronograma").floatThead("destroy");
                $("#divCronograma").html(data.HtmlCronograma);
                $("#tableCronograma").floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest("#divCronograma");
                    }
                });
                $("#divCronograma").scrollTop(cronScrollTop);
                $("#divCronograma").scrollLeft(cronScrollLeft);

                $('#tablePlanilha').floatThead('destroy');
                $("#divPlanilha").html(data.HtmlPlanilha);
                $('#tablePlanilha').floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('#divPlanilha');
                    }
                });
                $("#divPlanilha").scrollTop(planScrollTop);
                $("#divPlanilha").scrollLeft(planScrollLeft);

                $('#tableMedicao').floatThead('destroy');
                $("#divMedicao").html(data.HtmlMedicao);
                $('#tableMedicao').floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('#divMedicao');
                    }
                });
                $("#divMedicao").scrollTop(mediScrollTop);
                $("#divMedicao").scrollLeft(mediScrollLeft);

                $('#tableProducao').floatThead('destroy');
                $("#divProducao").html(data.HtmlProducao);
                $('#tableProducao').floatThead({
                    scrollContainer: function ($table) {
                        return $table.closest('#divProducao');
                    }
                });
                $("#divProducao").scrollTop(prodScrollTop);
                $("#divProducao").scrollLeft(prodScrollLeft);
            }
        };

        RequisicaoAjax(urlModificar, "json", parametros, "POST", true, true, sucesso);
    };

    this.Modificar = function (valor) {
        if (cronograma.valorAlterado != valor.innerText) {
            cronograma.ModificarInterno(valor.dataset["cron"], valor.dataset["serv"], valor.dataset["mesref"],
                valor.dataset["prod"], valor.dataset["plan"], valor.dataset["item"], valor.dataset["tipo"],
                valor.innerText);
        }
    };

    this.Salvar = function () {
        let parametros = {
            cronogramas: cronograma.cronogramas
        };
        let sucesso = function (data) {
            if (!data.Sucesso)
                Notificacao(data.Mensagem.Texto, TipoMensagem(data.Mensagem.Tipo));
            else {
                NotificacaoSucesso("Cronograma salvo com sucesso!");
                cronograma.Carregar();
            }
        };

        RequisicaoAjax(urlSalvar, "json", parametros, "POST", true, true, sucesso);
    };

    this.Cancelar = function () {
        let confirma = function () {
            window.close();
        };

        AlertaConfirmacao("Tem certeza?", "Os dados alterados serão perdidos.", confirma);
    };

    this.ModificarServico = function (idCronograma, idPlanilha, idPlanilhaItem) {
        MostrarModal("modalServico", "40%");
        LimparModal("modalServico");
        $("#hidIdCronograma").val(idCronograma);
        $("#hidIdPlanilha").val(idPlanilha);
        $("#hidIdPlanilhaItem").val(idPlanilhaItem);
        cronograma.CriarSelect2(idCronograma);
    };

    this.SalvarServico = function () {
        let idServico = $("#selIdServico").val() ? "+" + $("#selIdServico").val() : $("#selIdCronogramaServico").val();

        if (idServico)
            cronograma.ModificarInterno($("#hidIdCronograma").val(), null, null, null, $("#hidIdPlanilha").val(),
                $("#hidIdPlanilhaItem").val(), "IdCronogramaServico", idServico);

        cronograma.CancelarServico();
    };

    this.CancelarServico = function () {
        LimparModal("modalServico");
        EsconderModal("modalServico");
    };
}

$(document).on("focus", "td", function (e) {
    cronograma.valorFocus = e.currentTarget.innerText;
    cronograma.campoFocus = e.currentTarget;
});

$(document).on("blur", "td", function (e) {
    cronograma.valorAlterado = cronograma.valorFocus;
    cronograma.campoAlterado = cronograma.campoFocus;
    cronograma.Modificar(e.currentTarget);
});

$(document).on("click", "#tableProducao .toggle-td", function (e) {
    $(e.currentTarget).find("span").text(function (index, value) { return value == "-" ? "+" : "-"; });
    $(e.currentTarget.parentElement).prevUntil("tr.toggle-table").slideToggle(100, function () { });
});

$(document).on("click", "#tableCronograma .toggle-td", function (e) {
    let icone = $(e.currentTarget).find("i");
    if (icone.hasClass("fa-plus"))
        icone.removeClass("fa-plus").addClass("fa-minus");
    else
        icone.removeClass("fa-minus").addClass("fa-plus");

    let rowspan = $(e.currentTarget.parentElement).find('td[rowspan]').attr('rowspan') == "4" ? "2" : "4";
    $(e.currentTarget.parentElement).find("td[rowspan]").removeAttr("rowspan").attr("rowspan", rowspan);
    $(e.currentTarget.parentElement).nextUntil("tr.toggle-table", ".toggle-tr").slideToggle(100, function () { });
});

$(document).ready(function () {
    cronograma.Carregar();
    cronograma.CriarMenu();

    $("#selIdCronogramaServico").change(function (e) {
        if (e.currentTarget.value) {
            $("#selIdServico").val("").change();
            $("#selIdServico").prop("disabled", true);
        }
        else
            $("#selIdServico").prop("disabled", false);
    });

    $("#selIdServico").change(function (e) {
        if (e.currentTarget.value) {
            $("#selIdCronogramaServico").val("").change();
            $("#selIdCronogramaServico").prop("disabled", true);
        }
        else
            $("#selIdCronogramaServico").prop("disabled", false);
    });
});
