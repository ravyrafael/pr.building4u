﻿"use strict";

let cadastroAreas = new CadastroAreas();

function CadastroAreas() {
    this.CriarMenu = function () {
        let editar = PossuiPermissao("CadastroAreas", "Editar");

        if (editar) {
            let botoes = [];
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroAreas.Editar };

            botoes = botoes.concat(btnEditar);

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdArea", searchable: false, orderable: false, visible: false },
            { title: "Sigla", data: "SiglaArea", searchable: true, orderable: true },
            { title: "Nome", data: "NomeArea", searchable: true, orderable: true }
        ];
        let ordem = [
            [2, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdArea");

            MostrarModal("modalCadastro", "80%");
            $("#inpSiglaArea").prop("disabled", true);
            LimparModal("modalCadastro");

            let parametros = {
                idArea: id
            };
            let sucesso = function (data) {
                if (data.Area) {
                    $("#hidIdArea").val(data.Area.IdArea);
                    $("#inpSiglaArea").val(data.Area.SiglaArea);
                    $("#inpNomeArea").val(data.Area.NomeArea);
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idArea: $("#hidIdArea").val(),
                nomeArea: $("#inpNomeArea").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };
}

$(document).ready(function () {
    cadastroAreas.CriarMenu();
    cadastroAreas.CriarTabela();
});
