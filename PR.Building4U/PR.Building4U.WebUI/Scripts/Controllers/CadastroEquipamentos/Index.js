﻿"use strict";

let cadastroEquipamentos = new CadastroEquipamentos();

function CadastroEquipamentos() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroEquipamentos", "Incluir");
        let editar = PossuiPermissao("CadastroEquipamentos", "Editar");
        let sucatear = PossuiPermissao("CadastroEquipamentos", "Sucatear");

        if (incluir || editar || sucatear) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroEquipamentos.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroEquipamentos.Editar };
            let btnSucatear = { button: "medium red", label: "Sucatear", icon: "mdi mdi-server-remove", onClick: cadastroEquipamentos.Sucatear };

            botoes = sucatear ? botoes.concat(btnSucatear) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdEquipamento", searchable: false, orderable: false, visible: false },
            { title: "Patrimônio", data: "PatrimonioEquipamento", searchable: true, orderable: true },
            { title: "Tipo", data: "TipoEquipamento.NomeTipoEquipamento", searchable: true, orderable: true },
            { title: "Marca", data: "MarcaEquipamento", searchable: true, orderable: true },
            { title: "Modelo", data: "ModeloEquipamento", searchable: true, orderable: true },
            {
                title: "Aquisição", data: "AquisicaoEquipamento", searchable: false, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            {
                title: "Sucata", data: "SucataEquipamento", searchable: false, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            {
                title: "Fornecedor", data: "IndicadorTerceiro", searchable: false, orderable: true,
                render: function (data, type, row, meta) {
                    return data ? row["FornecedorEquipamento"] : "Progeo";
                }
            },
            { title: "Situação", data: "Situacao.NomeSituacao", searchable: true, orderable: true }
        ];
        let ordem = [
            [2, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.CriarSelect2 = function () {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };

        CriarSelect2Dinamico("selIdTipoEquipamento", urlListarTiposEquipamento, "Informe nome...", "modalCadastro", 2, paramSelect, false);
        CriarSelect2Dinamico("selIdObra", urlListarObras, "Informe código ou nome...", "modalCadastro", 2, paramSelect, false);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        $("#inpPatrimonioEquipamento").prop("disabled", false);
        $("#selIdTipoEquipamento").prop("disabled", false);
        $("#inpAquisicaoEquipamento").prop("disabled", false);
        $("#chkIndicadorTerceiro").prop("disabled", false);
        $("#inpFornecedorEquipamento").prop("disabled", true);
        $("#selIdObra").prop("disabled", false);
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdEquipamento");

            MostrarModal("modalCadastro", "80%");
            $("#inpPatrimonioEquipamento").prop("disabled", true);
            $("#selIdTipoEquipamento").prop("disabled", true);
            $("#inpAquisicaoEquipamento").prop("disabled", true);
            $("#chkIndicadorTerceiro").prop("disabled", true);
            $("#selIdObra").prop("disabled", true);
            LimparModal("modalCadastro");

            let parametros = {
                idEquipamento: id
            };
            let sucesso = function (data) {
                if (data.Equipamento) {
                    $("#hidIdEquipamento").val(data.Equipamento.IdEquipamento);
                    $("#inpPatrimonioEquipamento").val(data.Equipamento.PatrimonioEquipamento);
                    $("#inpMarcaEquipamento").val(data.Equipamento.MarcaEquipamento);
                    $("#inpModeloEquipamento").val(data.Equipamento.ModeloEquipamento);
                    $("#inpAquisicaoEquipamento").val(DateJsonToDateString(data.Equipamento.AquisicaoEquipamento)).change();
                    $("#chkIndicadorTerceiro").prop("checked", data.Equipamento.IndicadorTerceiro).change();
                    $("#inpFornecedorEquipamento").val(data.Equipamento.FornecedorEquipamento);
                    DefinirValorSelect2Dinamico("selIdTipoEquipamento", data.Equipamento.TipoEquipamento.IdTipoEquipamento, data.Equipamento.TipoEquipamento.NomeTipoEquipamento);
                    DefinirValorSelect2Dinamico("selIdObra", data.Equipamento.Obra.IdObra, PadLeft(data.Equipamento.Obra.CodigoObra, "0", 3) + " - " + data.Equipamento.Obra.NomeObra);
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Sucatear = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdEquipamento");
            let patrimonio = ObterCampoTabela("tabelaCadastro", "PatrimonioEquipamento");

            MostrarModal("modalSucata");
            LimparModal("modalSucata");
            $("#hidIdEquipamentoSucata").val(id);
            $("#inpSucataEquipamento").val(DateToString(new Date())).change();
            $("#spnPatrimonioEquipamento").html(patrimonio);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let tipoEquipamento = {
                IdTipoEquipamento: $("#selIdTipoEquipamento").val()
            };
            let obra = {
                IdObra: $("#selIdObra").val()
            };
            let parametros = {
                IdEquipamento: $("#hidIdEquipamento").val(),
                PatrimonioEquipamento: $("#inpPatrimonioEquipamento").val(),
                MarcaEquipamento: $("#inpMarcaEquipamento").val(),
                ModeloEquipamento: $("#inpModeloEquipamento").val(),
                AquisicaoEquipamento: $("#inpAquisicaoEquipamento").val(),
                IndicadorTerceiro: $("#chkIndicadorTerceiro").prop("checked"),
                FornecedorEquipamento: $("#inpFornecedorEquipamento").val(),
                TipoEquipamento: tipoEquipamento,
                Obra: obra
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdEquipamento").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };

    this.CancelarSucata = function () {
        LimparModal("modalSucata");
        EsconderModal("modalSucata");
    };

    this.ConfirmarSucata = function () {
        if (ValidarCamposObrigatorios("modalSucata")) {
            let parametros = {
                idEquipamento: $("#hidIdEquipamentoSucata").val(),
                sucataEquipamento: $("#inpSucataEquipamento").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalSucata");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            RequisicaoAjax(urlSucatear, "json", parametros, "POST", true, true, sucesso);
        }
    };
}

$(document).ready(function () {
    cadastroEquipamentos.CriarMenu();
    cadastroEquipamentos.CriarTabela();
    cadastroEquipamentos.CriarSelect2();

    $("#chkIndicadorTerceiro").change(function (e) {
        if (e.currentTarget.checked)
            $("#inpFornecedorEquipamento").prop("disabled", false);
        else
            $("#inpFornecedorEquipamento").prop("disabled", true);
    });
});
