"use strict";

let cadastroObras = new CadastroObras();

function CadastroObras() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroObras", "Incluir");
        let editar = PossuiPermissao("CadastroObras", "Editar");
        let excluir = PossuiPermissao("CadastroObras", "Excluir");

        if (incluir || editar || excluir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroObras.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroObras.Editar };
            let btnExcluir = { button: "medium red", label: "Excluir", icon: "mdi mdi-delete", onClick: cadastroObras.Excluir };

            botoes = excluir ? botoes.concat(btnExcluir) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdObra", searchable: false, orderable: false, visible: false },
            { title: "Código", data: "CodigoObra", searchable: true, orderable: true },
            { title: "Nome", data: "NomeObra", searchable: true, orderable: true },
            { title: "Área", data: "Area.SiglaArea", searchable: true, orderable: true },
            {
                title: "Data OS", data: "OrdemServico.DataOrdemServico", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return DateJsonToDateString(data);
                }
            },
            {
                title: "Prazo OS", data: "OrdemServico.PrazoOrdemServico", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return data ? data : "";
                }
            },
            { title: "Situação", data: "Situacao.NomeSituacao", searchable: true, orderable: true },
            {
                title: "Contrato", data: "Contrato.CodigoContrato", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return data ? data : "";
                }
            }
        ];
        let ordem = [
            [1, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.CriarSelect2 = function () {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };

        CriarSelect2("selIdArea", "Informe sigla ou nome...", "modalCadastro", false);
        CriarSelect2Dinamico("selIdContrato", urlListarContratos, "Informe código ou nome...", "modalCadastro", 2, paramSelect, false);

        CriarSelect2("selIdGerencia", "Informe sigla ou nome...", "modalCadastroGerencia", false);
        CriarSelect2Dinamico("selIdFuncionario", urlListarFuncionarios, "Informe código ou nome...", "modalCadastroGerencia", 2, paramSelect, false);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        $("#inpDataOrdemServico").prop("disabled", false);
        $("#inpPrazoOrdemServico").prop("disabled", false);
        LimparModal("modalCadastro");
        $("#divGerTitle").hide();
        $("#divGetBody").hide();
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdObra");

            MostrarModal("modalCadastro", "80%");
            LimparModal("modalCadastro");

            let parametros = {
                idObra: id
            };
            let sucesso = function (data) {
                if (data.Obra) {
                    $("#hidIdObra").val(data.Obra.IdObra);
                    $("#inpCodigoObra").val(data.Obra.CodigoObra);
                    $("#inpNomeObra").val(data.Obra.NomeObra);
                    $("#selIdArea").val(data.Obra.Area.IdArea).change();
                    if (data.Obra.Contrato && data.Obra.Contrato.IdContrato)
                        DefinirValorSelect2Dinamico("selIdContrato", data.Obra.Contrato.IdContrato, data.Obra.Contrato.CodigoContrato + " (" + data.Obra.Contrato.Cliente.NomeCliente + ")");
                    if (data.Obra.OrdemServico) {
                        $("#inpDataOrdemServico").val(DateJsonToDateString(data.Obra.OrdemServico.DataOrdemServico));
                        $("#inpPrazoOrdemServico").val(data.Obra.OrdemServico.PrazoOrdemServico);
                    }

                    AtualizarDatatable("tabelaGerencia");
                    $("#divGerTitle").show();
                    $("#divGetBody").show();
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Excluir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdObra");

            let confirma = function () {
                let parametros = {
                    idObra: id
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlExcluir, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idObra: $("#hidIdObra").val(),
                codigoObra: $("#inpCodigoObra").val(),
                nomeObra: $("#inpNomeObra").val(),
                idArea: $("#selIdArea").val(),
                idContrato: $("#selIdContrato").val(),
                dataOrdemServico: $("#inpDataOrdemServico").val(),
                prazoOrdemServico: $("#inpPrazoOrdemServico").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    if ($("#hidIdObra").val()) {
                        EsconderModal("modalCadastro");
                        AtualizarDatatable("tabelaCadastro");
                    }
                    else {
                        $("#hidIdObra").val(data.idObra);
                        AtualizarDatatable("tabelaCadastro");
                        AtualizarDatatable("tabelaGerencia");
                        $("#divGerTitle").show();
                        $("#divGetBody").show();
                    }
                }
            };

            if ($("#hidIdObra").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };

    this.CriarTabelaGerencia = function () {
        let colunas = [
            { title: "Id", data: "IdObraGerencia", searchable: false, orderable: false, visible: false },
            {
                title: "<button type='button' class='btn btn-sm btn-info btn-rounded' style='white-space:nowrap;' onclick='cadastroObras.IncluirGerencia()'><i class='fas fa-plus'></i> Incluir</button>",
                data: "IdObraGerencia", searchable: false, orderable: false,
                render: function (data, type, row, meta) {
                    return "<button class='btn btn-sm btn-danger btn-rounded' onclick='cadastroObras.ExcluirGerencia(" + data + ")'><i class='fas fa-trash'></i> Excluir</button>";
                }
            },
            { title: "Gerência", data: "Gerencia.NomeGerencia", searchable: true, orderable: true },
            { title: "Funcionário", data: "Funcionario.NomeFuncionario", searchable: true, orderable: true }
        ];
        let ordem = [
            [2, "asc"], [3, "asc"]
        ];
        let parametros = function (p) {
            p.idObra = $("#hidIdObra").val();
            return p;
        };

        CriarDatatableServerSide("tabelaGerencia", urlListarGerencias, colunas, ordem, true, parametros);
    };

    this.IncluirGerencia = function () {
        EsconderModal("modalCadastro");
        MostrarModal("modalCadastroGerencia", "80%");
        LimparModal("modalCadastroGerencia");
    };

    this.ExcluirGerencia = function (id) {
        let confirma = function () {
            let parametros = {
                idObraGerencia: id
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    AtualizarDatatable("tabelaGerencia");
                }
            };

            RequisicaoAjax(urlExcluirGerencia, "json", parametros, "POST", true, true, sucesso);
        };

        AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
    };

    this.SalvarGerencia = function () {
        if (ValidarCamposObrigatorios("modalCadastroGerencia")) {
            let parametros = {
                idObra: $("#hidIdObra").val(),
                idGerencia: $("#selIdGerencia").val(),
                idFuncionario: $("#selIdFuncionario").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastroGerencia");
                    AtualizarDatatable("tabelaGerencia");
                    MostrarModal("modalCadastro", "80%");
                }
            };

            RequisicaoAjax(urlIncluirGerencia, "json", parametros, "POST", true, false, sucesso);
        }
    };

    this.CancelarGerencia = function () {
        LimparModal("modalCadastroGerencia");
        EsconderModal("modalCadastroGerencia");
        MostrarModal("modalCadastro", "80%");
    };
}

$(document).ready(function () {
    cadastroObras.CriarMenu();
    cadastroObras.CriarTabela();
    cadastroObras.CriarSelect2();
    cadastroObras.CriarTabelaGerencia();

    $("#selIdArea").change(function (e) {
        if (e.currentTarget.value) {
            if ((e.currentTarget.value == "3" || e.currentTarget.value == "4")) {
                $("#inpDataOrdemServico").val("");
                $("#inpPrazoOrdemServico").val("");
                $("#inpDataOrdemServico").prop("disabled", true);
                $("#inpPrazoOrdemServico").prop("disabled", true);
            }
            else {
                $("#inpDataOrdemServico").prop("disabled", false);
                $("#inpPrazoOrdemServico").prop("disabled", false);
            }
        }
    });
});
