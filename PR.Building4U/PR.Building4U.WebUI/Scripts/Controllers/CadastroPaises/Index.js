﻿"use strict";

let cadastroPaises = new CadastroPaises();

function CadastroPaises() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroPaises", "Incluir");
        let editar = PossuiPermissao("CadastroPaises", "Editar");
        let excluir = PossuiPermissao("CadastroPaises", "Excluir");

        if (incluir || editar || excluir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroPaises.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroPaises.Editar };
            let btnExcluir = { button: "medium red", label: "Excluir", icon: "mdi mdi-delete", onClick: cadastroPaises.Excluir };

            botoes = excluir ? botoes.concat(btnExcluir) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdPais", searchable: false, orderable: false, visible: false },
            { title: "Código", data: "CodigoPais", searchable: true, orderable: true },
            { title: "Nome", data: "NomePais", searchable: true, orderable: true }
        ];
        let ordem = [
            [2, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        $("#inpCodigoPais").prop("disabled", false);
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdPais");

            MostrarModal("modalCadastro", "80%");
            $("#inpCodigoPais").prop("disabled", true);
            LimparModal("modalCadastro");

            let parametros = {
                idPais: id
            };
            let sucesso = function (data) {
                if (data.Pais) {
                    $("#hidIdPais").val(data.Pais.IdPais);
                    $("#inpCodigoPais").val(data.Pais.CodigoPais);
                    $("#inpNomePais").val(data.Pais.NomePais);
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Excluir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdPais");

            let confirma = function () {
                let parametros = {
                    idPais: id
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlExcluir, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idPais: $("#hidIdPais").val(),
                codigoPais: $("#inpCodigoPais").val(),
                nomePais: $("#inpNomePais").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdPais").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };
}

$(document).ready(function () {
    cadastroPaises.CriarMenu();
    cadastroPaises.CriarTabela();
});
