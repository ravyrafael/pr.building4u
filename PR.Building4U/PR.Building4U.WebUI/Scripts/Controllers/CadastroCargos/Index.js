﻿"use strict";

let cadastroCargos = new CadastroCargos();

function CadastroCargos() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroCargos", "Incluir");
        let editar = PossuiPermissao("CadastroCargos", "Editar");
        let excluir = PossuiPermissao("CadastroCargos", "Excluir");

        if (incluir || editar || excluir) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroCargos.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroCargos.Editar };
            let btnExcluir = { button: "medium red", label: "Excluir", icon: "mdi mdi-delete", onClick: cadastroCargos.Excluir };

            botoes = excluir ? botoes.concat(btnExcluir) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdCargo", searchable: false, orderable: false, visible: false },
            { title: "Nome", data: "NomeCargo", searchable: true, orderable: true },
            { title: "Sigla", data: "SiglaCargo", searchable: true, orderable: true },
            { title: "Custo", data: "Custo.NomeCusto", searchable: true, orderable: true }
        ];
        let ordem = [
            [3, "asc"], [1, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.CriarSelect2 = function () {
        CriarSelect2("selIdCusto", "Informe sigla ou nome...", "modalCadastro", false);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdCargo");

            MostrarModal("modalCadastro", "80%");
            LimparModal("modalCadastro");

            let parametros = {
                idCargo: id
            };
            let sucesso = function (data) {
                if (data.Cargo) {
                    $("#hidIdCargo").val(data.Cargo.IdCargo);
                    $("#inpSiglaCargo").val(data.Cargo.SiglaCargo);
                    $("#inpNomeCargo").val(data.Cargo.NomeCargo);
                    $("#selIdCusto").val(data.Cargo.Custo.IdCusto).change();
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Excluir = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdCargo");

            let confirma = function () {
                let parametros = {
                    idCargo: id
                };
                let sucesso = function (data) {
                    Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                    if (data.sucesso)
                        AtualizarDatatable("tabelaCadastro");
                };

                RequisicaoAjax(urlExcluir, "json", parametros, "POST", true, true, sucesso);
            };

            AlertaConfirmacao("Tem certeza?", "Não será possível recuperar os dados.", confirma);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let parametros = {
                idCargo: $("#hidIdCargo").val(),
                siglaCargo: $("#inpSiglaCargo").val(),
                nomeCargo: $("#inpNomeCargo").val(),
                idCusto: $("#selIdCusto").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdCargo").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };
}

$(document).ready(function () {
    cadastroCargos.CriarMenu();
    cadastroCargos.CriarTabela();
    cadastroCargos.CriarSelect2();
});
