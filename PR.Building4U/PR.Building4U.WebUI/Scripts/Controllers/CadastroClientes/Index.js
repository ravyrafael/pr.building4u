﻿"use strict";

let cadastroClientes = new CadastroClientes();

function CadastroClientes() {
    this.CriarMenu = function () {
        let incluir = PossuiPermissao("CadastroClientes", "Incluir");
        let editar = PossuiPermissao("CadastroClientes", "Editar");
        let desativar = PossuiPermissao("CadastroClientes", "Desativar");

        if (incluir || editar || desativar) {
            let botoes = [];
            let btnIncluir = { button: "medium blue", label: "Incluir", icon: "mdi mdi-plus", onClick: cadastroClientes.Incluir };
            let btnEditar = { button: "medium green", label: "Editar", icon: "mdi mdi-pencil", onClick: cadastroClientes.Editar };
            let btnDesativar = { button: "medium red", label: "Desativar", icon: "mdi mdi-tag-remove", onClick: cadastroClientes.Desativar };

            botoes = desativar ? botoes.concat(btnDesativar) : botoes;
            botoes = editar ? botoes.concat(btnEditar) : botoes;
            botoes = incluir ? botoes.concat(btnIncluir) : botoes;

            CriarMenuFlutuante(botoes);
        }
    };

    this.CriarTabela = function () {
        let colunas = [
            { title: "Id", data: "IdCliente", searchable: false, orderable: false, visible: false },
            {
                title: "CNPJ/CPF", data: "CnpjCliente", searchable: true, orderable: true,
                render: function (data, type, row, meta) {
                    return FormatarCnpjCpf(data);
                }
            },
            { title: "Razão Social", data: "RazaoSocialCliente", searchable: true, orderable: true },
            { title: "Nome", data: "NomeCliente", searchable: true, orderable: true },
            { title: "Situação", data: "Situacao.NomeSituacao", searchable: true, orderable: true }
        ];
        let ordem = [
            [3, "asc"]
        ];

        CriarDatatableServerSide("tabelaCadastro", urlListar, colunas, ordem, true);
    };

    this.CriarSelect2 = function () {
        let paramSelect = function (term) {
            return {
                codigoOuNome: term.term
            };
        };
        let paramSelectEstado = function (term) {
            return {
                codigoOuNome: term.term,
                idPais: $("#selIdPais").val()
            };
        };
        let paramSelectMunicipio = function (term) {
            return {
                codigoOuNome: term.term,
                idEstado: $("#selIdEstado").val()
            };
        };

        CriarSelect2Dinamico("selIdPais", urlListarPaises, "Informe código ou nome...", "modalCadastro", 2, paramSelect, false);
        CriarSelect2Dinamico("selIdEstado", urlListarEstados, "Informe sigla ou nome...", "modalCadastro", 2, paramSelectEstado, false);
        CriarSelect2Dinamico("selIdMunicipio", urlListarMunicipios, "Informe código ou nome...", "modalCadastro", 2, paramSelectMunicipio, false);
    };

    this.Incluir = function () {
        MostrarModal("modalCadastro", "80%");
        $("#inpCnpjCliente").prop("disabled", false);
        $("#selIdEstado").prop("disabled", true);
        $("#selIdMunicipio").prop("disabled", true);
        LimparModal("modalCadastro");
    };

    this.Editar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdCliente");

            MostrarModal("modalCadastro", "80%");
            $("#inpCnpjCliente").prop("disabled", true);
            $("#selIdEstado").prop("disabled", false);
            $("#selIdMunicipio").prop("disabled", false);
            LimparModal("modalCadastro");

            let parametros = {
                idCliente: id
            };
            let sucesso = function (data) {
                if (data.Cliente) {
                    $("#hidIdCliente").val(data.Cliente.IdCliente);
                    $("#inpCnpjCliente").val(data.Cliente.CnpjCliente);
                    $("#inpRazaoSocialCliente").val(data.Cliente.RazaoSocialCliente);
                    $("#inpNomeCliente").val(data.Cliente.NomeCliente);
                    $("#hidIdEndereco").val(data.Cliente.Endereco.IdEndereco);
                    $("#inpLogradouroEndereco").val(data.Cliente.Endereco.LogradouroEndereco);
                    $("#inpNumeroEndereco").val(data.Cliente.Endereco.NumeroEndereco);
                    $("#inpCepEndereco").val(data.Cliente.Endereco.CepEndereco).change();
                    $("#inpComplementoEndereco").val(data.Cliente.Endereco.ComplementoEndereco);
                    $("#inpDistritoEndereco").val(data.Cliente.Endereco.DistritoEndereco);
                    DefinirValorSelect2Dinamico("selIdPais", data.Cliente.Endereco.Municipio.Estado.Pais.IdPais, PadLeft(data.Cliente.Endereco.Municipio.Estado.Pais.CodigoPais.toString(), "0", 4) + " - " + data.Cliente.Endereco.Municipio.Estado.Pais.NomePais);
                    DefinirValorSelect2Dinamico("selIdEstado", data.Cliente.Endereco.Municipio.Estado.IdEstado, data.Cliente.Endereco.Municipio.Estado.SiglaEstado + " - " + data.Cliente.Endereco.Municipio.Estado.NomeEstado);
                    DefinirValorSelect2Dinamico("selIdMunicipio", data.Cliente.Endereco.Municipio.IdMunicipio, PadLeft(data.Cliente.Endereco.Municipio.CodigoMunicipio.toString(), "0", 4) + " - " + data.Cliente.Endereco.Municipio.NomeMunicipio);
                }
            };

            RequisicaoAjax(urlObter, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Desativar = function () {
        if (ValidarLinhaSelecionada("tabelaCadastro")) {
            let id = ObterCampoTabela("tabelaCadastro", "IdCliente");
            let nome = ObterCampoTabela("tabelaCadastro", "NomeCliente");

            MostrarModal("modalDesativar");
            LimparModal("modalDesativar");
            $("#hidIdClienteDesativar").val(id);
            $("#inpDesativarCliente").val(DateToString(new Date())).change();
            $("#spnNomeCliente").html(nome);
        }
    };

    this.Salvar = function () {
        if (ValidarCamposObrigatorios("modalCadastro")) {
            let municipio = {
                IdMunicipio: $("#selIdMunicipio").val()
            };
            let endereco = {
                IdEndereco: $("#hidIdEndereco").val(),
                LogradouroEndereco: $("#inpLogradouroEndereco").val(),
                NumeroEndereco: $("#inpNumeroEndereco").val(),
                ComplementoEndereco: $("#inpComplementoEndereco").val(),
                CepEndereco: $("#inpCepEndereco").val(),
                DistritoEndereco: $("#inpDistritoEndereco").val(),
                Municipio: municipio
            };
            let parametros = {
                IdCliente: $("#hidIdCliente").val(),
                CnpjCliente: $("#inpCnpjCliente").val(),
                RazaoSocialCliente: $("#inpRazaoSocialCliente").val(),
                NomeCliente: $("#inpNomeCliente").val(),
                Endereco: endereco
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalCadastro");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            if ($("#hidIdCliente").val())
                RequisicaoAjax(urlEditar, "json", parametros, "POST", true, true, sucesso);
            else
                RequisicaoAjax(urlIncluir, "json", parametros, "POST", true, true, sucesso);
        }
    };

    this.Cancelar = function () {
        LimparModal("modalCadastro");
        EsconderModal("modalCadastro");
    };

    this.CancelarDesativar = function () {
        LimparModal("modalDesativar");
        EsconderModal("modalDesativar");
    };

    this.ConfirmarDesativar = function () {
        if (ValidarCamposObrigatorios("modalDesativar")) {
            let parametros = {
                idCliente: $("#hidIdClienteDesativar").val(),
                dataDesativado: $("#inpDesativarCliente").val()
            };
            let sucesso = function (data) {
                Notificacao(data.mensagem.Texto, TipoMensagem(data.mensagem.Tipo));

                if (data.sucesso) {
                    EsconderModal("modalDesativar");
                    AtualizarDatatable("tabelaCadastro");
                }
            };

            RequisicaoAjax(urlDesativar, "json", parametros, "POST", true, true, sucesso);
        }
    };
}

$(document).ready(function () {
    cadastroClientes.CriarMenu();
    cadastroClientes.CriarTabela();
    cadastroClientes.CriarSelect2();

    $("#selIdPais").change(function (e) {
        $("#selIdEstado").val("").change();
        if (e.currentTarget.value)
            $("#selIdEstado").prop("disabled", false);
        else
            $("#selIdEstado").prop("disabled", true);
    });

    $("#selIdEstado").change(function (e) {
        $("#selIdMunicipio").val("").change();
        if (e.currentTarget.value)
            $("#selIdMunicipio").prop("disabled", false);
        else
            $("#selIdMunicipio").prop("disabled", true);
    });
});
