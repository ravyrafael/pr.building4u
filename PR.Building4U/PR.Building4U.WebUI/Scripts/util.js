﻿"use strict";

// #region Funções de automatização

$(document).ready(function () {
    $("input, textarea, .select2").on("change", function (e) {
        if (e.currentTarget.value) {
            RemoverClasseAviso(e.currentTarget.id);
            RemoverClasseErro(e.currentTarget.id);
        }
    });

    $(".modal").on("shown.bs.modal", function () {
        $(".modal-overlay").addClass("modal-overlay--show-modal");
    });

    $(".modal").on("hidden.bs.modal", function () {
        $(".modal-overlay").removeClass("modal-overlay--show-modal");
    });

    $(".numerico").on("keypress", function (e) {
        let charCode = (e.which) ? e.which : e.keyCode;
        return "48,49,50,51,52,53,54,55,56,57".indexOf(charCode) >= 0;
    });

    $(".datepicker").datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "dd/mm/yyyy"
    });

    $(".date-inputmask").inputmask("99/99/9999");
    $(".email-inputmask").inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[*{2,6}][*{1,2}].*{1,}[.*{2,6}][.*{1,2}]",
        greedy: !1,
        onBeforePaste: function (e, a) {
            e = e.toLowerCase();
            return e.replace("mailto:", "");
        },
        definitions: {
            "*": {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~/-]",
                cardinality: 1,
                casing: "lower"
            }
        },
        clearIncomplete: true
    });
    $(".phone-inputmask").inputmask({
        mask: ["(99) 9999-9999", "(99) 99999-9999"],
        keepStatic: true,
        clearIncomplete: true
    });
    $(".cpfcnpj-inputmask").inputmask({
        mask: ["999.999.999-99", "99.999.999/9999-99"],
        keepStatic: true,
        clearIncomplete: true
    });
    $(".cep-inputmask").inputmask({
        mask: "99999-999",
        clearIncomplete: true
    });
    $(".money-inputmask").inputmask("decimal", {
        alias: "numeric",
        groupSeparator: ".",
        autoGroup: true,
        digits: 2,
        radixPoint: ",",
        digitsOptional: false,
        allowMinus: false,
        allowPlus: false,
        rightAlign: false,
        placeholder: "",
        clearIncomplete: true
    });
});

// #endregion

/**
 * Adiciona dias em uma data
 * @param {number} days Quantidade de dias para adicionar
 * @returns {date} Data acrescida de dias
 */
Date.prototype.addDays = function (days) {
    let date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
};

/**
 * Cria uma notificação de informação
 * @param {string} mensagem Mensagem
 * @param {string} tipo Tipo da mensagem (info, aviso, sucesso, erro)
 */
function Notificacao(mensagem, tipo) {
    let heading;
    let icon;

    switch (tipo) {
        case "aviso":
            heading = "Aviso";
            icon = "warning";
            break;
        case "sucesso":
            heading = "Sucesso";
            icon = "success";
            break;
        case "erro":
            heading = "Erro";
            icon = "error";
            break;
        default:
            heading = "Informação";
            icon = "info";
    }

    $.toast({
        heading: heading,
        text: mensagem,
        position: "top-right",
        loader: true,
        loaderBg: "#ff6849",
        icon: icon,
        hideAfter: 3500,
        stack: 2
    });
}

/**
 * Cria uma notificação de informação
 * @param {string} mensagem Mensagem de informação
 */
function NotificacaoInfo(mensagem) {
    Notificacao(mensagem, "info");
}

/**
 * Cria uma notificação de aviso
 * @param {string} mensagem Mensagem de aviso
 */
function NotificacaoAviso(mensagem) {
    Notificacao(mensagem, "aviso");
}

/**
 * Cria uma notificação de sucesso
 * @param {string} mensagem Mensagem de sucesso
 */
function NotificacaoSucesso(mensagem) {
    Notificacao(mensagem, "sucesso");
}

/**
 * Cria uma notificação de erro
 * @param {string} mensagem Mensagem de erro
 */
function NotificacaoErro(mensagem) {
    Notificacao(mensagem, "erro");
}

/**
 * Faz uma requisição Ajax
 * @param {string} url URL de destino
 * @param {string} tipoDados Tipo de dados do retorno
 * @param {string} parametros Parâmetros informados
 * @param {string} tipoChamada Tipo de chamada (POST/GET)
 * @param {boolean} ehAsync Chamada é assíncrona?
 * @param {boolean} exibirCarregando Exibir carregando?
 * @param {function} callbackSuccesso Função executada caso sucesso
 * @param {function} callbackErro Função executada caso erro (não obrigatório)
 * @returns {json} JSON resultado da chamada
 */
function RequisicaoAjax(url, tipoDados, parametros, tipoChamada, ehAsync, exibirCarregando, callbackSuccesso, callbackErro) {
    return $.ajax({
        url: url,
        dataType: tipoDados,
        data: parametros,
        quietMillis: 100,
        type: tipoChamada,
        cache: false,
        async: ehAsync,
        beforeSend: function () {
            if (exibirCarregando)
                BloquearPagina();
        },
        success: callbackSuccesso,
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr.status == 403)
                NotificacaoErro("Você não tem permissão para acessar este recurso.");
            else if (xhr.status == 404)
                NotificacaoErro("Recurso não foi encontrado, contate o administrador.");
            else if (xhr.status == 500)
                NotificacaoErro("Erro interno no servidor, aguarde ou contate o administrador.");
            else
                NotificacaoErro("Erro na requisição, tente novamente ou contate o administrador.");

            if (callbackErro)
                callbackErro();
        },
        complete: function () {
            if (exibirCarregando)
                DesbloquearPagina();
        }
    });
}

/**
 * Faz uma requisição Ajax com envio de arquivo
 * @param {string} url URL de destino
 * @param {string} tipoDados Tipo de dados do retorno
 * @param {string} parametros Parâmetros informados
 * @param {string} tipoChamada Tipo de chamada (POST/GET)
 * @param {boolean} ehAsync Chamada é assíncrona?
 * @param {boolean} exibirCarregando Exibir carregando?
 * @param {function} callbackSuccesso Função executada caso sucesso
 * @param {function} callbackErro Função executada caso erro (não obrigatório)
 * @returns {json} JSON resultado da chamada
 */
function RequisicaoAjaxArquivo(url, tipoDados, parametros, tipoChamada, ehAsync, exibirCarregando, callbackSuccesso, callbackErro) {
    return $.ajax({
        url: url,
        dataType: tipoDados,
        data: parametros,
        quietMillis: 100,
        type: tipoChamada,
        cache: false,
        async: ehAsync,
        processData: false,
        contentType: false,
        beforeSend: function () {
            if (exibirCarregando)
                BloquearPagina();
        },
        success: callbackSuccesso,
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr.status == 403)
                NotificacaoErro("Você não tem permissão para acessar este recurso.");
            else if (xhr.status == 404)
                NotificacaoErro("Recurso não foi encontrado, contate o administrador.");
            else if (xhr.status == 500)
                NotificacaoErro("Erro interno no servidor, aguarde ou contate o administrador.");
            else
                NotificacaoErro("Erro na requisição, tente novamente ou contate o administrador.");

            if (callbackErro)
                callbackErro();
        },
        complete: function () {
            if (exibirCarregando)
                DesbloquearPagina();
        }
    });
}

/**
 * Bloqueia a página e exibe carregando
 */
function BloquearPagina() {
    $(".preloader").show();
}

/**
 * Desbloqueia a página e esconde carregando
 */
function DesbloquearPagina() {
    $(".preloader").fadeOut();
}

/**
 * Valida os campos obrigatórios do formulário
 * @param {string} body ID do body para validar os campos
 * @returns {boolean} Resultado da validação
 */
function ValidarCamposObrigatorios(body) {
    let corpo = body ? "#" + body : "";
    let campos = $(corpo + " input," + corpo + " textarea," + corpo + " .select2").filter("[required]");

    let retorno = true;

    for (let i = 0; i < campos.length; i++) {
        if ($("#" + campos[i].id).val() === "" || $("#" + campos[i].id).val() === null) {
            retorno = false;
            AdicionarClasseAviso(campos[i].id);
        }
    }

    if (!retorno)
        NotificacaoAviso("Informe os campos em destaque.");

    return retorno;
}

/**
 * Adiciona estado de erro no campo selecionado
 * @param {string} campo ID do campo selecionado
 * @param {boolean} focar É necessário focar no input selecionado
 */
function AdicionarClasseErro(campo, focar) {
    let input = document.getElementById(campo);

    $(input).closest(".form-group").addClass("has-danger");
    $(input).addClass("form-control-danger");

    if (focar)
        $(input).focus();
}

/**
 * Remove estado de erro do campo selecionado
 * @param {string} campo ID do campo selecionado
 */
function RemoverClasseErro(campo) {
    let input = document.getElementById(campo);

    $(input).closest(".form-group").removeClass("has-danger");
    $(input).removeClass("form-control-danger");
}

/**
 * Remove estado de erro de todos os campos do formulário
 */
function LimparCamposErro() {
    let campos = $("input, textarea, .select2");

    for (let i = 0; i < campos.length; i++) {
        RemoverClasseErro(campos[i].id);
    }
}

/**
 * Adiciona estado de aviso no campo selecionado
 * @param {string} campo ID do input selecionado
 * @param {boolean} focar É necessário focar no input selecionado
 */
function AdicionarClasseAviso(campo, focar) {
    let input = document.getElementById(campo);

    $(input).closest(".form-group").addClass("has-warning");
    $(input).addClass("form-control-warning");

    if (focar)
        $(input).focus();
}

/**
 * Remove estado de aviso do campo selecionado
 * @param {string} campo ID do campo selecionado
 */
function RemoverClasseAviso(campo) {
    let input = document.getElementById(campo);

    $(input).closest(".form-group").removeClass("has-warning");
    $(input).removeClass("form-control-warning");
}

/**
 * Remove estado de aviso de todos os campos do formulário
 */
function LimparCamposAviso() {
    let campos = $("input, textarea, .select2");

    for (let i = 0; i < campos.length; i++) {
        RemoverClasseAviso(campos[i].id);
    }
}

/**
 * Inicializa um datatable com server side
 * @param {string} campo ID da tabela
 * @param {string} url URL da chamada para preencher a tabela
 * @param {Object} colunas Objeto contendo as definições de cada coluna da tabela
 * @param {object} ordenacao Objeto contendo as ordenações pré-definidas da tabela
 * @param {boolean} autoWidth Define se a tabela faz auto-ajusta para colunas
 * @param {function} parametros Objeto contendo parâmetros adicionais
 * @param {function} rowCallback Função para executar no desenho de cada linha
 */
function CriarDatatableServerSide(campo, url, colunas, ordenacao, autoWidth, parametros, rowCallback) {
    let id = "#" + campo;
    let lastCol = { title: "", data: null, render: function () { return ""; }, searchable: false, orderable: false };
    colunas = colunas ? colunas.concat(lastCol) : colunas;

    if ($.fn.DataTable.isDataTable(id))
        $(id).DataTable().destroy();

    $(id).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: url,
            error: function (xhr, error, thrown) {
                if (xhr.status == 403)
                    NotificacaoErro("Você não tem permissão para acessar este recurso.");
                else if (xhr.status == 404)
                    NotificacaoErro("Recurso não foi encontrado, contate o administrador.");
                else if (xhr.status == 500)
                    NotificacaoErro("Erro interno no servidor, aguarde ou contate o administrador.");
                else
                    NotificacaoErro("Erro na requisição, tente novamente ou contate o administrador.");
            },
            type: "POST",
            data: parametros ? parametros : {}
        },
        responsive: {
            details: {
                type: "column",
                target: -1
            }
        },
        columnDefs: [{
            className: "control",
            orderable: false,
            targets: -1
        }],
        autoWidth: autoWidth ? true : false,
        colReorder: true,
        select: {
            style: "single",
            info: false
        },
        columns: colunas,
        aaSorting: ordenacao ? ordenacao : [],
        pageLength: 10,
        fnRowCallback: rowCallback ? rowCallback : null,
        language: {
            decimal: ",",
            thousands: ".",
            loadingRecords: "Carregando...",
            processing: "Processando...",
            search: "Pesquisar:",
            lengthMenu: "Mostrar _MENU_ linhas por página",
            zeroRecords: "Não foi encontrado nenhum registro",
            info: "Mostrando _START_ a _END_ de _TOTAL_ registros",
            infoEmpty: "Nenhum registro cadastrado",
            infoFiltered: "(Filtrado de _MAX_ registros)",
            select: { rows: "" },
            paginate: {
                first: "Primeiro",
                last: "Último",
                next: "Próximo",
                previous: "Anterior"
            }
        }
    });
}

/**
 * Atualiza os dados da tabela sem recarregar a página
 * @param {string} campo ID da tabela
 */
function AtualizarDatatable(campo) {
    let id = "#" + campo;
    $(id).DataTable().ajax.reload(null, false);
}

/**
 * Exibe o modal selecionado
 * @param {string} idModal ID do modal selecionado
 * @param {string} largura Largura desejada para exibir o modal
 */
function MostrarModal(idModal, largura) {
    let width = largura ? largura : "500px";
    $("#" + idModal).modal({ backdrop: false }).modal("show");
    $("#" + idModal + " .modal-dialog").css("max-width", width);
}

/**
 * Esconde o modal selecionado
 * @param {string} idModal ID do modal selecionado
 */
function EsconderModal(idModal) {
    $("#" + idModal).modal("hide");
}

/**
 * Limpa todos os campos do modal selecionado
 * @param {string} idModal ID do modal selecionado
 */
function LimparModal(idModal) {
    let modal = "#" + idModal;
    let campos = $(modal + " input," + modal + " textarea," + modal + " select.select2");

    for (let i = 0; i < campos.length; i++) {
        if (campos[i].id) {
            if (campos[i].className.toUpperCase().indexOf("SELECT2") >= 0)
                $("#" + campos[i].id).val("").change();

            if (campos[i].type == "checkbox" || campos[i].type == "radio")
                campos[i].checked = false;

            $("#" + campos[i].id).val("");
        }
    }

    LimparCamposAviso();
    LimparCamposErro();
}

/**
 * Mostra um alerta de confirmação
 * @param {string} titulo Mensagem principal
 * @param {string} mensagem Mensagem secundária
 * @param {function} callbackConfirma Função a ser executada quando clicar em confirmar
 * @param {function} callbackCancela Função a ser executada quando clicar em cancelar
 */
function AlertaConfirmacao(titulo, mensagem, callbackConfirma, callbackCancela) {
    swal({
        title: titulo,
        text: mensagem,
        icon: "warning",
        dangerMode: true,
        buttons:
        {
            cancel: {
                text: "Cancelar",
                value: null,
                visible: true,
                className: "",
                closeModal: true
            },
            confirm: {
                text: "Confirmar",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function (willDelete) {
        if (willDelete) {
            if (callbackConfirma)
                callbackConfirma();
        }
        else {
            if (callbackCancela)
                callbackCancela();
        }
    });
}

/**
 * Cria select2 dinâmico
 * @param {string} id ID do select2 desejado
 * @param {string} url URL da chamada para preencher o select2
 * @param {string} placeholder Placeholder para o select2
 * @param {string} modalContainer ID do modal que contém o select2
 * @param {number} caracterMinimo Mínimo de caracteres para efetuar a busca
 * @param {function} callbackParams Função para informar parâmetros de busca
 * @param {boolean} multiple Permite selecionar mais de um registro
 */
function CriarSelect2Dinamico(id, url, placeholder, modalContainer, caracterMinimo, callbackParams, multiple) {
    let select = $("#" + id);

    if (select.data("select2")) {
        select.select2("destroy");
    }

    select.select2({
        dropdownParent: modalContainer ? $("#" + modalContainer) : $("body"),
        allowClear: true,
        multiple: multiple,
        placeholder: placeholder,
        minimumInputLength: caracterMinimo,
        language: {
            inputTooShort: function (args) {
                let len = args.minimum - args.input.length;
                if (len > 1)
                    return "Digite mais " + len + " caracteres...";
                else
                    return "Digite mais " + len + " caracter...";
            },
            noResults: function () {
                return "Nenhum resultado encontrado.";
            }
        },
        ajax: {
            url: url,
            dataType: "json",
            quietMillis: 300,
            delay: 0,
            data: function (term) {
                return callbackParams(term);
            },
            processResults: function (data) {
                let resultado = [];
                if (data != null) {
                    $.each(data, function (index, item) {
                        resultado.push({
                            id: item.Value,
                            text: item.Text,
                            title: ''
                        });
                    });
                }
                return {
                    results: resultado
                };
            },
            cache: false
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        width: "100%"
    });

}

/**
 * Preenche select2 dinâmico com valores informados
 * @param {string} id ID do select2 selecionado
 * @param {string} valor Valor da opção definida
 * @param {string} texto Texto da opção definida
 */
function DefinirValorSelect2Dinamico(id, valor, texto) {
    $("#" + id).html("<option value='" + valor + "'>" + texto + "</option>");
    $("#" + id).val(valor).trigger("change");
}

/**
 * Cria select2 normal
 * @param {string} id ID do select2 desejado
 * @param {string} placeholder Placeholder para o select2
 * @param {string} modalContainer ID do modal que contém o select2
 * @param {boolean} multiple Permite selecionar mais de um registro
 */
function CriarSelect2(id, placeholder, modalContainer, multiple) {
    let select = $("#" + id);

    if (select.data("select2")) {
        select.select2("destroy");
    }

    select.select2({
        dropdownParent: modalContainer ? $("#" + modalContainer) : $("body"),
        allowClear: true,
        multiple: multiple,
        placeholder: placeholder,
        escapeMarkup: function (markup) {
            return markup;
        },
        language: {
            noResults: function () {
                return "Nenhum resultado encontrado.";
            }
        },
        width: "100%"
    });
}

/**
 * Faz um PadLeft no texto informado
 * @param {string} texto Texto para acrescentar o pad
 * @param {char} padCaracter Pad a ser acrescentado
 * @param {number} tamanho Tamanho total do texto
 * @returns {string} Texto com o pad informado
 */
function PadLeft(texto, padCaracter, tamanho) {
    let caracterePad = "";
    for (let i = 0; i < tamanho; i++) {
        caracterePad += padCaracter;
    }
    return caracterePad.substring(0, caracterePad.length - texto.length) + texto;
}

/**
 * Obtém o tipo de mensagem pelo código
 * @param {number} tipo Código do tipo de mensagem
 * @returns {string} Nome do tipo de mensagem
 */
function TipoMensagem(tipo) {
    switch (tipo) {
        case 2:
            return "aviso";
        case 3:
            return "sucesso";
        case 4:
            return "erro";
        default:
            return "info";
    }
}

/**
 * Verifica se o usuário possui permissão
 * @param {string} controller Nome do controller da função
 * @param {string} action Nome da action da função
 * @returns {boolean} Se o usuário possui permissão ou não
 */
function PossuiPermissao(controller, action) {
    let permissao = false;

    let parametros = {
        controller: controller,
        action: action
    };
    let sucesso = function (data) {
        if (data)
            permissao = data;
    };

    RequisicaoAjax(urlPossuiPermissao, "json", parametros, "POST", false, false, sucesso);

    return permissao;
}

/**
 * Converte data JSON para data
 * @param {string} dataJson Data no formato JSON
 * @returns {string} Data no formato brasileiro
 */
function DateJsonToDateString(dataJson) {
    if (dataJson && dataJson != "/Date(-62135589600000)/") {
        let data = new Date(parseInt(dataJson.substr(6)));
        return DateToString(data);
    }
    else
        return "";
}

/**
 * Converte data JSON para data com tempo
 * @param {string} dataJson Data no formato JSON
 * @returns {string} Data com tempo no formato brasileiro
 */
function DateJsonToDateTimeString(dataJson) {
    if (dataJson) {
        let data = new Date(parseInt(dataJson.substr(6)));
        return DateTimeToString(data);
    }
    else
        return "";
}

/**
 * Converte data texto para data JS
 * @param {Date} data Data no formato brasileiro
 * @returns {string} Data no formato JS
 */
function DateStringToDate(data) {
    if (data) {
        let partData = data.split("/");
        let dia = partData[0];
        let mes = partData[1];
        let ano = partData[2];
        let compData = mes + "/" + dia + "/" + ano;

        return new Date(compData);
    }
    else
        return null;
}

/**
 * Converte data JS para data texto
 * @param {Date} data Data no formato do JS
 * @returns {string} Data no formato brasileiro
 */
function DateToString(data) {
    if (data) {
        let dia = data.getDate();
        if (dia.toString().length == 1)
            dia = "0" + dia;
        let mes = data.getMonth() + 1;
        if (mes.toString().length == 1)
            mes = "0" + mes;
        let ano = data.getFullYear();
        return dia + "/" + mes + "/" + ano;
    }
    else
        return "";
}

/**
 * Converte data JS para data texto com tempo
 * @param {Date} data Data no formato do JS
 * @returns {string} Data com tempo no formato brasileiro
 */
function DateTimeToString(data) {
    if (data) {
        let dia = data.getDate();
        if (dia.toString().length == 1)
            dia = "0" + dia;
        let mes = data.getMonth() + 1;
        if (mes.toString().length == 1)
            mes = "0" + mes;
        let ano = data.getFullYear();
        let hora = data.getHours();
        if (hora.toString().length == 1)
            hora = "0" + hora;
        let minutos = data.getMinutes();
        if (minutos.toString().length == 1)
            minutos = "0" + minutos;
        let segundos = data.getSeconds();
        if (segundos.toString().length == 1)
            segundos = "0" + segundos;
        return dia + "/" + mes + "/" + ano + " " + hora + ":" + minutos + ":" + segundos;
    }
    else
        return "";
}

/**
 * Cria menu para executar ações da página
 * @param {object} botoes Botões para executar cada ação desejada
 * @param {function} callbackOpen Função para ser executada ao abrir menu
 * @param {function} callbackClose Função para ser executada ao fechar menu
 */
function CriarMenuFlutuante(botoes, callbackOpen, callbackClose) {
    new Fab({
        selector: "#menu-action",
        button: "large btn-themecolor",
        icon: "mdi mdi-plus",
        position: "bottom-right",
        direction: "vertical",
        buttons: botoes,//[
        //    {
        //        button: "medium red",
        //        label: "Excluir",
        //        icon: "mdi mdi-delete",
        //        onClick: function () {
        //            console.log("delete");
        //        }
        //    },
        //    {
        //        button: "medium green",
        //        label: "Editar",
        //        icon: "mdi mdi-pencil",
        //        onClick: function () {
        //            console.log("pencil");
        //        }
        //    },
        //    {
        //        button: "medium blue",
        //        label: "Incluir",
        //        icon: "mdi mdi-plus",
        //        onClick: function () {
        //            console.log("plus");
        //        }
        //    }
        //],
        onOpen: function () {
            if (callbackOpen)
                callbackOpen();
        },
        onClose: function () {
            if (callbackClose)
                callbackClose();
        }
    });
}

/**
 * Valida se existe alguma linha selecionada na tabela
 * @param {string} idTabela ID da tabela para fazer a validação
 * @returns {boolean} Resultado da validação
 */
function ValidarLinhaSelecionada(idTabela) {
    let tabela = $("#" + idTabela).DataTable();

    if (tabela.rows('.selected').data().length > 0)
        return true;
    else
        NotificacaoAviso("Selecione um registro na tabela.");

    return false;
}

/**
 * Obtém o valor do campo na linha selecionada da tabela
 * @param {string} idTabela ID da tabela desejada
 * @param {string} nomeColuna Nome do campo da tabela para buscar informação
 * @returns {string} Informação existente no campo da tabela
 */
function ObterCampoTabela(idTabela, nomeColuna) {
    let tabela = $("#" + idTabela).DataTable();

    let campo = $.map(tabela.rows('.selected').data(), function (item) {
        if (nomeColuna.indexOf(".") >= 0) {
            let sp = nomeColuna.split(".");
            if (sp.length == 2)
                return item[sp[0]][sp[1]];
            if (sp.length == 3)
                return item[sp[0]][sp[1]][sp[2]];
            if (sp.length == 4)
                return item[sp[0]][sp[1]][sp[2]][sp[3]];
            else
                return null;
        }
        else
            return item[nomeColuna];
    });

    return campo ? campo[0] : null;
}

/**
 * Valida se data inicial é menor que data final
 * @param {string} idDataInicio ID do campo da data inicial
 * @param {string} idDataTermino ID do campo da data final
 * @returns {boolean} Resultado da validação
 */
function ValidarDataMaior(idDataInicio, idDataTermino) {
    let dataInicio = $("#" + idDataInicio).val();
    let dataTermino = $("#" + idDataTermino).val();

    if (dataInicio && dataTermino) {
        let inicio = DateStringToDate(dataInicio);
        let termino = DateStringToDate(dataTermino);

        if (inicio < termino)
            return true;
        else {
            NotificacaoAviso("Data final não pode ser menor que data inicial.");
            AdicionarClasseAviso(idDataTermino);
            $("#" + idDataTermino).val("");
        }
    }
    else {
        NotificacaoAviso("Informe os campos em destaque.");
        AdicionarClasseAviso(idDataInicio);
        AdicionarClasseAviso(idDataTermino);
    }

    return false;
}

/**
 * Formata valor como CNPJ ou CPF, dependendo da quantidade de dígitos
 * @param {string} valor Valor a ser formatado
 * @returns {string} CNPJ ou CPF formatado
 */
function FormatarCnpjCpf(valor) {
    if (valor.length == 14)
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    else if (valor.length == 11)
        return valor.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "\$1.\$2.\$3\-\$4");
    else
        return "";
}

/**
 * Formata valor como dinheiro (formato brasileiro)
 * @param {number} valor Valor a ser formatado
 * @param {boolean} moeda Exibir prefixo de moeda?
 * @returns {string} Valor no formato brasileiro de dinheiro
 */
function FormatarDinheiro(valor, moeda) {
    if (moeda)
        return valor.toLocaleString("pt-br", { style: "currency", currency: "BRL" });
    else
        return valor.toLocaleString("pt-br", { minimumFractionDigits: 2 });
}

/**
 * Abre nova janela passando parâmetros via POST
 * @param {string} url URL destino da requisição
 * @param {object} data Valor a serem passados na requisição
 */
function AbrirJanelaPost(url, data) {
    var form = document.createElement("form");
    form.target = "_blank";
    form.method = "POST";
    form.action = url;
    form.style.display = "none";

    for (var key in data) {
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = key;
        input.value = data[key];
        form.appendChild(input);
    }

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

/**
 * Marca notificações como lida
 * @param {number} idNotificacao Identificador da notificação
 */
function MarcarNotificacao(idNotificacao) {
    var parametros = {
        idNotificacao: idNotificacao
    };
    var sucesso = function (data) {
        $("#centralNotificacoes").html(data);
        $(".message-center").slimScroll({
            position: "right",
            size: "5px",
            color: "#dcdcdc"
        });
    };

    RequisicaoAjax(urlMarcarNotificacao, "json", parametros, "POST", true, false, sucesso);
}
