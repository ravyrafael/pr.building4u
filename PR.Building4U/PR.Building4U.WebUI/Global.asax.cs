﻿using AutoMapper;
using PR.Building4U.Mapeamentos;
using PR.Building4U.SDK.InversaoControle;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PR.Building4U.WebUI
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Mapper.Initialize(cfg => cfg.CreateMissingTypeMaps = true);

            ResolvedorDeDependencias.Instance().CarregarMapeamentos(Map.ListaMapeamentos());
        }
    }
}
