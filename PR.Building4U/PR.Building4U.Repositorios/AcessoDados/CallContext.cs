﻿using System;
using System.Data;

namespace PR.Building4U.Repositorios.AcessoDados
{
    public class CallContext : IDisposable
    {
        [ThreadStatic]
        private static CallContext _instance;
        [ThreadStatic]
        private static int _instanceCounter;

        public Action<IDbConnection, string, object> HistoryCallback { get; set; }
        public Attribute CallerMetadata { get; set; }

        public static CallContext Instance
        {
            get
            {
                if (_instanceCounter++ == 0)
                {
                    _instance = new CallContext();
                }
                return _instance;
            }
        }

        private static void Release()
        {
            if (--_instanceCounter == 0)
            {
                _instance = null;
            }
        }

        public void Dispose()
        {
            Release();
        }
    }
}
