﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Building4U.Repositorios.AcessoDados
{
    public class DbAccessHelper : IDisposable
    {
        protected readonly string _nomeConexao;

        public DbAccessHelper(string nomeConexao)
        {
            _nomeConexao = nomeConexao;
        }

        public void Dispose()
        {
            // Method intentionally left empty.
        }

        #region "Métodos Query"

        public IEnumerable<T> Query<T>(string sql)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<T>(conexao, sql, null, null, true, null, null);
            }
        }

        public IEnumerable<T> Query<T>(string sql, object param)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<T>(conexao, sql, param, null, true, null, null);
            }
        }

        public IEnumerable<IDictionary<string, object>> Query(string sql, object param)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<IDictionary<string, object>>(conexao, sql, param, null, true, null, null);
            }
        }

        public IEnumerable<IDictionary<string, object>> Query(string sql, object param, IDbTransaction transaction)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<IDictionary<string, object>>(conexao, sql, param, transaction, true, null, null);
            }
        }

        public IEnumerable<IDictionary<string, object>> Query(string sql, object param, CommandType? commandType)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<IDictionary<string, object>>(conexao, sql, param, null, true, null, commandType);
            }
        }

        public IEnumerable<IDictionary<string, object>> Query(string sql, object param, IDbTransaction transaction, CommandType? commandType)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<IDictionary<string, object>>(conexao, sql, param, transaction, true, null, commandType);
            }
        }

        public IEnumerable<IDictionary<string, object>> Query(string sql, object param, IDbTransaction transaction, bool buffered, int? commandTimeout, CommandType? commandType)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<IDictionary<string, object>>(conexao, sql, param, transaction, buffered, commandTimeout, commandType);
            }
        }

        public IEnumerable<TReturn> Query<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<TFirst, TSecond, TReturn>(conexao, sql, map, param as object, transaction, buffered, splitOn, commandTimeout, commandType);
            }
        }

        public IEnumerable<TReturn> Query2<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map, string splitOn)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<TFirst, TSecond, TReturn>(conexao, sql, map, null, null, true, splitOn, null, null);
            }
        }

        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TReturn>(string sql, Func<TFirst, TSecond, TThird, TReturn> map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<TFirst, TSecond, TThird, TReturn>(conexao, sql, map, param as object, transaction, buffered, splitOn, commandTimeout, commandType);
            }
        }

        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<TFirst, TSecond, TThird, TFourth, TReturn>(conexao, sql, map, param as object, transaction, buffered, splitOn, commandTimeout, commandType);
            }
        }

        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(conexao, sql, map, param as object, transaction, buffered, splitOn, commandTimeout, commandType);
            }
        }

        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(conexao, sql, map, param as object, transaction, buffered, splitOn, commandTimeout, commandType);
            }
        }

        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                return Dapper.SqlMapper.Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(conexao, sql, map, param as object, transaction, buffered, splitOn, commandTimeout, commandType);
            }
        }

        #endregion

        #region "Métodos Execute"

        public int Execute(string sql, object parametro, IDbTransaction transaction, int? commandTimeout, CommandType? commandType)
        {
            using (var conexao = ConnectionFactory.Instance.GetIDbConnection(_nomeConexao))
            {
                using (var contexto = CallContext.Instance)
                {
                    if (contexto.CallerMetadata != null)
                    {
                        contexto.HistoryCallback(conexao, sql, parametro);
                    }
                }
                return Dapper.SqlMapper.Execute(conexao, sql, parametro, transaction, commandTimeout, commandType);
            }
        }

        public int Execute(string sql, object parametro)
        {
            return Execute(sql, parametro, null, null, null);
        }

        public int Execute(string sql, object parametro, CommandType tipoComando)
        {
            return Execute(sql, parametro, null, null, tipoComando);
        }
        
        #endregion
    }
}
