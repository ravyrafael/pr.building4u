﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace PR.Building4U.Repositorios.AcessoDados
{
    public class ConnectionFactory : IDisposable
    {
        private IDbConnection _connection;

        private bool InTransaction { get; set; }

        [ThreadStatic]
        private static volatile ConnectionFactory _instance;
        private static readonly object SYNC_ROOT = new object();

        private ConnectionFactory()
        {
            _connection = null;
        }

        public static ConnectionFactory Instance
        {
            get
            {
                if (_instance != null) return _instance;

                lock (SYNC_ROOT)
                {
                    if (_instance == null)
                        _instance = new ConnectionFactory();
                }

                return _instance;
            }
        }

        public DbAccessHelper GetConnection(string name)
        {
            return new DbAccessHelper(name);
        }

        internal IDbConnection GetIDbConnection(string name)
        {
            var configuration = ConfigurationManager.ConnectionStrings[name];

            if (configuration == null)
                throw new ConfigurationErrorsException(string.Format("Não foi possível criar uma conexão para a connectionstring ( {0} )", name));

            return CreateConnection(configuration);
        }

        private static IDbConnection CreateConnection(ConnectionStringSettings configuration)
        {
            var factory = DbProviderFactories.GetFactory(configuration.ProviderName);
            var connection = factory.CreateConnection();

            if (connection == null)
                throw new ConfigurationErrorsException(string.Format("Não foi possível criar uma conexão para a connectionstring ( {0} )", configuration.Name));

            connection.ConnectionString = configuration.ConnectionString;

            return connection;
        }

        public void Dispose()
        {
            if (_connection == null) return;

            if (_connection.State != ConnectionState.Closed)
                _connection.Close();

            _connection.Dispose();
            _connection = null;
        }

        public void Begin()
        {
            InTransaction = true;
        }

        public void End()
        {
            InTransaction = false;
        }
    }
}
