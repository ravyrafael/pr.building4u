using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using System;

namespace PR.Building4U.Repositorios
{
    public class ObraRepositorio : IObraRepositorio
    {
        public IEnumerable<Obra> ListarObras(string codigoOuNome, bool somenteAtivas, bool somenteObras)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeObra.ListarObras;
                var parametros = new DynamicParameters();

                if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeObra.ListarObrasFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                if (somenteAtivas)
                    sql += ScriptsDeObra.ListarObrasAtivas;

                if (somenteObras)
                    sql += ScriptsDeObra.ListarObrasObras;

                return connection.Query<RObra, RSituacao, RArea, ROrdemServico, RContrato, RCliente, RObra>(sql,
                    (obra, situacao, area, ordemServico, contrato, cliente) =>
                    {
                        obra.Situacao = situacao;
                        obra.OrdemServico = ordemServico;
                        if (contrato != null)
                            contrato.Cliente = cliente;
                        obra.Contrato = contrato;
                        obra.Area = area;
                        return obra;
                    }, parametros, splitOn: "IdObra, IdSituacao, IdArea, IdOrdemServico, IdContrato, IdCliente");
            }
        }

        public Obra ObterObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Query<RObra, RSituacao, RArea, ROrdemServico, RContrato, RCliente, RObra>(ScriptsDeObra.ObterObra,
                    (obra, situacao, area, ordemServico, contrato, cliente) =>
                    {
                        obra.Situacao = situacao;
                        obra.OrdemServico = ordemServico;
                        if (contrato != null)
                            contrato.Cliente = cliente;
                        obra.Contrato = contrato;
                        obra.Area = area;
                        return obra;
                    }, parametros, splitOn: "IdObra, IdSituacao, IdArea, IdOrdemServico, IdContrato, IdCliente").FirstOrDefault();
            }
        }

        public int IncluirObra(int idArea, int? idContrato, string codigoObra, string nomeObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdArea", idArea, DbType.Int32);
                parametros.Add("IdContrato", idContrato, DbType.Int32);
                parametros.Add("CodigoObra", codigoObra, DbType.AnsiString);
                parametros.Add("NomeObra", nomeObra, DbType.AnsiString);

                return connection.Query<int>(ScriptsDeObra.IncluirObra, parametros).FirstOrDefault();
            }
        }

        public int EditarObra(int idObra, int idArea, int? idContrato, string codigoObra, string nomeObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("IdArea", idArea, DbType.Int32);
                parametros.Add("IdContrato", idContrato, DbType.Int32);
                parametros.Add("CodigoObra", codigoObra, DbType.AnsiString);
                parametros.Add("NomeObra", nomeObra, DbType.AnsiString);

                return connection.Execute(ScriptsDeObra.EditarObra, parametros);
            }
        }

        public int ExcluirObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeObra.ExcluirObra, parametros);
            }
        }

        public int IncluirOrdemServico(int idObra, DateTime? dataOrdemServico, int? prazoOrdemServico)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("DataOrdemServico", dataOrdemServico, DbType.DateTime);
                parametros.Add("PrazoOrdemServico", prazoOrdemServico, DbType.Int32);

                return connection.Query<int>(ScriptsDeObra.IncluirOrdemServico, parametros).FirstOrDefault();
            }
        }

        public int EditarOrdemServico(int idOrdemServico, DateTime? dataOrdemServico, int? prazoOrdemServico)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdOrdemServico", idOrdemServico, DbType.Int32);
                parametros.Add("DataOrdemServico", dataOrdemServico, DbType.DateTime);
                parametros.Add("PrazoOrdemServico", prazoOrdemServico, DbType.Int32);

                return connection.Execute(ScriptsDeObra.EditarOrdemServico, parametros);
            }
        }

        public int ExcluirOrdemServico(int idOrdemServico)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdOrdemServico", idOrdemServico, DbType.Int32);

                return connection.Execute(ScriptsDeObra.ExcluirOrdemServico, parametros);
            }
        }
    }
}