using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;

namespace PR.Building4U.Repositorios
{
	public class GerenciaRepositorio : IGerenciaRepositorio
	{
		public IEnumerable<Gerencia> ListarGerencias(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
				var sql = ScriptsDeGerencia.ListarGerencias;
                var parametros = new DynamicParameters();

				if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeGerencia.ListarGerenciasFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RGerencia>(sql, parametros);
            }
        }

		public int IncluirGerencia(string nomeGerencia, string siglaGerencia)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("NomeGerencia", nomeGerencia, DbType.AnsiString);
                parametros.Add("SiglaGerencia", siglaGerencia, DbType.AnsiString);

                return connection.Query<int>(ScriptsDeGerencia.IncluirGerencia, parametros).FirstOrDefault();
            }
        }

		public int ExcluirGerencia(int idGerencia)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdGerencia", idGerencia, DbType.Int32);

                return connection.Execute(ScriptsDeGerencia.ExcluirGerencia, parametros);
            }
        }
	}
}