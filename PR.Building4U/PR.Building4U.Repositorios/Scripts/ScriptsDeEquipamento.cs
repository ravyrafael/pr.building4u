namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeEquipamento
	{
		public const string ListarEquipamentos = @"
			SELECT
	            e.id AS IdEquipamento,
	            e.marca_equipamento AS MarcaEquipamento,
	            e.modelo_equipamento AS ModeloEquipamento,
	            e.patrimonio_equipamento AS PatrimonioEquipamento,
	            e.indicador_terceiro AS IndicadorTerceiro,
	            e.fornecedor_equipamento AS FornecedorEquipamento,
                e.aquisicao_equipamento AS AquisicaoEquipamento,
                e.sucata_equipamento AS SucataEquipamento,
	            t.id AS IdTipoEquipamento,
	            t.nome_tipo AS NomeTipoEquipamento,
	            s.id AS IdSituacao,
	            s.sigla_situacao AS SiglaSituacao,
	            s.nome_situacao AS NomeSituacao,
	            s.tipo_situacao AS TipoSituacao
            FROM equipamento e
	            LEFT JOIN tipo_equipamento t ON t.id = e.tipo_equipamento_id
	            LEFT JOIN situacao s ON s.id = e.situacao_id";

		public const string ListarEquipamentosFiltro = @"
			WHERE (e.patrimonio_equipamento LIKE @CodigoOuNome
                OR upper(e.marca_equipamento) LIKE upper(@CodigoOuNome + '%')
                OR upper(e.modelo_equipamento) LIKE upper(@CodigoOuNome + '%'))";

        public const string ObterEquipamento = @"
			SELECT
	            e.id AS IdEquipamento,
	            e.marca_equipamento AS MarcaEquipamento,
	            e.modelo_equipamento AS ModeloEquipamento,
	            e.patrimonio_equipamento AS PatrimonioEquipamento,
	            e.indicador_terceiro AS IndicadorTerceiro,
	            e.fornecedor_equipamento AS FornecedorEquipamento,
                e.aquisicao_equipamento AS AquisicaoEquipamento,
                e.sucata_equipamento AS SucataEquipamento,
	            t.id AS IdTipoEquipamento,
	            t.nome_tipo AS NomeTipoEquipamento,
	            s.id AS IdSituacao,
	            s.sigla_situacao AS SiglaSituacao,
	            s.nome_situacao AS NomeSituacao,
	            s.tipo_situacao AS TipoSituacao
            FROM equipamento e
	            LEFT JOIN tipo_equipamento t ON t.id = e.tipo_equipamento_id
	            LEFT JOIN situacao s ON s.id = e.situacao_id
            WHERE e.id = @IdEquipamento";

		public const string IncluirEquipamento = @"
			INSERT INTO equipamento (tipo_equipamento_id, situacao_id, marca_equipamento, patrimonio_equipamento, modelo_equipamento, indicador_terceiro, fornecedor_equipamento, aquisicao_equipamento, sucata_equipamento)
            OUTPUT Inserted.ID
            VALUES (@IdTipoEquipamento, 9, @MarcaEquipamento, @PatrimonioEquipamento, @ModeloEquipamento, @IndicadorTerceiro, @FornecedorEquipamento, @AquisicaoEquipamento, NULL)";

		public const string EditarEquipamento = @"
			UPDATE equipamento
            SET marca_equipamento = @MarcaEquipamento,
	            modelo_equipamento = @ModeloEquipamento,
	            fornecedor_equipamento = @FornecedorEquipamento
            WHERE id = @IdEquipamento";

		public const string SucatearEquipamento = @"
			UPDATE equipamento
            SET sucata_equipamento = @SucataEquipamento,
                situacao_id = 10
            WHERE id = @IdEquipamento";

        public const string IniciarManutencaoEquipamento = @"
            INSERT INTO manutencao (equipamento_id, inicio_manutencao, termino_manutencao, sequencia_manutencao)
            VALUES (@IdEquipamento, @InicioManutencao, @TerminoManutencao, (SELECT isnull(max(sequencia_manutencao), 0) + 1 FROM manutencao WHERE equipamento_id = @IdEquipamento))";

        public const string TerminarManutencaoEquipamento = @"
            UPDATE manutencao
            SET termino_manutencao = @TerminoManutencao
            WHERE equipamento_id = @IdEquipamento
	            AND sequencia_manutencao = (SELECT max(sequencia_manutencao) FROM manutencao WHERE equipamento_id = @IdEquipamento)";

        public const string AlterarSituacaoEquipamento = @"
            UPDATE equipamento
            SET situacao_id = @SituacaoEquipamento
            WHERE id = @IdEquipamento";

        public const string ObterManutencaoEquipamento = @"
            SELECT
	            m.id AS IdManutencao,
	            m.inicio_manutencao AS InicioManutencao,
	            m.termino_manutencao AS TerminoManutencao,
	            m.sequencia_manutencao AS SequenciaManutencao,
	            e.id AS IdEquipamento,
	            e.marca_equipamento AS MarcaEquipamento,
	            e.modelo_equipamento AS ModeloEquipamento,
	            e.patrimonio_equipamento AS PatrimonioEquipamento
            FROM manutencao m
	            LEFT JOIN equipamento e ON e.id = m.equipamento_id
            WHERE m.equipamento_id = @IdEquipamento
	            AND m.sequencia_manutencao = (SELECT max(sequencia_manutencao) FROM manutencao WHERE equipamento_id = @IdEquipamento)";
    }
}