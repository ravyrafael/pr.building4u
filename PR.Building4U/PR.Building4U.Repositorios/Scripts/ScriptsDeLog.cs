﻿namespace PR.Building4U.Repositorios.Scripts
{
    public static class ScriptsDeLog
    {
        public const string IncluirLog = @"
            INSERT INTO log_table(usuario_id, login_usuario, controller_recurso, action_recurso, descricao_log, data_log, tipo_log)
            VALUES (@IdUsuario, @LoginUsuario, @Controller, @Action, @Descricao, @DataLog, @TipoLog)";
    }
}
