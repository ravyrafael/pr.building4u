namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeCronograma
	{
		public const string ListarCronogramas = @"
			SELECT 
	            c.id AS IdCronograma,
	            c.ordem_cronograma AS OrdemCronograma,
	            c.nome_cronograma AS NomeCronograma,
	            c.medicao_cronograma AS DiaMedicaoCronograma,
	            c.inicio_cronograma AS InicioCronograma,
	            c.termino_cronograma AS TerminoCronograma,
	            c.atualizacao_cronograma AS AtualizacaoCronograma,
	            o.id AS IdObra,
	            o.codigo_obra AS CodigoObra,
	            o.nome_obra AS NomeObra
            FROM cronograma c
	            LEFT JOIN obra o ON o.id = c.obra_id
            WHERE c.obra_id = @IdObra";

		public const string ObterCronograma = @"
			SELECT 
	            c.id AS IdCronograma,
	            c.ordem_cronograma AS OrdemCronograma,
	            c.nome_cronograma AS NomeCronograma,
	            c.medicao_cronograma AS DiaMedicaoCronograma,
	            c.inicio_cronograma AS InicioCronograma,
	            c.termino_cronograma AS TerminoCronograma,
	            c.atualizacao_cronograma AS AtualizacaoCronograma,
	            o.id AS IdObra,
	            o.codigo_obra AS CodigoObra,
	            o.nome_obra AS NomeObra
            FROM cronograma c
	            LEFT JOIN obra o ON o.id = c.obra_id
            WHERE c.id = @IdCronograma";

		public const string IncluirCronograma = @"
			INSERT INTO cronograma (obra_id, ordem_cronograma, nome_cronograma, medicao_cronograma)
            OUTPUT Inserted.ID
            VALUES (@IdObra, @OrdemCronograma, @NomeCronograma, @DiaMedicaoCronograma)";

		public const string EditarCronograma = @"
			UPDATE cronograma
            SET nome_cronograma = @NomeCronograma,
	            medicao_cronograma = @DiaMedicaoCronograma
            WHERE id = @IdCronograma";

        public const string ListarServicos = @"
            SELECT 
	            c.id AS IdCronogramaServico,
	            c.cronograma_id AS IdCronograma,
	            c.ordem_cron_servico AS OrdemServico,
	            c.nome_cron_servico AS NomeServico,
	            c.quantidade_total AS QuantidadeTotal,
	            c.quantidade_executada AS QuantidadeExecutada,
	            c.quantidade_real AS QuantidadeReal,
	            c.media_equipe AS MediaEquipe,
	            c.media_pes AS MediaPes,
	            c.media_pes_real AS MediaPesReal,
	            c.valor_total AS ValorServico,
	            c.duracao_total AS DuracaoTotal,
	            c.duracao_falta AS DuracaoFalta,
	            c.inicio_servico AS InicioServico,
	            c.termino_servico AS TerminoServico,
	            c.atualizacao_servico AS AtualizacaoServico,
	            c.id_cronograma_predecessora AS IdCronogramaPredecessora,
	            c.id_servico_predecessora AS IdServicoPredecessora,
	            c.tipo_predecessora AS TipoPredecessora,
	            c.atraso_predecessora AS AtrasoPredecessora,
	            c.chance_faturamento AS ChanceFaturamento,
	            s.id AS IdServico,
	            s.codigo_servico AS CodigoServico,
	            s.sigla_servico AS SiglaServico,
	            s.nome_servico AS NomeServico,
	            u.id AS IdUnidade,
	            u.sigla_unidade AS SiglaUnidade,
	            u.nome_unidade AS NomeUnidade
            FROM cronograma_servico c
	            LEFT JOIN servico s ON s.id = c.servico_id
	            LEFT JOIN unidade u ON u.id = s.unidade_id
            WHERE c.cronograma_id = @IdCronograma";

        public const string ListarMedicoes = @"
            SELECT 
	            id AS IdCronogramaServicoMedicao,
	            cronograma_servico_id AS IdCronogramaServico,
	            mes_referencia AS MesReferencia,
	            inicio_medicao AS InicioMedicao,
	            termino_medicao AS TerminoMedicao,
	            faturamento_medicao AS Faturamento,
	            quantidade_medicao AS Quantidade,
	            pes_medicao AS PesMedicao,
	            equipe_medicao AS EquipeMedicao,
	            tem_medicao AS TemMedicao
            FROM cronograma_servico_medicao
            WHERE cronograma_servico_id = @IdCronogramaServico";

        public const string IncluirMedicao = @"
            INSERT INTO cronograma_servico_medicao (cronograma_servico_id, mes_referencia, inicio_medicao, termino_medicao, faturamento_medicao, quantidade_medicao, pes_medicao, equipe_medicao, tem_medicao)
            OUTPUT Inserted.ID
            VALUES (@IdCronogramaServico, @MesReferencia, @InicioMedicao, @TerminoMedicao, @Faturamento, @Quantidade, @PesMedicao, @EquipeMedicao, @TemMedicao)";

        public const string AtualizarMedicao = @"
            UPDATE cronograma_servico_medicao
            SET inicio_medicao = @InicioMedicao,
	            termino_medicao = @TerminoMedicao,
	            faturamento_medicao = @Faturamento,
	            quantidade_medicao = @Quantidade,
	            pes_medicao = @PesMedicao,
	            equipe_medicao = @EquipeMedicao,
	            tem_medicao = @TemMedicao
            WHERE id = @IdCronogramaServicoMedicao";

        public const string ExcluirMedicao = @"
            DELETE FROM cronograma_servico_medicao
            WHERE id = @IdCronogramaServicoMedicao";

        public const string IncluirServico = @"
            INSERT INTO cronograma_servico (cronograma_id, servico_id, ordem_cron_servico, nome_cron_servico, quantidade_total, quantidade_executada, quantidade_real, media_equipe, media_pes, media_pes_real, valor_total, duracao_total, duracao_falta, inicio_servico, termino_servico, atualizacao_servico, id_cronograma_predecessora, id_servico_predecessora, tipo_predecessora, atraso_predecessora, chance_faturamento)
            OUTPUT Inserted.ID
            VALUES (@IdCronograma, @IdServico, @OrdemServico, @NomeServico, @QuantidadeTotal, @QuantidadeExecutada, @QuantidadeReal, @MediaEquipe, @MediaPes, @MediaPesReal, @ValorServico, @DuracaoTotal, @DuracaoFalta, @InicioServico, @TerminoServico, @AtualizacaoServico, @IdCronogramaPredecessora, @IdServicoPredecessora, @TipoPredecessora, @AtrasoPredecessora, @ChanceFaturamento)";

        public const string AtualizarServico = @"
            UPDATE cronograma_servico
            SET ordem_cron_servico = @OrdemServico,
	            nome_cron_servico = @NomeServico,
	            quantidade_total = @QuantidadeTotal,
	            quantidade_executada = @QuantidadeExecutada,
	            quantidade_real = @QuantidadeReal,
	            media_equipe = @MediaEquipe,
	            media_pes = @MediaPes,
	            media_pes_real = @MediaPesReal,
	            valor_total = @ValorServico,
	            duracao_total = @DuracaoTotal,
	            duracao_falta = @DuracaoFalta,
	            inicio_servico = @InicioServico,
	            termino_servico = @TerminoServico,
	            atualizacao_servico = @AtualizacaoServico,
	            id_cronograma_predecessora = @IdCronogramaPredecessora,
	            id_servico_predecessora = @IdServicoPredecessora,
	            tipo_predecessora = @TipoPredecessora,
	            atraso_predecessora = @AtrasoPredecessora,
	            chance_faturamento = @ChanceFaturamento
            WHERE id = @IdCronogramaServico";

        public const string ExcluirServicoMedicao = @"
            DELETE FROM cronograma_servico_medicao
            WHERE cronograma_servico_id = @IdCronogramaServico";

        public const string ExcluirServicoProducao = @"
            DELETE FROM producao
            WHERE cronograma_servico_id = @IdCronogramaServico";

        public const string ExcluirServico = @"
            DELETE FROM cronograma_servico
            WHERE id = @IdCronogramaServico";

        public const string AtualizarCronograma = @"
            UPDATE cronograma
            SET ordem_cronograma = @OrdemCronograma,
	            inicio_cronograma = @InicioCronograma,
	            termino_cronograma = @TerminoCronograma,
	            atualizacao_cronograma = @AtualizacaoCronograma
            WHERE id = @IdCronograma";
    }
}