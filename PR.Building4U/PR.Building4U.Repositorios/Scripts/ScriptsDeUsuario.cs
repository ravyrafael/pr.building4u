﻿namespace PR.Building4U.Repositorios.Scripts
{
    public static class ScriptsDeUsuario
    {
        public const string AutenticarUsuario = @"
            SELECT u.id
            FROM usuario u
                LEFT JOIN funcionario f ON f.id = u.funcionario_id
            WHERE u.login_usuario = @LoginUsuario
                AND u.senha_usuario = @SenhaUsuario
                AND f.demissao_funcionario IS NULL";

        public const string ObterUsuarioPorLogin = @"
            SELECT 
	            u.id AS IdUsuario,
	            u.login_usuario AS LoginUsuario,
	            usu.senha_usuario AS SenhaUsuario,
                u.reduzido_usuario AS NomeReduzidoUsuario,
	            f.id AS IdFuncionario,
	            f.chapa_funcionario AS ChapaFuncionario,
	            f.nome_funcionario AS NomeFuncionario,
                f.email_funcionario AS EmailFuncionario,
	            s.id AS IdSituacao,
	            s.sigla_situacao AS SiglaSituacao,
	            s.nome_situacao AS NomeSituacao,
	            p.id AS IdPermissao,
	            p.sigla_permissao AS SiglaPermissao,
	            p.nome_permissao AS NomePermissao
            FROM usuario u
	            LEFT JOIN funcionario f ON f.id = u.funcionario_id
	            LEFT JOIN situacao s ON s.id = u.situacao_id
	            LEFT JOIN permissao p ON p.id = u.permissao_id
	            LEFT JOIN usuario usu ON usu.id = u.id
            WHERE u.login_usuario = @LoginUsuario";

        public const string VerificarPermissaoUsuario = @"
            SELECT count(1)
            FROM recurso_permissao
            WHERE permissao_id = @Permissao
	            AND recurso_id IN (
		            SELECT id
		            FROM recurso
		            WHERE controller_recurso = @Controller
			            AND action_recurso IN (@Actions))";
    }
}
