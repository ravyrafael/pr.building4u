namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeFuncionario
	{
		public const string ListarFuncionarios = @"
			SELECT 
	            f.id AS IdFuncionario,
	            f.chapa_funcionario AS ChapaFuncionario,
	            f.nome_funcionario AS NomeFuncionario,
	            f.admissao_funcionario AS AdmissaoFuncionario,
	            f.demissao_funcionario AS DemissaoFuncionario,
	            f.email_funcionario AS EmailFuncionario,
                f.telefone_funcionario AS TelefoneFuncionario,
	            c.id AS IdCargo,
	            c.nome_cargo AS NomeCargo,
	            c.sigla_cargo AS SiglaCargo,
	            s.id AS IdSituacao,
	            s.sigla_situacao AS SiglaSituacao,
	            s.nome_situacao AS NomeSituacao,
	            s.tipo_situacao AS TipoSituacao
            FROM funcionario f
	            LEFT JOIN cargo c ON c.id = f.cargo_id
	            LEFT JOIN situacao s ON s.id = f.situacao_id";

		public const string ListarFuncionariosFiltro = @"
			WHERE (f.chapa_funcionario LIKE @CodigoOuNome
                OR upper(f.nome_funcionario) LIKE upper(@CodigoOuNome + '%'))";

		public const string ObterFuncionario = @"
			SELECT 
	            f.id AS IdFuncionario,
	            f.chapa_funcionario AS ChapaFuncionario,
	            f.nome_funcionario AS NomeFuncionario,
	            f.admissao_funcionario AS AdmissaoFuncionario,
	            f.demissao_funcionario AS DemissaoFuncionario,
	            f.email_funcionario AS EmailFuncionario,
                f.telefone_funcionario AS TelefoneFuncionario,
	            c.id AS IdCargo,
	            c.nome_cargo AS NomeCargo,
	            c.sigla_cargo AS SiglaCargo,
	            s.id AS IdSituacao,
	            s.sigla_situacao AS SiglaSituacao,
	            s.nome_situacao AS NomeSituacao,
	            s.tipo_situacao AS TipoSituacao,
	            e.id AS IdEndereco,
	            e.logradouro_endereco AS LogradouroEndereco,
	            e.numero_endereco AS NumeroEndereco,
	            e.complemento_endereco AS ComplementoEndereco,
	            e.cep_endereco AS CepEndereco,
	            e.distrito_endereco AS DistritoEndereco,
	            m.id AS IdMunicipio,
                m.nome_municipio AS NomeMunicipio,
                m.codigo_municipio AS CodigoMunicipio,
                t.id AS IdEstado,
                t.sigla_estado AS SiglaEstado,
                t.nome_estado AS NomeEstado,
                p.id AS IdPais,
                p.codigo_pais AS CodigoPais,
                p.nome_pais AS NomePais
            FROM funcionario f
	            LEFT JOIN cargo c ON c.id = f.cargo_id
	            LEFT JOIN situacao s ON s.id = f.situacao_id
	            LEFT JOIN endereco e ON e.id = f.endereco_id
	            LEFT JOIN municipio m ON m.id = e.municipio_id
	            LEFT JOIN estado t ON t.id = m.estado_id
	            LEFT JOIN pais p ON p.id = t.pais_id
            WHERE f.id = @IdFuncionario";

		public const string IncluirFuncionario = @"
			INSERT INTO funcionario (chapa_funcionario, nome_funcionario, admissao_funcionario, email_funcionario , telefone_funcionario, endereco_id, situacao_id, cargo_id)
            OUTPUT Inserted.ID
            VALUES (@ChapaFuncionario, @NomeFuncionario, @AdmissaoFuncionario, @EmailFuncionario, @TelefoneFuncionario, @IdEndereco, 3, @IdCargo)";

		public const string EditarFuncionario = @"
			UPDATE funcionario
            SET nome_funcionario = @NomeFuncionario,
                email_funcionario = @EmailFuncionario,
                telefone_funcionario = @TelefoneFuncionario,
                cargo_id = @IdCargo
            WHERE id = @IdFuncionario";

		public const string DemitirFuncionario = @"
			UPDATE funcionario
            SET demissao_funcionario = @DemissaoFuncionario,
                situacao_id = 4
            WHERE id = @IdFuncionario";

        public const string IniciarFeriasFuncionario = @"
            INSERT INTO ferias(funcionario_id, inicio_ferias, termino_ferias, sequencia_ferias)
            VALUES (@IdFuncionario, @InicioFerias, @TerminoFerias, (SELECT isnull(max(sequencia_ferias), 0) + 1 FROM ferias WHERE funcionario_id = @IdFuncionario))";

        public const string TerminarFeriasFuncionario = @"
            UPDATE ferias
            SET termino_ferias = @TerminoFerias
            WHERE funcionario_id = @IdFuncionario
	            AND sequencia_ferias = (SELECT max(sequencia_ferias) FROM ferias WHERE funcionario_id = @IdFuncionario)";

        public const string AlterarSituacaoFuncionario = @"
            UPDATE funcionario
            SET situacao_id = @SituacaoFuncionario
            WHERE id = @IdFuncionario";

        public const string ObterFeriasFuncionario = @"
            SELECT
	            f.id AS IdFerias,
	            f.inicio_ferias AS InicioFerias,
	            f.termino_ferias AS TerminoFerias,
	            f.sequencia_ferias AS SequenciaFerias,
	            c.id AS IdFuncionario,
	            c.chapa_funcionario AS ChapaFuncionario,
	            c.nome_funcionario AS NomeFuncionario
            FROM ferias f
	            LEFT JOIN funcionario c ON c.id = f.funcionario_id
            WHERE f.funcionario_id = @IdFuncionario
	            AND f.sequencia_ferias = (SELECT max(sequencia_ferias) FROM ferias WHERE funcionario_id = @IdFuncionario)";
    }
}