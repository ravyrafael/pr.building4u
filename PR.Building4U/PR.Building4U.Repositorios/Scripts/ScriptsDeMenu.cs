﻿namespace PR.Building4U.Repositorios.Scripts
{
    public static class ScriptsDeMenu
    {
        public const string ObterMenuPorPermissao = @"
            SELECT
                m.id AS IdRecurso,
                m.nome_recurso AS NomeRecurso,
                m.icone_recurso AS IconeRecurso,
                m.controller_recurso AS ControllerRecurso,
                m.action_recurso AS ActionRecurso,
                m.parametro_recurso AS ParametrosRecurso,
                m.ehmenu_recurso AS EhMenuRecurso
            FROM recurso m
            WHERE m.id IN (
	            SELECT DISTINCT
		            m.menu_pai_id AS MenuPai
	            FROM recurso m
	                INNER JOIN recurso_permissao mp ON mp.recurso_id = m.id
	            WHERE mp.permissao_id = 1
	                AND m.ehmenu_recurso = 1)";

        public const string ObterSubMenuPorPermissao = @"
            SELECT
	            m.id AS IdRecurso,
	            m.nome_recurso AS NomeRecurso,
	            m.icone_recurso AS IconeRecurso,
	            m.controller_recurso AS ControllerRecurso,
	            m.action_recurso AS ActionRecurso,
	            m.parametro_recurso AS ParametrosRecurso,
                m.ehmenu_recurso AS EhMenuRecurso
            FROM recurso m
	            INNER JOIN recurso_permissao mp ON mp.recurso_id = m.id
            WHERE mp.permissao_id = @PermissaoUsuario
	            AND m.menu_pai_id = @MenuPai
                AND m.ehmenu_recurso = 1";
    }
}
