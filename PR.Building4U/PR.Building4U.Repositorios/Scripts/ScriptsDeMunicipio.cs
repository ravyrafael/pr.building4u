﻿namespace PR.Building4U.Repositorios.Scripts
{
    public static class ScriptsDeMunicipio
    {
        public const string ListarMunicipios = @"
            SELECT
	            m.id AS IdMunicipio,
	            m.nome_municipio AS NomeMunicipio,
	            m.codigo_municipio AS CodigoMunicipio,
	            e.id AS IdEstado,
	            e.sigla_estado AS SiglaEstado,
	            e.nome_estado AS NomeEstado,
	            p.id AS IdPais,
	            p.codigo_pais AS CodigoPais,
	            p.nome_pais AS NomePais
            FROM municipio m
	            LEFT JOIN estado e ON e.id = m.estado_id
	            LEFT JOIN pais p ON p.id = e.pais_id";

        public const string ListarMunicipiosFiltro = @"
             WHERE upper(m.nome_municipio) LIKE upper(@CodigoOuNome + '%')
	            OR convert(VARCHAR(50), m.codigo_municipio) LIKE @CodigoOuNome";

        public const string ObterMunicipio = @"
             SELECT
	            m.id AS IdMunicipio,
	            m.nome_municipio AS NomeMunicipio,
	            m.codigo_municipio AS CodigoMunicipio,
	            e.id AS IdEstado,
	            e.sigla_estado AS SiglaEstado,
	            e.nome_estado AS NomeEstado,
	            p.id AS IdPais,
	            p.codigo_pais AS CodigoPais,
	            p.nome_pais AS NomePais
            FROM municipio m
	            LEFT JOIN estado e ON e.id = m.estado_id
	            LEFT JOIN pais p ON p.id = e.pais_id
            WHERE m.id = @IdMunicipio";

        public const string IncluirMunicipio = @"
            INSERT INTO municipio (estado_id, nome_municipio, codigo_municipio)
            OUTPUT Inserted.ID
            VALUES (@IdEstado, @NomeMunicipio, @CodigoMunicipio)";

        public const string EditarMunicipio = @"
            UPDATE municipio
            SET estado_id = @IdEstado,
	            nome_municipio = @NomeMunicipio
            WHERE id = @IdMunicipio";

        public const string ExcluirMunicipio = @"
            DELETE FROM municipio
            WHERE id = @IdMunicipio";

        public const string ListarMunicipiosPorEstado = @"
            SELECT
	            m.id AS IdMunicipio,
	            m.nome_municipio AS NomeMunicipio,
	            m.codigo_municipio AS CodigoMunicipio,
	            e.id AS IdEstado,
	            e.sigla_estado AS SiglaEstado,
	            e.nome_estado AS NomeEstado,
	            p.id AS IdPais,
	            p.codigo_pais AS CodigoPais,
	            p.nome_pais AS NomePais
            FROM municipio m
	            LEFT JOIN estado e ON e.id = m.estado_id
	            LEFT JOIN pais p ON p.id = e.pais_id
            WHERE m.estado_id = @IdEstado";

        public const string ListarMunicipiosPorEstadoFiltro = @"
                AND (upper(m.nome_municipio) LIKE upper(@CodigoOuNome + '%')
	            OR convert(VARCHAR(50), m.codigo_municipio) LIKE @CodigoOuNome)";
    }
}
