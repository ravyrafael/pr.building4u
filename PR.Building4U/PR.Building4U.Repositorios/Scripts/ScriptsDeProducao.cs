namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeProducao
	{
		public const string ListarProducaos = @"
			SELECT
	            id AS IdProducao,
	            cronograma_servico_id AS IdCronogramaServico,
	            data_producao AS DataProducao,
	            quantidade_producao AS QuantidadeProducao,
	            equipe_producao AS EquipeProducao,
	            observacao_producao AS ObservacaoProducao
            FROM producao
            WHERE cronograma_servico_id = @IdCronogramaServico";

		public const string IncluirProducao = @"
			INSERT INTO producao (cronograma_servico_id, data_producao, quantidade_producao, equipe_producao, observacao_producao)
            OUTPUT Inserted.ID
            VALUES (@IdCronogramaServico, @DataProducao, @QuantidadeProducao, @EquipeProducao, @ObservacaoProducao)";

		public const string EditarProducao = @"
			UPDATE producao
            SET quantidade_producao = @QuantidadeProducao,
                equipe_producao = @EquipeProducao,
                observacao_producao = @ObservacaoProducao
            WHERE id = @IdProducao";

		public const string ExcluirProducao = @"
			DELETE FROM producao
            WHERE id = @IdProducao";

        public const string SomarProducaoServico = @"
            SELECT sum(quantidade_producao)
            FROM producao
	        WHERE cronograma_servico_id = @IdCronogramaServico";
    }
}