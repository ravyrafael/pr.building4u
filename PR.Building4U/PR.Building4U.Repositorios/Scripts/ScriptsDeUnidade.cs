namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeUnidade
	{
        public const string ListarUnidades = @"
			SELECT
	            id AS IdUnidade,
	            sigla_unidade AS SiglaUnidade,
	            nome_unidade AS NomeUnidade
            FROM unidade";

        public const string ListarUnidadesFiltro = @"
			WHERE (upper(sigla_unidade) LIKE upper(@CodigoOuNome)
	            OR upper(nome_unidade) LIKE upper(@CodigoOuNome + '%'))";

        public const string ObterUnidade = @"
			SELECT
	            id AS IdUnidade,
	            sigla_unidade AS SiglaUnidade,
	            nome_unidade AS NomeUnidade
            FROM unidade
            WHERE id = @IdUnidade";

        public const string IncluirUnidade = @"
			INSERT INTO unidade (sigla_unidade, nome_unidade)
            OUTPUT Inserted.ID
            VALUES (@SiglaUnidade, @NomeUnidade)";

        public const string EditarUnidade = @"
			UPDATE unidade
            SET sigla_unidade = @SiglaUnidade,
                nome_unidade = @NomeUnidade
            WHERE id = @IdUnidade";

        public const string ExcluirUnidade = @"
			DELETE FROM unidade
            WHERE id = @IdUnidade";
    }
}