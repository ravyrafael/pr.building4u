namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeCliente
	{
		public const string ListarClientes = @"
			SELECT
	            c.id AS IdCliente,
	            c.cnpj_cliente AS CnpjCliente,
	            c.razao_social_cliente AS RazaoSocialCliente,
	            c.nome_cliente AS NomeCliente,
                s.id AS IdSituacao,
                s.sigla_situacao AS SiglaSituacao,
                s.nome_situacao AS NomeSituacao,
                s.tipo_situacao AS TipoSituacao
            FROM cliente c
                LEFT JOIN situacao s ON s.id = c.situacao_id";

		public const string ListarClientesFiltro = @"
			WHERE (c.cnpj_cliente LIKE @CodigoOuNome + '%'
                OR upper(c.razao_social_cliente) LIKE upper(@CodigoOuNome + '%')
                OR upper(c.nome_cliente) LIKE upper(@CodigoOuNome + '%'))";

		public const string ObterCliente = @"
			SELECT
	            c.id AS IdCliente,
	            c.cnpj_cliente AS CnpjCliente,
	            c.razao_social_cliente AS RazaoSocialCliente,
	            c.nome_cliente AS NomeCliente,
                s.id AS IdSituacao,
                s.sigla_situacao AS SiglaSituacao,
                s.nome_situacao AS NomeSituacao,
                s.tipo_situacao AS TipoSituacao,
	            e.id AS IdEndereco,
                e.logradouro_endereco AS LogradouroEndereco,
                e.numero_endereco AS NumeroEndereco,
                e.complemento_endereco AS ComplementoEndereco,
                e.cep_endereco AS CepEndereco,
                e.distrito_endereco AS DistritoEndereco,
                m.id AS IdMunicipio,
                m.nome_municipio AS NomeMunicipio,
                m.codigo_municipio AS CodigoMunicipio,
                t.id AS IdEstado,
                t.sigla_estado AS SiglaEstado,
                t.nome_estado AS NomeEstado,
                p.id AS IdPais,
                p.codigo_pais AS CodigoPais,
                p.nome_pais AS NomePais
            FROM cliente c
                LEFT JOIN situacao s ON s.id = c.situacao_id
	            LEFT JOIN endereco e ON e.id = c.endereco_id
	            LEFT JOIN municipio m ON m.id = e.municipio_id
                LEFT JOIN estado t ON t.id = m.estado_id
                LEFT JOIN pais p ON p.id = t.pais_id
            WHERE c.id = @IdCliente";

		public const string IncluirCliente = @"
			INSERT INTO cliente (endereco_id, situacao_id, cnpj_cliente, razao_social_cliente, nome_cliente)
            OUTPUT Inserted.ID
            VALUES (@IdEndereco, 1, @CnpjCliente, @RazaoSocialCliente, @NomeCliente)";

		public const string EditarCliente = @"
			UPDATE cliente
            SET razao_social_cliente = @RazaoSocialCliente,
	            nome_cliente = @NomeCliente
            WHERE id = @IdCliente";

		public const string DesativarCliente = @"
			UPDATE cliente
            SET situacao_id = 2
            WHERE id = @IdCliente";
	}
}