namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeCargo
	{
		public const string ListarCargos = @"
			SELECT 
            	c.id AS IdCargo,
	            c.nome_cargo AS NomeCargo,
	            c.sigla_cargo AS SiglaCargo,
	            u.id AS IdCusto,
	            u.sigla_custo AS SiglaCusto,
	            u.nome_custo AS NomeCusto
            FROM cargo c
	            LEFT JOIN custo u ON u.id = c.custo_id";

		public const string ListarCargosFiltro = @"
			WHERE (upper(sigla_cargo) LIKE upper(@CodigoOuNome)
	            OR upper(nome_cargo) LIKE upper(@CodigoOuNome + '%'))";

		public const string ObterCargo = @"
			SELECT 
	            c.id AS IdCargo,
	            c.nome_cargo AS NomeCargo,
	            c.sigla_cargo AS SiglaCargo,
                u.id AS IdCusto,
	            u.sigla_custo AS SiglaCusto,
	            u.nome_custo AS NomeCusto
            FROM cargo c
	            LEFT JOIN custo u ON u.id = c.custo_id
            WHERE c.id = @IdCargo";

		public const string IncluirCargo = @"
			INSERT INTO cargo (custo_id, nome_cargo, sigla_cargo)
            OUTPUT Inserted.ID
            VALUES (@IdCusto, @NomeCargo, upper(@SiglaCargo))";

		public const string EditarCargo = @"
			UPDATE cargo
            SET custo_id = @IdCusto,
	            nome_cargo = @NomeCargo,
	            sigla_cargo = upper(@SiglaCargo)
            WHERE id = @IdCargo";

		public const string ExcluirCargo = @"
			DELETE FROM cargo
            WHERE id = @IdCargo";
	}
}