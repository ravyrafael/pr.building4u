﻿namespace PR.Building4U.Repositorios.Scripts
{
    public static class ScriptsDeSituacao
    {
        public const string ListarSituacoes = @"
            SELECT
	            id AS IdSituacao,
	            sigla_situacao AS SiglaSituacao,
	            nome_situacao AS NomeSituacao,
	            tipo_situacao AS TipoSituacao
            FROM situacao";

        public const string ListarSituacoesFiltro = @"
            WHERE upper(nome_situacao) LIKE upper(@CodigoOuNome + '%')
	            OR upper(sigla_situacao) LIKE upper(@CodigoOuNome)";

        public const string ObterSituacao = @"
            SELECT
                id AS IdSituacao,
	            sigla_situacao AS SiglaSituacao,
	            nome_situacao AS NomeSituacao,
	            tipo_situacao AS TipoSituacao
            FROM situacao
            WHERE id = @IdSituacao";

        public const string EditarSituacao = @"
            UPDATE situacao
            SET nome_situacao = @NomeSituacao
            WHERE id = @IdSituacao";
    }
}
