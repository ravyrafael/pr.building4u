﻿namespace PR.Building4U.Repositorios.Scripts
{
    public static class ScriptsDeEstado
    {
        public const string ListarEstados = @"
            SELECT
	            e.id AS IdEstado,
	            e.sigla_estado AS SiglaEstado,
	            e.nome_estado AS NomeEstado,
	            p.id AS IdPais,
	            p.codigo_pais AS CodigoPais,
	            p.nome_pais AS NomePais
            FROM estado e
	            LEFT JOIN pais p ON p.id = e.pais_id";

        public const string ListarEstadosFiltro = @"
            WHERE upper(e.nome_estado) LIKE upper(@CodigoOuNome + '%')
	            OR upper(e.sigla_estado) LIKE upper(@CodigoOuNome)";

        public const string ObterEstado = @"
            SELECT
	            e.id AS IdEstado,
	            e.sigla_estado AS SiglaEstado,
	            e.nome_estado AS NomeEstado,
	            p.id AS IdPais,
	            p.codigo_pais AS CodigoPais,
	            p.nome_pais AS NomePais
            FROM estado e
	            LEFT JOIN pais p ON p.id = e.pais_id
            WHERE e.id = @IdEstado";

        public const string IncluirEstado = @"
            INSERT INTO estado (pais_id, nome_estado, sigla_estado)
            OUTPUT Inserted.ID
            VALUES (@IdPais, @NomeEstado, upper(@SiglaEstado))";

        public const string EditarEstado = @"
            UPDATE estado
            SET pais_id = @IdPais
	            nome_estado = @NomeEstado
            WHERE id = @IdEstado";

        public const string ExcluirEstado = @"
            DELETE FROM estado
            WHERE id = @IdEstado";

        public const string ListarEstadosPorPais = @"
            SELECT
	            e.id AS IdEstado,
	            e.sigla_estado AS SiglaEstado,
	            e.nome_estado AS NomeEstado,
	            p.id AS IdPais,
	            p.codigo_pais AS CodigoPais,
	            p.nome_pais AS NomePais
            FROM estado e
	            LEFT JOIN pais p ON p.id = e.pais_id
            WHERE e.pais_id = @IdPais";

        public const string ListarEstadosPorPaisFiltro = @"
                AND (upper(e.nome_estado) LIKE upper(@CodigoOuNome + '%')
	            OR upper(e.sigla_estado) LIKE upper(@CodigoOuNome))";
    }
}
