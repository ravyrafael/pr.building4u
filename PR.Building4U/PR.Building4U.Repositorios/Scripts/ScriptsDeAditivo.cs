namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeAditivo
	{
		public const string ListarAditivos = @"
			SELECT 
                a.id AS IdAditivo,
                a.codigo_aditivo AS CodigoAditivo,
                a.data_aditivo AS DataAditivo,
	            a.valor_aditivo AS ValorAditivo,
                a.prazo_aditivo AS PrazoAditivo
            FROM aditivo a
            WHERE a.contrato_id = @IdContrato";

		public const string IncluirAditivo = @"
			INSERT INTO aditivo (contrato_id, codigo_aditivo, data_aditivo, valor_aditivo, prazo_aditivo, escopo_aditivo)
            OUTPUT Inserted.ID
            VALUES (@IdContrato, @CodigoAditivo, @DataAditivo, @ValorAditivo, @PrazoAditivo, @EscopoAditivo)";

		public const string ExcluirAditivo = @"
			DELETE FROM aditivo
            WHERE id = @IdAditivo";
	}
}