namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeTipoEquipamento
	{
		public const string ListarTipoEquipamentos = @"
			SELECT
	            t.id AS IdTipoEquipamento,
	            t.nome_tipo AS NomeTipoEquipamento,
                u.id AS IdCusto,
	            u.sigla_custo AS SiglaCusto,
	            u.nome_custo AS NomeCusto
            FROM tipo_equipamento t
                LEFT JOIN custo u ON u.id = t.custo_id";

        public const string ListarTipoEquipamentosFiltro = @"
			WHERE upper(nome_tipo) LIKE upper(@CodigoOuNome + '%')";

		public const string ObterTipoEquipamento = @"
			SELECT
	            t.id AS IdTipoEquipamento,
	            t.nome_tipo AS NomeTipoEquipamento,
                u.id AS IdCusto,
	            u.sigla_custo AS SiglaCusto,
	            u.nome_custo AS NomeCusto
            FROM tipo_equipamento t
                LEFT JOIN custo u ON u.id = t.custo_id
            WHERE t.id = @IdTipoEquipamento";

		public const string IncluirTipoEquipamento = @"
			INSERT INTO tipo_equipamento (custo_id, nome_tipo)
            OUTPUT Inserted.ID
            VALUES (@IdCusto, @NomeTipoEquipamento)";

		public const string EditarTipoEquipamento = @"
			UPDATE tipo_equipamento
            SET custo_id = @IdCusto,
                nome_tipo = @NomeTipoEquipamento
            WHERE id = @IdTipoEquipamento";

		public const string ExcluirTipoEquipamento = @"
			DELETE FROM tipo_equipamento
            WHERE id = @IdTipoEquipamento";
	}
}