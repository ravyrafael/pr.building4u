﻿namespace PR.Building4U.Repositorios.Scripts
{
    public static class ScriptsDePais
    {
        public const string ListarPaises = @"
            SELECT
	            id AS IdPais,
	            codigo_pais AS CodigoPais,
	            nome_pais AS NomePais
            FROM pais";

        public const string ListarPaisesFiltro = @"
            WHERE upper(nome_pais) LIKE upper(@CodigoOuNome + '%')
	            OR convert(VARCHAR(50), codigo_pais) LIKE @CodigoOuNome";

        public const string ObterPais = @"
            SELECT
                id AS IdPais,
	            codigo_pais AS CodigoPais,
	            nome_pais AS NomePais
            FROM pais
            WHERE id = @IdPais";

        public const string IncluirPais = @"
            INSERT INTO pais (codigo_pais, nome_pais)
            OUTPUT Inserted.ID
            VALUES (@CodigoPais, @NomePais)";

        public const string EditarPais = @"
            UPDATE pais
            SET nome_pais = @NomePais
            WHERE id = @IdPais";

        public const string ExcluirPais = @"
            DELETE FROM pais
            WHERE id = @IdPais";
    }
}
