namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeNotificacao
	{
		public const string ListarNotificacoes = @"
			SELECT
	            id AS IdNotificacao,
	            titulo_notificacao AS TituloNotificacao,
	            texto_notificacao AS TextoNotificacao,
	            data_notificacao AS DataNotificacao,
	            tipo_notificacao AS TipoNotificacao,
                lido_notificacao AS LidoNotificacao
            FROM notificacao
            WHERE funcionario_id = @IdFuncionario
	            AND data_notificacao >= dateadd(day, -7, getdate())";

		public const string IncluirNotificacao = @"
			INSERT INTO notificacao (funcionario_id, titulo_notificacao, texto_notificacao, data_notificacao, tipo_notificacao, lido_notificacao)
            OUTPUT Inserted.ID
            VALUES (@IdFuncionario, @TituloNotificacao, @TextoNotificacao, @DataNotificacao, @TipoNotificacao, 0)";

        public const string MarcarNotificacaoComoLida = @"
            UPDATE notificacao
            SET lido_notificacao = 1";

        public const string MarcarNotificacaoComoLidaFiltro1 = @"
            WHERE id = @IdNotificacao";

        public const string MarcarNotificacaoComoLidaFiltro2 = @"
            WHERE funcionario_id = @IdFuncionario";
    }
}