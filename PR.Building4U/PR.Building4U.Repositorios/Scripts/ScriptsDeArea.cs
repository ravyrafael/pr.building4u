namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeArea
	{
		public const string ListarAreas = @"
			SELECT
	            id AS IdArea,
	            sigla_area AS SiglaArea,
	            nome_area AS NomeArea
            FROM area";

		public const string ListarAreasFiltro = @"
			WHERE (upper(sigla_area) LIKE upper(@CodigoOuNome)
	            OR upper(nome_area) LIKE upper(@CodigoOuNome + '%'))";

		public const string ObterArea = @"
			SELECT
	            id AS IdArea,
	            sigla_area AS SiglaArea,
	            nome_area AS NomeArea
            FROM area
            WHERE id = @IdArea";

		public const string EditarArea = @"
			UPDATE area
            SET nome_area = @NomeArea
            WHERE id = @IdArea";
	}
}