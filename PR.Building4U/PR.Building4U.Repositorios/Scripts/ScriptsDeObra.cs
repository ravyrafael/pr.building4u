namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeObra
	{
		public const string ListarObras = @"
			SELECT 
	            o.id AS IdObra,
	            o.codigo_obra AS CodigoObra,
	            o.nome_obra AS NomeObra,
	            s.id AS IdSituacao,
	            s.sigla_situacao AS SiglaSituacao,
	            s.nome_situacao AS NomeSituacao,
	            s.tipo_situacao AS TipoSituacao,
	            a.id AS IdArea,
	            a.sigla_area AS SiglaArea,
	            a.nome_area AS NomeArea,
	            os.id AS IdOrdemServico,
	            os.data_os AS DataOrdemServico,
	            os.prazo_os AS PrazoOrdemServico,
	            c.id AS IdContrato,
	            c.codigo_contrato AS CodigoContrato,
	            c.data_contrato AS DataContrato,
	            c.valor_contrato AS ValorContrato,
	            c.prazo_contrato AS PrazoContrato,
	            c.escopo_contrato AS EscopoContrato,
	            l.id AS IdCliente,
	            l.cnpj_cliente AS CnpjCliente,
	            l.nome_cliente AS NomeCliente,
	            l.razao_social_cliente AS RazaoSocialCliente
            FROM obra o
	            LEFT JOIN situacao s ON s.id = o.situacao_id
	            LEFT JOIN area a ON a.id = o.area_id
	            LEFT JOIN contrato c ON c.id = o.contrato_id
	            LEFT JOIN cliente l ON l.id = c.cliente_id
	            LEFT JOIN ordem_servico os ON os.obra_id = o.id
            WHERE 1 = 1";

        public const string ListarObrasFiltro = @"
			AND (o.codigo_obra LIKE upper(@CodigoOuNome + '%')
	            OR upper(o.nome_obra) LIKE upper(@CodigoOuNome + '%'))";

        public const string ListarObrasAtivas = @"
            AND s.id = 6";

        public const string ListarObrasObras = @"
            AND (o.area_id = 1 OR o.area_id = 2)";

        public const string ObterObra = @"
			SELECT 
	            o.id AS IdObra,
	            o.codigo_obra AS CodigoObra,
	            o.nome_obra AS NomeObra,
	            s.id AS IdSituacao,
	            s.sigla_situacao AS SiglaSituacao,
	            s.nome_situacao AS NomeSituacao,
	            s.tipo_situacao AS TipoSituacao,
	            a.id AS IdArea,
	            a.sigla_area AS SiglaArea,
	            a.nome_area AS NomeArea,
	            os.id AS IdOrdemServico,
	            os.data_os AS DataOrdemServico,
	            os.prazo_os AS PrazoOrdemServico,
	            c.id AS IdContrato,
	            c.codigo_contrato AS CodigoContrato,
	            c.data_contrato AS DataContrato,
	            c.valor_contrato AS ValorContrato,
	            c.prazo_contrato AS PrazoContrato,
	            c.escopo_contrato AS EscopoContrato,
	            l.id AS IdCliente,
	            l.cnpj_cliente AS CnpjCliente,
	            l.nome_cliente AS NomeCliente,
	            l.razao_social_cliente AS RazaoSocialCliente
            FROM obra o
	            LEFT JOIN situacao s ON s.id = o.situacao_id
	            LEFT JOIN area a ON a.id = o.area_id
	            LEFT JOIN contrato c ON c.id = o.contrato_id
	            LEFT JOIN cliente l ON l.id = c.cliente_id
	            LEFT JOIN ordem_servico os ON os.obra_id = o.id
            WHERE o.id = @IdObra";

		public const string IncluirObra = @"
			INSERT INTO obra (situacao_id, area_id, contrato_id, codigo_obra, nome_obra)
            OUTPUT Inserted.ID
            VALUES (6, @IdArea, @IdContrato, @CodigoObra, upper(@NomeObra))";

		public const string EditarObra = @"
			UPDATE obra
            SET area_id = @IdArea,
	            contrato_id = @IdContrato,
	            codigo_obra = @CodigoObra,
	            nome_obra = upper(@NomeObra)
            WHERE id = @IdObra";

		public const string ExcluirObra = @"
			DELETE FROM obra
            WHERE id = @IdObra";

        public const string IncluirOrdemServico = @"
			INSERT INTO ordem_servico (obra_id, data_os, prazo_os)
            OUTPUT Inserted.ID
            VALUES (@IdObra, @DataOrdemServico, @PrazoOrdemServico)";

        public const string EditarOrdemServico = @"
			UPDATE ordem_servico
            SET data_os = @DataOrdemServico,
	            prazo_os = @PrazoOrdemServico
            WHERE id = @IdOrdemServico";

        public const string ExcluirOrdemServico = @"
			DELETE FROM ordem_servico
            WHERE id = @IdOrdemServico";
    }
}