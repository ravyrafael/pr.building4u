namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeGerencia
	{
		public const string ListarGerencias = @"
			SELECT
	            id AS IdGerencia,
	            nome_gerencia AS NomeGerencia,
	            sigla_gerencia AS SiglaGerencia
            FROM gerencia";

		public const string ListarGerenciasFiltro = @"
			WHERE (upper(nome_gerencia) LIKE upper(@CodigoOuNome + '%')
	            OR upper(sigla_gerencia) LIKE upper(@CodigoOuNome + '%'))";

		public const string IncluirGerencia = @"
			INSERT INTO gerencia (nome_gerencia, sigla_gerencia)
            OUTPUT Inserted.ID
            VALUES (@NomeGerencia, upper(@SiglaGerencia))";

		public const string ExcluirGerencia = @"
			DELETE FROM gerencia
            WHERE id = @IdGerencia";
	}
}