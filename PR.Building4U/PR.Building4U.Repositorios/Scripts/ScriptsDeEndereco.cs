﻿namespace PR.Building4U.Repositorios.Scripts
{
    public static class ScriptsDeEndereco
    {
        public const string ListarEnderecosPorMunicipio = @"
            SELECT
	            d.id AS IdEndereco,
	            d.logradouro_endereco AS LogradouroEndereco,
	            d.numero_endereco AS NumeroEndereco,
	            d.complemento_endereco AS ComplementoEndereco,
	            d.cep_endereco AS CepEndereco,
	            d.distrito_endereco AS DistritoEndereco,
                m.id AS IdMunicipio,
                m.nome_municipio AS NomeMunicipio,
                m.codigo_municipio AS CodigoMunicipio,
                e.id AS IdEstado,
                e.sigla_estado AS SiglaEstado,
                e.nome_estado AS NomeEstado,
                p.id AS IdPais,
                p.codigo_pais AS CodigoPais,
                p.nome_pais AS NomePais
            FROM endereco d
	            LEFT JOIN municipio m ON m.id = d.municipio_id
                LEFT JOIN estado e ON e.id = m.estado_id
                LEFT JOIN pais p ON p.id = e.pais_id
            WHERE d.municipio_id = @IdMunicipio";

        public const string IncluirEndereco = @"
            INSERT INTO endereco (municipio_id, logradouro_endereco, numero_endereco, complemento_endereco, cep_endereco, distrito_endereco)
            OUTPUT Inserted.ID
            VALUES (@IdMunicipio, @LogradouroEndereco, @NumeroEndereco, @ComplementoEndereco, @CepEndereco, @DistritoEndereco)";

        public const string EditarEndereco = @"
            UPDATE endereco
            SET municipio_id = @IdMunicipio,
                logradouro_endereco = @LogradouroEndereco,
                numero_endereco = @NumeroEndereco,
                complemento_endereco = @ComplementoEndereco,
                cep_endereco = @CepEndereco,
                distrito_endereco = @DistritoEndereco
            WHERE id = @IdEndereco";
    }
}
