namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeNecessario
	{
        public const string CancelarNecessarioAprovPorObra = @"
			UPDATE necessario_aprov
            SET indicador_aprov = 0
            WHERE obra_id = @IdObra";

        public const string ExcluirNecessarioAprovPorObra = @"
			DELETE FROM necessario_aprov
            WHERE obra_id = @IdObra";

        public const string IncluirNecessarioAprovPorObra = @"
			INSERT INTO necessario_aprov (cargo_id, obra_id, quantidade_necessario, indicador_aprov)
            SELECT cargo_id, obra_id, quantidade_necessario, 0 FROM necessario WHERE obra_id = @IdObra";

        public const string ListarNecessarioAprovPorObra = @"
			SELECT
	            n.id AS IdNecessario,
	            n.quantidade_necessario AS QuantidadeNecessario,
	            o.id AS IdObra,
	            o.codigo_obra AS CodigoObra,
	            o.nome_obra AS NomeObra,
	            c.id AS IdCargo,
	            c.nome_cargo AS NomeCargo,
                u.id AS IdCusto,
                u.sigla_custo AS SiglaCusto,
                u.nome_custo AS NomeCusto
            FROM necessario_aprov n
	            LEFT JOIN obra o ON o.id = n.obra_id
	            LEFT JOIN cargo c ON c.id = n.cargo_id
                LEFT JOIN custo u ON u.id = c.custo_id
            WHERE n.obra_id = @IdObra";

        public const string VerificarNecessarioAprov = @"
			SELECT 1
            FROM necessario_aprov
            WHERE obra_id = @IdObra
	            AND cargo_id = @IdCargo";

        public const string IncluirNecessarioAprov = @"
			INSERT INTO necessario_aprov (cargo_id, obra_id, quantidade_necessario, indicador_aprov)
            VALUES (@IdCargo, @IdObra, @QuantidadeNecessario, 0)";

        public const string AtualizarNecessarioAprov = @"
			UPDATE necessario_aprov
            SET quantidade_necessario = @QuantidadeNecessario
            WHERE obra_id = @IdObra
	            AND cargo_id = @IdCargo";

        public const string ExcluirNecessarioAprov = @"
			DELETE FROM necessario_aprov
            WHERE obra_id = @IdObra
	            AND cargo_id = @IdCargo";

        public const string EnviarNecessarioAprovPorObra = @"
			UPDATE necessario_aprov
            SET indicador_aprov = 1
            WHERE obra_id = @IdObra";

        public const string ListarNecessarioPorObra = @"
			SELECT
                1 AS IdNecessario,
	            sum(un.QuantidadeNecessario) AS QuantidadeNecessario,
	            sum(un.QuantidadeAprovacao) AS QuantidadeAprovacao,
	            un.IdCargo AS IdCargo,
	            un.NomeCargo AS NomeCargo,
	            un.IdCusto AS IdCusto,
	            un.NomeCusto AS NomeCusto
            FROM
	            (SELECT
	                n.quantidade_necessario AS QuantidadeNecessario,
	                0 AS QuantidadeAprovacao,
	                c.id AS IdCargo,
	                c.nome_cargo AS NomeCargo,
	                u.id AS IdCusto,
	                u.nome_custo AS NomeCusto
	            FROM necessario n
	                LEFT JOIN obra o ON o.id = n.obra_id
	                LEFT JOIN cargo c ON c.id = n.cargo_id
	                LEFT JOIN custo u ON u.id = c.custo_id
	            WHERE n.obra_id = @IdObra
	            UNION
	            SELECT
		            0 AS QuantidadeNecessario,
	                n.quantidade_necessario AS QuantidadeAprovacao,
	                c.id AS IdCargo,
	                c.nome_cargo AS NomeCargo,
	                u.id AS IdCusto,
	                u.nome_custo AS NomeCusto
	            FROM necessario_aprov n
	                LEFT JOIN obra o ON o.id = n.obra_id
	                LEFT JOIN cargo c ON c.id = n.cargo_id
	                LEFT JOIN custo u ON u.id = c.custo_id
	            WHERE n.obra_id = @IdObra AND n.indicador_aprov = 1) AS un
            GROUP BY un.IdCargo, un.NomeCargo, un.IdCusto, un.NomeCusto";

        public const string ExcluirNecessarioPorObra = @"
			DELETE FROM necessario
            WHERE obra_id = @IdObra";

        public const string IncluirNecessarioPorObra = @"
			INSERT INTO necessario (cargo_id, obra_id, quantidade_necessario)
            SELECT cargo_id, obra_id, quantidade_necessario FROM necessario_aprov WHERE obra_id = @IdObra";

        public const string VerificarNecessarioAprovPorObra = @"
            SELECT count(1)
            FROM necessario_aprov
            WHERE obra_id = @IdObra
	            AND indicador_aprov = 1";
    }
}