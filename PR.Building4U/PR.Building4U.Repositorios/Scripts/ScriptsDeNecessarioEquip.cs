namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeNecessarioEquip
	{
        public const string CancelarNecessarioAprovPorObra = @"
			UPDATE nec_equip_aprov
            SET indicador_aprov = 0
            WHERE obra_id = @IdObra";

        public const string ExcluirNecessarioAprovPorObra = @"
			DELETE FROM nec_equip_aprov
            WHERE obra_id = @IdObra";

        public const string IncluirNecessarioAprovPorObra = @"
			INSERT INTO nec_equip_aprov (tipo_equipamento_id, obra_id, quantidade_necessario, indicador_aprov)
            SELECT tipo_equipamento_id, obra_id, quantidade_necessario, 0 FROM nec_equip WHERE obra_id = @IdObra";

        public const string ListarNecessarioAprovPorObra = @"
			SELECT
	            n.id AS IdNecessario,
	            n.quantidade_necessario AS QuantidadeNecessario,
	            o.id AS IdObra,
	            o.codigo_obra AS CodigoObra,
	            o.nome_obra AS NomeObra,
	            t.id AS IdTipoEquipamento,
	            t.nome_tipo AS NomeTipoEquipamento,
	            c.id AS IdCusto,
	            c.sigla_custo AS SiglaCusto,
	            c.nome_custo AS NomeCusto
            FROM nec_equip_aprov n
	            LEFT JOIN obra o ON o.id = n.obra_id
	            LEFT JOIN tipo_equipamento t ON t.id = n.tipo_equipamento_id
	            LEFT JOIN custo c ON c.id = t.custo_id
            WHERE n.obra_id = @IdObra";

        public const string VerificarNecessarioAprov = @"
			SELECT 1
            FROM nec_equip_aprov
            WHERE obra_id = @IdObra
	            AND tipo_equipamento_id = @IdTipoEquipamento";

        public const string IncluirNecessarioAprov = @"
			INSERT INTO nec_equip_aprov (tipo_equipamento_id, obra_id, quantidade_necessario, indicador_aprov)
            VALUES (@IdTipoEquipamento, @IdObra, @QuantidadeNecessario, 0)";

        public const string AtualizarNecessarioAprov = @"
			UPDATE nec_equip_aprov
            SET quantidade_necessario = @QuantidadeNecessario
            WHERE obra_id = @IdObra
	            AND tipo_equipamento_id = @IdTipoEquipamento";

        public const string ExcluirNecessarioAprov = @"
			DELETE FROM nec_equip_aprov
            WHERE obra_id = @IdObra
	            AND tipo_equipamento_id = @IdTipoEquipamento";

        public const string EnviarNecessarioAprovPorObra = @"
			UPDATE nec_equip_aprov
            SET indicador_aprov = 1
            WHERE obra_id = @IdObra";

        public const string ListarNecessarioPorObra = @"
			SELECT
                1 AS IdNecessario,
	            sum(un.QuantidadeNecessario) AS QuantidadeNecessario,
	            sum(un.QuantidadeAprovacao) AS QuantidadeAprovacao,
	            un.IdTipoEquipamento AS IdTipoEquipamento,
	            un.NomeTipoEquipamento AS NomeTipoEquipamento,
	            un.IdCusto AS IdCusto,
	            un.NomeCusto AS NomeCusto
            FROM
	            (SELECT
	                n.quantidade_necessario AS QuantidadeNecessario,
	                0 AS QuantidadeAprovacao,
	                t.id AS IdTipoEquipamento,
	                t.nome_tipo AS NomeTipoEquipamento,
	                c.id AS IdCusto,
	                c.nome_custo AS NomeCusto
	            FROM nec_equip n
	                LEFT JOIN obra o ON o.id = n.obra_id
	                LEFT JOIN tipo_equipamento t ON t.id = n.tipo_equipamento_id
	                LEFT JOIN custo c ON c.id = t.custo_id
	            WHERE n.obra_id = @IdObra
	            UNION
	            SELECT
		            0 AS QuantidadeNecessario,
	                n.quantidade_necessario AS QuantidadeAprovacao,
	                t.id AS IdTipoEquipamento,
	                t.nome_tipo AS NomeTipoEquipamento,
	                c.id AS IdCusto,
	                c.nome_custo AS NomeCusto
	            FROM nec_equip_aprov n
	                LEFT JOIN obra o ON o.id = n.obra_id
	                LEFT JOIN tipo_equipamento t ON t.id = n.tipo_equipamento_id
	                LEFT JOIN custo c ON c.id = t.custo_id
	            WHERE n.obra_id = @IdObra AND n.indicador_aprov = 1) AS un
            GROUP BY un.IdTipoEquipamento, un.NomeTipoEquipamento, un.IdCusto, un.NomeCusto";

        public const string ExcluirNecessarioPorObra = @"
			DELETE FROM nec_equip
            WHERE obra_id = @IdObra";

        public const string IncluirNecessarioPorObra = @"
			INSERT INTO nec_equip (tipo_equipamento_id, obra_id, quantidade_necessario)
            SELECT tipo_equipamento_id, obra_id, quantidade_necessario FROM nec_equip_aprov WHERE obra_id = @IdObra";

        public const string VerificarNecessarioAprovPorObra = @"
            SELECT count(1)
            FROM nec_equip_aprov
            WHERE obra_id = @IdObra
	            AND indicador_aprov = 1";
    }
}