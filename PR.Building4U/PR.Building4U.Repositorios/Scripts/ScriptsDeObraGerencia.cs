namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeObraGerencia
	{
		public const string ListarGerenciasPorObra = @"
			SELECT 
	            og.id AS IdObraGerencia,
	            ob.id AS IdObra,
	            ob.codigo_obra AS CodigoObra,
	            ob.nome_obra AS NomeObra,
	            ge.id AS IdGerencia,
	            ge.nome_gerencia AS NomeGerencia,
	            ge.sigla_gerencia AS SiglaGerencia,
	            fu.id AS IdFuncionario,
	            fu.chapa_funcionario AS ChapaFuncionario,
	            fu.nome_funcionario AS NomeFuncionario
            FROM obra_gerencia og
	            LEFT JOIN obra ob ON ob.id = og.obra_id
	            LEFT JOIN gerencia ge ON ge.id = og.gerencia_id
	            LEFT JOIN funcionario fu ON fu.id = og.funcionario_id
            WHERE ob.id = @IdObra";

		public const string IncluirObraGerencia = @"
			INSERT INTO obra_gerencia (obra_id, gerencia_id, funcionario_id)
            OUTPUT Inserted.ID
            VALUES (@IdObra, @IdGerencia, @IdFuncionario)";

		public const string ExcluirObraGerencia = @"
			DELETE FROM obra_gerencia
            WHERE id = @IdObraGerencia";
	}
}