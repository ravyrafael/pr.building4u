namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDePlanilha
	{
		public const string ListarPlanilhas = @"
			SELECT
	            id AS IdPlanilha,
	            cronograma_id AS IdCronograma,
	            cp_planilha AS CpPlanilha,
	            rev_planilha AS RevPlanilha
            FROM planilha
            WHERE cronograma_id = @IdCronograma";

        public const string ObterPlanilha = @"
			SELECT
	            id AS IdPlanilha,
	            cronograma_id AS IdCronograma,
	            cp_planilha AS CpPlanilha,
	            rev_planilha AS RevPlanilha
            FROM planilha
            WHERE id = @IdPlanilha";

        public const string ListarItens = @"
			SELECT
	            pi.id AS IdPlanilhaItem,
	            pi.cronograma_servico_id AS IdCronogramaServico,
                s.sigla_servico AS NomeCronogramaServico,
	            pi.planilha_id AS IdPlanilha,
	            pi.codigo_item_planilha AS CodigoItem,
	            pi.descricao_item_planilha AS DescricaoItem,
	            pi.preco_item_planilha AS PrecoItem,
	            pi.quantidade_item_planilha AS QuantidadeItem,
	            pi.quantidade_acerto_item AS QuantidadeAcertoItem,
	            pi.eh_titulo_item AS EhTituloItem,
	            pi.eh_item_controle AS EhItemControle,
	            u.id AS IdUnidade,
	            u.sigla_unidade AS SiglaUnidade,
	            u.nome_unidade AS NomeUnidade
            FROM planilha_item pi
	            LEFT JOIN unidade u ON u.id = pi.unidade_id
                LEFT JOIN cronograma_servico cs ON cs.id = pi.cronograma_servico_id
                LEFT JOIN servico s ON s.id = cs.servico_id
            WHERE pi.planilha_id = @IdPlanilha";

        public const string ListarItensMedicao = @"
            SELECT
	            id AS IdPlanilhaItemMedicao,
	            planilha_item_id AS IdPlanilhaItem,
	            periodo_medicao AS PeriodoMedicao,
	            quantidade_medicao AS QuantidadeMedicao,
	            valor_medicao AS ValorMedicao
            FROM planilha_item_medicao
            WHERE planilha_item_id = @IdPlanilhaItem";

        public const string AtualizarItem = @"
			UPDATE planilha_item
            SET quantidade_acerto_item = @QuantidadeAcertoItem,
                eh_item_controle = @EhItemControle,
                cronograma_servico_id = @IdCronogramaServico
            WHERE id = @IdPlanilhaItem";

        public const string IncluirPlanilha = @"
            INSERT INTO planilha (cronograma_id, cp_planilha, rev_planilha)
            OUTPUT Inserted.ID
            VALUES (@IdCronograma, @CpPlanilha, @RevPlanilha)";

        public const string IncluirItem = @"
            INSERT INTO planilha_item (cronograma_servico_id, planilha_id, unidade_id, codigo_item_planilha, descricao_item_planilha, preco_item_planilha, quantidade_item_planilha, quantidade_acerto_item, eh_titulo_item, eh_item_controle)
            OUTPUT Inserted.ID
            VALUES (@IdCronogramaServico, @IdPlanilha, @IdUnidade, @CodigoItem, @DescricaoItem, @PrecoItem, @QuantidadeItem, @QuantidadeAcertoItem, @EhTituloItem, @EhItemControle)";

        public const string ExcluirPlanilha  = @"
            DELETE FROM planilha
            WHERE id = @IdPlanilha";

        public const string ExcluirItens = @"
            DELETE FROM planilha_item
            WHERE planilha_id = @IdPlanilha";

        public const string SomarValorMedicao = @"
            SELECT sum(pim.valor_medicao)
            FROM planilha_item_medicao pim
	            LEFT JOIN planilha_item pi ON pi.id = pim.planilha_item_id
            WHERE pi.cronograma_servico_id = @IdCronogramaServico
	            AND pim.periodo_medicao = @MesReferencia";

        public const string SomarQuantidadeMedicao = @"
            SELECT sum(pim.quantidade_medicao)
            FROM planilha_item_medicao pim
	            LEFT JOIN planilha_item pi ON pi.id = pim.planilha_item_id
            WHERE pi.cronograma_servico_id = @IdCronogramaServico
	            AND pim.periodo_medicao = @MesReferencia
                AND pi.eh_item_controle = 1";

        public const string VerificarMedicao = @"
            SELECT 1
            FROM planilha_item_medicao pim
	            LEFT JOIN planilha_item pi ON pi.id = pim.planilha_item_id
	            LEFT JOIN planilha p ON p.id = pi.planilha_id
            WHERE p.cronograma_id = @IdCronograma
            	AND pim.periodo_medicao >= @MesReferencia";

        public const string ListarMedicoes = @"
            SELECT
	            p.id AS idPlanilha,
	            pim.periodo_medicao AS PeriodoMedicao,
	            sum(pim.valor_medicao) AS ValorMedicao
            FROM planilha_item_medicao pim
	            LEFT JOIN planilha_item pi ON pi.id = pim.planilha_item_id
	            LEFT JOIN planilha p ON p.id = pi.planilha_id
            WHERE p.id = @IdPlanilha
            GROUP BY p.id, pim.periodo_medicao";

        public const string IncluirMedicao = @"
            INSERT INTO planilha_item_medicao (planilha_item_id, periodo_medicao, quantidade_medicao, valor_medicao)
            OUTPUT Inserted.ID
            VALUES (@IdPlanilhaItem, @PeriodoMedicao, @QuantidadeMedicao, @ValorMedicao)";

        public const string ExcluirMedicao = @"
            DELETE FROM planilha_item_medicao
            WHERE periodo_medicao = @PeriodoMedicao
                AND planilha_item_id IN (
						SELECT id
						FROM planilha_item
						WHERE planilha_id = @IdPlanilha)";
    }
}