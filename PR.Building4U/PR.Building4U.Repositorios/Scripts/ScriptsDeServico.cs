namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeServico
	{
		public const string ListarServicos = @"
			SELECT
	            s.id AS IdServico,
	            s.codigo_servico AS CodigoServico,
	            s.sigla_servico AS SiglaServico,
	            s.nome_servico AS NomeServico,
	            u.id AS IdUnidade,
	            u.sigla_unidade AS SiglaUnidade,
	            u.nome_unidade AS NomeUnidade
            FROM servico s
	            LEFT JOIN unidade u ON u.id = s.unidade_id";

		public const string ListarServicosFiltro = @"
			WHERE (upper(sigla_servico) LIKE upper(@CodigoOuNome + '%')
		        OR upper(nome_servico) LIKE upper(@CodigoOuNome + '%'))";

		public const string ObterServico = @"
			SELECT
	            s.id AS IdServico,
	            s.codigo_servico AS CodigoServico,
	            s.sigla_servico AS SiglaServico,
	            s.nome_servico AS NomeServico,
	            u.id AS IdUnidade,
	            u.sigla_unidade AS SiglaUnidade,
	            u.nome_unidade AS NomeUnidade
            FROM servico s
	            LEFT JOIN unidade u ON u.id = s.unidade_id
            WHERE 1 = 1";

        public const string ObterServicoFiltroId = @"
            AND s.id = @IdServico";

        public const string ObterServicoFiltroCodigo = @"
            AND s.codigo_servico = @CodigoServico";

        public const string IncluirServico = @"

            INSERT INTO servico (unidade_id, codigo_servico, sigla_servico, nome_servico)
            VALUES (@IdUnidade, @CodigoServico, upper(@SiglaServico), @NomeServico)";

		public const string EditarServico = @"
			UPDATE servico
            SET sigla_servico = @SiglaServico,
	            nome_servico = @NomeServico,
	            unidade_id = @IdUnidade
            WHERE id = @IdServico";

		public const string ExcluirServico = @"
			DELETE FROM servico
            WHERE id = @IdServico";
	}
}