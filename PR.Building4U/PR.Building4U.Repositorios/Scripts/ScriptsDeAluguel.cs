namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeAluguel
	{
        public const string ListarAlugueis = @"
			SELECT
                al.id AS IdAluguel,
                al.sequencia_aluguel AS SequenciaAluguel,
                eq.id AS IdEquipamento,
                eq.patrimonio_equipamento AS PatrimonioEquipamento,
                eq.marca_equipamento AS MarcaEquipamento,
                eq.modelo_equipamento AS ModeloEquipamento,
                sieq.nome_situacao AS SituacaoEquipamento,
                sieq.sigla_situacao AS SiglaSituacaoEquipamento,
                cu.nome_custo AS NomeCusto,
                ti.nome_tipo AS TipoEquipamento,
                ob.id as IdObra,
                ob.codigo_obra as CodigoObra,
                ob.nome_obra AS NomeObra,
                al.inicio_aluguel AS InicioAluguel,
                re.inicio_reserva AS InicioReserva,
                (SELECT obsu.nome_obra
                    FROM aluguel alsu
                        LEFT JOIN obra obsu ON obsu.id = alsu.obra_id
                    WHERE alsu.equipamento_id = al.equipamento_id
                        AND alsu.sequencia_aluguel = (SELECT max(sequencia_aluguel)-1 FROM aluguel WHERE equipamento_id = al.equipamento_id)) AS ObraOrigem,
                obre.nome_obra AS ObraDestino,
                siob.sigla_situacao AS SituacaoObra,
                ma.termino_manutencao AS TerminoManutencao
            FROM aluguel al
                LEFT JOIN equipamento eq ON eq.id = al.equipamento_id
                LEFT JOIN situacao sieq ON sieq.id = eq.situacao_id
                LEFT JOIN tipo_equipamento ti ON ti.id = eq.tipo_equipamento_id
                LEFT JOIN custo cu ON cu.id = ti.custo_id
                LEFT JOIN obra ob ON ob.id = al.obra_id
                LEFT JOIN situacao siob ON siob.id = ob.situacao_id
                LEFT JOIN reserva_aluguel re ON re.equipamento_id = al.equipamento_id
                LEFT JOIN obra obre ON obre.id = re.obra_id
                LEFT JOIN manutencao ma ON ma.equipamento_id = eq.id
            WHERE al.sequencia_aluguel = (SELECT max(sequencia_aluguel) FROM aluguel WHERE equipamento_id = al.equipamento_id)
                AND sieq.id <> 10";

        public const string ListarAlugueisFiltroObras = @"
                AND ob.id IN @Obras";

        public const string ListarAlugueisFiltroTipos = @"
                AND ti.id IN @Tipos";

        public const string IncluirAluguel = @"
            INSERT INTO aluguel (equipamento_id, obra_id, inicio_aluguel, termino_aluguel, sequencia_aluguel)
            OUTPUT Inserted.ID            
            VALUES (@IdEquipamento, @IdObra, @DataAluguel, NULL, @SequenciaAluguel)";

        public const string FinalizarAluguel = @"
            UPDATE aluguel
            SET termino_aluguel = @DataTermino
            WHERE equipamento_id = @IdEquipamento
	            AND sequencia_aluguel = @SequenciaAluguel";

        public const string IncluirReserva = @"
            INSERT INTO reserva_aluguel (obra_id, equipamento_id, inicio_reserva)
            OUTPUT Inserted.ID            
            VALUES (@IdObra, @IdEquipamento, @DataReserva)";

        public const string ExcluirReserva = @"
            DELETE FROM reserva_aluguel
            WHERE equipamento_id = @IdEquipamento";

        public const string ObterAluguelPorEquipamento = @"
            SELECT
                al.id AS IdAluguel,
                al.sequencia_aluguel AS SequenciaAluguel,
                eq.id AS IdEquipamento,
                eq.patrimonio_equipamento AS PatrimonioEquipamento,
                eq.marca_equipamento AS MarcaEquipamento,
                eq.modelo_equipamento AS ModeloEquipamento,
                sieq.nome_situacao AS SituacaoEquipamento,
                sieq.sigla_situacao AS SiglaSituacaoEquipamento,
                cu.nome_custo AS NomeCusto,
                ti.nome_tipo AS TipoEquipamento,
                ob.id as IdObra,
                ob.codigo_obra as CodigoObra,
                ob.nome_obra AS NomeObra,
                al.inicio_aluguel AS InicioAluguel,
                re.inicio_reserva AS InicioReserva,
                (SELECT obsu.nome_obra
                    FROM aluguel alsu
                        LEFT JOIN obra obsu ON obsu.id = alsu.obra_id
                    WHERE alsu.equipamento_id = al.equipamento_id
                        AND alsu.sequencia_aluguel = (SELECT max(sequencia_aluguel)-1 FROM aluguel WHERE equipamento_id = al.equipamento_id)) AS ObraOrigem,
                obre.nome_obra AS ObraDestino,
                siob.sigla_situacao AS SituacaoObra,
                ma.termino_manutencao AS TerminoManutencao
            FROM aluguel al
                LEFT JOIN equipamento eq ON eq.id = al.equipamento_id
                LEFT JOIN situacao sieq ON sieq.id = eq.situacao_id
                LEFT JOIN tipo_equipamento ti ON ti.id = eq.tipo_equipamento_id
                LEFT JOIN custo cu ON cu.id = ti.custo_id
                LEFT JOIN obra ob ON ob.id = al.obra_id
                LEFT JOIN situacao siob ON siob.id = ob.situacao_id
                LEFT JOIN reserva_aluguel re ON re.equipamento_id = al.equipamento_id
                LEFT JOIN obra obre ON obre.id = re.obra_id
                LEFT JOIN manutencao ma ON ma.equipamento_id = eq.id
            WHERE al.sequencia_aluguel = (SELECT max(sequencia_aluguel) FROM aluguel WHERE equipamento_id = al.equipamento_id)
                AND eq.id = @IdEquipamento";

        public const string ExcluirAluguel = @"
            DELETE FROM aluguel
            WHERE equipamento_id = @IdEquipamento
	            AND sequencia_aluguel = @SequenciaAluguel";

        public const string ReativarAluguel = @"
            UPDATE aluguel
            SET termino_aluguel = NULL
            WHERE equipamento_id = @IdEquipamento
	            AND sequencia_aluguel = @SequenciaAluguel";

        public const string VerificarDestino = @"
            SELECT count(1)
            FROM reserva_aluguel
            WHERE equipamento_id = @IdEquipamento";
    }
}