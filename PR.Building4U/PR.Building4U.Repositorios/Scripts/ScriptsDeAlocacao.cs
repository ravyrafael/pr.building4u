namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeAlocacao
	{
		public const string ListarAlocacoes = @"
			SELECT
	            al.id AS IdAlocacao,
	            al.sequencia_alocacao AS SequenciaAlocacao,
	            fu.id AS IdFuncionario,
	            fu.chapa_funcionario AS ChapaFuncionario,
	            fu.nome_funcionario AS NomeFuncionario,
                sifu.nome_situacao AS SituacaoFuncionario,
                sifu.sigla_situacao AS SiglaSituacaoFuncionario,
	            cu.nome_custo AS NomeCusto,
	            ca.nome_cargo AS NomeCargo,
                ob.id as IdObra,
                ob.codigo_obra as CodigoObra,
	            ob.nome_obra AS NomeObra,
	            al.inicio_alocacao AS InicioAlocacao,
	            re.inicio_reserva AS InicioReserva,
	            (SELECT obsu.nome_obra
		            FROM alocacao alsu
			            LEFT JOIN obra obsu ON obsu.id = alsu.obra_id
		            WHERE alsu.funcionario_id = al.funcionario_id
			            AND alsu.sequencia_alocacao = (SELECT max(sequencia_alocacao)-1 FROM alocacao WHERE funcionario_id = al.funcionario_id)) AS ObraOrigem,
	            obre.nome_obra AS ObraDestino,
	            siob.sigla_situacao AS SituacaoObra,
                fe.termino_ferias AS TerminoFerias
            FROM alocacao al
	            LEFT JOIN funcionario fu ON fu.id = al.funcionario_id
	            LEFT JOIN situacao sifu ON sifu.id = fu.situacao_id
	            LEFT JOIN cargo ca ON ca.id = fu.cargo_id
	            LEFT JOIN custo cu ON cu.id = ca.custo_id
	            LEFT JOIN obra ob ON ob.id = al.obra_id
	            LEFT JOIN situacao siob ON siob.id = ob.situacao_id
	            LEFT JOIN reserva re ON re.funcionario_id = al.funcionario_id
	            LEFT JOIN obra obre ON obre.id = re.obra_id
                LEFT JOIN ferias fe ON fe.funcionario_id = fu.id
            WHERE al.sequencia_alocacao = (SELECT max(sequencia_alocacao) FROM alocacao WHERE funcionario_id = al.funcionario_id)
                AND sifu.id <> 4";

        public const string ListarAlocacoesFiltroObras = @"
                AND ob.id IN @Obras";

        public const string ListarAlocacoesFiltroCargos = @"
                AND ca.id IN @Cargos";

        public const string IncluirAlocacao = @"
            INSERT INTO alocacao (funcionario_id, obra_id, equipe_id, inicio_alocacao, termino_alocacao, sequencia_alocacao)
            OUTPUT Inserted.ID            
            VALUES (@IdFuncionario, @IdObra, NULL, @DataAlocacao, NULL, @SequenciaAlocacao)";

        public const string FinalizarAlocacao = @"
            UPDATE alocacao
            SET termino_alocacao = @DataTermino
            WHERE funcionario_id = @IdFuncionario
	            AND sequencia_alocacao = @SequenciaAlocacao";

        public const string IncluirReserva = @"
            INSERT INTO reserva (obra_id, funcionario_id, inicio_reserva)
            OUTPUT Inserted.ID            
            VALUES (@IdObra, @IdFuncionario, @DataReserva)";

        public const string ExcluirReserva = @"
            DELETE FROM reserva
            WHERE funcionario_id = @IdFuncionario";

        public const string ObterAlocacaoPorFuncionario = @"
            SELECT
	            al.id AS IdAlocacao,
	            al.sequencia_alocacao AS SequenciaAlocacao,
	            fu.id AS IdFuncionario,
	            fu.chapa_funcionario AS ChapaFuncionario,
	            fu.nome_funcionario AS NomeFuncionario,
                sifu.nome_situacao AS SituacaoFuncionario,
                sifu.sigla_situacao AS SiglaSituacaoFuncionario,
	            cu.nome_custo AS NomeCusto,
	            ca.nome_cargo AS NomeCargo,
	            ob.id as IdObra,
                ob.codigo_obra as CodigoObra,
                ob.nome_obra AS NomeObra,
	            al.inicio_alocacao AS InicioAlocacao,
	            re.inicio_reserva AS InicioReserva,
	            (SELECT obsu.nome_obra
		            FROM alocacao alsu
			            LEFT JOIN obra obsu ON obsu.id = alsu.obra_id
		            WHERE alsu.funcionario_id = al.funcionario_id
			            AND alsu.sequencia_alocacao = (SELECT max(sequencia_alocacao)-1 FROM alocacao WHERE funcionario_id = al.funcionario_id)) AS ObraOrigem,
	            obre.nome_obra AS ObraDestino,
	            siob.sigla_situacao AS SituacaoObra
            FROM alocacao al
	            LEFT JOIN funcionario fu ON fu.id = al.funcionario_id
	            LEFT JOIN situacao sifu ON sifu.id = fu.situacao_id
	            LEFT JOIN cargo ca ON ca.id = fu.cargo_id
	            LEFT JOIN custo cu ON cu.id = ca.custo_id
	            LEFT JOIN obra ob ON ob.id = al.obra_id
	            LEFT JOIN situacao siob ON siob.id = ob.situacao_id
	            LEFT JOIN reserva re ON re.funcionario_id = al.funcionario_id
	            LEFT JOIN obra obre ON obre.id = re.obra_id
            WHERE al.sequencia_alocacao = (SELECT max(sequencia_alocacao) FROM alocacao WHERE funcionario_id = al.funcionario_id)
                AND fu.id = @IdFuncionario";

        public const string ExcluirAlocacao = @"
            DELETE FROM alocacao
            WHERE funcionario_id = @IdFuncionario
	            AND sequencia_alocacao = @SequenciaAlocacao";

        public const string ReativarAlocacao = @"
            UPDATE alocacao
            SET termino_alocacao = NULL
            WHERE funcionario_id = @IdFuncionario
	            AND sequencia_alocacao = @SequenciaAlocacao";

        public const string VerificarDestino = @"
            SELECT count(1)
            FROM reserva
            WHERE funcionario_id = @IdFuncionario";
    }
}