namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeContrato
	{
		public const string ListarContratos = @"
			SELECT
                c.id AS IdContrato,
                c.codigo_contrato AS CodigoContrato,
                c.data_contrato AS DataContrato,
	            sum(c.valor_contrato) AS ValorContrato,
                sum(isnull(a.valor_aditivo, 0)) AS ValorAditivo,
                sum(c.prazo_contrato) AS PrazoContrato,
				sum(isnull(a.prazo_aditivo, 0)) AS PrazoAditivo,
                l.id AS IdCliente,
                l.cnpj_cliente AS CnpjCliente,
                l.razao_social_cliente AS RazaoSocialCliente,
                l.nome_cliente AS NomeCliente
            FROM contrato c
	            LEFT JOIN aditivo a ON a.contrato_id = c.id
                LEFT JOIN cliente l ON l.id = c.cliente_id";

		public const string ListarContratosFiltro = @"
			WHERE (upper(c.codigo_contrato) LIKE upper(@CodigoOuNome + '%')
                OR upper(l.razao_social_cliente) LIKE upper(@CodigoOuNome + '%')
                OR upper(l.nome_cliente) LIKE upper(@CodigoOuNome + '%'))";

        public const string ListarContratosGrupo = @"
            GROUP BY c.id, c.codigo_contrato, c.data_contrato, l.id, l.cnpj_cliente, l.razao_social_cliente, l.nome_cliente";


        public const string ObterContrato = @"
			SELECT 
	            c.id AS IdContrato,
	            c.codigo_contrato AS CodigoContrato,
	            c.data_contrato AS DataContrato,
	            c.valor_contrato AS ValorContrato,
	            c.prazo_contrato AS PrazoContrato,
	            c.escopo_contrato AS EscopoContrato,
	            l.id AS IdCliente,
                l.cnpj_cliente AS CnpjCliente,
                l.razao_social_cliente AS RazaoSocialCliente,
                l.nome_cliente AS NomeCliente
            FROM contrato c
	            LEFT JOIN cliente l ON l.id = c.cliente_id
            WHERE c.id = @IdContrato";

		public const string IncluirContrato = @"
			INSERT INTO contrato (cliente_id, codigo_contrato, data_contrato, valor_contrato, prazo_contrato, escopo_contrato)
            OUTPUT Inserted.ID
            VALUES (@IdCliente, @CodigoContrato, @DataContrato, @ValorContrato, @PrazoContrato, @EscopoContrato)";

		public const string EditarContrato = @"
			UPDATE contrato
            SET escopo_contrato = @EscopoContrato
            WHERE id = @IdContrato";

		public const string ExcluirContrato = @"
			DELETE FROM contrato
            WHERE id = @IdContrato";
	}
}