namespace PR.Building4U.Repositorios.Scripts
{
	public static class ScriptsDeCusto
	{
		public const string ListarCustos = @"
			SELECT 
	            id AS IdCusto,
	            sigla_custo AS SiglaCusto,
	            nome_custo AS NomeCusto
            FROM custo";

        public const string ListarCustosFiltro = @"
            WHERE (upper(sigla_custo) LIKE upper(@CodigoOuNome)
		        OR upper(nome_custo) LIKE upper(@CodigoOuNome + '%'))";

        public const string ObterCusto = @"
			SELECT 
	            id AS IdCusto,
	            sigla_custo AS SiglaCusto,
	            nome_custo AS NomeCusto
            FROM custo
            WHERE id = @IdCusto";

		public const string IncluirCusto = @"
			INSERT INTO custo (sigla_custo, nome_custo)
            OUTPUT Inserted.ID
            VALUES (upper(@SiglaCusto), @NomeCusto)";

		public const string EditarCusto = @"
			UPDATE custo
            SET nome_custo = @NomeCusto
            WHERE id = @IdCusto";

		public const string ExcluirCusto = @"
			DELETE FROM custo
            WHERE id = @IdCusto";
	}
}