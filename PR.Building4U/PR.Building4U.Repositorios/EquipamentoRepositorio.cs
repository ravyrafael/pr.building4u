using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using System;

namespace PR.Building4U.Repositorios
{
	public class EquipamentoRepositorio : IEquipamentoRepositorio
	{
		public IEnumerable<Equipamento> ListarEquipamentos(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
				var sql = ScriptsDeEquipamento.ListarEquipamentos;
                var parametros = new DynamicParameters();

				if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeEquipamento.ListarEquipamentosFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<REquipamento, RTipoEquipamento, RSituacao, REquipamento>(sql,
                    (equipamento, tipoEquipamento, situacao) =>
                    {
                        equipamento.TipoEquipamento = tipoEquipamento;
                        equipamento.Situacao = situacao;
                        return equipamento;
                    }, parametros, splitOn: "IdEquipamento, IdTipoEquipamento, IdSituacao");
            }
        }

		public Equipamento ObterEquipamento(int idEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);

                return connection.Query<REquipamento, RTipoEquipamento, RSituacao, REquipamento>(ScriptsDeEquipamento.ObterEquipamento,
                    (equipamento, tipoEquipamento, situacao) =>
                    {
                        equipamento.TipoEquipamento = tipoEquipamento;
                        equipamento.Situacao = situacao;
                        return equipamento;
                    }, parametros, splitOn: "IdEquipamento, IdTipoEquipamento, IdSituacao").FirstOrDefault();
            }
        }

		public int IncluirEquipamento(string marcaEquipamento, string modeloEquipamento, string patrimonioEquipamento, int idTipoEquipamento, bool indicadorTerceiro, string fornecedorEquipamento, DateTime aquisicaoEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("MarcaEquipamento", marcaEquipamento, DbType.AnsiString);
                parametros.Add("ModeloEquipamento", modeloEquipamento, DbType.AnsiString);
                parametros.Add("PatrimonioEquipamento", patrimonioEquipamento, DbType.AnsiString);
                parametros.Add("IdTipoEquipamento", idTipoEquipamento, DbType.Int32);
                parametros.Add("IndicadorTerceiro", indicadorTerceiro, DbType.Boolean);
                parametros.Add("FornecedorEquipamento", fornecedorEquipamento, DbType.AnsiString);
                parametros.Add("AquisicaoEquipamento", aquisicaoEquipamento, DbType.DateTime);

                return connection.Query<int>(ScriptsDeEquipamento.IncluirEquipamento, parametros).FirstOrDefault();
            }
        }

		public int EditarEquipamento(int idEquipamento, string marcaEquipamento, string modeloEquipamento, string fornecedorEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);
                parametros.Add("MarcaEquipamento", marcaEquipamento, DbType.AnsiString);
                parametros.Add("ModeloEquipamento", modeloEquipamento, DbType.AnsiString);
                parametros.Add("FornecedorEquipamento", fornecedorEquipamento, DbType.AnsiString);

                return connection.Execute(ScriptsDeEquipamento.EditarEquipamento, parametros);
            }
        }

		public int SucatearEquipamento(int idEquipamento, DateTime sucataEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);
                parametros.Add("SucataEquipamento", sucataEquipamento, DbType.DateTime);

                return connection.Execute(ScriptsDeEquipamento.SucatearEquipamento, parametros);
            }
        }

        public int IniciarManutencaoEquipamento(int idEquipamento, DateTime inicioManutencao, DateTime terminoManutencao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);
                parametros.Add("InicioManutencao", inicioManutencao, DbType.DateTime);
                parametros.Add("TerminoManutencao", terminoManutencao, DbType.DateTime);

                return connection.Execute(ScriptsDeEquipamento.IniciarManutencaoEquipamento, parametros);
            }
        }

        public int TerminarManutencaoEquipamento(int idEquipamento, DateTime terminoManutencao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);
                parametros.Add("TerminoManutencao", terminoManutencao, DbType.DateTime);

                return connection.Execute(ScriptsDeEquipamento.TerminarManutencaoEquipamento, parametros);
            }
        }

        public int AlterarSituacaoEquipamento(int idEquipamento, int situacaoEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);
                parametros.Add("SituacaoEquipamento", situacaoEquipamento, DbType.Int32);

                return connection.Execute(ScriptsDeEquipamento.AlterarSituacaoEquipamento, parametros);
            }
        }

        public Manutencao ObterManutencaoEquipamento(int idEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);

                return connection.Query<RManutencao, REquipamento, RManutencao>(ScriptsDeEquipamento.ObterManutencaoEquipamento,
                    (manutencao, equipamento) =>
                    {
                        manutencao.Equipamento = equipamento;
                        return manutencao;
                    }, parametros, splitOn: "IdManutencao, IdEquipamento").FirstOrDefault();
            }
        }
    }
}