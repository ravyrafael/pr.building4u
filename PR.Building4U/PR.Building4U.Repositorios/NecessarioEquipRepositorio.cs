using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;

namespace PR.Building4U.Repositorios
{
	public class NecessarioEquipRepositorio : INecessarioEquipRepositorio
	{
        public int CancelarNecessarioAprovPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeNecessarioEquip.CancelarNecessarioAprovPorObra, parametros);
            }
        }

        public int ExcluirNecessarioAprovPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeNecessarioEquip.ExcluirNecessarioAprovPorObra, parametros);
            }
        }

        public int IncluirNecessarioAprovPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeNecessarioEquip.IncluirNecessarioAprovPorObra, parametros);
            }
        }

        public IEnumerable<NecessarioEquip> ListarNecessarioAprovPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Query<RNecessarioEquip, RObra, RTipoEquipamento, RCusto, RNecessarioEquip>(ScriptsDeNecessarioEquip.ListarNecessarioAprovPorObra,
                   (necessario, obra, tipoEquipamento, custo) =>
                   {
                       tipoEquipamento.Custo = custo;
                       necessario.TipoEquipamento = tipoEquipamento;
                       necessario.Obra = obra;
                       return necessario;
                   }, parametros, splitOn: "IdNecessario, IdObra, IdTipoEquipamento, IdCusto");
            }
        }

        public bool VerificarNecessarioAprov(int idObra, int idTipoEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("IdTipoEquipamento", idTipoEquipamento, DbType.Int32);

                return connection.Query<bool>(ScriptsDeNecessarioEquip.VerificarNecessarioAprov, parametros).FirstOrDefault();
            }
        }

        public int IncluirNecessarioAprov(int idObra, int idTipoEquipamento, int quantidadeNecessario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("IdTipoEquipamento", idTipoEquipamento, DbType.Int32);
                parametros.Add("QuantidadeNecessario", quantidadeNecessario, DbType.Int32);

                return connection.Execute(ScriptsDeNecessarioEquip.IncluirNecessarioAprov, parametros);
            }
        }

        public int AtualizarNecessarioAprov(int idObra, int idTipoEquipamento, int quantidadeNecessario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("IdTipoEquipamento", idTipoEquipamento, DbType.Int32);
                parametros.Add("QuantidadeNecessario", quantidadeNecessario, DbType.Int32);

                return connection.Execute(ScriptsDeNecessarioEquip.AtualizarNecessarioAprov, parametros);
            }
        }

        public int ExcluirNecessarioAprov(int idObra, int idTipoEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("IdTipoEquipamento", idTipoEquipamento, DbType.Int32);

                return connection.Execute(ScriptsDeNecessarioEquip.ExcluirNecessarioAprov, parametros);
            }
        }

        public int EnviarNecessarioAprovPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeNecessarioEquip.EnviarNecessarioAprovPorObra, parametros);
            }
        }

        public IEnumerable<NecessarioEquipAprov> ListarNecessarioPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Query<RNecessarioEquipAprov, RTipoEquipamento, RCusto, RNecessarioEquipAprov>(ScriptsDeNecessarioEquip.ListarNecessarioPorObra,
                   (necessario, tipoEquipamento, custo) =>
                   {
                       tipoEquipamento.Custo = custo;
                       necessario.TipoEquipamento = tipoEquipamento;
                       return necessario;
                   }, parametros, splitOn: "IdNecessario, IdTipoEquipamento, IdCusto");
            }
        }

        public int ExcluirNecessarioPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeNecessarioEquip.ExcluirNecessarioPorObra, parametros);
            }
        }

        public int IncluirNecessarioPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeNecessarioEquip.IncluirNecessarioPorObra, parametros);
            }
        }

        public int VerificarNecessarioAprovPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Query<int>(ScriptsDeNecessarioEquip.VerificarNecessarioAprovPorObra, parametros).FirstOrDefault();
            }
        }
    }
}