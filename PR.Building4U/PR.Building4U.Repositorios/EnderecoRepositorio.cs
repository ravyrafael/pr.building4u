﻿using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using PR.Building4U.Repositorios.Scripts;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace PR.Building4U.Repositorios
{
    public class EnderecoRepositorio : IEnderecoRepositorio
    {
        public IEnumerable<Endereco> ListarEnderecosPorMunicipio(int idMunicipio)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdMunicipio", idMunicipio, DbType.Int32);

                return connection.Query<REndereco, RMunicipio, REstado, RPais, REndereco>(ScriptsDeEndereco.ListarEnderecosPorMunicipio, (endereco, municipio, estado, pais) =>
                {
                    estado.Pais = pais;
                    municipio.Estado = estado;
                    endereco.Municipio = municipio;
                    return endereco;
                }, parametros, splitOn: "IdMunicipio, IdEstado, IdPais");
            }
        }

        public int IncluirEndereco(string logradouroEndereco, string numeroEndereco, string complementoEndereco, string cepEndereco, string distritoEndereco, int idMunicipio)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("LogradouroEndereco", logradouroEndereco, DbType.AnsiString);
                parametros.Add("NumeroEndereco", numeroEndereco, DbType.AnsiString);
                parametros.Add("ComplementoEndereco", complementoEndereco, DbType.AnsiString);
                parametros.Add("CepEndereco", cepEndereco, DbType.AnsiString);
                parametros.Add("DistritoEndereco", distritoEndereco, DbType.AnsiString);
                parametros.Add("IdMunicipio", idMunicipio, DbType.Int32);

                return connection.Query<int>(ScriptsDeEndereco.IncluirEndereco, parametros).FirstOrDefault();
            }
        }

        public int EditarEndereco(int idEndereco, string logradouroEndereco, string numeroEndereco, string complementoEndereco, string cepEndereco, string distritoEndereco, int idMunicipio)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEndereco", idEndereco, DbType.Int32);
                parametros.Add("LogradouroEndereco", logradouroEndereco, DbType.AnsiString);
                parametros.Add("NumeroEndereco", numeroEndereco, DbType.AnsiString);
                parametros.Add("ComplementoEndereco", complementoEndereco, DbType.AnsiString);
                parametros.Add("CepEndereco", cepEndereco, DbType.AnsiString);
                parametros.Add("DistritoEndereco", distritoEndereco, DbType.AnsiString);
                parametros.Add("IdMunicipio", idMunicipio, DbType.Int32);

                return connection.Execute(ScriptsDeEndereco.EditarEndereco, parametros);
            }
        }
    }
}
