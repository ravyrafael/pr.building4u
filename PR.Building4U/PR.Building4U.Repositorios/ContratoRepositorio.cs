using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using System;

namespace PR.Building4U.Repositorios
{
	public class ContratoRepositorio : IContratoRepositorio
	{
		public IEnumerable<Contrato> ListarContratos(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
				var sql = ScriptsDeContrato.ListarContratos;
                var parametros = new DynamicParameters();

				if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeContrato.ListarContratosFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                sql += ScriptsDeContrato.ListarContratosGrupo;

                return connection.Query<RContrato, RCliente, RContrato>(sql,
                    (contrato, cliente) =>
                    {
                        contrato.Cliente = cliente;
                        return contrato;
                    }, parametros, splitOn: "IdContrato, IdCliente");
            }
        }

		public Contrato ObterContrato(int idContrato)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdContrato", idContrato, DbType.Int32);

                return connection.Query<RContrato, RCliente, RContrato>(ScriptsDeContrato.ObterContrato,
                    (contrato, cliente) =>
                    {
                        contrato.Cliente = cliente;
                        return contrato;
                    }, parametros, splitOn: "IdContrato, IdCliente").FirstOrDefault();
            }
        }

		public int IncluirContrato(int idCliente, string codigoContrato, DateTime dataContrato, decimal valorContrato, int prazoContrato, string escopoContrato)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCliente", idCliente, DbType.Int32);
                parametros.Add("CodigoContrato", codigoContrato, DbType.AnsiString);
                parametros.Add("DataContrato", dataContrato, DbType.DateTime);
                parametros.Add("ValorContrato", valorContrato, DbType.Decimal);
                parametros.Add("PrazoContrato", prazoContrato, DbType.Int32);
                parametros.Add("EscopoContrato", escopoContrato, DbType.AnsiString);

                return connection.Query<int>(ScriptsDeContrato.IncluirContrato, parametros).FirstOrDefault();
            }
        }

		public int EditarContrato(int idContrato, string escopoContrato)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdContrato", idContrato, DbType.Int32);
                parametros.Add("EscopoContrato", escopoContrato, DbType.AnsiString);

                return connection.Execute(ScriptsDeContrato.EditarContrato, parametros);
            }
        }

		public int ExcluirContrato(int idContrato)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdContrato", idContrato, DbType.Int32);

                return connection.Execute(ScriptsDeContrato.ExcluirContrato, parametros);
            }
        }
	}
}