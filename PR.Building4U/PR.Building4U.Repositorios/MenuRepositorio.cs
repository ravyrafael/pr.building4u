﻿using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using PR.Building4U.Repositorios.Scripts;
using System.Collections.Generic;
using System.Data;

namespace PR.Building4U.Repositorios
{
    public class MenuRepositorio : IMenuRepositorio
    {
        public IEnumerable<Recurso> ObterMenuPorPermissao(int permissaoUsuario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("PermissaoUsuario", permissaoUsuario, DbType.Int32);

                return connection.Query<RRecurso>(ScriptsDeMenu.ObterMenuPorPermissao, parametros);
            }
        }

        public IEnumerable<Recurso> ObterSubMenuPorPermissao(int permissaoUsuario, int menuPai)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("PermissaoUsuario", permissaoUsuario, DbType.Int32);
                parametros.Add("MenuPai", menuPai, DbType.Int32);

                return connection.Query<RRecurso>(ScriptsDeMenu.ObterSubMenuPorPermissao, parametros);
            }
        }
    }
}
