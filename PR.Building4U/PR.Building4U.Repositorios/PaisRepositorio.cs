﻿using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using PR.Building4U.Repositorios.Scripts;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace PR.Building4U.Repositorios
{
    public class PaisRepositorio : IPaisRepositorio
    {
        public IEnumerable<Pais> ListarPaises(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDePais.ListarPaises;
                var parametros = new DynamicParameters();

                if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDePais.ListarPaisesFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RPais>(sql, parametros);
            }
        }

        public Pais ObterPais(int idPais)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdPais", idPais, DbType.Int32);

                return connection.Query<RPais>(ScriptsDePais.ObterPais, parametros).FirstOrDefault();
            }
        }

        public int IncluirPais(int codigoPais, string nomePais)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("CodigoPais", codigoPais, DbType.Int32);
                parametros.Add("NomePais", nomePais, DbType.AnsiString);

                return connection.Query<int>(ScriptsDePais.IncluirPais, parametros).FirstOrDefault();
            }
        }

        public int EditarPais(int idPais, string nomePais)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdPais", idPais, DbType.Int32);
                parametros.Add("NomePais", nomePais, DbType.AnsiString);

                return connection.Execute(ScriptsDePais.EditarPais, parametros);
            }
        }

        public int ExcluirPais(int idPais)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdPais", idPais, DbType.Int32);

                return connection.Execute(ScriptsDePais.ExcluirPais, parametros);
            }
        }
    }
}
