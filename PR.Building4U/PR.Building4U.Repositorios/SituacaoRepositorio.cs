﻿using System.Data;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using Dapper;
using PR.Building4U.Entidades;
using System.Collections.Generic;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using System.Linq;

namespace PR.Building4U.Repositorios 
{
	public class SituacaoRepositorio : ISituacaoRepositorio
	{
        public IEnumerable<Situacao> ListarSituacoes(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeSituacao.ListarSituacoes;
                var parametros = new DynamicParameters();

                if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeSituacao.ListarSituacoesFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RSituacao>(sql, parametros);
            }
        }

        public Situacao ObterSituacao(int idSituacao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdSituacao", idSituacao, DbType.Int32);

                return connection.Query<RSituacao>(ScriptsDeSituacao.ObterSituacao, parametros).FirstOrDefault();
            }
        }

        public int EditarSituacao(int idSituacao, string nomeSituacao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdSituacao", idSituacao, DbType.Int32);
                parametros.Add("NomeSituacao", nomeSituacao, DbType.AnsiString);

                return connection.Execute(ScriptsDeSituacao.EditarSituacao, parametros);
            }
        }
    }
}