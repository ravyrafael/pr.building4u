using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;

namespace PR.Building4U.Repositorios
{
	public class AreaRepositorio : IAreaRepositorio
	{
		public IEnumerable<Area> ListarAreas(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
				var sql = ScriptsDeArea.ListarAreas;
                var parametros = new DynamicParameters();

				if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeArea.ListarAreasFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RArea>(sql, parametros);
            }
        }

		public Area ObterArea(int idArea)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdArea", idArea, DbType.Int32);

                return connection.Query<RArea>(ScriptsDeArea.ObterArea, parametros).FirstOrDefault();
            }
        }

		public int EditarArea(int idArea, string nomeArea)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdArea", idArea, DbType.Int32);
                parametros.Add("NomeArea", nomeArea, DbType.AnsiString);

                return connection.Execute(ScriptsDeArea.EditarArea, parametros);
            }
        }
	}
}