﻿using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using PR.Building4U.Repositorios.Scripts;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace PR.Building4U.Repositorios
{
    public class EstadoRepositorio : IEstadoRepositorio
    {
        public IEnumerable<Estado> ListarEstados(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeEstado.ListarEstados;
                var parametros = new DynamicParameters();

                if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeEstado.ListarEstadosFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<REstado, RPais, REstado>(sql, (estado, pais) =>
                {
                    estado.Pais = pais;
                    return estado;
                }, parametros, splitOn: "IdPais");
            }
        }

        public Estado ObterEstado(int idEstado)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEstado", idEstado, DbType.Int32);

                return connection.Query<REstado, RPais, REstado>(ScriptsDeEstado.ObterEstado, (estado, pais) =>
                {
                    estado.Pais = pais;
                    return estado;
                }, parametros, splitOn: "IdPais").FirstOrDefault();
            }
        }

        public int IncluirEstado(string siglaEstado, string nomeEstado, int idPais)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("SiglaEstado", siglaEstado, DbType.AnsiString);
                parametros.Add("NomeEstado", nomeEstado, DbType.AnsiString);
                parametros.Add("IdPais", idPais, DbType.Int32);

                return connection.Query<int>(ScriptsDeEstado.IncluirEstado, parametros).FirstOrDefault();
            }
        }

        public int EditarEstado(int idEstado, string nomeEstado, int idPais)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEstado", idEstado, DbType.Int32);
                parametros.Add("NomeEstado", nomeEstado, DbType.AnsiString);
                parametros.Add("IdPais", idPais, DbType.Int32);

                return connection.Execute(ScriptsDeEstado.EditarEstado, parametros);
            }
        }

        public int ExcluirEstado(int idEstado)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEstado", idEstado, DbType.Int32);

                return connection.Execute(ScriptsDeEstado.ExcluirEstado, parametros);
            }
        }

        public IEnumerable<Estado> ListarEstadosPorPais (int idPais, string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeEstado.ListarEstadosPorPais;
                var parametros = new DynamicParameters();
                parametros.Add("IdPais", idPais, DbType.Int32);

                if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeEstado.ListarEstadosPorPaisFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<REstado, RPais, REstado>(sql, (estado, pais) =>
                {
                    estado.Pais = pais;
                    return estado;
                }, parametros, splitOn: "IdPais");
            }
        }
    }
}
