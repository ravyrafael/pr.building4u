using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using System;

namespace PR.Building4U.Repositorios
{
	public class FuncionarioRepositorio : IFuncionarioRepositorio
	{
		public IEnumerable<Funcionario> ListarFuncionarios(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
				var sql = ScriptsDeFuncionario.ListarFuncionarios;
                var parametros = new DynamicParameters();

				if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeFuncionario.ListarFuncionariosFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RFuncionario, RCargo, RSituacao, RFuncionario>
                    (sql, (funcionario, cargo, situacao) =>
                    {
                        funcionario.Situacao = situacao;
                        funcionario.Cargo = cargo;
                        return funcionario;
                    }, parametros, splitOn: "IdCargo, IdSituacao");
            }
        }

		public Funcionario ObterFuncionario(int idFuncionario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);

                return connection.Query<RFuncionario, RCargo, RSituacao, REndereco, RMunicipio, REstado, RPais, RFuncionario>
                    (ScriptsDeFuncionario.ObterFuncionario, (funcionario, cargo, situacao, endereco, municipio, estado, pais) =>
                    {
                        estado.Pais = pais;
                        municipio.Estado = estado;
                        endereco.Municipio = municipio;
                        funcionario.Endereco = endereco;
                        funcionario.Situacao = situacao;
                        funcionario.Cargo = cargo;
                        return funcionario;
                    }, parametros, splitOn: "IdCargo, IdSituacao, IdEndereco, IdMunicipio, IdEstado, IdPais").FirstOrDefault();
            }
        }

		public int IncluirFuncionario(string chapaFuncionario, string nomeFuncionario, DateTime admissaoFuncionario, string emailFuncionario, string telefoneFuncionario, int idEndereco, int idCargo)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("ChapaFuncionario", chapaFuncionario, DbType.AnsiString);
                parametros.Add("NomeFuncionario", nomeFuncionario, DbType.AnsiString);
                parametros.Add("AdmissaoFuncionario", admissaoFuncionario, DbType.DateTime);
                parametros.Add("EmailFuncionario", emailFuncionario, DbType.AnsiString);
                parametros.Add("TelefoneFuncionario", telefoneFuncionario, DbType.AnsiString);
                parametros.Add("IdEndereco", idEndereco, DbType.Int32);
                parametros.Add("IdCargo", idCargo, DbType.Int32);

                return connection.Query<int>(ScriptsDeFuncionario.IncluirFuncionario, parametros).FirstOrDefault();
            }
        }

		public int EditarFuncionario(int idFuncionario, string nomeFuncionario, string emailFuncionario, string telefoneFuncionario, int idCargo)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);
                parametros.Add("NomeFuncionario", nomeFuncionario, DbType.AnsiString);
                parametros.Add("EmailFuncionario", emailFuncionario, DbType.AnsiString);
                parametros.Add("TelefoneFuncionario", telefoneFuncionario, DbType.AnsiString);
                parametros.Add("IdCargo", idCargo, DbType.Int32);

                return connection.Execute(ScriptsDeFuncionario.EditarFuncionario, parametros);
            }
        }

		public int DemitirFuncionario(int idFuncionario, DateTime demissaoFuncionario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);
                parametros.Add("DemissaoFuncionario", demissaoFuncionario, DbType.DateTime);

                return connection.Execute(ScriptsDeFuncionario.DemitirFuncionario, parametros);
            }
        }

        public int IniciarFeriasFuncionario(int idFuncionario, DateTime inicioFerias, DateTime terminoFerias)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);
                parametros.Add("InicioFerias", inicioFerias, DbType.DateTime);
                parametros.Add("TerminoFerias", terminoFerias, DbType.DateTime);

                return connection.Execute(ScriptsDeFuncionario.IniciarFeriasFuncionario, parametros);
            }
        }

        public int TerminarFeriasFuncionario(int idFuncionario, DateTime terminoFerias)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);
                parametros.Add("TerminoFerias", terminoFerias, DbType.DateTime);

                return connection.Execute(ScriptsDeFuncionario.TerminarFeriasFuncionario, parametros);
            }
        }

        public int AlterarSituacaoFuncionario(int idFuncionario, int situacaoFuncionario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);
                parametros.Add("SituacaoFuncionario", situacaoFuncionario, DbType.Int32);

                return connection.Execute(ScriptsDeFuncionario.AlterarSituacaoFuncionario, parametros);
            }
        }

        public Ferias ObterFeriasFuncionario(int idFuncionario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);

                return connection.Query<RFerias, RFuncionario, RFerias>(ScriptsDeFuncionario.ObterFeriasFuncionario,
                    (ferias, funcionario) =>
                    {
                        ferias.Funcionario = funcionario;
                        return ferias;
                    }, parametros, splitOn: "IdFerias, IdFuncionario").FirstOrDefault();
            }
        }
    }
}