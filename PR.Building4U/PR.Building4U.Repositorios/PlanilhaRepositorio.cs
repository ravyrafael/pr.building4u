using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using System;

namespace PR.Building4U.Repositorios
{
	public class PlanilhaRepositorio : IPlanilhaRepositorio
	{
		public IEnumerable<Planilha> ListarPlanilhas(int idCronograma)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronograma", idCronograma, DbType.Int32);

                return connection.Query<RPlanilha>(ScriptsDePlanilha.ListarPlanilhas, parametros);
            }
        }

        public Planilha ObterPlanilha(int idPlanilha)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdPlanilha", idPlanilha, DbType.Int32);

                return connection.Query<RPlanilha>(ScriptsDePlanilha.ObterPlanilha, parametros).FirstOrDefault();
            }
        }

        public IEnumerable<PlanilhaItem> ListarItens(int idPlanilha)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdPlanilha", idPlanilha, DbType.Int32);

                return connection.Query<RPlanilhaItem, RUnidade, RPlanilhaItem>(ScriptsDePlanilha.ListarItens,
                    (item, unidade) =>
                    {
                        item.Unidade = unidade;
                        return item;
                    }, parametros, splitOn: "IdPlanilhaItem, IdUnidade");
            }
        }

        public IEnumerable<PlanilhaItemMedicao> ListarItensMedicao(int idPlanilhaItem)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdPlanilhaItem", idPlanilhaItem, DbType.Int32);

                return connection.Query<RPlanilhaItemMedicao>(ScriptsDePlanilha.ListarItensMedicao, parametros);
            }
        }

        public int AtualizarItem(int idPlanilhaItem, decimal? quantidadeAcertoItem, bool ehItemControle, int? idCronogramaServico)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdPlanilhaItem", idPlanilhaItem, DbType.Int32);
                parametros.Add("QuantidadeAcertoItem", quantidadeAcertoItem, DbType.Decimal);
                parametros.Add("EhItemControle", ehItemControle, DbType.Boolean);
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);

                return connection.Execute(ScriptsDePlanilha.AtualizarItem, parametros);
            }
        }

        public int IncluirPlanilha(int idCronograma, string cpPlanilha, int revPlanilha)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronograma", idCronograma, DbType.Int32);
                parametros.Add("CpPlanilha", cpPlanilha, DbType.AnsiString);
                parametros.Add("RevPlanilha", revPlanilha, DbType.Int32);

                return connection.Query<int>(ScriptsDePlanilha.IncluirPlanilha, parametros).FirstOrDefault();
            }
        }

        public int IncluirItem(int? idCronogramaServico, int idPlanilha, int? idUnidade, string codigoItem, string descricaoItem, decimal? precoItem, decimal? quantidadeItem, decimal? quantidadeAcertoItem, bool ehTituloItem, bool ehItemControle)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);
                parametros.Add("IdPlanilha", idPlanilha, DbType.Int32);
                parametros.Add("IdUnidade", idUnidade, DbType.Int32);
                parametros.Add("CodigoItem", codigoItem, DbType.AnsiString);
                parametros.Add("DescricaoItem", descricaoItem, DbType.AnsiString);
                parametros.Add("PrecoItem", precoItem, DbType.Decimal);
                parametros.Add("QuantidadeItem", quantidadeItem, DbType.Decimal);
                parametros.Add("QuantidadeAcertoItem", quantidadeAcertoItem, DbType.Decimal);
                parametros.Add("EhTituloItem", ehTituloItem, DbType.Boolean);
                parametros.Add("EhItemControle", ehItemControle, DbType.Boolean);

                return connection.Query<int>(ScriptsDePlanilha.IncluirItem, parametros).FirstOrDefault();
            }
        }

        public int ExcluirPlanilha(int idPlanilha)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdPlanilha", idPlanilha, DbType.Int32);

                return connection.Execute(ScriptsDePlanilha.ExcluirPlanilha, parametros);
            }
        }

        public int ExcluirItens(int idPlanilha)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdPlanilha", idPlanilha, DbType.Int32);

                return connection.Execute(ScriptsDePlanilha.ExcluirItens, parametros);
            }
        }

        public decimal? SomarValorMedicao(int idCronogramaServico, DateTime mesReferencia)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);
                parametros.Add("MesReferencia", mesReferencia, DbType.DateTime);

                return connection.Query<decimal?>(ScriptsDePlanilha.SomarValorMedicao, parametros).FirstOrDefault();
            }
        }

        public decimal? SomarQuantidadeMedicao(int idCronogramaServico, DateTime mesReferencia)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);
                parametros.Add("MesReferencia", mesReferencia, DbType.DateTime);

                return connection.Query<decimal?>(ScriptsDePlanilha.SomarQuantidadeMedicao, parametros).FirstOrDefault();
            }
        }

        public bool VerificarMedicao(int idCronograma, DateTime mesReferencia)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronograma", idCronograma, DbType.Int32);
                parametros.Add("MesReferencia", mesReferencia, DbType.DateTime);

                return connection.Query<int>(ScriptsDePlanilha.VerificarMedicao, parametros).FirstOrDefault() > 0;
            }
        }

        public IEnumerable<Medicao> ListarMedicoes(int idPlanilha)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdPlanilha", idPlanilha, DbType.Int32);

                return connection.Query<RMedicao>(ScriptsDePlanilha.ListarMedicoes, parametros);
            }
        }

        public int IncluirMedicao(int idPlanilhaItem, DateTime periodoMedicao, decimal quantidadeMedicao, decimal valorMedicao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdPlanilhaItem", idPlanilhaItem, DbType.Int32);
                parametros.Add("PeriodoMedicao", periodoMedicao, DbType.DateTime);
                parametros.Add("QuantidadeMedicao", quantidadeMedicao, DbType.Decimal);
                parametros.Add("ValorMedicao", valorMedicao, DbType.Decimal);

                return connection.Query<int>(ScriptsDePlanilha.IncluirMedicao, parametros).FirstOrDefault();
            }
        }

        public int ExcluirMedicao(int idPlanilha, DateTime periodoMedicao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdPlanilha", idPlanilha, DbType.Int32);
                parametros.Add("PeriodoMedicao", periodoMedicao, DbType.DateTime);

                return connection.Execute(ScriptsDePlanilha.ExcluirMedicao, parametros);
            }
        }
    }
}