﻿using Dapper;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Util;
using System.Data;

namespace PR.Building4U.Repositorios
{
    public class LogRepositorio : ILogRepositorio
    {
        public int IncluirLog(InformacoesLog informacoesLog)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdUsuario", informacoesLog.IdUsuario, DbType.AnsiString);
                parametros.Add("LoginUsuario", informacoesLog.LoginUsuario, DbType.AnsiString);
                parametros.Add("Controller", informacoesLog.Controller, DbType.AnsiString);
                parametros.Add("Action", informacoesLog.Action, DbType.AnsiString);
                parametros.Add("Descricao", informacoesLog.Descricao, DbType.AnsiString);
                parametros.Add("DataLog", informacoesLog.DataLog, DbType.DateTime);
                parametros.Add("TipoLog", informacoesLog.TipoLog, DbType.Int32);

                return connection.Execute(ScriptsDeLog.IncluirLog, parametros);
            }
        }

        public int IncluirLog(InformacoesLog informacoesLog, string descricao, TipoLog tipoLog)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdUsuario", informacoesLog.IdUsuario, DbType.AnsiString);
                parametros.Add("LoginUsuario", informacoesLog.LoginUsuario, DbType.AnsiString);
                parametros.Add("Controller", informacoesLog.Controller, DbType.AnsiString);
                parametros.Add("Action", informacoesLog.Action, DbType.AnsiString);
                parametros.Add("Descricao", descricao, DbType.AnsiString);
                parametros.Add("DataLog", informacoesLog.DataLog, DbType.DateTime);
                parametros.Add("TipoLog", tipoLog, DbType.Int32);

                return connection.Execute(ScriptsDeLog.IncluirLog, parametros);
            }
        }
    }
}