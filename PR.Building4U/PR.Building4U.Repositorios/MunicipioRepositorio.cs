﻿using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using PR.Building4U.Repositorios.Scripts;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace PR.Building4U.Repositorios
{
    public class MunicipioRepositorio : IMunicipioRepositorio
    {
        public IEnumerable<Municipio> ListarMunicipios(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeMunicipio.ListarMunicipios;
                var parametros = new DynamicParameters();

                if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeMunicipio.ListarMunicipiosFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RMunicipio, REstado, RPais, RMunicipio>(sql, (municipio, estado, pais) =>
                {
                    estado.Pais = pais;
                    municipio.Estado = estado;
                    return municipio;
                }, parametros, splitOn: "IdEstado, IdPais");
            }
        }

        public Municipio ObterMunicipio(int idMunicipio)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdMunicipio", idMunicipio, DbType.Int32);

                return connection.Query<RMunicipio, REstado, RPais, RMunicipio>(ScriptsDeMunicipio.ObterMunicipio, (municipio, estado, pais) =>
                {
                    estado.Pais = pais;
                    municipio.Estado = estado;
                    return municipio;
                }, parametros, splitOn: "IdEstado, IdPais").FirstOrDefault();
            }
        }

        public int IncluirMunicipio(int codigoMunicipio, string nomeMunicipio, int idEstado)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("CodigoMunicipio", codigoMunicipio, DbType.Int32);
                parametros.Add("NomeMunicipio", nomeMunicipio, DbType.AnsiString);
                parametros.Add("IdEstado", idEstado, DbType.Int32);

                return connection.Query<int>(ScriptsDeMunicipio.IncluirMunicipio, parametros).FirstOrDefault();
            }
        }

        public int EditarMunicipio(int idMunicipio, string nomeMunicipio, int idEstado)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdMunicipio", idMunicipio, DbType.Int32);
                parametros.Add("NomeMunicipio", nomeMunicipio, DbType.AnsiString);
                parametros.Add("IdEstado", idEstado, DbType.Int32);

                return connection.Execute(ScriptsDeMunicipio.EditarMunicipio, parametros);
            }
        }

        public int ExcluirMunicipio(int idMunicipio)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdMunicipio", idMunicipio, DbType.Int32);

                return connection.Execute(ScriptsDeMunicipio.ExcluirMunicipio, parametros);
            }
        }

        public IEnumerable<Municipio> ListarMunicipiosPorEstado(int idEstado, string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeMunicipio.ListarMunicipiosPorEstado;
                var parametros = new DynamicParameters();
                parametros.Add("IdEstado", idEstado, DbType.Int32);

                if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeMunicipio.ListarMunicipiosPorEstadoFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RMunicipio, REstado, RPais, RMunicipio>(sql, (municipio, estado, pais) =>
                {
                    estado.Pais = pais;
                    municipio.Estado = estado;
                    return municipio;
                }, parametros, splitOn: "IdEstado, IdPais");
            }
        }
    }
}
