using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;

namespace PR.Building4U.Repositorios
{
    public class ObraGerenciaRepositorio : IObraGerenciaRepositorio
    {
        public IEnumerable<ObraGerencia> ListarGerenciasPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Query<RObraGerencia, RObra, RGerencia, RFuncionario, RObraGerencia>
                    (ScriptsDeObraGerencia.ListarGerenciasPorObra, (obraGerencia, obra, gerencia, funcionario) =>
                    {
                        obraGerencia.Obra = obra;
                        obraGerencia.Gerencia = gerencia;
                        obraGerencia.Funcionario = funcionario;
                        return obraGerencia;
                    }, parametros, splitOn: "IdObraGerencia, IdObra, IdGerencia, IdFuncionario");
            }
        }

        public int IncluirObraGerencia(int idObra, int idGerencia, int idFuncionario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("IdGerencia", idGerencia, DbType.Int32);
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);

                return connection.Query<int>(ScriptsDeObraGerencia.IncluirObraGerencia, parametros).FirstOrDefault();
            }
        }

        public int ExcluirObraGerencia(int idObraGerencia)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObraGerencia", idObraGerencia, DbType.Int32);

                return connection.Execute(ScriptsDeObraGerencia.ExcluirObraGerencia, parametros);
            }
        }
    }
}