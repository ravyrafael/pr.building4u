using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;

namespace PR.Building4U.Repositorios
{
	public class CustoRepositorio : ICustoRepositorio
	{
		public IEnumerable<Custo> ListarCustos(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeCusto.ListarCustos;
                var parametros = new DynamicParameters();

                if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeCusto.ListarCustosFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RCusto>(sql, parametros);
            }
        }

		public Custo ObterCusto(int idCusto)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCusto", idCusto, DbType.Int32);

                return connection.Query<RCusto>(ScriptsDeCusto.ObterCusto, parametros).FirstOrDefault();
            }
        }

		public int IncluirCusto(string siglaCusto, string nomeCusto)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("SiglaCusto", siglaCusto, DbType.AnsiString);
                parametros.Add("NomeCusto", nomeCusto, DbType.AnsiString);

                return connection.Query<int>(ScriptsDeCusto.IncluirCusto, parametros).FirstOrDefault();
            }
        }

		public int EditarCusto(int idCusto, string nomeCusto)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCusto", idCusto, DbType.Int32);
                parametros.Add("NomeCusto", nomeCusto, DbType.AnsiString);

                return connection.Execute(ScriptsDeCusto.EditarCusto, parametros);
            }
        }

		public int ExcluirCusto(int idCusto)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCusto", idCusto, DbType.Int32);

                return connection.Execute(ScriptsDeCusto.ExcluirCusto, parametros);
            }
        }
	}
}