using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;

namespace PR.Building4U.Repositorios
{
    public class ClienteRepositorio : IClienteRepositorio
    {
        public IEnumerable<Cliente> ListarClientes(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeCliente.ListarClientes;
                var parametros = new DynamicParameters();

                if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeCliente.ListarClientesFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RCliente, RSituacao, RCliente>(sql,
                    (cliente, situacao) =>
                    {
                        cliente.Situacao = situacao;
                        return cliente;
                    }, parametros, splitOn: "IdCliente, IdSituacao");
            }
        }

        public Cliente ObterCliente(int idCliente)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCliente", idCliente, DbType.Int32);

                return connection.Query<RCliente, RSituacao, REndereco, RMunicipio, REstado, RPais, RCliente>(ScriptsDeCliente.ObterCliente,
                    (cliente, situacao, endereco, municipio, estado, pais) =>
                    {
                        estado.Pais = pais;
                        municipio.Estado = estado;
                        endereco.Municipio = municipio;
                        cliente.Endereco = endereco;
                        cliente.Situacao = situacao;
                        return cliente;
                    }, parametros, splitOn: "IdCliente, IdSituacao, IdEndereco, IdMunicipio, IdEstado, IdPais").FirstOrDefault();
            }
        }

        public int IncluirCliente(string cnpjCliente, string razaoSocialCliente, string nomeCliente, int idEndereco)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("CnpjCliente", cnpjCliente, DbType.AnsiString);
                parametros.Add("RazaoSocialCliente", razaoSocialCliente, DbType.AnsiString);
                parametros.Add("NomeCliente", nomeCliente, DbType.AnsiString);
                parametros.Add("IdEndereco", idEndereco, DbType.Int32);

                return connection.Query<int>(ScriptsDeCliente.IncluirCliente, parametros).FirstOrDefault();
            }
        }

        public int EditarCliente(int idCliente, string razaoSocialCliente, string nomeCliente)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCliente", idCliente, DbType.Int32);
                parametros.Add("RazaoSocialCliente", razaoSocialCliente, DbType.AnsiString);
                parametros.Add("NomeCliente", nomeCliente, DbType.AnsiString);

                return connection.Execute(ScriptsDeCliente.EditarCliente, parametros);
            }
        }

        public int DesativarCliente(int idCliente)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCliente", idCliente, DbType.Int32);

                return connection.Execute(ScriptsDeCliente.DesativarCliente, parametros);
            }
        }
    }
}