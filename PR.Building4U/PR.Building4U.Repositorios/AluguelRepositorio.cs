using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using System;

namespace PR.Building4U.Repositorios
{
	public class AluguelRepositorio : IAluguelRepositorio
	{
		public IEnumerable<Aluguel> ListarAlugueis(int[] obras, int[] tiposEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
				var sql = ScriptsDeAluguel.ListarAlugueis;
                var parametros = new DynamicParameters();

                if (obras != null)
                {
                    parametros.Add("Obras", obras);
                    sql += ScriptsDeAluguel.ListarAlugueisFiltroObras;
                }
                if (tiposEquipamento != null)
                {
                    parametros.Add("Tipos", tiposEquipamento);
                    sql += ScriptsDeAluguel.ListarAlugueisFiltroTipos;
                }

                return connection.Query<RAluguel>(sql, parametros);
            }
        }

        public int IncluirAluguel(int idEquipamento, int idObra, int sequenciaAluguel, DateTime dataAluguel)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("SequenciaAluguel", sequenciaAluguel, DbType.Int32);
                parametros.Add("DataAluguel", dataAluguel, DbType.DateTime);

                return connection.Query<int>(ScriptsDeAluguel.IncluirAluguel, parametros).FirstOrDefault();
            }
        }

        public int FinalizarAluguel(int idEquipamento, int sequenciaAluguel, DateTime dataTermino)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);
                parametros.Add("SequenciaAluguel", sequenciaAluguel, DbType.Int32);
                parametros.Add("DataTermino", dataTermino, DbType.DateTime);

                return connection.Execute(ScriptsDeAluguel.FinalizarAluguel, parametros);
            }
        }

        public int IncluirReserva(int idEquipamento, int idObra, DateTime dataReserva)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("DataReserva", dataReserva, DbType.DateTime);

                return connection.Query<int>(ScriptsDeAluguel.IncluirReserva, parametros).FirstOrDefault();
            }
        }

        public int ExcluirReserva(int idEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);

                return connection.Execute(ScriptsDeAluguel.ExcluirReserva, parametros);
            }
        }

        public Aluguel ObterAluguelPorEquipamento(int idEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);

                return connection.Query<RAluguel>(ScriptsDeAluguel.ObterAluguelPorEquipamento, parametros).FirstOrDefault();
            }
        }

        public int ExcluirAluguel(int idEquipamento, int sequenciaAluguel)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);
                parametros.Add("SequenciaAluguel", sequenciaAluguel, DbType.Int32);

                return connection.Execute(ScriptsDeAluguel.ExcluirAluguel, parametros);
            }
        }

        public int ReativarAluguel(int idEquipamento, int sequenciaAluguel)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);
                parametros.Add("SequenciaAluguel", sequenciaAluguel, DbType.Int32);

                return connection.Execute(ScriptsDeAluguel.ReativarAluguel, parametros);
            }
        }

        public int VerificarDestino(int idEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdEquipamento", idEquipamento, DbType.Int32);

                return connection.Query<int>(ScriptsDeAluguel.VerificarDestino, parametros).FirstOrDefault();
            }
        }
    }
}