using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;

namespace PR.Building4U.Repositorios
{
	public class CargoRepositorio : ICargoRepositorio
	{
		public IEnumerable<Cargo> ListarCargos(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
				var sql = ScriptsDeCargo.ListarCargos;
                var parametros = new DynamicParameters();

				if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeCargo.ListarCargosFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RCargo, RCusto, RCargo>(sql, (cargo, custo) =>
                {
                    cargo.Custo = custo;
                    return cargo;
                }, parametros, splitOn: "IdCargo, IdCusto");
            }
        }

		public Cargo ObterCargo(int idCargo)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdCargo", idCargo, DbType.Int32);

                return connection.Query<RCargo, RCusto, RCargo>(ScriptsDeCargo.ObterCargo, (cargo, custo) =>
                {
                    cargo.Custo = custo;
                    return cargo;
                }, parametros, splitOn: "IdCargo, IdCusto").FirstOrDefault();
            }
        }

		public int IncluirCargo(string nomeCargo, string siglaCargo, int idCusto)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("NomeCargo", nomeCargo, DbType.AnsiString);
                parametros.Add("SiglaCargo", siglaCargo, DbType.AnsiString);
                parametros.Add("IdCusto", idCusto, DbType.Int32);

                return connection.Query<int>(ScriptsDeCargo.IncluirCargo, parametros).FirstOrDefault();
            }
        }

		public int EditarCargo(int idCargo, string nomeCargo, string siglaCargo, int idCusto)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdCargo", idCargo, DbType.Int32);
                parametros.Add("NomeCargo", nomeCargo, DbType.AnsiString);
                parametros.Add("SiglaCargo", siglaCargo, DbType.AnsiString);
                parametros.Add("IdCusto", idCusto, DbType.Int32);

                return connection.Execute(ScriptsDeCargo.EditarCargo, parametros);
            }
        }

		public int ExcluirCargo(int idCargo)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdCargo", idCargo, DbType.Int32);

                return connection.Execute(ScriptsDeCargo.ExcluirCargo, parametros);
            }
        }
	}
}