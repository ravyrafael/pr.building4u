using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;

namespace PR.Building4U.Repositorios
{
	public class UnidadeRepositorio : IUnidadeRepositorio
	{
		public IEnumerable<Unidade> ListarUnidades(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
				var sql = ScriptsDeUnidade.ListarUnidades;
                var parametros = new DynamicParameters();

				if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeUnidade.ListarUnidadesFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RUnidade>(sql, parametros);
            }
        }

		public Unidade ObterUnidade(int idUnidade)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdUnidade", idUnidade, DbType.Int32);

                return connection.Query<RUnidade>(ScriptsDeUnidade.ObterUnidade, parametros).FirstOrDefault();
            }
        }

		public int IncluirUnidade(string siglaUnidade, string nomeUnidade)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("SiglaUnidade", siglaUnidade, DbType.AnsiString);
                parametros.Add("NomeUnidade", nomeUnidade, DbType.AnsiString);

                return connection.Query<int>(ScriptsDeUnidade.IncluirUnidade, parametros).FirstOrDefault();
            }
        }

		public int EditarUnidade(int idUnidade, string siglaUnidade, string nomeUnidade)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdUnidade", idUnidade, DbType.Int32);
                parametros.Add("SiglaUnidade", siglaUnidade, DbType.AnsiString);
                parametros.Add("NomeUnidade", nomeUnidade, DbType.AnsiString);

                return connection.Execute(ScriptsDeUnidade.EditarUnidade, parametros);
            }
        }

		public int ExcluirUnidade(int idUnidade)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdUnidade", idUnidade, DbType.Int32);

                return connection.Execute(ScriptsDeUnidade.ExcluirUnidade, parametros);
            }
        }
	}
}