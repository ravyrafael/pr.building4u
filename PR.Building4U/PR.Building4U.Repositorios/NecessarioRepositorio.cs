using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;

namespace PR.Building4U.Repositorios
{
    public class NecessarioRepositorio : INecessarioRepositorio
    {
        public int CancelarNecessarioAprovPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeNecessario.CancelarNecessarioAprovPorObra, parametros);
            }
        }

        public int ExcluirNecessarioAprovPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeNecessario.ExcluirNecessarioAprovPorObra, parametros);
            }
        }

        public int IncluirNecessarioAprovPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeNecessario.IncluirNecessarioAprovPorObra, parametros);
            }
        }

        public IEnumerable<Necessario> ListarNecessarioAprovPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Query<RNecessario, RObra, RCargo, RCusto, RNecessario>(ScriptsDeNecessario.ListarNecessarioAprovPorObra,
                   (necessario, obra, cargo, custo) =>
                   {
                       cargo.Custo = custo;
                       necessario.Cargo = cargo;
                       necessario.Obra = obra;
                       return necessario;
                   }, parametros, splitOn: "IdNecessario, IdObra, IdCargo, IdCusto");
            }
        }

        public bool VerificarNecessarioAprov(int idObra, int idCargo)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("IdCargo", idCargo, DbType.Int32);

                return connection.Query<bool>(ScriptsDeNecessario.VerificarNecessarioAprov, parametros).FirstOrDefault();
            }
        }

        public int IncluirNecessarioAprov(int idObra, int idCargo, int quantidadeNecessario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("IdCargo", idCargo, DbType.Int32);
                parametros.Add("QuantidadeNecessario", quantidadeNecessario, DbType.Int32);

                return connection.Execute(ScriptsDeNecessario.IncluirNecessarioAprov, parametros);
            }
        }

        public int AtualizarNecessarioAprov(int idObra, int idCargo, int quantidadeNecessario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("IdCargo", idCargo, DbType.Int32);
                parametros.Add("QuantidadeNecessario", quantidadeNecessario, DbType.Int32);

                return connection.Execute(ScriptsDeNecessario.AtualizarNecessarioAprov, parametros);
            }
        }

        public int ExcluirNecessarioAprov(int idObra, int idCargo)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("IdCargo", idCargo, DbType.Int32);

                return connection.Execute(ScriptsDeNecessario.ExcluirNecessarioAprov, parametros);
            }
        }

        public int EnviarNecessarioAprovPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeNecessario.EnviarNecessarioAprovPorObra, parametros);
            }
        }

        public IEnumerable<NecessarioAprov> ListarNecessarioPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Query<RNecessarioAprov, RCargo, RCusto, RNecessarioAprov>(ScriptsDeNecessario.ListarNecessarioPorObra,
                   (necessario, cargo, custo) =>
                   {
                       cargo.Custo = custo;
                       necessario.Cargo = cargo;
                       return necessario;
                   }, parametros, splitOn: "IdNecessario, IdCargo, IdCusto");
            }
        }

        public int ExcluirNecessarioPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeNecessario.ExcluirNecessarioPorObra, parametros);
            }
        }

        public int IncluirNecessarioPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Execute(ScriptsDeNecessario.IncluirNecessarioPorObra, parametros);
            }
        }

        public int VerificarNecessarioAprovPorObra(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Query<int>(ScriptsDeNecessario.VerificarNecessarioAprovPorObra, parametros).FirstOrDefault();
            }
        }
    }
}