﻿using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using PR.Building4U.Repositorios.Scripts;
using System.Data;
using System.Linq;
using System.Text;

namespace PR.Building4U.Repositorios
{
    public class UsuarioRepositorio : IUsuarioRepositorio
    {
        public int? AutenticarUsuario(string loginUsuario, string senhaUsuario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("LoginUsuario", loginUsuario.Trim(), DbType.AnsiString);
                parametros.Add("SenhaUsuario", senhaUsuario, DbType.AnsiString);

                return connection.Query<int?>(ScriptsDeUsuario.AutenticarUsuario, parametros).FirstOrDefault();
            }
        }

        public Usuario ObterUsuarioPorLogin(string loginUsuario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("LoginUsuario", loginUsuario.Trim(), DbType.AnsiString);

                return connection.Query
                    <RUsuario, RFuncionario, RSituacao, RPermissao, RUsuario>
                    (ScriptsDeUsuario.ObterUsuarioPorLogin, (usuario, funcionario, situacao, permissao) =>
                {
                    usuario.Funcionario = funcionario;
                    usuario.Situacao = situacao;
                    usuario.Permissao = permissao;
                    return usuario;
                }, parametros, splitOn: "IdFuncionario, IdSituacao, IdPermissao").FirstOrDefault();
            }
        }

        public int VerificarPermissaoUsuario(int permissao, string controller, string[] actions)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeUsuario.VerificarPermissaoUsuario;
                var actionBuild = new StringBuilder("");
                foreach (var action in actions)
                {
                    if (actionBuild.Length > 0)
                        actionBuild.Append(", '" + action + "'");
                    else
                        actionBuild.Append("'" + action + "'");
                }

                sql = sql.Replace("@Actions", actionBuild.ToString());

                var parametros = new DynamicParameters();
                parametros.Add("Permissao", permissao, DbType.Int32);
                parametros.Add("Controller", controller, DbType.AnsiString);

                return connection.Query<int>(sql, parametros).FirstOrDefault();
            }
        }
    }
}
