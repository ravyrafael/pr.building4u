using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;

namespace PR.Building4U.Repositorios
{
    public class TipoEquipamentoRepositorio : ITipoEquipamentoRepositorio
    {
        public IEnumerable<TipoEquipamento> ListarTipoEquipamentos(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeTipoEquipamento.ListarTipoEquipamentos;
                var parametros = new DynamicParameters();

                if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeTipoEquipamento.ListarTipoEquipamentosFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RTipoEquipamento, RCusto, RTipoEquipamento>(sql,
                    (tipoEquipamento, custo) =>
                    {
                        tipoEquipamento.Custo = custo;
                        return tipoEquipamento;
                    }, parametros, splitOn: "IdTipoEquipamento, IdCusto");
            }
        }

        public TipoEquipamento ObterTipoEquipamento(int idTipoEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdTipoEquipamento", idTipoEquipamento, DbType.Int32);

                return connection.Query<RTipoEquipamento, RCusto, RTipoEquipamento>(ScriptsDeTipoEquipamento.ObterTipoEquipamento,
                    (tipoEquipamento, custo) =>
                    {
                        tipoEquipamento.Custo = custo;
                        return tipoEquipamento;
                    }, parametros, splitOn: "IdTipoEquipamento, IdCusto").FirstOrDefault();
            }
        }

        public int IncluirTipoEquipamento(string nomeTipoEquipamento, int idCusto)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("NomeTipoEquipamento", nomeTipoEquipamento, DbType.AnsiString);
                parametros.Add("IdCusto", idCusto, DbType.Int32);

                return connection.Query<int>(ScriptsDeTipoEquipamento.IncluirTipoEquipamento, parametros).FirstOrDefault();
            }
        }

        public int EditarTipoEquipamento(int idTipoEquipamento, string nomeTipoEquipamento, int idCusto)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdTipoEquipamento", idTipoEquipamento, DbType.Int32);
                parametros.Add("NomeTipoEquipamento", nomeTipoEquipamento, DbType.AnsiString);
                parametros.Add("IdCusto", idCusto, DbType.Int32);

                return connection.Execute(ScriptsDeTipoEquipamento.EditarTipoEquipamento, parametros);
            }
        }

        public int ExcluirTipoEquipamento(int idTipoEquipamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdTipoEquipamento", idTipoEquipamento, DbType.Int32);

                return connection.Execute(ScriptsDeTipoEquipamento.ExcluirTipoEquipamento, parametros);
            }
        }
    }
}