using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;

namespace PR.Building4U.Repositorios
{
    public class ServicoRepositorio : IServicoRepositorio
    {
        public IEnumerable<Servico> ListarServicos(string codigoOuNome)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeServico.ListarServicos;
                var parametros = new DynamicParameters();

                if (!string.IsNullOrWhiteSpace(codigoOuNome))
                {
                    sql += ScriptsDeServico.ListarServicosFiltro;
                    parametros.Add("CodigoOuNome", codigoOuNome, DbType.AnsiString);
                }

                return connection.Query<RServico, RUnidade, RServico>(sql,
                    (servico, unidade) =>
                    {
                        servico.Unidade = unidade;
                        return servico;
                    }, parametros, splitOn: "IdServico, IdUnidade");
            }
        }

        public Servico ObterServico(int? idServico, int? codigoServico)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeServico.ObterServico;
                var parametros = new DynamicParameters();

                if (idServico.HasValue)
                {
                    sql += ScriptsDeServico.ObterServicoFiltroId;
                    parametros.Add("IdServico", idServico, DbType.Int32);
                }

                if (codigoServico.HasValue)
                {
                    sql += ScriptsDeServico.ObterServicoFiltroCodigo;
                    parametros.Add("CodigoServico", codigoServico, DbType.Int32);
                }

                return connection.Query<RServico, RUnidade, RServico>(sql,
                    (servico, unidade) =>
                    {
                        servico.Unidade = unidade;
                        return servico;
                    }, parametros, splitOn: "IdServico, IdUnidade").FirstOrDefault();
            }
        }

        public int IncluirServico(int codigoServico, string siglaServico, string nomeServico, int idUnidade)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("CodigoServico", codigoServico, DbType.Int32);
                parametros.Add("SiglaServico", siglaServico, DbType.AnsiString);
                parametros.Add("NomeServico", nomeServico, DbType.AnsiString);
                parametros.Add("IdUnidade", idUnidade, DbType.Int32);

                return connection.Query<int>(ScriptsDeServico.IncluirServico, parametros).FirstOrDefault();
            }
        }

        public int EditarServico(int idServico, string siglaServico, string nomeServico, int idUnidade)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdServico", idServico, DbType.Int32);
                parametros.Add("SiglaServico", siglaServico, DbType.AnsiString);
                parametros.Add("NomeServico", nomeServico, DbType.AnsiString);
                parametros.Add("IdUnidade", idUnidade, DbType.Int32);

                return connection.Execute(ScriptsDeServico.EditarServico, parametros);
            }
        }

        public int ExcluirServico(int idServico)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdServico", idServico, DbType.Int32);

                return connection.Execute(ScriptsDeServico.ExcluirServico, parametros);
            }
        }
    }
}