using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using System;

namespace PR.Building4U.Repositorios
{
    public class CronogramaRepositorio : ICronogramaRepositorio
    {
        public IEnumerable<Cronograma> ListarCronogramas(int idObra)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);

                return connection.Query<RCronograma, RObra, RCronograma>(ScriptsDeCronograma.ListarCronogramas,
                    (cronograma, obra) =>
                    {
                        cronograma.Obra = obra;
                        return cronograma;
                    }, parametros, splitOn: "IdCronograma, IdObra");
            }
        }

        public Cronograma ObterCronograma(int idCronograma)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronograma", idCronograma, DbType.Int32);

                return connection.Query<RCronograma, RObra, RCronograma>(ScriptsDeCronograma.ObterCronograma,
                    (cronograma, obra) =>
                    {
                        cronograma.Obra = obra;
                        return cronograma;
                    }, parametros, splitOn: "IdCronograma, IdObra").FirstOrDefault();
            }
        }

        public int IncluirCronograma(int idObra, int ordemCronograma, string nomeCronograma, int diaMedicaoCronograma)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("OrdemCronograma", ordemCronograma, DbType.Int32);
                parametros.Add("NomeCronograma", nomeCronograma, DbType.AnsiString);
                parametros.Add("DiaMedicaoCronograma", diaMedicaoCronograma, DbType.Int32);

                return connection.Query<int>(ScriptsDeCronograma.IncluirCronograma, parametros).FirstOrDefault();
            }
        }

        public int EditarCronograma(int idCronograma, string nomeCronograma, int diaMedicaoCronograma)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronograma", idCronograma, DbType.Int32);
                parametros.Add("NomeCronograma", nomeCronograma, DbType.AnsiString);
                parametros.Add("DiaMedicaoCronograma", diaMedicaoCronograma, DbType.Int32);

                return connection.Execute(ScriptsDeCronograma.EditarCronograma, parametros);
            }
        }

        public IEnumerable<CronogramaServico> ListarServicos(int idCronograma)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronograma", idCronograma, DbType.Int32);

                return connection.Query<RCronogramaServico, RServico, RUnidade, RCronogramaServico>(
                    ScriptsDeCronograma.ListarServicos, (cronograma, servico, unidade) =>
                    {
                        servico.Unidade = unidade;
                        cronograma.Servico = servico;
                        return cronograma;
                    }, parametros, splitOn: "IdCronogramaServico, IdServico, IdUnidade");
            }
        }

        public IEnumerable<CronogramaServicoMedicao> ListarMedicoes(int idCronogramaServico)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);

                return connection.Query<RCronogramaServicoMedicao>(ScriptsDeCronograma.ListarMedicoes, parametros);
            }
        }

        public int IncluirMedicao(int idCronogramaServico, DateTime mesReferencia, DateTime inicioMedicao,
            DateTime terminoMedicao, decimal? faturamento, decimal? quantidade, decimal? pesMedicao,
            decimal? equipeMedicao, bool temMedicao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);
                parametros.Add("MesReferencia", mesReferencia, DbType.DateTime);
                parametros.Add("InicioMedicao", inicioMedicao, DbType.DateTime);
                parametros.Add("TerminoMedicao", terminoMedicao, DbType.DateTime);
                parametros.Add("Faturamento", faturamento, DbType.Decimal);
                parametros.Add("Quantidade", quantidade, DbType.Decimal);
                parametros.Add("PesMedicao", pesMedicao, DbType.Decimal);
                parametros.Add("EquipeMedicao", equipeMedicao, DbType.Decimal);
                parametros.Add("TemMedicao", temMedicao, DbType.Boolean);

                return connection.Execute(ScriptsDeCronograma.IncluirMedicao, parametros);
            }
        }

        public int AtualizarMedicao(int idCronogramaServicoMedicao, DateTime inicioMedicao, DateTime terminoMedicao,
            decimal? faturamento, decimal? quantidade, decimal? pesMedicao, decimal? equipeMedicao, bool temMedicao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServicoMedicao", idCronogramaServicoMedicao, DbType.Int32);
                parametros.Add("InicioMedicao", inicioMedicao, DbType.DateTime);
                parametros.Add("TerminoMedicao", terminoMedicao, DbType.DateTime);
                parametros.Add("Faturamento", faturamento, DbType.Decimal);
                parametros.Add("Quantidade", quantidade, DbType.Decimal);
                parametros.Add("PesMedicao", pesMedicao, DbType.Decimal);
                parametros.Add("EquipeMedicao", equipeMedicao, DbType.Decimal);
                parametros.Add("TemMedicao", temMedicao, DbType.Boolean);

                return connection.Execute(ScriptsDeCronograma.AtualizarMedicao, parametros);
            }
        }

        public int ExcluirMedicao(int idCronogramaServicoMedicao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServicoMedicao", idCronogramaServicoMedicao, DbType.Int32);

                return connection.Execute(ScriptsDeCronograma.ExcluirMedicao, parametros);
            }
        }

        public int IncluirServico(int idCronograma, int idServico, int ordemServico, string nomeServico,
            decimal quantidadeTotal, decimal quantidadeExecutada, decimal? quantidadeReal, decimal mediaEquipe,
            decimal mediaPes, decimal? mediaPesReal, decimal valorServico, int duracaoTotal, int duracaoFalta,
            DateTime? inicioServico, DateTime? terminoServico, DateTime? atualizacaoServico,
            int? idCronogramaPredecessora, int? idServicoPredecessora, char? tipoPredecessora,
            int? atrasoPredecessora, decimal chanceFaturamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronograma", idCronograma, DbType.Int32);
                parametros.Add("IdServico", idServico, DbType.Int32);
                parametros.Add("OrdemServico", ordemServico, DbType.Int32);
                parametros.Add("NomeServico", nomeServico, DbType.AnsiString);
                parametros.Add("QuantidadeTotal", quantidadeTotal, DbType.Decimal);
                parametros.Add("QuantidadeExecutada", quantidadeExecutada, DbType.Decimal);
                parametros.Add("QuantidadeReal", quantidadeReal, DbType.Decimal);
                parametros.Add("MediaEquipe", mediaEquipe, DbType.Decimal);
                parametros.Add("MediaPes", mediaPes, DbType.Decimal);
                parametros.Add("MediaPesReal", mediaPesReal, DbType.Decimal);
                parametros.Add("ValorServico", valorServico, DbType.Decimal);
                parametros.Add("DuracaoTotal", duracaoTotal, DbType.Int32);
                parametros.Add("DuracaoFalta", duracaoFalta, DbType.Int32);
                parametros.Add("InicioServico", inicioServico, DbType.DateTime);
                parametros.Add("TerminoServico", terminoServico, DbType.DateTime);
                parametros.Add("AtualizacaoServico", atualizacaoServico, DbType.DateTime);
                parametros.Add("IdCronogramaPredecessora", idCronogramaPredecessora, DbType.Int32);
                parametros.Add("IdServicoPredecessora", idServicoPredecessora, DbType.Int32);
                parametros.Add("TipoPredecessora", tipoPredecessora, DbType.AnsiString);
                parametros.Add("AtrasoPredecessora", atrasoPredecessora, DbType.Int32);
                parametros.Add("ChanceFaturamento", chanceFaturamento, DbType.Decimal);

                return connection.Query<int>(ScriptsDeCronograma.IncluirServico, parametros).FirstOrDefault();
            }
        }

        public int AtualizarServico(int idCronogramaServico, int ordemServico, string nomeServico,
            decimal quantidadeTotal, decimal quantidadeExecutada, decimal? quantidadeReal, decimal mediaEquipe,
            decimal mediaPes, decimal? mediaPesReal, decimal valorServico, int duracaoTotal, int duracaoFalta,
            DateTime? inicioServico, DateTime? terminoServico, DateTime? atualizacaoServico,
            int? idCronogramaPredecessora, int? idServicoPredecessora, char? tipoPredecessora,
            int? atrasoPredecessora, decimal chanceFaturamento)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);
                parametros.Add("OrdemServico", ordemServico, DbType.Int32);
                parametros.Add("NomeServico", nomeServico, DbType.AnsiString);
                parametros.Add("QuantidadeTotal", quantidadeTotal, DbType.Decimal);
                parametros.Add("QuantidadeExecutada", quantidadeExecutada, DbType.Decimal);
                parametros.Add("QuantidadeReal", quantidadeReal, DbType.Decimal);
                parametros.Add("MediaEquipe", mediaEquipe, DbType.Decimal);
                parametros.Add("MediaPes", mediaPes, DbType.Decimal);
                parametros.Add("MediaPesReal", mediaPesReal, DbType.Decimal);
                parametros.Add("ValorServico", valorServico, DbType.Decimal);
                parametros.Add("DuracaoTotal", duracaoTotal, DbType.Int32);
                parametros.Add("DuracaoFalta", duracaoFalta, DbType.Int32);
                parametros.Add("InicioServico", inicioServico, DbType.DateTime);
                parametros.Add("TerminoServico", terminoServico, DbType.DateTime);
                parametros.Add("AtualizacaoServico", atualizacaoServico, DbType.DateTime);
                parametros.Add("IdCronogramaPredecessora", idCronogramaPredecessora, DbType.Int32);
                parametros.Add("IdServicoPredecessora", idServicoPredecessora, DbType.Int32);
                parametros.Add("TipoPredecessora", tipoPredecessora, DbType.AnsiString);
                parametros.Add("AtrasoPredecessora", atrasoPredecessora, DbType.Int32);
                parametros.Add("ChanceFaturamento", chanceFaturamento, DbType.Decimal);

                return connection.Execute(ScriptsDeCronograma.AtualizarServico, parametros);
            }
        }

        public int ExcluirServicoMedicao(int idCronogramaServico)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);

                return connection.Execute(ScriptsDeCronograma.ExcluirServicoMedicao, parametros);
            }
        }

        public int ExcluirServicoProducao(int idCronogramaServico)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);

                return connection.Execute(ScriptsDeCronograma.ExcluirServicoProducao, parametros);
            }
        }

        public int ExcluirServico(int idCronogramaServico)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);

                return connection.Execute(ScriptsDeCronograma.ExcluirServico, parametros);
            }
        }

        public int AtualizarCronograma(int idCronograma, int ordemCronograma, DateTime? inicioCronograma,
            DateTime? terminoCronograma, DateTime? atualizacaoCronograma)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronograma", idCronograma, DbType.Int32);
                parametros.Add("OrdemCronograma", ordemCronograma, DbType.Int32);
                parametros.Add("InicioCronograma", inicioCronograma, DbType.DateTime);
                parametros.Add("TerminoCronograma", terminoCronograma, DbType.DateTime);
                parametros.Add("AtualizacaoCronograma", atualizacaoCronograma, DbType.DateTime);

                return connection.Execute(ScriptsDeCronograma.AtualizarCronograma, parametros);
            }
        }
    }
}