using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using System;

namespace PR.Building4U.Repositorios
{
    public class NotificacaoRepositorio : INotificacaoRepositorio
    {
        public IEnumerable<Notificacao> ListarNotificacoes(int idFuncionario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);

                return connection.Query<RNotificacao>(ScriptsDeNotificacao.ListarNotificacoes, parametros);
            }
        }

        public int IncluirNotificacao(int idFuncionario, string tituloNotificacao, string textoNotificacao, DateTime dataNotificacao, int tipoNotificacao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);
                parametros.Add("TituloNotificacao", tituloNotificacao, DbType.AnsiString);
                parametros.Add("TextoNotificacao", textoNotificacao, DbType.AnsiString);
                parametros.Add("DataNotificacao", dataNotificacao, DbType.DateTime);
                parametros.Add("TipoNotificacao", tipoNotificacao, DbType.Int32);

                return connection.Query<int>(ScriptsDeNotificacao.IncluirNotificacao, parametros).FirstOrDefault();
            }
        }

        public int MarcarNotificacaoComoLida(int? idNotificacao, int? idFuncionario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeNotificacao.MarcarNotificacaoComoLida;
                var parametros = new DynamicParameters();

                if (idNotificacao.HasValue)
                {
                    sql += ScriptsDeNotificacao.MarcarNotificacaoComoLidaFiltro1;
                    parametros.Add("IdNotificacao", idNotificacao, DbType.Int32);
                }

                if (idFuncionario.HasValue)
                {
                    sql += ScriptsDeNotificacao.MarcarNotificacaoComoLidaFiltro2;
                    parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);
                }

                return connection.Execute(sql, parametros);
            }
        }
    }
}