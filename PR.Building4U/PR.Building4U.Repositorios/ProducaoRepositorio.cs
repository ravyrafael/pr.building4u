using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using System;

namespace PR.Building4U.Repositorios
{
	public class ProducaoRepositorio : IProducaoRepositorio
	{
		public IEnumerable<Producao> ListarProducaos(int idCronogramaServico)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);

                return connection.Query<RProducao>(ScriptsDeProducao.ListarProducaos, parametros);
            }
        }

		public int IncluirProducao(int idCronogramaServico, DateTime dataProducao, decimal? quantidadeProducao, decimal? equipeProducao, string observacaoProducao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);
                parametros.Add("DataProducao", dataProducao, DbType.DateTime);
                parametros.Add("QuantidadeProducao", quantidadeProducao, DbType.Decimal);
                parametros.Add("EquipeProducao", equipeProducao, DbType.Decimal);
                parametros.Add("ObservacaoProducao", observacaoProducao, DbType.AnsiString);

                return connection.Query<int>(ScriptsDeProducao.IncluirProducao, parametros).FirstOrDefault();
            }
        }

		public int AtualizarProducao(int idProducao, decimal? quantidadeProducao, decimal? equipeProducao, string observacaoProducao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdProducao", idProducao, DbType.Int32);
                parametros.Add("QuantidadeProducao", quantidadeProducao, DbType.Decimal);
                parametros.Add("EquipeProducao", equipeProducao, DbType.Decimal);
                parametros.Add("ObservacaoProducao", observacaoProducao, DbType.AnsiString);

                return connection.Execute(ScriptsDeProducao.EditarProducao, parametros);
            }
        }

		public int ExcluirProducao(int idProducao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdProducao", idProducao, DbType.Int32);

                return connection.Execute(ScriptsDeProducao.ExcluirProducao, parametros);
            }
        }

        public decimal SomarProducaoServico(int idCronogramaServico)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdCronogramaServico", idCronogramaServico, DbType.Int32);

                return connection.Query<decimal?>(ScriptsDeProducao.SomarProducaoServico, parametros).FirstOrDefault() ?? 0;
            }
        }
    }
}