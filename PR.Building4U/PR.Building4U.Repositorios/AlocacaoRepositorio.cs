using System.Data;
using System.Collections.Generic;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using System.Linq;
using System;

namespace PR.Building4U.Repositorios
{
    public class AlocacaoRepositorio : IAlocacaoRepositorio
    {
        public IEnumerable<Alocacao> ListarAlocacoes(int[] obras, int[] cargos)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var sql = ScriptsDeAlocacao.ListarAlocacoes;
                var parametros = new DynamicParameters();
                if (obras != null)
                {
                    parametros.Add("Obras", obras);
                    sql += ScriptsDeAlocacao.ListarAlocacoesFiltroObras;
                }
                if (cargos != null)
                {
                    parametros.Add("Cargos", cargos);
                    sql += ScriptsDeAlocacao.ListarAlocacoesFiltroCargos;
                }

                return connection.Query<RAlocacao>(sql, parametros);
            }
        }

        public int IncluirAlocacao(int idFuncionario, int idObra, int sequenciaAlocacao, DateTime dataAlocacao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("SequenciaAlocacao", sequenciaAlocacao, DbType.Int32);
                parametros.Add("DataAlocacao", dataAlocacao, DbType.DateTime);

                return connection.Query<int>(ScriptsDeAlocacao.IncluirAlocacao, parametros).FirstOrDefault();
            }
        }

        public int FinalizarAlocacao(int idFuncionario, int sequenciaAlocacao, DateTime dataTermino)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);
                parametros.Add("SequenciaAlocacao", sequenciaAlocacao, DbType.Int32);
                parametros.Add("DataTermino", dataTermino, DbType.DateTime);

                return connection.Execute(ScriptsDeAlocacao.FinalizarAlocacao, parametros);
            }
        }

        public int IncluirReserva(int idFuncionario, int idObra, DateTime dataReserva)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);
                parametros.Add("IdObra", idObra, DbType.Int32);
                parametros.Add("DataReserva", dataReserva, DbType.DateTime);

                return connection.Query<int>(ScriptsDeAlocacao.IncluirReserva, parametros).FirstOrDefault();
            }
        }

        public int ExcluirReserva(int idFuncionario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);

                return connection.Execute(ScriptsDeAlocacao.ExcluirReserva, parametros);
            }
        }

        public Alocacao ObterAlocacaoPorFuncionario(int idFuncionario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);

                return connection.Query<RAlocacao>(ScriptsDeAlocacao.ObterAlocacaoPorFuncionario, parametros).FirstOrDefault();
            }
        }

        public int ExcluirAlocacao(int idFuncionario, int sequenciaAlocacao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);
                parametros.Add("SequenciaAlocacao", sequenciaAlocacao, DbType.Int32);

                return connection.Execute(ScriptsDeAlocacao.ExcluirAlocacao, parametros);
            }
        }

        public int ReativarAlocacao(int idFuncionario, int sequenciaAlocacao)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);
                parametros.Add("SequenciaAlocacao", sequenciaAlocacao, DbType.Int32);

                return connection.Execute(ScriptsDeAlocacao.ReativarAlocacao, parametros);
            }
        }

        public int VerificarDestino(int idFuncionario)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdFuncionario", idFuncionario, DbType.Int32);

                return connection.Query<int>(ScriptsDeAlocacao.VerificarDestino, parametros).FirstOrDefault();
            }
        }
    }
}