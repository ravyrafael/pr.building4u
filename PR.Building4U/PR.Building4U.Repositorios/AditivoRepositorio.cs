using System.Data;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using PR.Building4U.Entidades;
using PR.Building4U.Fronteiras.Repositorios;
using PR.Building4U.Repositorios.Scripts;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.Repositorios.Entidades;
using System;

namespace PR.Building4U.Repositorios
{
	public class AditivoRepositorio : IAditivoRepositorio
	{
		public IEnumerable<Aditivo> ListarAditivosPorContrato(int idContrato)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdContrato", idContrato, DbType.Int32);

                return connection.Query<RAditivo>(ScriptsDeAditivo.ListarAditivos, parametros);
            }
        }

		public int IncluirAditivo(int idContrato, string codigoAditivo, DateTime dataAditivo, decimal valorAditivo, int prazoAditivo, string escopoAditivo)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
                parametros.Add("IdContrato", idContrato, DbType.Int32);
                parametros.Add("CodigoAditivo", codigoAditivo, DbType.AnsiString);
                parametros.Add("DataAditivo", dataAditivo, DbType.DateTime);
                parametros.Add("ValorAditivo", valorAditivo, DbType.Decimal);
                parametros.Add("PrazoAditivo", prazoAditivo, DbType.Int32);
                parametros.Add("EscopoAditivo", escopoAditivo, DbType.AnsiString);

                return connection.Query<int>(ScriptsDeAditivo.IncluirAditivo, parametros).FirstOrDefault();
            }
        }

		public int ExcluirAditivo(int idAditivo)
        {
            using (var connection = ConnectionFactory.Instance.GetConnection("CorpBuilding"))
            {
                var parametros = new DynamicParameters();
				parametros.Add("IdAditivo", idAditivo, DbType.Int32);

                return connection.Execute(ScriptsDeAditivo.ExcluirAditivo, parametros);
            }
        }
	}
}