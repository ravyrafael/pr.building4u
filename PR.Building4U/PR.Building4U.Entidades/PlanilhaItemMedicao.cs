﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class PlanilhaItemMedicao
    {
        public virtual int IdPlanilhaItemMedicao { get; set; } // id
        public virtual int IdPlanilhaItem { get; set; } // planilha_item_id
        public virtual DateTime PeriodoMedicao { get; set; } // periodo_medicao
        public virtual decimal QuantidadeMedicao { get; set; } // quantidade_medicao
        public virtual decimal ValorMedicao { get; set; } // valor_medicao
    }
}
