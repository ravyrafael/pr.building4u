﻿namespace PR.Building4U.Entidades
{
    public abstract class Area
    {
        public virtual int IdArea { get; set; } // id
        public virtual string SiglaArea { get; set; } // sigla_area
        public virtual string NomeArea { get; set; } // nome_area
    }
}
