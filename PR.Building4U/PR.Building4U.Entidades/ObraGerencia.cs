﻿namespace PR.Building4U.Entidades
{
    public abstract class ObraGerencia
    {
        public virtual int IdObraGerencia { get; set; } // id

        public virtual Obra Obra { get; set; } // obra_id
        public virtual Gerencia Gerencia { get; set; } // gerencia_id
        public virtual Funcionario Funcionario { get; set; } // funcionario_id
    }
}
