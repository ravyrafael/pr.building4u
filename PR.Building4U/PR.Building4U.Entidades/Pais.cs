﻿namespace PR.Building4U.Entidades
{
    public abstract class Pais
    {
        public virtual int IdPais { get; set; } // id
        public virtual int CodigoPais { get; set; } // codigo_pais
        public virtual string NomePais { get; set; } // nome_pais
    }
}
