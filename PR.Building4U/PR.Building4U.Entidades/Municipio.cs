﻿namespace PR.Building4U.Entidades
{
    public abstract class Municipio
    {
        public virtual int IdMunicipio { get; set; } // id
        public virtual string NomeMunicipio { get; set; } // nome_municipio
        public virtual int CodigoMunicipio { get; set; } // codigo_municipio

        public virtual Estado Estado { get; set; } // estado_id
    }
}
