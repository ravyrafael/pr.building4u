﻿using System.Collections.Generic;

namespace PR.Building4U.Entidades
{
    public abstract class Planilha
    {
        public virtual int IdPlanilha { get; set; } // id
        public virtual int IdCronograma { get; set; } // cronograma_id
        public virtual string CpPlanilha { get; set; } // cp_planilha
        public virtual int RevPlanilha { get; set; } // rev_planilha

        public virtual List<PlanilhaItem> Itens { get; set; }
    }
}
