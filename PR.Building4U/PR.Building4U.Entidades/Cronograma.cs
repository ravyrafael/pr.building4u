﻿using System;
using System.Collections.Generic;

namespace PR.Building4U.Entidades
{
    public abstract class Cronograma
    {
        public virtual int IdCronograma { get; set; } // id
        public virtual int OrdemCronograma { get; set; } // ordem_cronograma
        public virtual string NomeCronograma { get; set; } // nome_cronograma
        public virtual int DiaMedicaoCronograma { get; set; } // medicao_cronograma
        public virtual DateTime? InicioCronograma { get; set; } // inicio_cronograma
        public virtual DateTime? TerminoCronograma { get; set; } // termino_cronograma
        public virtual DateTime? AtualizacaoCronograma { get; set; } // atualizacao_cronograma

        public virtual Obra Obra { get; set; } // obra_id

        public virtual List<CronogramaServico> Servicos { get; set; }
        public virtual List<int> ServicosRemovidos { get; set; }

        public virtual List<Planilha> Planilhas { get; set; }
    }
}
