﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class Medicao
    {
        public virtual int IdPlanilha { get; set; }
        public virtual DateTime PeriodoMedicao { get; set; }
        public virtual decimal ValorMedicao { get; set; }
    }
}
