﻿namespace PR.Building4U.Entidades
{
    public abstract class Cliente
    {
        public virtual int IdCliente { get; set; } // id
        public virtual string CnpjCliente { get; set; } // cnpj_cliente
        public virtual string RazaoSocialCliente { get; set; } // razao_social_cliente
        public virtual string NomeCliente { get; set; } // nome_cliente

        public virtual Endereco Endereco { get; set; } // endereco_id
        public virtual Situacao Situacao { get; set; } // situacao_id
    }
}
