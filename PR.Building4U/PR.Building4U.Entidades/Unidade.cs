﻿namespace PR.Building4U.Entidades
{
    public abstract class Unidade
    {
        public virtual int IdUnidade { get; set; } // id
        public virtual string SiglaUnidade { get; set; } // sigla_unidade
        public virtual string NomeUnidade { get; set; } // nome_unidade
    }
}
