﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class Alocacao
    {
        public virtual int IdAlocacao { get; set; } // id
        public virtual int SequenciaAlocacao { get; set; } // sequencia_alocacao

        public virtual int IdFuncionario { get; set; }
        public virtual string ChapaFuncionario { get; set; }
        public virtual string NomeFuncionario { get; set; }
        public virtual string SituacaoFuncionario { get; set; }
        public virtual string SiglaSituacaoFuncionario { get; set; }
        public virtual string NomeCusto { get; set; }
        public virtual string NomeCargo { get; set; }
        public virtual int IdObra { get; set; }
        public virtual string CodigoObra { get; set; }
        public virtual string NomeObra { get; set; }
        public virtual DateTime InicioAlocacao { get; set; }
        public virtual DateTime? InicioReserva { get; set; }
        public virtual string ObraOrigem { get; set; }
        public virtual string ObraDestino { get; set; }
        public virtual string SituacaoObra { get; set; }
        public virtual DateTime? TerminoFerias { get; set; }
    }
}
