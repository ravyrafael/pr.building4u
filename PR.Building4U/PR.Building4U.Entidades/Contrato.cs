﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class Contrato
    {
        public virtual int IdContrato { get; set; } // id
        public virtual string CodigoContrato { get; set; } // codigo_contrato
        public virtual DateTime DataContrato { get; set; } // data_contrato
        public virtual decimal ValorContrato { get; set; } // valor_contrato
        public virtual decimal ValorAditivo { get; set; } // valor_aditivo
        public virtual int PrazoContrato { get; set; } // prazo_contrato
        public virtual int PrazoAditivo { get; set; } // prazo_aditivo
        public virtual string EscopoContrato { get; set; } // escopo_contrato

        public virtual Cliente Cliente { get; set; } // cliente_id
    }
}
