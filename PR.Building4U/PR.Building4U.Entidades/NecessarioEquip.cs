﻿namespace PR.Building4U.Entidades
{
    public abstract class NecessarioEquip
    {
        public virtual int IdNecessario { get; set; } // id

        public virtual TipoEquipamento TipoEquipamento { get; set; } // tipo_equipamento_id
        public virtual Obra Obra { get; set; } // obra_id

        public virtual int QuantidadeNecessario { get; set; } // quantidade_necessario
        public virtual bool IndicadorAprovacao { get; set; } // indicador_aprov
    }
}
