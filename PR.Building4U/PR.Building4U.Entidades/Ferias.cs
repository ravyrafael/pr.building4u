﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class Ferias
    {
        public virtual int IdFerias { get; set; } // id
        public virtual DateTime InicioFerias { get; set; } // inicio_ferias
        public virtual DateTime TerminoFerias { get; set; } // termino_ferias
        public virtual int SequenciaFerias { get; set; } // sequencia_ferias

        public virtual Funcionario Funcionario { get; set; } // funcionario_id
    }
}
