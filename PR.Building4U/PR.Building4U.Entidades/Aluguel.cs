﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class Aluguel
    {
        public virtual int IdAluguel { get; set; } // id
        public virtual int SequenciaAluguel { get; set; } // sequencia_aluguel

        public virtual int IdEquipamento { get; set; }
        public virtual string PatrimonioEquipamento { get; set; }
        public virtual string TipoEquipamento { get; set; }
        public virtual string MarcaEquipamento { get; set; }
        public virtual string ModeloEquipamento { get; set; }
        public virtual string NomeCusto { get; set; }
        public virtual string SituacaoEquipamento { get; set; }
        public virtual string SiglaSituacaoEquipamento { get; set; }
        public virtual int IdObra { get; set; }
        public virtual string CodigoObra { get; set; }
        public virtual string NomeObra { get; set; }
        public virtual DateTime InicioAluguel { get; set; }
        public virtual DateTime? InicioReservaAluguel { get; set; }
        public virtual string ObraOrigem { get; set; }
        public virtual string ObraDestino { get; set; }
        public virtual string SituacaoObra { get; set; }
        public virtual DateTime? TerminoManutencao { get; set; }
    }
}
