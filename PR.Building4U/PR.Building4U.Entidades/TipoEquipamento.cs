﻿namespace PR.Building4U.Entidades
{
    public abstract class TipoEquipamento
    {
        public virtual int IdTipoEquipamento { get; set; } // id
        public virtual string NomeTipoEquipamento { get; set; } // nome_tipo

        public virtual Custo Custo { get; set; } // custo_id
    }
}
