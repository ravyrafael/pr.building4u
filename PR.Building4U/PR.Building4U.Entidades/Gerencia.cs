﻿namespace PR.Building4U.Entidades
{
    public abstract class Gerencia
    {
        public virtual int IdGerencia { get; set; } // id
        public virtual string NomeGerencia { get; set; } // nome_gerencia
        public virtual string SiglaGerencia { get; set; } // sigla_gerencia
    }
}
