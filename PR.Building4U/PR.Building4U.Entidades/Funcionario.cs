﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class Funcionario
    {
        public virtual int IdFuncionario { get; set; } // id
        public virtual string ChapaFuncionario { get; set; } // chapa_funcionario
        public virtual string NomeFuncionario { get; set; } // nome_funcionario
        public virtual DateTime AdmissaoFuncionario { get; set; } // admissao_funcionario
        public virtual DateTime? DemissaoFuncionario { get; set; } // demissao_funcionario
        public virtual string EmailFuncionario { get; set; } // email_funcionario
        public virtual string TelefoneFuncionario { get; set; } // telefone_funcionario

        public virtual Endereco Endereco { get; set; } // endereco_id
        public virtual Situacao Situacao { get; set; } // situacao_id
        public virtual Cargo Cargo { get; set; } // cargo_id
        public virtual Obra Obra { get; set; }
    }
}
