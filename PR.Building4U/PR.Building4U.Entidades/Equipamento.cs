﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class Equipamento
    {
        public virtual int IdEquipamento { get; set; } // id
        public virtual string MarcaEquipamento { get; set; } // marca_equipamento
        public virtual string ModeloEquipamento { get; set; } // modelo_equipamento
        public virtual string PatrimonioEquipamento { get; set; } // patrimonio_equipamento
        public virtual bool IndicadorTerceiro { get; set; } // indicador_terceiro
        public virtual string FornecedorEquipamento { get; set; } // fornecedor_equipamento
        public virtual DateTime AquisicaoEquipamento { get; set; } // aquisicao_equipamento
        public virtual DateTime? SucataEquipamento { get; set; } // sucata_equipamento

        public virtual TipoEquipamento TipoEquipamento { get; set; } // tipo_equipamento_id
        public virtual Situacao Situacao { get; set; } // situacao_id
        public virtual Obra Obra { get; set; }
    }
}
