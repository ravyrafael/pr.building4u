﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class Aditivo
    {
        public virtual int IdAditivo { get; set; } // id
        public virtual string CodigoAditivo { get; set; } // codigo_aditivo
        public virtual DateTime DataAditivo { get; set; } // data_aditivo
        public virtual decimal ValorAditivo { get; set; } // valor_aditivo
        public virtual int PrazoAditivo { get; set; } // prazo_aditivo
        public virtual string EscopoAditivo { get; set; } // escopo_aditivo

        public virtual Contrato Contrato { get; set; } // contrato_id
    }
}
