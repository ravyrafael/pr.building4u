﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class Manutencao
    {
        public virtual int IdManutencao { get; set; } // id
        public virtual DateTime InicioManutencao { get; set; } // inicio_manutencao
        public virtual DateTime TerminoManutencao { get; set; } // termino_manutencao
        public virtual int SequenciaManutencao { get; set; } // sequencia_manutencao

        public virtual Equipamento Equipamento { get; set; } // equipamento_id
    }
}
