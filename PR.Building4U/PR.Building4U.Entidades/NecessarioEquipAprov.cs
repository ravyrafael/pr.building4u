﻿namespace PR.Building4U.Entidades
{
    public abstract class NecessarioEquipAprov
    {
        public virtual int IdNecessario { get; set; } // id
        public virtual int QuantidadeNecessario { get; set; } // quantidade_necessario
        public virtual int QuantidadeAprovacao { get; set; } // quantidade_necessario

        public virtual TipoEquipamento TipoEquipamento { get; set; } // cargo_id
    }
}
