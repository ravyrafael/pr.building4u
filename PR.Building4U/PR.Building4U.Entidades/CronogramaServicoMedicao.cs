﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class CronogramaServicoMedicao
    {
        public virtual int IdCronogramaServicoMedicao { get; set; } // id
        public virtual int IdCronogramaServico { get; set; } // servico_id
        public virtual DateTime MesReferencia { get; set; } // mes_referencia
        public virtual DateTime InicioMedicao { get; set; } // inicio_medicao
        public virtual DateTime TerminoMedicao { get; set; } // termino_medicao
        public virtual decimal? Faturamento { get; set; } // faturamento_medicao
        public virtual decimal? Quantidade { get; set; } // quantidade_medicao
        public virtual decimal? PesMedicao { get; set; } // pes_medicao
        public virtual decimal? EquipeMedicao { get; set; } // equipe_medicao
        public virtual bool TemMedicao { get; set; } // tem_medicao
    }
}
