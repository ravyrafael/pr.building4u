﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class Producao
    {
        public virtual int IdProducao { get; set; } // id
        public virtual int IdCronogramaServico { get; set; } // cronograma_servico_id
        public virtual DateTime DataProducao { get; set; } // data_producao
        public virtual decimal? QuantidadeProducao { get; set; } // quantidade_producao
        public virtual decimal? EquipeProducao { get; set; } // equipe_producao
        public virtual string ObservacaoProducao { get; set; } // observacao_producao
    }
}
