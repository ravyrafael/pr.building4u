﻿namespace PR.Building4U.Entidades
{
    public abstract class Custo
    {
        public virtual int IdCusto { get; set; } // id
        public virtual string SiglaCusto { get; set; } // sigla_custo
        public virtual string NomeCusto { get; set; } // nome_custo
    }
}
