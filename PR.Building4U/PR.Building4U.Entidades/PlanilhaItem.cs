﻿using System.Collections.Generic;

namespace PR.Building4U.Entidades
{
    public abstract class PlanilhaItem
    {
        public virtual int IdPlanilhaItem { get; set; } // id
        public virtual int? IdCronogramaServico { get; set; } // cronograma_servico_id
        public virtual string NomeCronogramaServico { get; set; }
        public virtual string IdServicoAux { get; set; }
        public virtual int IdPlanilha { get; set; } // planilha_id
        public virtual string CodigoItem { get; set; } // codigo_item_planilha
        public virtual string DescricaoItem { get; set; } // descricao_item_planilha
        public virtual decimal? PrecoItem { get; set; } // preco_item_planilha
        public virtual decimal? QuantidadeItem { get; set; } // quantidade_item_planilha
        public virtual decimal? QuantidadeAcertoItem { get; set; } // quantidade_acerto_item
        public virtual bool EhTituloItem { get; set; } // eh_titulo_item
        public virtual bool EhItemControle { get; set; } // eh_item_controle

        public virtual Unidade Unidade { get; set; } // unidade_id

        public virtual List<PlanilhaItemMedicao> Medicoes { get; set; }
    }
}
