﻿namespace PR.Building4U.Entidades
{
    public abstract class Obra
    {
        public virtual int IdObra { get; set; } // id
        public virtual string CodigoObra { get; set; } // codigo_obra
        public virtual string NomeObra { get; set; } // nome_obra

        public virtual Situacao Situacao { get; set; } // situacao_id
        public virtual Area Area { get; set; } // area_id
        public virtual Contrato Contrato { get; set; } // contrato_id
        public virtual OrdemServico OrdemServico { get; set; }
    }
}
