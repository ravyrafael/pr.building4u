﻿using System;
using System.Collections.Generic;

namespace PR.Building4U.Entidades
{
    public abstract class CronogramaServico
    {
        public virtual int IdCronogramaServico { get; set; } // id
        public virtual int IdCronograma { get; set; } // cronograma_id
        public virtual int OrdemServico { get; set; } // ordem_cron_servico
        public virtual string NomeServico { get; set; } // nome_cron_servico
        public virtual decimal QuantidadeTotal { get; set; } // quantidade_total
        public virtual decimal QuantidadeExecutada { get; set; } // quantidade_executada
        public virtual decimal? QuantidadeReal { get; set; } // quantidade_real
        public virtual decimal MediaEquipe { get; set; } // media_equipe
        public virtual decimal MediaPes { get; set; } // media_pes
        public virtual decimal? MediaPesReal { get; set; } // media_pes_real
        public virtual decimal ValorServico { get; set; } // valor_total
        public virtual int DuracaoTotal { get; set; } // duracao_total
        public virtual int DuracaoFalta { get; set; } // duracao_falta
        public virtual DateTime? InicioServico { get; set; } // inicio_servico
        public virtual DateTime? TerminoServico { get; set; } // termino_servico
        public virtual DateTime? AtualizacaoServico { get; set; } // atualizacao_servico
        public virtual int? IdCronogramaPredecessora { get; set; } // id_cronograma_predecessora
        public virtual int? IdServicoPredecessora { get; set; } // id_servico_predecessora
        public virtual char? TipoPredecessora { get; set; } // tipo_predecessora
        public virtual int? AtrasoPredecessora { get; set; } // atraso_predecessora
        public virtual decimal ChanceFaturamento { get; set; } // chance_faturamento

        public virtual Servico Servico { get; set; } // servico_id

        public virtual List<CronogramaServicoMedicao> Medicoes { get; set; }
        public virtual List<int> MedicoesRemovidas { get; set; }

        public virtual List<Producao> Producoes { get; set; }
        public virtual List<int> ProducoesRemovidas { get; set; }
    }
}
