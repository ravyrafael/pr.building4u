﻿namespace PR.Building4U.Entidades
{
    public abstract class Estado
    {
        public virtual int IdEstado { get; set; } // id
        public virtual string SiglaEstado { get; set; } // sigla_estado
        public virtual string NomeEstado { get; set; } // nome_estado

        public virtual Pais Pais { get; set; } // pais_id
    }
}
