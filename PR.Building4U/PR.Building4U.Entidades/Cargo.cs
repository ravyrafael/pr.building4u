﻿namespace PR.Building4U.Entidades
{
    public abstract class Cargo
    {
        public virtual int IdCargo { get; set; } // id
        public virtual string NomeCargo { get; set; } // nome_cargo
        public virtual string SiglaCargo { get; set; } // sigla_cargo

        public virtual Custo Custo { get; set; } // custo_id
    }
}
