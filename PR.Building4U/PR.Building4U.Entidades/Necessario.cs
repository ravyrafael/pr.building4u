﻿namespace PR.Building4U.Entidades
{
    public abstract class Necessario
    {
        public virtual int IdNecessario { get; set; } // id

        public virtual Cargo Cargo { get; set; } // cargo_id
        public virtual Obra Obra { get; set; } // obra_id

        public virtual int QuantidadeNecessario { get; set; } // quantidade_necessario
        public virtual bool IndicadorAprovacao { get; set; } // indicador_aprov
    }
}
