﻿using PR.Building4U.Util;

namespace PR.Building4U.Entidades
{
    public abstract class Situacao
    {
        public virtual int IdSituacao { get; set; } // id
        public virtual string SiglaSituacao { get; set; } // sigla_situacao
        public virtual string NomeSituacao { get; set; } // nome_situacao
        public virtual TipoSituacao TipoSituacao { get; set; } // tipo_situacao
    }
}
