﻿using System.Security;

namespace PR.Building4U.Entidades
{
    public abstract class Usuario
    {
        public virtual int IdUsuario { get; set; } // id
        public virtual string LoginUsuario { get; set; } // login_usuario
        public virtual string SenhaUsuario { get; set; } // senha_usuario
        public virtual string NomeReduzidoUsuario { get; set; } // reduzido_usuario

        public virtual Funcionario Funcionario { get; set; } // funcionario_id
        public virtual Situacao Situacao { get; set; } // situacao_id
        public virtual Permissao Permissao { get; set; } // permissao_id
    }
}
