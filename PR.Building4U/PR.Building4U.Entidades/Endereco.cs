﻿namespace PR.Building4U.Entidades
{
    public abstract class Endereco
    {
        public virtual int IdEndereco { get; set; } // id
        public virtual string LogradouroEndereco { get; set; } // logradouro_endereco
        public virtual string NumeroEndereco { get; set; } // numero_endereco
        public virtual string ComplementoEndereco { get; set; } // complemento_endereco
        public virtual string CepEndereco { get; set; } // cep_endereco
        public virtual string DistritoEndereco { get; set; } // distrito_endereco

        public virtual Municipio Municipio { get; set; } // municipio_id
    }
}
