﻿using PR.Building4U.Util;
using System;

namespace PR.Building4U.Entidades
{
    public abstract class Notificacao
    {
        public virtual int IdNotificacao { get; set; } // id
        public virtual string TituloNotificacao { get; set; } // titulo_notificacao
        public virtual string TextoNotificacao { get; set; } // texto_notificacao
        public virtual DateTime DataNotificacao { get; set; } // data_notificacao
        public virtual TipoNotificacao TipoNotificacao { get; set; } // tipo_notificacao
        public virtual bool LidoNotificacao { get; set; } // lido_notificacao
    }
}
