﻿namespace PR.Building4U.Entidades
{
    public abstract class Servico
    {
        public virtual int IdServico { get; set; } // id
        public virtual int CodigoServico { get; set; } // codigo_servico
        public virtual string SiglaServico { get; set; } // sigla_servico
        public virtual string NomeServico { get; set; } // nome_servico

        public virtual Unidade Unidade { get; set; } // unidade_id
    }
}
