﻿namespace PR.Building4U.Entidades
{
    public abstract class NecessarioAprov
    {
        public virtual int IdNecessario { get; set; } // id
        public virtual int QuantidadeNecessario { get; set; } // quantidade_necessario
        public virtual int QuantidadeAprovacao { get; set; } // quantidade_necessario

        public virtual Cargo Cargo { get; set; } // cargo_id
    }
}
