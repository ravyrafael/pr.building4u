﻿using System.Collections.Generic;

namespace PR.Building4U.Entidades
{
    public abstract class Recurso
    {
        public virtual int IdRecurso { get; set; } // id
        public virtual string NomeRecurso { get; set; } // nome_recurso
        public virtual string IconeRecurso { get; set; } // icone_recurso
        public virtual string ControllerRecurso { get; set; } // controller_recurso
        public virtual string ActionRecurso { get; set; } // action_recurso
        public virtual string ParametrosRecurso { get; set; } // parametros_recurso
        public virtual bool EhMenuRecurso { get; set; } // ehmenu_recurso

        public virtual List<Recurso> SubMenu { get; set; } // menu_pai_id
    }
}
