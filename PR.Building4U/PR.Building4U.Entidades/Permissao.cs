﻿namespace PR.Building4U.Entidades
{
    public abstract class Permissao
    {
        public virtual int IdPermissao { get; set; } // id
        public virtual string SiglaPermissao { get; set; } // sigla_permissao
        public virtual string NomePermissao { get; set; } // nome_permissao
    }
}
