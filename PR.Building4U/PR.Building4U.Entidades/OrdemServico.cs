﻿using System;

namespace PR.Building4U.Entidades
{
    public abstract class OrdemServico
    {
        public virtual int IdOrdemServico { get; set; } // id
        public virtual DateTime? DataOrdemServico { get; set; } // data_os
        public virtual int? PrazoOrdemServico { get; set; } // prazo_os
    }
}
