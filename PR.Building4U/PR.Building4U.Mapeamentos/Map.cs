﻿using PR.Building4U.SDK.InversaoControle;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PR.Building4U.Mapeamentos
{
    public static class Map
    {
        public static Mapeamento[] ListaMapeamentos()
        {
            var listaMapeamentos = new List<Mapeamento>();

            //Mapeamento de executores
            var executores = Assembly.Load("PR.Building4U.Executores").GetTypes().Where(d => d.IsClass && !d.IsAbstract && d.Name.Contains("Executor")).ToList();
            foreach (var executor in executores)
            {
                var iExecutor = ((TypeInfo)executor).ImplementedInterfaces.FirstOrDefault(d => d.Name.Contains("Executor"));
                if (iExecutor != null)
                {
                    listaMapeamentos.Add(new Mapeamento(iExecutor, executor));
                }
            }
            
            //Mapeamento de Repositorios
            var repositorios = Assembly.Load("PR.Building4U.Repositorios").GetTypes().Where(d => d.IsClass && !d.IsAbstract && d.Name.Contains("Repositorio")).ToList();
            foreach (var repositorio in repositorios)
            {
                var iRepositorio = ((TypeInfo)repositorio).ImplementedInterfaces.FirstOrDefault(d => d.Name.Contains("Repositorio"));
                if (iRepositorio != null)
                {
                    listaMapeamentos.Add(new Mapeamento(iRepositorio, repositorio));
                }
            }

            return listaMapeamentos.ToArray();
        }
    }
}
