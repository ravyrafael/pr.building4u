﻿using System;

namespace PR.Building4U.SDK.InversaoControle
{
    public class MapeamentoInstancia : IMapeamentoInstancia
    {
        public string Nome { get; private set; }

        public Type De { get; private set; }

        public object Para { get; private set; }

        public MapeamentoInstancia(Type de, object para) : this(string.Empty, de, para)
        {
        }

        public MapeamentoInstancia(string nome, Type de, object para)
        {
            Nome = nome;
            De = de;
            Para = para;
        }
    }
}
