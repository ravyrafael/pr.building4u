﻿using PR.Building4U.SDK.Transacao;
using System;
using System.Collections.Generic;
using System.Linq;
using Unity;
using Unity.Interception;
using Unity.Interception.PolicyInjection.Pipeline;
using Unity.Interception.PolicyInjection.MatchingRules;
using Unity.Interception.Interceptors.InstanceInterceptors.InterfaceInterception;
using Unity.Interception.ContainerIntegration;
using Unity.Interception.PolicyInjection;
using Unity.Resolution;
using System.Configuration;

namespace PR.Building4U.SDK.InversaoControle
{
    public class ResolvedorDeDependencias : IDisposable
    {
        private static volatile ResolvedorDeDependencias _instance;
        private static volatile object _sync = new object();
        private readonly IUnityContainer _container;

        private ResolvedorDeDependencias(IUnityContainer container)
        {
            _container = container;
            _container.AddNewExtension<Interception>();
            AdicionarConfiguracaoDeIntercepcao("TransacaoPolicy", typeof(TransacaoAttribute), new TransacaoHandler());
        }

        public void AdicionarConfiguracaoDeIntercepcao(string nomePolitica, Type tipoAtributo, ICallHandler instanciaHandler)
        {
            _container.Configure<Interception>()
                            .AddPolicy(nomePolitica)
                            .AddMatchingRule(new CustomAttributeMatchingRule(tipoAtributo, false))
                            .AddCallHandler(instanciaHandler);
        }

        public static ResolvedorDeDependencias Instance()
        {
            if (_instance != null) return _instance;

            lock (_sync)
            {
                if (_instance == null)
                    _instance = new ResolvedorDeDependencias(new UnityContainer());
            }

            return _instance;
        }

        /// <summary>
        /// Carrega mapeamentos
        /// </summary>
        /// <param name="mapeamentos"></param>
        public void CarregarMapeamentos(params IMapeamento[] mapeamentos)
        {
            lock (_sync)
            {
                foreach (var mapeamento in mapeamentos)
                    RegistrarMapeamento(mapeamento, false);
            }
        }

        /// <summary>
        /// Carrega mapeamentos sobreescrevendo mapeamentos já existentes
        /// </summary>
        /// <param name="mapeamentos"></param>
        public void CarregarMapeamentosSobrescrevendo(params IMapeamento[] mapeamentos)
        {
            lock (_sync)
            {
                foreach (var mapeamento in mapeamentos)
                    RegistrarMapeamento(mapeamento, true);
            }
        }

        private void RegistrarMapeamento(IMapeamento mapeamento, bool sobreescrever)
        {
            if (string.IsNullOrEmpty(mapeamento.Nome))
            {
                if (!sobreescrever && _container.IsRegistered(mapeamento.De))
                    throw new ConfigurationErrorsException($"Já existe registro de mapeamento para este tipo ( {mapeamento.De.FullName} ), verifique o tipo ou faça o registro explicitando um nome.");
                RegistrarMapeamentoSemNome(mapeamento);
            }
            else
            {
                if (!sobreescrever && _container.IsRegistered(mapeamento.De, mapeamento.Nome))
                    throw new ConfigurationErrorsException(string.Format("Já existe registro de mapeamento nomeado para este tipo ( {0} ), verifique o tipo.", mapeamento.De.FullName));

                RegistrarMapeamentoComNome(mapeamento);
            }
        }

        private void RegistrarMapeamentoComNome(IMapeamento mapeamento)
        {
            if (mapeamento is Mapeamento)
                _container.RegisterType(mapeamento.De, ((Mapeamento)mapeamento).Para, mapeamento.Nome,
                    new Interceptor<InterfaceInterceptor>(), new InterceptionBehavior<PolicyInjectionBehavior>());
            else
                _container.RegisterInstance(mapeamento.De, mapeamento.Nome, ((MapeamentoInstancia)mapeamento).Para);
        }

        private void RegistrarMapeamentoSemNome(IMapeamento mapeamento)
        {
            if (mapeamento is Mapeamento)
                _container.RegisterType(mapeamento.De, ((Mapeamento)mapeamento).Para, new Interceptor<InterfaceInterceptor>(),
                    new InterceptionBehavior<PolicyInjectionBehavior>());
            else
                _container.RegisterInstance(mapeamento.De, ((MapeamentoInstancia)mapeamento).Para);
        }

        public void LimparMapeamentos()
        {
            foreach (var registration in _container.Registrations.Where(r => r.LifetimeManager != null))
            {
                if (registration.RegisteredType.Namespace != null && registration.RegisteredType.Namespace.Contains("Microsoft.Practices.Unity")) continue;
                registration.LifetimeManager.RemoveValue();
            }
        }

        /// <summary>
        /// Obtem instância de objeto baseado em um mapeamento que seja nomeado.
        /// Este método é para ser utilizado quando possuir dois mapeamentos onde as interfaces base são iguais.
        /// </summary>
        /// <typeparam name="T">Tipo do objeto que deseja obter a injeção</typeparam>
        /// <returns></returns>
        public T ObterInstanciaDe<T>()
        {
            return ObterInstanciaDe<T>(string.Empty);
        }

        public T ObterInstanciaDe<T>(params IMapeamentoInstancia[] parametros)
        {
            return ObterInstanciaDe<T>(string.Empty, parametros);
        }

        /// <summary>
        /// Obtem instância de objeto baseado em um mapeamento que seja nomeado.
        /// Este método é para ser utilizado quando possuir dois mapeamentos onde as interfaces base são iguais.
        /// </summary>
        /// <typeparam name="T">Tipo do objeto que deseja obter a injeção</typeparam>
        /// <param name="nome">Nome do mapeamento para o tipo</param>
        /// <returns></returns>
        public T ObterInstanciaDe<T>(string nome)
        {
            return ObterInstanciaDe<T>(nome, null);
        }

        public T ObterInstanciaDe<T>(string nome, params IMapeamentoInstancia[] parametros)
        {
            try
            {
                T resolved;

                var parameters = BuildParameters(parametros);

                if (string.IsNullOrEmpty(nome))
                    resolved = !parameters.Any() ? _container.Resolve<T>() : _container.Resolve<T>(parameters);
                else
                    resolved = !parameters.Any() ? _container.Resolve<T>(nome) : _container.Resolve<T>(nome, parameters);

                return resolved;
            }
            catch (Exception e)
            {
                throw new ConfigurationErrorsException($"Ocorreu erro ao tentar obter o tipo {typeof(T).Name}, verifique o injetor.", e);
            }
        }

        private static ResolverOverride[] BuildParameters(IMapeamentoInstancia[] parametros)
        {
            if (parametros == null || !parametros.Any()) return new ResolverOverride[0];

            var resultado = new List<ResolverOverride>();

            resultado.AddRange(parametros.Where(p => !string.IsNullOrEmpty(p.Nome)).Select(p => new ParameterOverride(p.Nome, p.Para)));
            resultado.AddRange(parametros.Where(p => string.IsNullOrEmpty(p.Nome)).Select(p => new DependencyOverride(p.De, p.Para)));

            return resultado.ToArray();
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return _container.Resolve(serviceType);
            }
            catch (ResolutionFailedException)
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return _container.ResolveAll(serviceType);
            }
            catch (ResolutionFailedException)
            {
                return new List<object>();
            }
        }

        public void Dispose()
        {
            _container.Dispose();
        }
    }
}
