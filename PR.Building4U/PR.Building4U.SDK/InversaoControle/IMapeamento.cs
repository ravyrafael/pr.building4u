﻿using System;

namespace PR.Building4U.SDK.InversaoControle
{
    public interface IMapeamento
    {
        string Nome { get; }

        Type De { get; }
    }
}
