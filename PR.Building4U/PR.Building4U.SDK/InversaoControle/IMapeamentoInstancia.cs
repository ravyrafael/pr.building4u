﻿namespace PR.Building4U.SDK.InversaoControle
{
    public interface IMapeamentoInstancia : IMapeamento
    {
        object Para { get; }
    }
}
