﻿using PR.Building4U.Fronteiras;
using PR.Building4U.Fronteiras.Executores.AdministrarLog;
using PR.Building4U.Repositorios.AcessoDados;
using PR.Building4U.SDK.InversaoControle;
using PR.Building4U.Util;
using System;
using System.Data.SqlClient;
using System.Reflection;
using System.Transactions;
using Unity.Interception.PolicyInjection.Pipeline;

namespace PR.Building4U.SDK.Transacao
{
    internal class TransacaoHandler : ICallHandler
    {
        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            using (var tx = new TransactionScope())
            {
                ConnectionFactory.Instance.Begin();

                var result = getNext()(input, getNext);

                if (result.Exception == null)
                    tx.Complete();

                ConnectionFactory.Instance.End();
                ConnectionFactory.Instance.Dispose();

                if (result.Exception != null)
                {
                    var ex = result.Exception as SqlException;
                    dynamic resultado = Activator.CreateInstance(((MethodInfo)input.MethodBase).ReturnType);

                    if (ex.Errors.Count > 0 && (ex.Errors[0].Number == 2601 || ex.Errors[0].Number == 2627))
                    {
                        result.Exception = null;
                        resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI001);
                        result.ReturnValue = resultado;
                    }
                    else if (ex.Errors.Count > 0 && ex.Errors[0].Number == 547)
                    {
                        result.Exception = null;
                        resultado.Mensagem = new Mensagem(TipoMensagem.Aviso, Mensagens.AVI002);
                        result.ReturnValue = resultado;
                    }
                    else
                    {
                        var requisicao = input.Arguments[0] as RequisicaoBase;
                        var executor = ResolvedorDeDependencias.Instance().ObterInstanciaDe<IExecutor<IncluirLogRequisicao, IncluirLogResultado>>();
                        executor.Executar(new IncluirLogRequisicao { InformacoesLog = requisicao.InformacoesLog, Mensagem = result.Exception.Message, TipoLog = TipoLog.Erro });
                    }
                }

                return result;
            }
        }

        public int Order { get; set; }
    }
}