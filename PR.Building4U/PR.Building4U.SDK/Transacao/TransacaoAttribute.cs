﻿using System;

namespace PR.Building4U.SDK.Transacao
{
    [AttributeUsage(AttributeTargets.Method)]
    public class TransacaoAttribute : Attribute
    {
    }
}